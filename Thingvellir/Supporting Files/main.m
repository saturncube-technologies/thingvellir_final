//
//  main.m
//  Thingvellir
//
//  Created by saturncube on 24/05/18.
//  Copyright © 2018 saturncube. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}

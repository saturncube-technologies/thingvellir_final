//
//  ParkingWCShopViewController.m
//  Thingvellir
//
//  Created by saturncube on 01/06/18.
//  Copyright © 2018 saturncube. All rights reserved.
//

#import "ParkingWCShopViewController.h"
#import "LanguageViewController.h"
#import "PlaceListViewController.h"
#import "ImagesCell.h"
#import "PWSMapViewController.h"

#import <CoreLocation/CoreLocation.h>

@interface ParkingWCShopViewController ()<CLLocationManagerDelegate, GMSMapViewDelegate, UIWebViewDelegate, LanguageViewControllerDelegate> {
    CLLocation *currentLocation;

    CLLocationManager *locationManager;
    CLLocationCoordinate2D coordinates;
    NSMutableArray *arrPlacesData, *arrLat, *arrLong;

    NSString *decodedString, *base64String;
    NSDictionary *Diclabels;

    BOOL isLocationGet;

}

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *navigationVwHeightContraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *imageViewHeightContraint;

@property (strong, nonatomic) IBOutlet UILabel *naviTitle;
@property (strong, nonatomic) IBOutlet UIButton *btnLanguage;
@property (strong, nonatomic) IBOutlet UIView *firstVw;
@property (strong, nonatomic) IBOutlet UIButton *btnSaveImgs;

@property (strong, nonatomic) IBOutlet UIWebView *detailWebVw;
@property (strong, nonatomic) IBOutlet UILabel *lblDetail;
@property (strong, nonatomic) IBOutlet UIView *secondVw;
@property (strong, nonatomic) IBOutlet GMSMapView *mapVw;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *webVwHeightContraint;
@property (strong, nonatomic) IBOutlet UIImageView *imgVw;

@property (strong, nonatomic) IBOutlet UISegmentedControl *changeMapTypeSegment;
@property (strong, nonatomic) IBOutlet UIButton *btnFullScreenMap;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *secondVwHeightConstraint;

@end

@implementation ParkingWCShopViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //============ Navigationbar and Collection view custom height ==================
    if (DEVICE_IS_IPHONE_X) {
        _navigationVwHeightContraint.constant = 84;
        
    }else if (DEVICE_IS_IPHONE_4){
        
    }else if (DEVICE_IS_IPHONE_5){
        _imageViewHeightContraint.constant = 200;
    }else if (DEVICE_IS_IPHONE_6){
        _imageViewHeightContraint.constant = 230;

    }else if (DEVICE_IS_IPHONE_6_PLUS){
    }else if (DEVICE_IS_IPAD) {
        _imageViewHeightContraint.constant = 390;
        _secondVwHeightConstraint.constant = 350;
        _webVwHeightContraint.constant = 100;
    }
    
    //============ Initialised ==================
    arrPlacesData = [[NSMutableArray alloc] init];
    arrLat = [[NSMutableArray alloc] init];
    arrLong = [[NSMutableArray alloc] init];
    
    //============ Get Current Location ==================
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.distanceFilter  = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [locationManager startUpdatingLocation];
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        [locationManager requestWhenInUseAuthorization];
    }
    
//    coordinates = CLLocationCoordinate2DMake(locationManager.location.coordinate.latitude, locationManager.location.coordinate.longitude);

    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:locationManager.location.coordinate.latitude
                                                            longitude:locationManager.location.coordinate.longitude
                                                                 zoom:12];
    self.mapVw.camera = camera;
   
    
//    [_mapVw setMyLocationEnabled:YES];
    _mapVw.myLocationEnabled  = YES;

    _mapVw.delegate = self;
    


    //======= Call method =======
    [self getPlacesByType];
    
    _strLanguage = [[NSUserDefaults standardUserDefaults] valueForKey:PREFERENCE_LANGUAGE];

    
    NSDictionary *value = [[NSUserDefaults standardUserDefaults] objectForKey:PREFERENCE_LANGUAGE_LABEL];
    Diclabels = [[NSDictionary alloc] initWithDictionary:value];
    
    self.imgVw.clipsToBounds = YES;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [locationManager stopUpdatingLocation];
    locationManager = nil;
}

#pragma mark -------------------
#pragma mark --- Language change delegate method
#pragma mark -------------------

- (void)dataFromController:(NSString *)string {
    NSLog(@" string is : %@",string);
    _strLanguage = [NSString stringWithFormat:@"%@", string];
    [[NSUserDefaults standardUserDefaults] setObject:_strLanguage forKey:PREFERENCE_LANGUAGE];
    NSLog(@"_strLanguageCode : %@",_strLanguage);
}

#pragma mark -------------------
#pragma mark --- Select Map Type
#pragma mark -------------------

- (IBAction)changeMapTypeSegmentTapped:(UISegmentedControl *)sender {
    NSInteger selectedSegment = sender.selectedSegmentIndex;
    
    if (selectedSegment == 0) {
        _mapVw.mapType = kGMSTypeNormal;
    }else {
        _mapVw.mapType = kGMSTypeSatellite;
    }
}

#pragma mark -------------------------
#pragma mark Button Actions
#pragma mark -------------------------

- (IBAction)btnBackTapped:(UIButton *)sender {

    UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
    PlaceListViewController *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"PlaceListViewController"];
    NSArray *controllers = [NSArray arrayWithObject:VC];
    navigationController.viewControllers = controllers;
}

- (IBAction)btnSelectLanguageTapped:(UIButton *)sender {
    LanguageViewController *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"LanguageViewController"];
    VC.delegate = self;
    VC.strSelectedLanguage = _strLanguage;
    [self.navigationController pushViewController:VC animated:YES];
}

- (IBAction)btnSaveImagesTapped:(UIButton *)sender {
    [self showLoadingIndicator];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSURL *imageUrl = [NSURL URLWithString:[arrPlacesData valueForKey:@"image"]];
        
        dispatch_async(dispatch_get_main_queue(), ^(void) {
            [self stopIndicator];
            UIImage *aImage = [UIImage imageWithData:[NSData dataWithContentsOfURL:imageUrl]];
            UIImageWriteToSavedPhotosAlbum(aImage, nil, nil, nil);
            
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:[Diclabels valueForKey:@"txt_thingvellir"] message:[Diclabels valueForKey:@"MSG_PHOTO_SAVED_SUCCESSFULLY"] preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *ok = [UIAlertAction actionWithTitle:[Diclabels valueForKey:@"OK"] style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) { }];
            
            [alert addAction:ok];
            [self presentViewController:alert animated:YES completion:nil];
        });
    });
   
}

- (IBAction)btnMapTapped:(UIButton *)sender {
    PWSMapViewController *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"PWSMapViewController"];
   
    if ([_strLocationType isEqualToString:@"2"]) {
        //    VC.dicDetails = _dicPlaceDetail;
        VC.arrMutData = arrPlacesData;
        VC.strLocationType = _strLocationType;
        VC.strNavTitle = _strNavTitle;

    }else if ([_strLocationType isEqualToString:@"3"]) {
        VC.arrMutData = arrPlacesData;
        VC.strLocationType = _strLocationType;
        VC.strNavTitle = _strNavTitle;

    }else if ([_strLocationType isEqualToString:@"4"]) {
        VC.arrMutData = arrPlacesData;
        VC.strLocationType = _strLocationType;
        VC.strNavTitle = _strNavTitle;

    }else if ([_strLocationType isEqualToString:@"5"]) {
        VC.arrMutData = arrPlacesData;
        VC.strLocationType = _strLocationType;
        VC.strNavTitle = _strNavTitle;
        
    }
    
    [self.navigationController pushViewController:VC animated:YES];
}



#pragma mark ---------------------------
#pragma mark -- CLLocationManagerDelegate
#pragma mark ---------------------------

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    NSLog(@"OldLocation %f %f", oldLocation.coordinate.latitude, oldLocation.coordinate.longitude);
    NSLog(@"NewLocation %f %f", newLocation.coordinate.latitude, newLocation.coordinate.longitude);
    coordinates = newLocation.coordinate;
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse) {
        // user allowed
        isLocationGet = YES;
        [locationManager startUpdatingLocation];
    }
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    
    if ([error domain] == kCLErrorDomain) {
        
        // We handle CoreLocation-related errors here
        switch ([error code]) {
                // "Don't Allow" on two successive app launches is the same as saying "never allow". The user
                // can reset this for all apps by going to Settings > General > Reset > Reset Location Warnings.
            case kCLErrorDenied:
                isLocationGet = NO;
                
            case kCLErrorLocationUnknown:
                isLocationGet = NO;
                
            default:
                break;
        }
        
    } else {
        // We handle all non-CoreLocation errors here
    }
}
#pragma mark -------------------------
#pragma mark API response
#pragma mark -------------------------

- (void)getPlacesByType {
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                       _strLocationType, @"location_type",
                                       [NSNumber numberWithFloat:locationManager.location.coordinate.latitude], @"latitude",
                                       [NSNumber numberWithFloat:locationManager.location.coordinate.longitude], @"longitude",
                                       nil];
    
    NSLog(@"Parameters is: %@",parameters);
    
    [self showLoadingIndicator];
    
    [APIHelper POST:kApiGetPlacesByType parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSLog(@"responseObject %@ :", responseObject);
        
        arrPlacesData = (NSMutableArray *)[responseObject valueForKey:@"result"];
        NSLog(@"arrPlacesData %@:",arrPlacesData);

        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            [self setData];
            [self stopIndicator];
        }];

    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            
            [self stopIndicator];
            
            UIAlertController *alertController = [UIAlertController
                                                  alertControllerWithTitle:[Diclabels valueForKey:@"txt_thingvellir"]
                                                  message:nil
                                                  preferredStyle:UIAlertControllerStyleAlert
                                                  ];
            
            if (error.code == NSURLErrorNotConnectedToInternet) {
                alertController.message = [Diclabels objectForKey:@"MSG_INTERNET_UNAVAILABLE"];
            }else{
                alertController.message = [Diclabels objectForKey:@"MSG_REQUEST_FAIL"];
            }
            
            [alertController addAction:[UIAlertAction
                                        actionWithTitle:[Diclabels valueForKey:@"OK"]
                                        style:UIAlertActionStyleCancel
                                        handler:^(UIAlertAction * _Nonnull action) {
                                            [self.navigationController popViewControllerAnimated:YES];
                                        }]
             ];
            
            [alertController addAction:[UIAlertAction
                                        actionWithTitle:[Diclabels objectForKey:@"txt_retry"]
                                        style:UIAlertActionStyleDefault
                                        handler:^(UIAlertAction * _Nonnull action) {
                                            
                                            [self getPlacesByType];
                                        }]
             ];
            
            [self presentViewController:alertController animated:YES completion:nil];
            
        }];
    }];
}

#pragma mark -------------------------
#pragma mark SetData
#pragma mark -------------------------

- (void)setData {
    
    //============ Segment ===============
    _changeMapTypeSegment.layer.cornerRadius = 4;
    _changeMapTypeSegment.clipsToBounds = YES;
    
    _naviTitle.text = _strNavTitle;
    [_naviTitle setFont:[UIFont fontWithName:FONT_ROBOTO_REGULAR size:15]];
    [_btnLanguage.titleLabel setFont:[UIFont fontWithName:FONT_ROBOTO_REGULAR size:15]];
    
    
    _lblDetail.textColor = colorFromRGB(102, 102, 102);
    [_lblDetail setFont:[UIFont fontWithName:FONT_ROBOTO_REGULAR size:13]];
    

    //=============== Set data according selected language ===================
    
    if ([_strLanguage isEqualToString:@"txt_eng"]) {
        
        base64String = [arrPlacesData valueForKey:@"description"];
        [_btnLanguage setTitle:@"EN" forState:UIControlStateNormal];
        [_btnSaveImgs setTitle:[Diclabels valueForKey:@"txt_save_image"] forState:UIControlStateNormal];
        
        [_btnFullScreenMap setTitle:[Diclabels valueForKey:@"txt_full_screen_map"] forState:UIControlStateNormal];
        [_changeMapTypeSegment setTitle:[Diclabels valueForKey:@"txt_normal"] forSegmentAtIndex:0];
        [_changeMapTypeSegment setTitle:[Diclabels valueForKey:@"txt_satellite"] forSegmentAtIndex:1];

    }else if([_strLanguage isEqualToString:@"txt_islenska"]) {
        
        base64String = [arrPlacesData valueForKey:@"description_islenska"];
        [_btnLanguage setTitle:@"IS" forState:UIControlStateNormal];
        [_btnSaveImgs setTitle:[Diclabels valueForKey:@"txt_save_image"] forState:UIControlStateNormal];
        
        [_btnFullScreenMap setTitle:[Diclabels valueForKey:@"txt_full_screen_map"] forState:UIControlStateNormal];
        [_changeMapTypeSegment setTitle:[Diclabels valueForKey:@"txt_normal"] forSegmentAtIndex:0];
        [_changeMapTypeSegment setTitle:[Diclabels valueForKey:@"txt_satellite"] forSegmentAtIndex:1];

    }else if([_strLanguage isEqualToString:@"txt_German"]) {
        
        base64String = [arrPlacesData valueForKey:@"description_german"];
        [_btnLanguage setTitle:@"DE" forState:UIControlStateNormal];
        [_btnSaveImgs setTitle:[Diclabels valueForKey:@"txt_save_image"] forState:UIControlStateNormal];
        
        [_btnFullScreenMap setTitle:[Diclabels valueForKey:@"txt_full_screen_map"] forState:UIControlStateNormal];
        [_changeMapTypeSegment setTitle:[Diclabels valueForKey:@"txt_normal"] forSegmentAtIndex:0];
        [_changeMapTypeSegment setTitle:[Diclabels valueForKey:@"txt_satellite"] forSegmentAtIndex:1];

    }else if([_strLanguage isEqualToString:@"txt_French"]) {
        
        base64String = [arrPlacesData valueForKey:@"description_french"];
        [_btnLanguage setTitle:@"FR" forState:UIControlStateNormal];
        [_btnSaveImgs setTitle:[Diclabels valueForKey:@"txt_save_image"] forState:UIControlStateNormal];
        
        [_btnFullScreenMap setTitle:[Diclabels valueForKey:@"txt_full_screen_map"] forState:UIControlStateNormal];
        [_changeMapTypeSegment setTitle:[Diclabels valueForKey:@"txt_normal"] forSegmentAtIndex:0];
        [_changeMapTypeSegment setTitle:[Diclabels valueForKey:@"txt_satellite"] forSegmentAtIndex:1];

    }else if([_strLanguage isEqualToString:@"txt_Chinese"]) {
        
        base64String = [arrPlacesData valueForKey:@"description_chinese"];
        [_btnLanguage setTitle:@"ZH" forState:UIControlStateNormal];
        [_btnSaveImgs setTitle:[Diclabels valueForKey:@"txt_save_image"] forState:UIControlStateNormal];
        
        [_btnFullScreenMap setTitle:[Diclabels valueForKey:@"txt_full_screen_map"] forState:UIControlStateNormal];
        [_changeMapTypeSegment setTitle:[Diclabels valueForKey:@"txt_normal"] forSegmentAtIndex:0];
        [_changeMapTypeSegment setTitle:[Diclabels valueForKey:@"txt_satellite"] forSegmentAtIndex:1];

    }else if([_strLanguage isEqualToString:@"txt_Danish"]) {
        
        base64String = [arrPlacesData valueForKey:@"description_danish"];
        [_btnLanguage setTitle:@"DA" forState:UIControlStateNormal];
        [_btnSaveImgs setTitle:[Diclabels valueForKey:@"txt_save_image"] forState:UIControlStateNormal];
        
        [_btnFullScreenMap setTitle:[Diclabels valueForKey:@"txt_full_screen_map"] forState:UIControlStateNormal];
        [_changeMapTypeSegment setTitle:[Diclabels valueForKey:@"txt_normal"] forSegmentAtIndex:0];
        [_changeMapTypeSegment setTitle:[Diclabels valueForKey:@"txt_satellite"] forSegmentAtIndex:1];

    }else if([_strLanguage isEqualToString:@"txt_Spanish"]) {
        
        base64String = [arrPlacesData valueForKey:@"description_spanish"];
        [_btnLanguage setTitle:@"ES" forState:UIControlStateNormal];
        [_btnSaveImgs setTitle:[Diclabels valueForKey:@"txt_save_image"] forState:UIControlStateNormal];
        
        [_btnFullScreenMap setTitle:[Diclabels valueForKey:@"txt_full_screen_map"] forState:UIControlStateNormal];
        [_changeMapTypeSegment setTitle:[Diclabels valueForKey:@"txt_normal"] forSegmentAtIndex:0];
        [_changeMapTypeSegment setTitle:[Diclabels valueForKey:@"txt_satellite"] forSegmentAtIndex:1];

    }
    
    [self decodeString:base64String];
    [_detailWebVw loadHTMLString:decodedString baseURL:nil];
    
    [_imgVw sd_setImageWithURL:[NSURL URLWithString:[arrPlacesData valueForKey:@"image"]]];

    // Map pins
    NSArray *arrLatLongTemp = [arrPlacesData valueForKey:@"locations"];
    
    GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] init];

    for (NSDictionary *dictionary in arrLatLongTemp)
    {
        coordinates.latitude = [dictionary[@"latitude"] floatValue];
        coordinates.longitude = [dictionary[@"longitude"] floatValue];
        // Creates a marker in the center of the map.
        
        GMSMarker *marker = [[GMSMarker alloc] init];

        if ([_strLocationType isEqualToString:@"2"]) {
            marker.icon = [UIImage imageNamed:(@"souvenier_shops_pin")];

        }else if ([_strLocationType isEqualToString:@"3"]) {
            marker.icon = [UIImage imageNamed:(@"map_parking")];

        }else if ([_strLocationType isEqualToString:@"4"]) {
            marker.icon = [UIImage imageNamed:(@"wc_pin.png")];
        
        }else if ([_strLocationType isEqualToString:@"5"]) {
            marker.icon = [UIImage imageNamed:(@"camping_pin.png")];
        }
        
        marker.position = CLLocationCoordinate2DMake(coordinates.latitude, coordinates.longitude);
        bounds = [bounds includingCoordinate:marker.position];
        //        marker.title = dictionary[@"place_title"];
        marker.map = _mapVw;
    }
    
    [CATransaction begin];
    [CATransaction setValue:[NSNumber numberWithFloat: 0.01f] forKey:kCATransactionAnimationDuration];
    // change the camera, set the zoom, whatever.  Just make sure to call the animate* method.
//    [self.mapVw animateToCameraPosition: camera];
    [_mapVw animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds withPadding:30.0f]];
    [CATransaction commit];
    
    
    
}

- (void)decodeString: (NSString *)base64String {
    
    NSData *decodedData = [[NSData alloc] initWithBase64EncodedString:base64String options:0];
    decodedString = [[NSString alloc] initWithData:decodedData encoding:NSUTF8StringEncoding];
    decodedString = [NSString stringWithFormat:@"<span style=\"font-family: %@; font-size: %i\">%@</span>",
                     @"Roboto-Regular",
                     14,
                     decodedString];
    NSLog(@"Decode String Value: %@", decodedString);
}

#pragma mark ----------------
#pragma mark WebView Delegate
#pragma mark ----------------

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    //    [MBProgressHUD showHUDAddedTo:self.detailWebVw animated:YES];
    CGFloat height = [[webView stringByEvaluatingJavaScriptFromString:@"document.body.scrollHeight"] floatValue];
    NSLog(@"Webview height is:: %f", height);
    _webVwHeightContraint.constant = height;
    //    [MBProgressHUD hideHUDForView:self.detailWebVw animated:YES];
}

@end

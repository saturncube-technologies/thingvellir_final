//
//  LeftMenuViewController.m
//  Thingvellir
//
//  Created by saturncube on 26/05/18.
//  Copyright © 2018 saturncube. All rights reserved.
//

#import "LeftMenuViewController.h"
#import "MenuCell.h"

#import "AdmissionInfoViewController.h"
#import "ParkingWCShopViewController.h"
#import "DownloadImagesViewController.h"
#import "AboutViewController.h"
#import "MoreAppViewController.h"

#import "APIHelper.h"

@interface LeftMenuViewController () {
    
    NSMutableArray *arrMenuOptions, *arrMenuIcons, *arrLabels;
    NSDictionary *Diclabels;
    
    NSArray *arrLink;
}

@property (strong, nonatomic) IBOutlet UILabel *lblTitle;
@property (strong, nonatomic) IBOutlet UITableView *tblMenu;

@end

@implementation LeftMenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    arrLabels = [[NSMutableArray alloc] init];
    
    arrMenuOptions = [NSMutableArray arrayWithObjects:@"Admission info", @"Parking", @"wc", @"Souvenir Shops", @"Camping", @"Download Images", @"Share this app", @"About", @"More Apps", nil];
    
    arrMenuIcons = [NSMutableArray arrayWithObjects:
                    [UIImage imageNamed:@"admission_info"],
                    [UIImage imageNamed:@"parking"],
                    [UIImage imageNamed:@"wc"],
                    [UIImage imageNamed:@"souvenier_shops"],
                    [UIImage imageNamed:@"camping"],
                    [UIImage imageNamed:@"download_images"],
                    [UIImage imageNamed:@"share_white"],
                    [UIImage imageNamed:@"about"],
                    [UIImage imageNamed:@"more_app"],
                    nil];
    
   
//    [_lblTitle setFont:[UIFont fontWithName:FONT_ROBOTO_REGULAR size:16]];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(languageUpdated:)
                                                 name:@"LanguageChanged"
                                               object:nil];
    
    _strLanguageCode = [[NSUserDefaults standardUserDefaults] valueForKey:PREFERENCE_LANGUAGE];

//    NSDictionary *value = [[NSUserDefaults standardUserDefaults] objectForKey:PREFERENCE_LANGUAGE_LABEL];
//    
//    Diclabels = [[NSDictionary alloc] initWithDictionary:value];
    
    
    if(Diclabels.count == 0) {
        [self getLabels];
    }
    
    [self getAboutData];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    
    _strLanguageCode = [[NSUserDefaults standardUserDefaults] valueForKey:PREFERENCE_LANGUAGE];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(hideMenuoption:)
                                                 name:@"Menu"
                                               object:nil];
}

- (void)languageUpdated:(NSNotification *)notification {
    NSLog(@"recieved");
    _strLanguageCode = [[NSUserDefaults standardUserDefaults] valueForKey:PREFERENCE_LANGUAGE];
//    if(Diclabels.count == 0) {
        [self getLabels];
//    }
    [_tblMenu reloadData];
}

- (void)hideMenuoption:(NSNotification *)notification {
    NSLog(@"recieved");
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TableView Datasource Delegate Event

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return arrMenuOptions.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    MenuCell *cell = (MenuCell *)[tableView dequeueReusableCellWithIdentifier:@"MenuCell"];
    
//    cell.lblTitle.text = [arrMenuOptions objectAtIndex:indexPath.row];
    cell.menuImgVw.image = [arrMenuIcons objectAtIndex:indexPath.row];
    
    NSDictionary *value = [[NSUserDefaults standardUserDefaults] objectForKey:PREFERENCE_LANGUAGE_LABEL];
    
    Diclabels = [[NSDictionary alloc] initWithDictionary:value];
//    NSLog(@"Diclabels %@",Diclabels);
    
    [cell.lblTitle setFont:[UIFont fontWithName:FONT_ROBOTO_REGULAR size:14]];
  
    if ([_strLanguageCode isEqualToString:@"txt_eng"]) {
      
        if (indexPath.row == 0) {
            cell.lblTitle.text = [Diclabels valueForKey:@"txt_admission_info"];
        }else if (indexPath.row == 1) {
            cell.lblTitle.text = [Diclabels valueForKey:@"txt_parking"];
        }else if (indexPath.row == 2) {
            cell.lblTitle.text = [Diclabels valueForKey:@"txt_wc"];
        }else if (indexPath.row == 3) {
            cell.lblTitle.text = [Diclabels valueForKey:@"txt_souvenir_shops"];
        }else if (indexPath.row == 4) {
            cell.lblTitle.text = [Diclabels valueForKey:@"txt_camping"];
        }else if (indexPath.row == 5) {
            cell.lblTitle.text = [Diclabels valueForKey:@"txt_download_images"];
        }else if (indexPath.row == 6) {
            cell.lblTitle.text = [Diclabels valueForKey:@"txt_share_app"];
        }else if (indexPath.row == 7) {
            cell.lblTitle.text = [Diclabels valueForKey:@"txt_about"];
        }else if (indexPath.row == 8) {
            cell.lblTitle.text = [Diclabels valueForKey:@"txt_more_apps"];
        }
        
    }else if ([_strLanguageCode isEqualToString:@"txt_islenska"]) {
        if (indexPath.row == 0) {
            cell.lblTitle.text = [Diclabels valueForKey:@"txt_admission_info"];
        }else if (indexPath.row == 1) {
            cell.lblTitle.text = [Diclabels valueForKey:@"txt_parking"];
        }else if (indexPath.row == 2) {
            cell.lblTitle.text = [Diclabels valueForKey:@"txt_wc"];
        }else if (indexPath.row == 3) {
            cell.lblTitle.text = [Diclabels valueForKey:@"txt_souvenir_shops"];
        }else if (indexPath.row == 4) {
            cell.lblTitle.text = [Diclabels valueForKey:@"txt_camping"];
        }else if (indexPath.row == 5) {
            cell.lblTitle.text = [Diclabels valueForKey:@"txt_download_images"];
        }else if (indexPath.row == 6) {
            cell.lblTitle.text = [Diclabels valueForKey:@"txt_share_app"];
        }else if (indexPath.row == 7) {
            cell.lblTitle.text = [Diclabels valueForKey:@"txt_about"];
        }else if (indexPath.row == 8) {
            cell.lblTitle.text = [Diclabels valueForKey:@"txt_more_apps"];
        }
        
    }else if ([_strLanguageCode isEqualToString:@"txt_German"]) {
    
        if (indexPath.row == 0) {
            cell.lblTitle.text = [Diclabels valueForKey:@"txt_admission_info"];
        }else if (indexPath.row == 1) {
            cell.lblTitle.text = [Diclabels valueForKey:@"txt_parking"];
        }else if (indexPath.row == 2) {
            cell.lblTitle.text = [Diclabels valueForKey:@"txt_wc"];
        }else if (indexPath.row == 3) {
            cell.lblTitle.text = [Diclabels valueForKey:@"txt_souvenir_shops"];
        }else if (indexPath.row == 4) {
            cell.lblTitle.text = [Diclabels valueForKey:@"txt_camping"];
        }else if (indexPath.row == 5) {
            cell.lblTitle.text = [Diclabels valueForKey:@"txt_download_images"];
        }else if (indexPath.row == 6) {
            cell.lblTitle.text = [Diclabels valueForKey:@"txt_share_app"];
        }else if (indexPath.row == 7) {
            cell.lblTitle.text = [Diclabels valueForKey:@"txt_about"];
        }else if (indexPath.row == 8) {
            cell.lblTitle.text = [Diclabels valueForKey:@"txt_more_apps"];
        }
        
    }else if ([_strLanguageCode isEqualToString:@"txt_French"]) {
        if (indexPath.row == 0) {
            cell.lblTitle.text = [Diclabels valueForKey:@"txt_admission_info"];
        }else if (indexPath.row == 1) {
            cell.lblTitle.text = [Diclabels valueForKey:@"txt_parking"];
        }else if (indexPath.row == 2) {
            cell.lblTitle.text = [Diclabels valueForKey:@"txt_wc"];
        }else if (indexPath.row == 3) {
            cell.lblTitle.text = [Diclabels valueForKey:@"txt_souvenir_shops"];
        }else if (indexPath.row == 4) {
            cell.lblTitle.text = [Diclabels valueForKey:@"txt_camping"];
        }else if (indexPath.row == 5) {
            cell.lblTitle.text = [Diclabels valueForKey:@"txt_download_images"];
        }else if (indexPath.row == 6) {
            cell.lblTitle.text = [Diclabels valueForKey:@"txt_share_app"];
        }else if (indexPath.row == 7) {
            cell.lblTitle.text = [Diclabels valueForKey:@"txt_about"];
        }else if (indexPath.row == 8) {
            cell.lblTitle.text = [Diclabels valueForKey:@"txt_more_apps"];
        }
        
    }else if ([_strLanguageCode isEqualToString:@"txt_Spanish"]) {
        if (indexPath.row == 0) {
            cell.lblTitle.text = [Diclabels valueForKey:@"txt_admission_info"];
        }else if (indexPath.row == 1) {
            cell.lblTitle.text = [Diclabels valueForKey:@"txt_parking"];
        }else if (indexPath.row == 2) {
            cell.lblTitle.text = [Diclabels valueForKey:@"txt_wc"];
        }else if (indexPath.row == 3) {
            cell.lblTitle.text = [Diclabels valueForKey:@"txt_souvenir_shops"];
        }else if (indexPath.row == 4) {
            cell.lblTitle.text = [Diclabels valueForKey:@"txt_camping"];
        }else if (indexPath.row == 5) {
            cell.lblTitle.text = [Diclabels valueForKey:@"txt_download_images"];
        }else if (indexPath.row == 6) {
            cell.lblTitle.text = [Diclabels valueForKey:@"txt_share_app"];
        }else if (indexPath.row == 7) {
            cell.lblTitle.text = [Diclabels valueForKey:@"txt_about"];
        }else if (indexPath.row == 8) {
            cell.lblTitle.text = [Diclabels valueForKey:@"txt_more_apps"];
        }
        
        
    }else if ([_strLanguageCode isEqualToString:@"txt_Chinese"]) {
   
        if (indexPath.row == 0) {
            cell.lblTitle.text = [Diclabels valueForKey:@"txt_admission_info"];
        }else if (indexPath.row == 1) {
            cell.lblTitle.text = [Diclabels valueForKey:@"txt_parking"];
        }else if (indexPath.row == 2) {
            cell.lblTitle.text = [Diclabels valueForKey:@"txt_wc"];
        }else if (indexPath.row == 3) {
            cell.lblTitle.text = [Diclabels valueForKey:@"txt_souvenir_shops"];
        }else if (indexPath.row == 4) {
            cell.lblTitle.text = [Diclabels valueForKey:@"txt_camping"];
        }else if (indexPath.row == 5) {
            cell.lblTitle.text = [Diclabels valueForKey:@"txt_download_images"];
        }else if (indexPath.row == 6) {
            cell.lblTitle.text = [Diclabels valueForKey:@"txt_share_app"];
        }else if (indexPath.row == 7) {
            cell.lblTitle.text = [Diclabels valueForKey:@"txt_about"];
        }else if (indexPath.row == 8) {
            cell.lblTitle.text = [Diclabels valueForKey:@"txt_more_apps"];
        }
        
    }else if ([_strLanguageCode isEqualToString:@"txt_Danish"]) {
        if (indexPath.row == 0) {
            cell.lblTitle.text = [Diclabels valueForKey:@"txt_admission_info"];
        }else if (indexPath.row == 1) {
            cell.lblTitle.text = [Diclabels valueForKey:@"txt_parking"];
        }else if (indexPath.row == 2) {
            cell.lblTitle.text = [Diclabels valueForKey:@"txt_wc"];
        }else if (indexPath.row == 3) {
            cell.lblTitle.text = [Diclabels valueForKey:@"txt_souvenir_shops"];
        }else if (indexPath.row == 4) {
            cell.lblTitle.text = [Diclabels valueForKey:@"txt_camping"];
        }else if (indexPath.row == 5) {
            cell.lblTitle.text = [Diclabels valueForKey:@"txt_download_images"];
        }else if (indexPath.row == 6) {
            cell.lblTitle.text = [Diclabels valueForKey:@"txt_share_app"];
        }else if (indexPath.row == 7) {
            cell.lblTitle.text = [Diclabels valueForKey:@"txt_about"];
        }else if (indexPath.row == 8) {
            cell.lblTitle.text = [Diclabels valueForKey:@"txt_more_apps"];
        }
        
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSDictionary *value = [[NSUserDefaults standardUserDefaults] objectForKey:PREFERENCE_LANGUAGE_LABEL];
    
    Diclabels = [[NSDictionary alloc] initWithDictionary:value];
    
    UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
 
    if (indexPath.row == 0) {
        
        AdmissionInfoViewController *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"AdmissionInfoViewController"];
        //VC.title = [NSString stringWithFormat:@"Demo %d", indexPath.row];
        VC.strType = @"1";
        VC.strLanguage = _strLanguageCode;
        VC.strNavTitle = [Diclabels valueForKey:@"txt_admission_info"];
        NSArray *controllers = [NSArray arrayWithObject:VC];
        navigationController.viewControllers = controllers;
        
    }else if (indexPath.row == 1) {
        
        ParkingWCShopViewController *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"ParkingWCShopViewController"];
        VC.strLocationType = @"3";
        VC.strNavTitle = [Diclabels valueForKey:@"txt_parking"];
        VC.strLanguage = _strLanguageCode;
        NSArray *controllers = [NSArray arrayWithObject:VC];
        navigationController.viewControllers = controllers;
     
    }else if (indexPath.row == 2) {
        
        ParkingWCShopViewController *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"ParkingWCShopViewController"];
        VC.strLocationType = @"4";
        VC.strNavTitle = [Diclabels valueForKey:@"txt_wc"];
        VC.strLanguage = _strLanguageCode;
        NSArray *controllers = [NSArray arrayWithObject:VC];
        navigationController.viewControllers = controllers;
        
    }else if (indexPath.row == 3) {
        
        ParkingWCShopViewController *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"ParkingWCShopViewController"];
        VC.strLocationType = @"2";
        VC.strNavTitle = [Diclabels valueForKey:@"txt_souvenir_shops"];
        VC.strLanguage = _strLanguageCode;
        NSArray *controllers = [NSArray arrayWithObject:VC];
        navigationController.viewControllers = controllers;
        
    }else if (indexPath.row == 4) {
        
        ParkingWCShopViewController *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"ParkingWCShopViewController"];
        VC.strLocationType = @"5";
        VC.strNavTitle = [Diclabels valueForKey:@"txt_camping"];
        VC.strLanguage = _strLanguageCode;
        NSArray *controllers = [NSArray arrayWithObject:VC];
        navigationController.viewControllers = controllers;
        
    }else if (indexPath.row == 5) {
        
        DownloadImagesViewController *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"DownloadImagesViewController"];
        VC.strLanguage = _strLanguageCode;
        VC.strNavTitle = [Diclabels valueForKey:@"txt_download_images"];
        NSArray *controllers = [NSArray arrayWithObject:VC];
        navigationController.viewControllers = controllers;
        
    }else if (indexPath.row == 6) {
        
        [self shareApp];
        
    }else if (indexPath.row == 7) {
        
        AboutViewController *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"AboutViewController"];
        VC.strType = @"2";
        VC.strLanguage = _strLanguageCode;
        VC.strNavTitle = [Diclabels valueForKey:@"txt_about"];
        NSArray *controllers = [NSArray arrayWithObject:VC];
        navigationController.viewControllers = controllers;
        
    }else if (indexPath.row == 8) {
        
        MoreAppViewController *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"MoreAppViewController"];
        VC.strLanguage = _strLanguageCode;
        VC.strNavTitle = [Diclabels valueForKey:@"txt_more_apps"];
        NSArray *controllers = [NSArray arrayWithObject:VC];
        navigationController.viewControllers = controllers;
    }

    [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];

}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

#pragma mark -------------------------
#pragma mark API response
#pragma mark -------------------------

- (void)getLabels {
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionaryWithObjectsAndKeys:_strLanguageCode, @"language", nil];
    
    NSLog(@"Parameters is: %@",parameters);
    
    [self showLoadingIndicator];
    
    [APIHelper POST:kApiGetLabel parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [self stopIndicator];
        
//        NSLog(@"responseObject %@ :", responseObject);
        
        arrLabels = (NSMutableArray *)[responseObject valueForKey:@"result"];

        [[NSUserDefaults standardUserDefaults] setObject:arrLabels forKey:PREFERENCE_LANGUAGE_LABEL];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [_tblMenu reloadData];
        
//        [self setLabels];
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        NSDictionary *labels = [[NSUserDefaults standardUserDefaults] objectForKey: PREFERENCE_LANGUAGE_LABEL];
        
        Diclabels = [[NSDictionary alloc] initWithDictionary:labels];
        
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            
            [self stopIndicator];
            
            UIAlertController *alertController = [UIAlertController
                                                  alertControllerWithTitle:[Diclabels valueForKey:@"txt_thingvellir"]
                                                  message:nil
                                                  preferredStyle:UIAlertControllerStyleAlert
                                                  ];
            
            if (error.code == NSURLErrorNotConnectedToInternet) {
                alertController.message = [Diclabels valueForKey:@"MSG_INTERNET_UNAVAILABLE"];
            }else{
                alertController.message = [Diclabels valueForKey:@"MSG_REQUEST_FAIL"];
            }
            
            [alertController addAction:[UIAlertAction
                                        actionWithTitle:[Diclabels valueForKey:@"OK"]
                                        style:UIAlertActionStyleCancel
                                        handler:^(UIAlertAction * _Nonnull action) {
                                            [self.navigationController popViewControllerAnimated:YES];
                                        }]
             ];
            
            [alertController addAction:[UIAlertAction
                                        actionWithTitle:[Diclabels valueForKey:@"txt_retry"]
                                        style:UIAlertActionStyleDefault
                                        handler:^(UIAlertAction * _Nonnull action) {
                                            
                                            [self getLabels];
                                        }]
             ];
            
            [self presentViewController:alertController animated:YES completion:nil];
            
        }];
        
    }];
}

- (void)getAboutData {
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                       @"2", @"type",
                                       nil];
    
    NSLog(@"Parameters is: %@",parameters);
    
    [self showLoadingIndicator];
    
    [APIHelper POST:kApiContentData parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSLog(@"responseObject %@ :", responseObject);
        
        arrLink = (NSMutableArray *)[responseObject valueForKey:@"result"];
        
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            [self stopIndicator];
        }];
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            
            [self stopIndicator];
            
            UIAlertController *alertController = [UIAlertController
                                                  alertControllerWithTitle:[Diclabels valueForKey:@"txt_thingvellir"]
                                                  
                                                  message:nil
                                                  preferredStyle:UIAlertControllerStyleAlert
                                                  ];
            
            if (error.code == NSURLErrorNotConnectedToInternet) {
                alertController.message = [Diclabels objectForKey:@"MSG_INTERNET_UNAVAILABLE"];
            }else{
                alertController.message = [Diclabels objectForKey:@"MSG_REQUEST_FAIL"];
            }
            
            [alertController addAction:[UIAlertAction
                                        actionWithTitle:[Diclabels valueForKey:@"OK"]
                                        style:UIAlertActionStyleCancel
                                        handler:^(UIAlertAction * _Nonnull action) {
                                            [self.navigationController popViewControllerAnimated:YES];
                                        }]
             ];
            
            [alertController addAction:[UIAlertAction
                                        actionWithTitle:[Diclabels objectForKey:@"txt_retry"]
                                        style:UIAlertActionStyleDefault
                                        handler:^(UIAlertAction * _Nonnull action) {
                                            
                                            [self getAboutData];
                                        }]
             ];
            
            [self presentViewController:alertController animated:YES completion:nil];
            
        }];
    }];
}

- (void)shareApp {
    
    NSURL *myWebsite = [NSURL URLWithString:[arrLink valueForKey:@"shareable_app_link"]];
    
    NSArray *objectsToShare = @[myWebsite];
    
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
    
    activityVC.excludedActivityTypes = @[UIActivityTypeAirDrop,
                                         UIActivityTypePrint,
                                         UIActivityTypeAssignToContact,
                                         UIActivityTypeSaveToCameraRoll,
                                         UIActivityTypeAddToReadingList,
                                         UIActivityTypePostToFlickr,
                                         UIActivityTypePostToVimeo,
                                         UIActivityTypeMail,
                                         UIActivityTypePostToFacebook,
                                         UIActivityTypePostToTwitter,
                                         UIActivityTypeMessage,
                                         ];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:activityVC animated:YES completion:nil];
    });
    
    
    if (DEVICE_IS_IPAD) {
        activityVC.popoverPresentationController.sourceRect = CGRectMake(0, self.view.frame.size.height,[[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.width);
        activityVC.popoverPresentationController.sourceView = self.view;
    }
}

@end

//
//  AdmissionInfoViewController.m
//  Thingvellir
//
//  Created by saturncube on 01/06/18.
//  Copyright © 2018 saturncube. All rights reserved.
//

#import "AdmissionInfoViewController.h"
#import "LanguageViewController.h"
#import "PlaceListViewController.h"

#import "ImagesCell.h"

@interface AdmissionInfoViewController ()<UIWebViewDelegate, LanguageViewControllerDelegate> {
    
    NSMutableArray *arrPlacesData, *arrPlaceImages, *arrPlacesImagesNames;
    NSString *decodedString, *base64String;
    
    NSDictionary *Diclabels;

    NSUInteger index;

}

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *navigationVwHeightContraint;

@property (strong, nonatomic) IBOutlet UILabel *naviTitle;
@property (strong, nonatomic) IBOutlet UIButton *btnLanguage;
@property (strong, nonatomic) IBOutlet UIView *firstVw;
@property (strong, nonatomic) IBOutlet UIButton *btnSaveImgs;


@property (strong, nonatomic) IBOutlet UILabel *lblDetail;
@property (strong, nonatomic) IBOutlet UIWebView *detailWebVw;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *webVwHeightContraint;
@property (strong, nonatomic) IBOutlet UIImageView *imgVw;
@property (strong, nonatomic) IBOutlet UIButton *btnNext;
@property (strong, nonatomic) IBOutlet UIButton *btnPrev;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *imgVwHeightContraint;

@end

@implementation AdmissionInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    index = 0;
    
    //============ Navigationbar and Collection view custom height ==================
    if (DEVICE_IS_IPHONE_X) {
        _navigationVwHeightContraint.constant = 84;
        
    }else if (DEVICE_IS_IPHONE_4){
        
    }else if (DEVICE_IS_IPHONE_5){
        _imgVwHeightContraint.constant = 200;
    }else if (DEVICE_IS_IPHONE_6){
        _imgVwHeightContraint.constant = 220;

    }else if (DEVICE_IS_IPHONE_6_PLUS){
        //        _collectionVwHeightConstraint.constant =
        
    }else if (DEVICE_IS_IPAD) {
        _imgVwHeightContraint.constant = 390;
        _webVwHeightContraint.constant = 150;
    }
    
    //============ Array initialised ==================
    arrPlacesData = [[NSMutableArray alloc] init];
    arrPlaceImages = [[NSMutableArray alloc] init];
    arrPlacesImagesNames = [[NSMutableArray alloc] init];
    
    
    [self getAdmissionInfo];
    _strLanguage = [[NSUserDefaults standardUserDefaults] valueForKey:PREFERENCE_LANGUAGE];
    
        
    NSDictionary *value = [[NSUserDefaults standardUserDefaults] objectForKey:PREFERENCE_LANGUAGE_LABEL];
    Diclabels = [[NSDictionary alloc] initWithDictionary:value];

//    _scrollVw.pagingEnabled = YES;
//    _scrollVw.alwaysBounceVertical = NO;
//    _scrollVw.alwaysBounceHorizontal = YES;
//    NSInteger numberOfViews = 4;
//    
//    for (int i = 0; i < numberOfViews; i++) {
//        
//        CGFloat xOrigin = i*320;
//        _imgVw.image = [UIImage imageNamed:[NSString stringWithFormat:@"image%d",i+1]];
//        _imgVw.contentMode = UIViewContentModeScaleAspectFit;
//        [self.scrollVw addSubview:_imgVw];
//        _imgVw.tag = i;
//    }
    
    [_btnPrev setEnabled:NO];

    self.imgVw.clipsToBounds = YES;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dataFromController:(NSString *)string {
    NSLog(@" string is : %@",string);
    _strLanguage = [NSString stringWithFormat:@"%@", string];
    [[NSUserDefaults standardUserDefaults] setObject:_strLanguage forKey:PREFERENCE_LANGUAGE];
    NSLog(@"_strLanguageCode : %@",_strLanguage);
}

#pragma mark -------------------------
#pragma mark Button Actions
#pragma mark -------------------------

- (IBAction)btnPrevTapped:(UIButton *)sender {
    
    --index;

    [_btnNext setEnabled:YES];

    if (index <= 0)
    {
        index = 0;
        [_btnPrev setEnabled:NO];
    }
    [UIView animateWithDuration:0.2
                     animations:^{
                         // animations go here
                         [_imgVw sd_setImageWithURL:[NSURL URLWithString:[arrPlaceImages objectAtIndex:index]]];
                     }];

}

- (IBAction)btnNextTapped:(UIButton *)sender {
    
    ++index;

    [_btnPrev setEnabled:YES];

    if (index >= [arrPlaceImages count])
    {
        index = [arrPlaceImages count] - 1;
        [_btnNext setEnabled:NO];
    }
    [UIView animateWithDuration:0.2
                     animations:^{
                         // animations go here
                           [_imgVw sd_setImageWithURL:[NSURL URLWithString:[arrPlaceImages objectAtIndex:index]]];
                     }];
    
}

- (IBAction)btnBackTapped:(UIButton *)sender {

    UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
    PlaceListViewController *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"PlaceListViewController"];
    NSArray *controllers = [NSArray arrayWithObject:VC];
    navigationController.viewControllers = controllers;
    
//    [self.navigationController popToViewController:navigationController animated:YES];
}

- (IBAction)btnSelectLanguageTapped:(UIButton *)sender {
    LanguageViewController *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"LanguageViewController"];
    VC.delegate = self;
    VC.strSelectedLanguage = _strLanguage;
    [self.navigationController pushViewController:VC animated:YES];
}

- (IBAction)btnSaveImagesTapped:(UIButton *)sender {

    [self showLoadingIndicator];

    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        NSURL *imageUrl = [NSURL URLWithString:[arrPlaceImages objectAtIndex:index]];
        
        dispatch_async(dispatch_get_main_queue(), ^(void) {
            [self stopIndicator];
            UIImage *aImage = [UIImage imageWithData:[NSData dataWithContentsOfURL:imageUrl]];
            UIImageWriteToSavedPhotosAlbum(aImage, nil, nil, nil);
            
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:[Diclabels valueForKey:@"txt_thingvellir"] message:[Diclabels valueForKey:@"MSG_PHOTO_SAVED_SUCCESSFULLY"] preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *ok = [UIAlertAction actionWithTitle:[Diclabels valueForKey:@"OK"] style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {}];
            
            [alert addAction:ok];
            [self presentViewController:alert animated:YES completion:nil];
        });
    });
    
}



#pragma mark -------------------------
#pragma mark API response
#pragma mark -------------------------

- (void)getAdmissionInfo {
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                       _strType, @"type",
                                       nil];
    
    NSLog(@"Parameters is: %@",parameters);
    
    [self showLoadingIndicator];
    
    [APIHelper POST:kApiContentData parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSLog(@"responseObject %@ :", responseObject);
        
        arrPlacesData = (NSMutableArray *)[responseObject valueForKey:@"result"];
        
        NSArray *arrImages = [arrPlacesData valueForKey:@"images"];
        
        for (NSDictionary *dicPlaces in arrImages) {
            
            [arrPlaceImages addObject:[dicPlaces valueForKey:@"image_name"]];
            NSLog(@"arrPlaceImages %@:",arrPlaceImages);
        }
        
        
        for (NSDictionary *dic in arrPlaceImages) {
            NSString *url = [NSString stringWithFormat:@"%@",dic];
            NSArray *parts = [url componentsSeparatedByString:@"/"];
            [arrPlacesImagesNames addObject:[parts lastObject]];
        }
        NSLog(@"Images name of arrLocalImg %@",arrPlacesImagesNames);
        
        
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            
            [self stopIndicator];
            [self setData];
            
        }];
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            
            [self stopIndicator];
            
            UIAlertController *alertController = [UIAlertController
                                                  alertControllerWithTitle:[Diclabels valueForKey:@"txt_thingvellir"]
                                                  message:nil
                                                  preferredStyle:UIAlertControllerStyleAlert
                                                  ];
            
            if (error.code == NSURLErrorNotConnectedToInternet) {
                alertController.message = [Diclabels objectForKey:@"MSG_INTERNET_UNAVAILABLE"];
            }else{
                alertController.message = [Diclabels objectForKey:@"MSG_REQUEST_FAIL"];
            }
            
            [alertController addAction:[UIAlertAction
                                        actionWithTitle:[Diclabels valueForKey:@"OK"]
                                        style:UIAlertActionStyleCancel
                                        handler:^(UIAlertAction * _Nonnull action) {
                                            [self.navigationController popViewControllerAnimated:YES];
                                        }]
             ];
            
            [alertController addAction:[UIAlertAction
                                        actionWithTitle:[Diclabels objectForKey:@"txt_retry"]
                                        style:UIAlertActionStyleDefault
                                        handler:^(UIAlertAction * _Nonnull action) {
                                            
                                            [self getAdmissionInfo];
                                        }]
             ];
            
            [self presentViewController:alertController animated:YES completion:nil];
            
        }];
    }];
}

#pragma mark -------------------------
#pragma mark SetData
#pragma mark -------------------------

- (void)setData {
    
    _naviTitle.text = _strNavTitle;
    [_naviTitle setFont:[UIFont fontWithName:FONT_ROBOTO_REGULAR size:15]];
    [_btnLanguage.titleLabel setFont:[UIFont fontWithName:FONT_ROBOTO_REGULAR size:15]];
    
    _lblDetail.textColor = colorFromRGB(102, 102, 102);
    [_lblDetail setFont:[UIFont fontWithName:FONT_ROBOTO_REGULAR size:13]];
    
//    _lblDetail.text = [arrPlacesData valueForKey:@"content_desc"];
    
//    NSString *base64String = [arrPlacesData valueForKey:@"content_desc"];
  
    [_imgVw sd_setImageWithURL:[NSURL URLWithString:[arrPlaceImages objectAtIndex:index]]];
    if (arrPlaceImages.count <= 1) {
        _btnNext.hidden = YES;
        _btnPrev.hidden = YES;
    }
    
    //=============== Set data according selected language ===================
    
    if ([_strLanguage isEqualToString:@"txt_eng"]) {
    
        base64String = [arrPlacesData valueForKey:@"content_desc"];
        [_btnLanguage setTitle:@"EN" forState:UIControlStateNormal];

        [_btnSaveImgs setTitle:[Diclabels valueForKey:@"txt_save_image"] forState:UIControlStateNormal];

    }else if([_strLanguage isEqualToString:@"txt_islenska"]) {
        
        base64String = [arrPlacesData valueForKey:@"content_desc_islenska"];
        [_btnLanguage setTitle:@"IS" forState:UIControlStateNormal];
        [_btnSaveImgs setTitle:[Diclabels valueForKey:@"txt_save_image"] forState:UIControlStateNormal];

    }else if([_strLanguage isEqualToString:@"txt_German"]) {
        
        base64String = [arrPlacesData valueForKey:@"content_desc_german"];
        [_btnLanguage setTitle:@"DE" forState:UIControlStateNormal];
        [_btnSaveImgs setTitle:[Diclabels valueForKey:@"txt_save_image"] forState:UIControlStateNormal];

    }else if([_strLanguage isEqualToString:@"txt_French"]) {
        
        base64String = [arrPlacesData valueForKey:@"content_desc_french"];
        [_btnLanguage setTitle:@"FR" forState:UIControlStateNormal];
        [_btnSaveImgs setTitle:[Diclabels valueForKey:@"txt_save_image"] forState:UIControlStateNormal];

    }else if([_strLanguage isEqualToString:@"txt_Chinese"]) {
        
        base64String = [arrPlacesData valueForKey:@"content_desc_chinese"];
        [_btnLanguage setTitle:@"ZH" forState:UIControlStateNormal];
        [_btnSaveImgs setTitle:[Diclabels valueForKey:@"txt_save_image"] forState:UIControlStateNormal];

    }else if([_strLanguage isEqualToString:@"txt_Danish"]) {
        
        base64String = [arrPlacesData valueForKey:@"content_desc_danish"];
        [_btnLanguage setTitle:@"DA" forState:UIControlStateNormal];
        [_btnSaveImgs setTitle:[Diclabels valueForKey:@"txt_save_image"] forState:UIControlStateNormal];

    }else if([_strLanguage isEqualToString:@"txt_Spanish"]) {
        
        base64String = [arrPlacesData valueForKey:@"content_desc_spanish"];
        [_btnLanguage setTitle:@"ES" forState:UIControlStateNormal];
        [_btnSaveImgs setTitle:[Diclabels valueForKey:@"txt_save_image"] forState:UIControlStateNormal];

    }
    
    [self decodeString:base64String];
    [_detailWebVw loadHTMLString:decodedString baseURL:nil];
    
}

- (void)decodeString: (NSString *)base64String {
    
    NSData *decodedData = [[NSData alloc] initWithBase64EncodedString:base64String options:0];
    decodedString = [[NSString alloc] initWithData:decodedData encoding:NSUTF8StringEncoding];
    decodedString = [NSString stringWithFormat:@"<span style=\"font-family: %@; font-size: %i\">%@</span>",
                     @"Roboto-Regular",
                     14,
                     decodedString];
    NSLog(@"Decode String Value: %@", decodedString);
}

#pragma mark ----------------
#pragma mark WebView Delegate
#pragma mark ----------------

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    //    [MBProgressHUD showHUDAddedTo:self.detailWebVw animated:YES];
    CGFloat height = [[webView stringByEvaluatingJavaScriptFromString:@"document.body.scrollHeight"] floatValue];
    NSLog(@"Webview height is:: %f", height);
    _webVwHeightContraint.constant = height;
    //    [MBProgressHUD hideHUDForView:self.detailWebVw animated:YES];
}

@end

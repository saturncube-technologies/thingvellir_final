//
//  PlaceListViewController.m
//  Thingvellir
//
//  Created by saturncube on 25/05/18.
//  Copyright © 2018 saturncube. All rights reserved.
//

#import "PlaceListViewController.h"
#import "PlaceCell.h"
#import "PlaceDetailViewController.h"
#import "LanguageViewController.h"
#import "CustomInfoWindow.h"
#import "DBHelper.h"
#import "PlacesInfo.h"

//#import "UIImageView+WebCache.h"
#import <CoreLocation/CoreLocation.h>

@interface PlaceListViewController ()<UITableViewDelegate, UITableViewDataSource, CLLocationManagerDelegate, UIImagePickerControllerDelegate,LanguageViewControllerDelegate> {
    UIRefreshControl *refreshControl;
    
    CLLocation *currentLocation;
    
    CLLocationManager *locationManager;
    CLLocationCoordinate2D coordinates;
    
    NSMutableArray *arrLabels ,*arrPlacesData, *arrPlaceImages, *arrLocalPlacesImg, *arrPlacesDetailImgs, *arrPlacesDetailImgsPath, *arrLat, *arrLong, *arrLocalImg, *arrLocalAudio, *arrAudioIslenska, *arrAudioGerman, *arrAudioFrench, *arrAudioSpanish, *arrAudioChinese, *arrAudioDanish, *arrNewPlaces;
    //*arrLocalImages;
    
    __weak IBOutlet UIButton *btnPin;
    __weak IBOutlet UIButton *btnMenu;
    IBOutlet UIButton *btnLanguage;
    IBOutlet UIButton *btnFilter;
    
    BOOL selected;
    NSString *strPlaceId, *rendomStr;
    NSData *pngData;
    NSMutableArray * arrLikeVisitedTemp ;
    Utility *utility;
    
    NSUserDefaults *userDefaults;
    
    NSArray *paths;
//    NSString *anImagePath;
    NSString *documentsPath;
    
    NSDictionary *Diclabels;

    BOOL isLocationGet;
    
    NSString *strDown, *oldTime;
    
    NSTimer *timer;
    
    NSDateFormatter *formatter;

    int anIndex;
}

@property (nonatomic, strong) MNShowcaseView *showcaseView;

@property (nonatomic, strong) DBHelper *dbHelper;
@property (nonatomic, strong) PlacesInfo *placeInfo;
@property (nonatomic, strong) NSArray *arrPlacesInfo;
@property (nonatomic, strong) NSArray *arrDownFlagEn;

@property (strong, nonatomic) IBOutlet UILabel *navTitle;
@property (strong, nonatomic) IBOutlet UIView *mainVw;
@property (strong, nonatomic) IBOutlet GMSMapView *mapVw;
@property (strong, nonatomic) IBOutlet UILabel *lblNoData;

@property (weak, nonatomic) IBOutlet UITableView *tblPlaceList;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *navigationVwHeightContraint;
@property (weak, nonatomic) IBOutlet UIView *filterVw;
@property (strong, nonatomic) IBOutlet UILabel *lblFilter;
@property (strong, nonatomic) IBOutlet UILabel *lblFav;
@property (strong, nonatomic) IBOutlet UIButton *btnFav;
@property (strong, nonatomic) IBOutlet UILabel *lblPlace;
@property (strong, nonatomic) IBOutlet UIButton *btnVisited;
@property (strong, nonatomic) IBOutlet UIButton *btnApply;
@property (strong, nonatomic) IBOutlet UIButton *btnReset;
@property (strong, nonatomic) IBOutlet UIButton *btnDownloadAll;

@property (strong, nonatomic) IBOutlet UISegmentedControl *changeMapTypeSegment;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *mainVwBottomConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *mapVwBottomConstraint;

@end

@implementation PlaceListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
  
//    _activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    _showcaseView = [MNShowcaseView new];
    _showcaseView.highlightedColorDefault = [UIColor redColor];
    _showcaseView.selectionTypeDefault = MNSelection_Rectangle;
    
    formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd_MM_yyyy_HH_mm_ss"];
    
    _btnDownloadAll.selected = NO;
    
    _filterVw.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);

    self.dbHelper  = [[DBHelper alloc] init];
    self.placeInfo = [[PlacesInfo alloc]init];
    utility = [[Utility alloc]init];
    
    userDefaults = [NSUserDefaults standardUserDefaults];
    
    paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    documentsPath = [paths objectAtIndex:0]; //Get the docs directory
  
    
    if ([utility isInternetConnected]) {
        btnMenu.enabled = YES;
    }else {
        btnMenu.enabled = NO;
    }
    
    
//    [self.dbHelper loadTableData:DB_TABLENAME_PLACEMASTER];
//    NSLog(@"DB result is %@",self.dbHelper.arrResults);
    
    
    //============ Get Current Location ==================
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.distanceFilter  = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [locationManager startUpdatingLocation];
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        [locationManager requestWhenInUseAuthorization];
    }
    [locationManager setDelegate:self];
  
    
    coordinates = CLLocationCoordinate2DMake(coordinates.latitude, coordinates.longitude);
    _mapVw.delegate = self;
    
    [_mapVw setMyLocationEnabled:YES];

 
    //============ Array initialised ==================
    arrLabels  = [[NSMutableArray alloc] init];
    arrPlacesData  = [[NSMutableArray alloc] init];
    arrPlaceImages = [[NSMutableArray alloc] init];
    arrLat         = [[NSMutableArray alloc] init];
    arrLong        = [[NSMutableArray alloc] init];
    arrLocalImg    = [[NSMutableArray alloc] init];
    arrLocalAudio  = [[NSMutableArray alloc] init];
    arrAudioIslenska = [[NSMutableArray alloc] init];
    arrLikeVisitedTemp = [[NSMutableArray alloc] init];
    arrLocalPlacesImg = [[NSMutableArray alloc] init];
//    arrLocalImages = [[NSMutableArray alloc] init];
    arrAudioGerman = [[NSMutableArray alloc] init];
    arrAudioFrench = [[NSMutableArray alloc] init];
    arrAudioSpanish = [[NSMutableArray alloc] init];
    arrAudioChinese = [[NSMutableArray alloc] init];
    arrAudioDanish = [[NSMutableArray alloc] init];
    arrPlacesDetailImgs = [[NSMutableArray alloc] init];
    arrPlacesDetailImgsPath = [[NSMutableArray alloc] init];
    arrNewPlaces = [[NSMutableArray alloc] init];
    
    
    //============ Button shape ==================
    btnPin.layer.cornerRadius = btnPin.frame.size.width/2;
    btnPin.clipsToBounds = YES;
    
    //============ Segment ==================
    _changeMapTypeSegment.layer.cornerRadius = 4;
    _changeMapTypeSegment.clipsToBounds = YES;
    
    //============ Tableview ==================
  //  _tblPlaceList.rowHeight = UITableViewAutomaticDimension;
//    _tblPlaceList.estimatedRowHeight = 150; // set to whatever
    
//   _tblPlaceList.allowsSelectionDuringEditing = NO;
//    self.tblPlaceList.editing = NO;
    
    
    _tblPlaceList.tableFooterView = [UIView new];
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tblPlaceList.frame.size.width, 70)];
    [self.tblPlaceList setTableFooterView:view];
    
    _tblPlaceList.hidden = NO;
//    _mainVwBottomConstraint.constant = 70;
//    _mapVwBottomConstraint.constant = 70;
//    _segmentBottomContraint.constant = 70;
    _filterVw.hidden = YES;
   
    //============ Navigationbar custom height ==================
    if (DEVICE_IS_IPHONE_X) {
        _navigationVwHeightContraint.constant = 84;
    }
    
    //============ Refresh control ==================
    refreshControl = [[UIRefreshControl alloc]init];
    [_tblPlaceList addSubview:refreshControl];
    [refreshControl addTarget:self action:@selector(refreshTable) forControlEvents:UIControlEventValueChanged];
  
//    [self.placeInfo loadData];
//    [self loadData];
   
}

- (void)getAllDownFlag {
    if ([_strLanguageCode isEqualToString:@"txt_eng"]) {
       
        
        NSString *query = [NSString stringWithFormat:@"select down_txt_eng from %@",DB_TABLENAME_PLACEMASTER];
        
        // Get the results.
        if (self.arrDownFlagEn != nil) {
            self.arrDownFlagEn = nil;
        }
        self.arrDownFlagEn = [[NSArray alloc] initWithArray:[self.dbHelper loadDataFromDB:query]];
        
        
        //    NSString *strDbDownloadFlag = [NSString stringWithFormat:@"select down_txt_eng from %@",DB_TABLENAME_PLACEMASTER];
        //
        //    NSLog(@"strDbDownloadFlag%@",strDbDownloadFlag);
        //
        //    NSString *str = strDbDownloadFlag;
        NSLog(@"str %@",self.arrDownFlagEn);
        
        NSString *strAllENFlagDl ;
        NSString *strAnyOneNotDL;
        NSLog(@"strAllENFlag is %@",strAllENFlagDl);
        for (NSArray * arr in _arrDownFlagEn) {
            NSString * strValu = [NSString stringWithFormat:@"%@", [arr objectAtIndex:0]];
            if (![strValu isEqualToString:@"1"] ) {
                strAllENFlagDl = @"1";
            }else{
                
            }
            
        }
        
        
        if (![strAllENFlagDl isEqualToString: @"1"]) {
            [_btnDownloadAll setImage:[UIImage imageNamed:@"delete"] forState:UIControlStateNormal];
        }
    }else if([_strLanguageCode isEqualToString:@"txt_islenska"]) {
        
        NSString *query = [NSString stringWithFormat:@"select down_txt_islenska from %@",DB_TABLENAME_PLACEMASTER];
        
        // Get the results.
        if (self.arrDownFlagEn != nil) {
            self.arrDownFlagEn = nil;
        }
        self.arrDownFlagEn = [[NSArray alloc] initWithArray:[self.dbHelper loadDataFromDB:query]];
        
        
        NSLog(@"str %@",self.arrDownFlagEn);
        
        NSString *strAllENFlagDl ;
      
        NSLog(@"strAllIsFlag is %@",strAllENFlagDl);
        for (NSArray * arr in _arrDownFlagEn) {
            NSString * strValu = [NSString stringWithFormat:@"%@", [arr objectAtIndex:0]];
            if (![strValu isEqualToString:@"1"] ) {
                strAllENFlagDl = @"1";
            }else{
                
            }
            
        }
        
        
        if (![strAllENFlagDl isEqualToString: @"1"]) {
            [_btnDownloadAll setImage:[UIImage imageNamed:@"delete"] forState:UIControlStateNormal];
        }
    }
  
//    if ([strAllENFlagDl isEqualToString:@"1"]) {
//         [_btnDownloadAll setImage:[UIImage imageNamed:@"delete"] forState:UIControlStateNormal];
//    }

    //        if ([strDownLocalFlag isEqualToString:@"0"]) {
    //             [_btnDownloadAll setImage:[UIImage imageNamed:@"download_white"] forState:UIControlStateNormal];
    //        }
    //
    //        if ([strDownLocalFlag isEqualToString:@"1"]) {
    //            [_btnDownloadAll setImage:[UIImage imageNamed:@"delete"] forState:UIControlStateNormal];
    //        }
    
}

-(void)loadData {
    // Form the query.
    NSString *query = [NSString stringWithFormat:@"select * from %@",DB_TABLENAME_PLACEMASTER];
    
    // Get the results.
    if (self.arrPlacesInfo != nil) {
        self.arrPlacesInfo = nil;
    }
    self.arrPlacesInfo = [[NSArray alloc] initWithArray:[self.dbHelper loadDataFromDB:query]];
    
    // Reload the table view.
//    [self.tblPlaceList reloadData];
    
    /*
    NSInteger indexOfPlaceName = [self.dbHelper.arrColumnNames indexOfObject:DB_TABLEFIELD_NAME];
    NSInteger indexOfDetail = [self.dbHelper.arrColumnNames indexOfObject:DB_TABLEFIELD_DESCRIPTION];
    NSInteger indexOfPlaceImages = [self.dbHelper.arrColumnNames indexOfObject:DB_TABLEFIELD_IMAGE];
    
    //    NSInteger indexOfAge = [self.dbManager.arrColumnNames indexOfObject:@"age"];
    
    // Set the loaded data to the appropriate cell labels.
    cell.lblPlaceName.text = [NSString stringWithFormat:@"%@", [[self.arrPlacesInfo objectAtIndex:indexPath.row] objectAtIndex:indexOfPlaceName]];
    
    cell.lblPlaceDetail.text = [NSString stringWithFormat:@"%@", [[self.arrPlacesInfo objectAtIndex:indexPath.row] objectAtIndex:indexOfDetail]];
    
    UIImage *myImage = [UIImage imageWithContentsOfFile:[[self.arrPlacesInfo objectAtIndex:indexPath.row] objectAtIndex:indexOfPlaceImages]];
    cell.placeImgVw.image = myImage;
     */
}

//TODO: refresh your data
- (void)refreshTable {
    [refreshControl endRefreshing];
    
    [self getNewDistancePlacesList];

//    [_tblPlaceList reloadData];
    
    if ([utility isInternetConnected]) {
        btnMenu.enabled = YES;
        btnLanguage.enabled = YES;
        [btnLanguage setTitleColor:colorFromRGB(255, 255, 255) forState:UIControlStateNormal];
        
    }else {
        btnMenu.enabled = NO;
        btnLanguage.enabled = NO;
        [btnLanguage setTitleColor:colorFromRGBA(255, 255, 255, 0.5) forState:UIControlStateNormal];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    NSString *strIntro = [userDefaults valueForKey:PREFERENCE_INTRO_FLAG];
    //    NSLog(@"strIntro is %@",strIntro);
    
    if ([strIntro isEqualToString:@"1"]) {
        
        [self setUpShowcaseView];
        [_showcaseView showOnMainWindow];
        
    }else {
    }
    
}

- (void)setUpShowcaseView {
    
    [userDefaults setObject:@"0" forKey:PREFERENCE_INTRO_FLAG];
    
    NSMutableArray<MNShowcaseItem*> *arrItems = [NSMutableArray new];
    
    //    _strLanguageCode = [userDefaults valueForKey:PREFERENCE_LANGUAGE];
    //    if ([_strLanguageCode isEqualToString:@"txt_eng"]) {
    
    // About left button
    MNShowcaseItem *item = [[MNShowcaseItem alloc] initWithViewToFocus:btnMenu title:@"Left Menu" description:@"General Info, Parking, Camping and More"];
    item.highlightedColor = [UIColor whiteColor];
    item.buttonTitle = @"  NEXT  ";
    item.buttonPosition = MNButtonPosition_BottomRight;
    //    item.titleColor = colorFromRGB(237, 33, 41);
    //    item.descriptionColor = colorFromRGB(237, 33, 41);
    [arrItems addObject:item];
    
    item = [[MNShowcaseItem alloc] initWithViewToFocus:btnLanguage title:@"Language" description:@"Set language"];
    item.highlightedColor = [UIColor whiteColor];
    item.buttonTitle = @"  NEXT  ";
    item.buttonPosition = MNButtonPosition_BottomRight;
    //    item.titleColor = colorFromRGB(237, 33, 41);
    //    item.descriptionColor = colorFromRGB(237, 33, 41);
    [arrItems addObject:item];
    
    item = [[MNShowcaseItem alloc] initWithViewToFocus:_btnDownloadAll title:@"Download" description:@"Download content to your device."];
    item.highlightedColor = [UIColor whiteColor];
    item.buttonTitle = @"  NEXT  ";
    item.buttonPosition = MNButtonPosition_BottomRight;
    //    item.titleColor = colorFromRGB(237, 33, 41);
    //    item.descriptionColor = colorFromRGB(237, 33, 41);
    [arrItems addObject:item];
    
    
    item = [[MNShowcaseItem alloc] initWithViewToFocus:btnFilter title:@"Filter" description:@"Favourites, Visited Places"];
    item.highlightedColor = [UIColor whiteColor];
    item.selectionType = MNSelection_EllipseOutside;
    item.buttonTitle = @"  DONE  ";
    item.buttonPosition = MNButtonPosition_BottomRight;
    //    item.titleColor = colorFromRGB(237, 33, 41);
    //    item.descriptionColor = colorFromRGB(237, 33, 41);
    [arrItems addObject:item];
    
    
    [_showcaseView setShowcaseItems:arrItems];
    // Set button background
    [_showcaseView.button setTitleColor:colorFromRGB(237, 33, 41) forState:UIControlStateNormal];
    _showcaseView.button.backgroundColor = [UIColor whiteColor];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
//    [btnMenu setImage:[UIImage imageNamed:@"side_menu"] forState:UIControlStateNormal];
    
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(didLocationUpdate:)
//                                                 name:@"LocationUpdate"
//                                               object:nil];
    [self setDownloadAllButtonImage];
    _strLanguageCode = [userDefaults valueForKey:PREFERENCE_LANGUAGE];

  
    if ([_strLanguageCode isEqualToString:@"txt_eng"]) {
        if ([[userDefaults valueForKey:PREFERENCE_DOWN_FLAG_EN]isEqualToString:@"1"]) {
            [_btnDownloadAll setImage:[UIImage imageNamed:@"delete"] forState:UIControlStateNormal];

        }else{
            [_btnDownloadAll setImage:[UIImage imageNamed:@"download_white"] forState:UIControlStateNormal];

        }
        
    }else if([_strLanguageCode isEqualToString:@"txt_islenska"]) {
        if ([[userDefaults valueForKey:PREFERENCE_DOWN_FLAG_IS]isEqualToString:@"1"]) {
            [_btnDownloadAll setImage:[UIImage imageNamed:@"delete"] forState:UIControlStateNormal];

        }else{
            [_btnDownloadAll setImage:[UIImage imageNamed:@"download_white"] forState:UIControlStateNormal];

        }
    }else if([_strLanguageCode isEqualToString:@"txt_German"]) {
        if ([[userDefaults valueForKey:PREFERENCE_DOWN_FLAG_GR]isEqualToString:@"1"]) {
            [_btnDownloadAll setImage:[UIImage imageNamed:@"delete"] forState:UIControlStateNormal];

        }else{
            [_btnDownloadAll setImage:[UIImage imageNamed:@"download_white"] forState:UIControlStateNormal];

        }
    }else if([_strLanguageCode isEqualToString:@"txt_French"]) {
        if ([[userDefaults valueForKey:PREFERENCE_DOWN_FLAG_FR]isEqualToString:@"1"]) {
            [_btnDownloadAll setImage:[UIImage imageNamed:@"delete"] forState:UIControlStateNormal];

        }else{
            [_btnDownloadAll setImage:[UIImage imageNamed:@"download_white"] forState:UIControlStateNormal];

        }

    }else if([_strLanguageCode isEqualToString:@"txt_Spanish"]) {
        if ([[userDefaults valueForKey:PREFERENCE_DOWN_FLAG_ES]isEqualToString:@"1"]) {
            [_btnDownloadAll setImage:[UIImage imageNamed:@"delete"] forState:UIControlStateNormal];

        }else{
            [_btnDownloadAll setImage:[UIImage imageNamed:@"download_white"] forState:UIControlStateNormal];

        }
        
    }else if([_strLanguageCode isEqualToString:@"txt_Chinese"]) {
        if ([[userDefaults valueForKey:PREFERENCE_DOWN_FLAG_CH]isEqualToString:@"1"]) {
            [_btnDownloadAll setImage:[UIImage imageNamed:@"delete"] forState:UIControlStateNormal];

        }else{
            [_btnDownloadAll setImage:[UIImage imageNamed:@"download_white"] forState:UIControlStateNormal];

        }
        
    }else if([_strLanguageCode isEqualToString:@"txt_Danish"]) {
        if ([[userDefaults valueForKey:PREFERENCE_DOWN_FLAG_DA]isEqualToString:@"1"]) {
            [_btnDownloadAll setImage:[UIImage imageNamed:@"delete"] forState:UIControlStateNormal];

        }else{
            [_btnDownloadAll setImage:[UIImage imageNamed:@"download_white"] forState:UIControlStateNormal];

        }
        
    }
    
    

    if ([utility isInternetConnected]) {
        btnMenu.enabled = YES;
        btnLanguage.enabled = YES;
        [btnLanguage setTitleColor:colorFromRGB(255, 255, 255) forState:UIControlStateNormal];

    }else {
        btnMenu.enabled = NO;
        btnLanguage.enabled = NO;
        [btnLanguage setTitleColor:colorFromRGBA(255, 255, 255, 0.5) forState:UIControlStateNormal];
    }
    
    [_tblPlaceList reloadData];


    if(Diclabels.count == 0) {
        [self getLabels];
    }

    
    NSDate * now = [NSDate date];
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    [outputFormatter setDateFormat:@"dd-MM-YYYY HH:mm"];
    NSString *currentTime = [outputFormatter stringFromDate:now];
    NSLog(@"currentTime %@", currentTime);
    
    //    isUpdateLocation = [userDefaults objectForKey:PREFERENCE_CHECK_TIME];
    //    if (isUpdateLocation == YES) {
    
    if ([userDefaults objectForKey:PREFERENCE_CHECK_TIME] != nil) {
        
        if ([userDefaults objectForKey:PREFERENCE_CHECK_TIME]) {
            
            if ([userDefaults objectForKey:PREFERENCE_LOCATION_UPDATER] == nil) {
                
                [userDefaults setObject:currentTime forKey:PREFERENCE_LOCATION_UPDATER];
                
            } else {
                
                oldTime = [userDefaults valueForKey:PREFERENCE_LOCATION_UPDATER];
                NSLog(@"oldTime is %@", oldTime);
                
                NSString *date1fj =[NSString stringWithFormat:@"%@",oldTime];
                NSString *date2fj =[NSString stringWithFormat:@"%@",currentTime];
                
                NSDateFormatter *df=[[NSDateFormatter alloc] init];
                // Set the date format according to your needs
                //[df setDateFormat:@"dd-mm-yyyy hh:mm"]; //for 12 hour format
                [df setDateFormat:@"dd-MM-YYYY HH:mm"];  // for 24 hour format
                
                NSDate *date1 = [df dateFromString:date1fj];
                NSDate *date2 = [df dateFromString:date2fj];
                NSLog(@"%@",date1);
                NSLog(@"%@",date2);
                
                NSTimeInterval interval = [date2 timeIntervalSinceDate:date1];
                int hours = (int)interval / 3600;             // integer division to get the hours part
                int minutes = (interval - (hours*3600)) / 60; // interval minus hours part (in seconds) divided by 60 yields minutes
                NSString *timeDiff = [NSString stringWithFormat:@"%d:%02d", hours, minutes];
                
                NSLog(@"Final time %@",timeDiff);
                
                if (minutes >= 2) {
                    [userDefaults setObject:currentTime forKey:PREFERENCE_LOCATION_UPDATER];
                    [self getNewDistancePlacesList];
                }
                
            }
        }
    }
    
}

- (void)didLocationUpdate:(NSNotification *)notification {
    NSLog(@"recieved");
    locationManager.delegate = self;
    [locationManager startUpdatingLocation];
    
}

- (void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    
//    [locationManager stopUpdatingLocation];
//    locationManager = nil;
    [self stopIndicator];
    
//    [userDefaults setObject:arrPlacesData forKey:@"MainArrayData"];
    
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    //============ Set Font ==================
    [_navTitle setFont:[UIFont fontWithName:FONT_ROBOTO_REGULAR size:15]];
    [btnLanguage.titleLabel setFont:[UIFont fontWithName:FONT_ROBOTO_REGULAR size:15]];
    
    [_lblFilter setFont:[UIFont fontWithName:FONT_ROBOTO_REGULAR size:14]];
    [_lblFav setFont:[UIFont fontWithName:FONT_ROBOTO_REGULAR size:13]];
    [_lblPlace setFont:[UIFont fontWithName:FONT_ROBOTO_REGULAR size:13]];
    [_lblNoData setFont:[UIFont fontWithName:FONT_ROBOTO_REGULAR size:14]];
    [_lblNoData setTextColor:colorFromRGB(102, 102, 102)];

    [_btnFav setTitleColor:colorFromRGB(102, 102, 102) forState:UIControlStateNormal];
    [_btnFav.titleLabel setFont:[UIFont fontWithName:FONT_ROBOTO_REGULAR size:14]];
    [_btnVisited setTitleColor:colorFromRGB(102, 102, 102) forState:UIControlStateNormal];
    [_btnVisited.titleLabel setFont:[UIFont fontWithName:FONT_ROBOTO_REGULAR size:14]];
    [_btnApply.titleLabel setFont:[UIFont fontWithName:FONT_ROBOTO_REGULAR size:14]];
    [_btnReset setTitleColor:colorFromRGB(102, 102, 102) forState:UIControlStateNormal];
    [_btnReset.titleLabel setFont:[UIFont fontWithName:FONT_ROBOTO_REGULAR size:14]];
    
    if (DEVICE_IS_IPAD) {
        
    }

}

- (void)setDownloadAllButtonImage {
    [self getAllDownFlag];

//    strDown = [userDefaults valueForKey:PREFERENCE_STRDOWN];

    if ([_strLanguageCode isEqualToString:@"txt_eng"]) {
        strDown = [userDefaults valueForKey:PREFERENCE_DOWN_FLAG_EN];
        
    }else if([_strLanguageCode isEqualToString:@"txt_islenska"]) {
        strDown = [userDefaults valueForKey:PREFERENCE_DOWN_FLAG_IS];
        
    }else if([_strLanguageCode isEqualToString:@"txt_German"]) {
        strDown = [userDefaults valueForKey:PREFERENCE_DOWN_FLAG_GR];
        
    }else if([_strLanguageCode isEqualToString:@"txt_French"]) {
        strDown = [userDefaults valueForKey:PREFERENCE_DOWN_FLAG_FR];
        
    }else if([_strLanguageCode isEqualToString:@"txt_Spanish"]) {
        strDown = [userDefaults valueForKey:PREFERENCE_DOWN_FLAG_ES];
        
    }else if([_strLanguageCode isEqualToString:@"txt_Chinese"]) {
        strDown = [userDefaults valueForKey:PREFERENCE_DOWN_FLAG_CH];
        
    }else if([_strLanguageCode isEqualToString:@"txt_Danish"]) {
        strDown = [userDefaults valueForKey:PREFERENCE_DOWN_FLAG_DA];
    }

    NSLog(@"String down %@",strDown);
    
    if ([strDown isEqualToString:@"0"])
    {
        // Downloading all data
        NSLog(@"----Download----");
    
        for(int i=0; i<arrPlacesData.count; i++) {
            strPlaceId = [[arrPlacesData valueForKey:@"place_id"] objectAtIndex:i];
            
            NSArray *arrResult = [self.dbHelper checkIfDataAlreadyExist:DB_TABLENAME_PLACEMASTER place_id:strPlaceId colomnNM:@"place_id"];
            
            if (arrResult.count > 0) {
                
                int columnNum = 0;
                if ([_strLanguageCode isEqualToString:@"txt_eng"]) {
                    columnNum = 38;
                }else if([_strLanguageCode isEqualToString:@"txt_islenska"]) {
                    columnNum = 39;
                }else if([_strLanguageCode isEqualToString:@"txt_German"]) {
                    columnNum = 40;
                }else if([_strLanguageCode isEqualToString:@"txt_French"]) {
                    columnNum = 41;
                }else if([_strLanguageCode isEqualToString:@"txt_Spanish"]) {
                    columnNum = 42;
                }else if([_strLanguageCode isEqualToString:@"txt_Chinese"]) {
                    columnNum = 43;
                }else if([_strLanguageCode isEqualToString:@"txt_Danish"]) {
                    columnNum = 44;
                }
                
                NSString *strDownFlag = [NSString stringWithFormat:@"%@", [[arrResult objectAtIndex:0] objectAtIndex:columnNum]];
     
                if ([strDownFlag isEqualToString:@"0"]) {
                    [_btnDownloadAll setImage:[UIImage imageNamed:@"download_white"] forState:UIControlStateNormal];
                }
            }
        }
        
    }else {
        
        // Deleting all data
        NSLog(@"----Delete----");
        
        for(int i=0; i<arrPlacesData.count; i++) {
            
            strPlaceId = [[arrPlacesData valueForKey:@"place_id"] objectAtIndex:i];
            
            NSArray *arrResult = [self.dbHelper checkIfDataAlreadyExist:DB_TABLENAME_PLACEMASTER place_id:strPlaceId colomnNM:@"place_id"];
            
            if (arrResult.count > 0) {
                int columnNum = 0;
                if ([_strLanguageCode isEqualToString:@"txt_eng"]) {
                    columnNum = 38;
                }else if([_strLanguageCode isEqualToString:@"txt_islenska"]) {
                    columnNum = 39;
                }else if([_strLanguageCode isEqualToString:@"txt_German"]) {
                    columnNum = 40;
                }else if([_strLanguageCode isEqualToString:@"txt_French"]) {
                    columnNum = 41;
                }else if([_strLanguageCode isEqualToString:@"txt_Spanish"]) {
                    columnNum = 42;
                }else if([_strLanguageCode isEqualToString:@"txt_Chinese"]) {
                    columnNum = 43;
                }else if([_strLanguageCode isEqualToString:@"txt_Danish"]) {
                    columnNum = 44;
                }
                
                NSString *strDownFlag = [NSString stringWithFormat:@"%@", [[arrResult objectAtIndex:0] objectAtIndex:columnNum]];
            
                if ([strDownFlag isEqualToString:@"1"]) {
                    [_btnDownloadAll setImage:[UIImage imageNamed:@"delete"] forState:UIControlStateNormal];

                }
            }
        }
    }

}

#pragma mark --------------------------------------
#pragma mark Language Change custom Delegate method
#pragma mark --------------------------------------

- (void)dataFromController:(NSString *)string {
    
    NSLog(@" string is : %@",string);
    _strLanguageCode = [NSString stringWithFormat:@"%@", string];
    [[NSUserDefaults standardUserDefaults] setObject:_strLanguageCode forKey:PREFERENCE_LANGUAGE];
    NSLog(@"_strLanguageCode : %@",_strLanguageCode);
   
    [_tblPlaceList reloadData];
    
    [self setDownloadAllButtonImage];
}


#pragma mark -------------------
#pragma mark --- Select Map Type
#pragma mark -------------------

- (IBAction)changeMapTypeSegmentTapped:(UISegmentedControl *)sender {
    
    NSInteger selectedSegment = sender.selectedSegmentIndex;
    
    if (selectedSegment == 0) {
        _mapVw.mapType = kGMSTypeNormal;
    }else {
        _mapVw.mapType = kGMSTypeSatellite;
    }
    
}

#pragma mark -------------------------
#pragma mark -- CLLocationManagerDelegate
#pragma mark -------------------------

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    
    NSLog(@"OldLocation %f %f", oldLocation.coordinate.latitude, oldLocation.coordinate.longitude);
    NSLog(@"NewLocation %f %f", newLocation.coordinate.latitude, newLocation.coordinate.longitude);
    coordinates = newLocation.coordinate;
    
    NSLog(@"Location is updating.....");
    
    if (isLocationGet) {
        NSLog(@"OldLocation %f %f", oldLocation.coordinate.latitude, oldLocation.coordinate.longitude);
        NSLog(@"NewLocation %f %f", newLocation.coordinate.latitude, newLocation.coordinate.longitude);
        coordinates = newLocation.coordinate;
        
        [self getRandomString];
        isLocationGet = NO;
        
//        [_tblPlaceList reloadData];
    }
    
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    
    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse) {
        // user allowed
        isLocationGet = YES;
        [locationManager startUpdatingLocation];
        
    }else if ([CLLocationManager authorizationStatus]==kCLAuthorizationStatusDenied) {
        
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:[Diclabels valueForKey:@"msg_enable_location_permission"]
                                              message:[Diclabels valueForKey:@"msg_location_service"]
                                              preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *goToSettings = [UIAlertAction
                                       actionWithTitle:[Diclabels valueForKey:@"txt_settings"]
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction *action)
                                       {
                                           //Simple way to open settings module
                                           NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                                           //                                           [[UIApplication sharedApplication] openURL:url];
                                           
                                           UIApplication *application = [UIApplication sharedApplication];
                                           [application openURL:url options:@{} completionHandler:nil];
                                           
                                       }];
        
        [alertController addAction:goToSettings];
        
        [self presentViewController:alertController animated:YES completion:^{
            
        }];
    }
    
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    
    if ([error domain] == kCLErrorDomain) {
        
        // We handle CoreLocation-related errors here
        switch ([error code]) {
                // "Don't Allow" on two successive app launches is the same as saying "never allow". The user
                // can reset this for all apps by going to Settings > General > Reset > Reset Location Warnings.
            case kCLErrorDenied:
                isLocationGet = NO;
                
            case kCLErrorLocationUnknown:
                isLocationGet = NO;

            case kCLAuthorizationStatusAuthorizedWhenInUse:
                isLocationGet = YES;
                [locationManager startUpdatingLocation];
                
            case kCLAuthorizationStatusAuthorizedAlways:
                isLocationGet = YES;
                [locationManager startUpdatingLocation];
                
            default:
                break;
        }
        
    } else {
        // We handle all non-CoreLocation errors here
    }
}


#pragma mark ---------------------------------
#pragma mark -- TableView Datasource Delegate Event
#pragma mark ---------------------------------

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return arrPlacesData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    PlaceCell *cell = (PlaceCell *)[tableView dequeueReusableCellWithIdentifier:@"PlaceCell"];
    
    cell.lblPlaceName.textColor = colorFromRGB(33, 33, 33);
    [cell.lblPlaceName setFont:[UIFont fontWithName:FONT_ROBOTO_BOLD size:14]];
    
    cell.lblPlaceDetail.textColor = colorFromRGB(102, 102, 102);
    [cell.lblPlaceDetail setFont:[UIFont fontWithName:FONT_ROBOTO_REGULAR size:13]];
    
    cell.lblKm.textColor = colorFromRGB(33, 33, 33);
    [cell.lblKm setFont:[UIFont fontWithName:FONT_ROBOTO_REGULAR size:13]];
    
    cell.placeImgVw.clipsToBounds = YES;

    cell.btnSaveAndDelete.tag = indexPath.row;
    [cell.btnSaveAndDelete addTarget:self action:@selector(btnSaveAndDeleteTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.btnLike.tag = indexPath.row;
    [cell.btnLike addTarget:self action:@selector(btnLikeTapped:) forControlEvents:UIControlEventTouchUpInside];
    
  
//    if ([utility isInternetConnected]) {
////        [cell.placeImgVw sd_setImageWithURL:[NSURL URLWithString: [arrPlaceImages objectAtIndex:indexPath.row]] placeholderImage:[UIImage imageNamed:@"picture"]];
//    }else {
//    }
    
//    cell.lblPlaceName.text = [[arrPlacesData valueForKey:@"place_title"] objectAtIndex:indexPath.row];
//    cell.lblPlaceDetail.text = [[arrPlacesData valueForKey:@"place_desc_none_html"] objectAtIndex:indexPath.row];
    
    /*
    CLLocation *originLocation = [[CLLocation alloc] initWithLatitude:locationManager.location.coordinate.latitude longitude:locationManager.location.coordinate.longitude];
    NSString *strLatitudeDes  = [NSString stringWithFormat:@"%@", [[arrPlacesData objectAtIndex:indexPath.row]valueForKey:@"latitude"]];
    NSString *strLongtudeDes = [NSString stringWithFormat:@"%@", [[arrPlacesData objectAtIndex:indexPath.row]valueForKey:@"longitude"]];

    coordinates.latitude = [strLatitudeDes doubleValue];
    coordinates.longitude = [strLongtudeDes doubleValue];
    
    CLLocation *destinationLocation = [[CLLocation alloc] initWithLatitude:coordinates.latitude longitude:coordinates.longitude];
   
    CLLocationDistance distance = [originLocation distanceFromLocation:destinationLocation];
    
    NSLog(@"distance is %f", distance/1000.0);
    */
    
    double kilometer = [[[arrPlacesData valueForKey:@"distance"] objectAtIndex:indexPath.row] doubleValue];
    cell.lblKm.text = [NSString stringWithFormat:@"%.2f km", kilometer];//(distance/1000.0)];
    /*[[arrPlacesData valueForKey:@"distance"] objectAtIndex:indexPath.row]*/
    
    if ([_strLanguageCode isEqualToString:@"txt_eng"]) {
        cell.lblPlaceName.text = [[arrPlacesData valueForKey:@"place_title"] objectAtIndex:indexPath.row];
        cell.lblPlaceDetail.text = [[arrPlacesData valueForKey:@"place_desc_none_html"] objectAtIndex:indexPath.row];
        [btnLanguage setTitle:@"EN" forState:UIControlStateNormal];

    }else if ([_strLanguageCode isEqualToString:@"txt_islenska"]) {
        cell.lblPlaceName.text = [[arrPlacesData valueForKey:@"place_title_islenska"] objectAtIndex:indexPath.row];
        cell.lblPlaceDetail.text = [[arrPlacesData valueForKey:@"place_desc_islenska_none_html"] objectAtIndex:indexPath.row];
        [btnLanguage setTitle:@"IS" forState:UIControlStateNormal];

    }else if ([_strLanguageCode isEqualToString:@"txt_German"]) {
        cell.lblPlaceName.text = [[arrPlacesData valueForKey:@"place_title_german"] objectAtIndex:indexPath.row];
        cell.lblPlaceDetail.text = [[arrPlacesData valueForKey:@"place_desc_german_none_html"] objectAtIndex:indexPath.row];
        [btnLanguage setTitle:@"DE" forState:UIControlStateNormal];

    }else if ([_strLanguageCode isEqualToString:@"txt_French"]) {
        cell.lblPlaceName.text = [[arrPlacesData valueForKey:@"place_title_french"] objectAtIndex:indexPath.row];
        cell.lblPlaceDetail.text = [[arrPlacesData valueForKey:@"place_desc_french_none_html"] objectAtIndex:indexPath.row];
        [btnLanguage setTitle:@"FR" forState:UIControlStateNormal];

    }else if ([_strLanguageCode isEqualToString:@"txt_Spanish"]) {
        cell.lblPlaceName.text = [[arrPlacesData valueForKey:@"place_title_spanish"] objectAtIndex:indexPath.row];
        cell.lblPlaceDetail.text = [[arrPlacesData valueForKey:@"place_desc_spanish_none_html"] objectAtIndex:indexPath.row];
        [btnLanguage setTitle:@"ES" forState:UIControlStateNormal];

    }else if ([_strLanguageCode isEqualToString:@"txt_Chinese"]) {
        cell.lblPlaceName.text = [[arrPlacesData valueForKey:@"place_title_chinese"] objectAtIndex:indexPath.row];
        cell.lblPlaceDetail.text = [[arrPlacesData valueForKey:@"place_desc_chinese_none_html"] objectAtIndex:indexPath.row];
        [btnLanguage setTitle:@"ZH" forState:UIControlStateNormal];

    }else if ([_strLanguageCode isEqualToString:@"txt_Danish"]) {
        cell.lblPlaceName.text = [[arrPlacesData valueForKey:@"place_title_danish"] objectAtIndex:indexPath.row];
        cell.lblPlaceDetail.text = [[arrPlacesData valueForKey:@"place_desc_danish_none_html"] objectAtIndex:indexPath.row];
        [btnLanguage setTitle:@"DA" forState:UIControlStateNormal];
    }
  
//        NSArray *arrGetImageMain = [userDefaults valueForKey:@"MainImageData"];
//        [arrLocalPlacesImg removeAllObjects];
//        [arrLocalPlacesImg addObjectsFromArray:arrGetImageMain];
    UIImage *myImage = [UIImage imageWithContentsOfFile:[[arrPlacesData valueForKey:@"thumbImage"] objectAtIndex:indexPath.row]];
    cell.placeImgVw.image = myImage;
    
    
    strPlaceId = [[arrPlacesData valueForKey:@"place_id"] objectAtIndex:indexPath.row];
//        NSLog(@"%@", strPlaceId);
    
    NSArray *arrResult = [self.dbHelper checkIfDataAlreadyExist:DB_TABLENAME_PLACEMASTER place_id:strPlaceId colomnNM:@"place_id"];
    
    //Like flag
    if (arrResult.count > 0) {
        NSString *strFavLocalStatus = [NSString stringWithFormat:@"%@", [[arrResult objectAtIndex:0]objectAtIndex:9]];

        if ([strFavLocalStatus isEqualToString:@"0"]) {
            cell.likeImgVw.image = [UIImage imageNamed:@"favourite_gray"];
        }else{
            cell.likeImgVw.image = [UIImage imageNamed:@"favourite_red"];
        }
    }else {
        NSString *strFavLiveStatus = [NSString stringWithFormat:@"%@", [[arrPlacesData objectAtIndex:indexPath.row] valueForKey:@"isLike"]];//[[arrPlacesData valueForKey:@"isLike"] objectAtIndex:indexPath.row];
        if ([strFavLiveStatus isEqualToString:@"0" ]) {
            cell.likeImgVw.image = [UIImage imageNamed:@"favourite_gray"];
        }else{
            cell.likeImgVw.image = [UIImage imageNamed:@"favourite_red"];
        }
    }
    
   
    
    //Download flag
    if (arrResult.count > 0) {
//        NSString *strDownLocalFlag;
        int columnNum = 0;
        if ([_strLanguageCode isEqualToString:@"txt_eng"]) {
            columnNum = 38;
            
//            NSInteger indexOfDownFlagEn = [self.dbHelper.arrColumnNames indexOfObject:DB_TABLEFIELD_DOWN_FLAG_EN];
//            strDownLocalFlag = [NSString stringWithFormat:@"%@", [[arrResult objectAtIndex:0] objectAtIndex:indexOfDownFlagEn]];
//
        }else if([_strLanguageCode isEqualToString:@"txt_islenska"]) {
            columnNum = 39;
//            NSInteger indexOfDownFlagEn = [self.dbHelper.arrColumnNames indexOfObject:DB_TABLEFIELD_DOWN_FLAG_EN];
//            strDownLocalFlag = [NSString stringWithFormat:@"%@", [[arrResult objectAtIndex:0] objectAtIndex:indexOfDownFlagEn]];
        }else if([_strLanguageCode isEqualToString:@"txt_German"]) {
            columnNum = 40;
//            NSInteger indexOfDownFlagEn = [self.dbHelper.arrColumnNames indexOfObject:DB_TABLEFIELD_DOWN_FLAG_EN];
//            strDownLocalFlag = [NSString stringWithFormat:@"%@", [[arrResult objectAtIndex:0] objectAtIndex:indexOfDownFlagEn]];
        }else if([_strLanguageCode isEqualToString:@"txt_French"]) {
            columnNum = 41;
            
//            NSInteger indexOfDownFlagEn = [self.dbHelper.arrColumnNames indexOfObject:DB_TABLEFIELD_DOWN_FLAG_EN];
//            strDownLocalFlag = [NSString stringWithFormat:@"%@", [[arrResult objectAtIndex:0] objectAtIndex:indexOfDownFlagEn]];
            
        }else if([_strLanguageCode isEqualToString:@"txt_Spanish"]) {
            columnNum = 42;
//            NSInteger indexOfDownFlagEn = [self.dbHelper.arrColumnNames indexOfObject:DB_TABLEFIELD_DOWN_FLAG_EN];
//            strDownLocalFlag = [NSString stringWithFormat:@"%@", [[arrResult objectAtIndex:0] objectAtIndex:indexOfDownFlagEn]];
            
        }else if([_strLanguageCode isEqualToString:@"txt_Chinese"]) {
            columnNum = 43;
//            NSInteger indexOfDownFlagEn = [self.dbHelper.arrColumnNames indexOfObject:DB_TABLEFIELD_DOWN_FLAG_EN];
//            strDownLocalFlag = [NSString stringWithFormat:@"%@", [[arrResult objectAtIndex:0] objectAtIndex:indexOfDownFlagEn]];
            
        }else if([_strLanguageCode isEqualToString:@"txt_Danish"]) {
            columnNum = 44;
//            NSInteger indexOfDownFlagEn = [self.dbHelper.arrColumnNames indexOfObject:DB_TABLEFIELD_DOWN_FLAG_EN];
//            strDownLocalFlag = [NSString stringWithFormat:@"%@", [[arrResult objectAtIndex:0] objectAtIndex:indexOfDownFlagEn]];
        }
        
        NSString *strDownLocalFlag = [NSString stringWithFormat:@"%@", [[arrResult objectAtIndex:0] objectAtIndex:columnNum]];
        //        NSString *strDownLocalFlag = [NSString stringWithFormat:@"%@", [[arrResult objectAtIndex:0]objectAtIndex:14]];
        
        if ([strDownLocalFlag isEqualToString:@"0"]) {
            cell.imgSaveDel.image = [UIImage imageNamed:@"download_gray"];
            
//          [_btnDownloadAll setImage:[UIImage imageNamed:@"download_white"] forState:UIControlStateNormal];
            
        }else {
            cell.imgSaveDel.image = [UIImage imageNamed:@"delete_place"];
//          [_btnDownloadAll setImage:[UIImage imageNamed:@"delete"] forState:UIControlStateNormal];
        }
        

    }else {
      
        NSString *strDownLiveFlag;
        
        if ([_strLanguageCode isEqualToString:@"txt_eng"]) {
            strDownLiveFlag = [[arrPlacesData valueForKey:@"down_txt_eng"] objectAtIndex:indexPath.row];
            
        }else if([_strLanguageCode isEqualToString:@"txt_islenska"]) {
            strDownLiveFlag = [[arrPlacesData valueForKey:@"down_txt_islenska"] objectAtIndex:indexPath.row];
      
        }else if([_strLanguageCode isEqualToString:@"txt_German"]) {
            strDownLiveFlag = [[arrPlacesData valueForKey:@"down_txt_german"] objectAtIndex:indexPath.row];
        
        }else if([_strLanguageCode isEqualToString:@"txt_French"]) {
           strDownLiveFlag = [[arrPlacesData valueForKey:@"down_txt_french"] objectAtIndex:indexPath.row];
        
        }else if([_strLanguageCode isEqualToString:@"txt_Spanish"]) {
            strDownLiveFlag = [[arrPlacesData valueForKey:@"down_txt_spanish"] objectAtIndex:indexPath.row];
     
        }else if([_strLanguageCode isEqualToString:@"txt_Chinese"]) {
            strDownLiveFlag = [[arrPlacesData valueForKey:@"down_txt_chinese"] objectAtIndex:indexPath.row];
    
        }else if([_strLanguageCode isEqualToString:@"txt_Danish"]) {
            strDownLiveFlag = [[arrPlacesData valueForKey:@"down_txt_danish"] objectAtIndex:indexPath.row];
        }
        
        
        if ([strDownLiveFlag isEqualToString:@"0"]) {
            cell.imgSaveDel.image = [UIImage imageNamed:@"download_gray"];
//                [_btnDownloadAll setImage:[UIImage imageNamed:@"download_white"] forState:UIControlStateNormal];
        }else {
            cell.imgSaveDel.image = [UIImage imageNamed:@"delete_place"];
//                [_btnDownloadAll setImage:[UIImage imageNamed:@"delete"] forState:UIControlStateNormal];
        }

    }
  
    //Visit flag
    if (arrResult.count > 0) {
        NSString *strVisitLocalStatus = [NSString stringWithFormat:@"%@", [[arrResult objectAtIndex:0]objectAtIndex:10]];

        if ([strVisitLocalStatus isEqualToString:@"0"]) {
            cell.contentView.backgroundColor = [UIColor whiteColor];
        }else {
            cell.contentView.backgroundColor = colorFromRGB(199, 229, 204);
        }
    }else {
        NSString *strVisitLiveFlag = [NSString stringWithFormat:@"%@", [[arrPlacesData objectAtIndex:indexPath.row] valueForKey:@"isVisited"]];// [[arrPlacesData valueForKey:@"isVisited"] objectAtIndex:indexPath.row];
        if ([strVisitLiveFlag isEqualToString:@"0"]) {
            cell.contentView.backgroundColor = [UIColor whiteColor];
        }else {
            cell.contentView.backgroundColor = colorFromRGB(199, 229, 204);
        }
    }

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    [self showLoadingIndicator];

    strPlaceId = [[arrPlacesData valueForKey:@"place_id"] objectAtIndex:indexPath.row];
    NSString *strDownFlag = [[arrPlacesData valueForKey:@"down_flag"] objectAtIndex:indexPath.row];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        //Background Thread
        
        NSArray *arrTemp = [self.dbHelper checkIfDataAlreadyExist:DB_TABLENAME_PLACEMASTER place_id:strPlaceId colomnNM:@"place_id"];
        NSLog(@"arrTemp %@ ::", arrTemp);
        
        [userDefaults setObject:arrTemp forKey:@"Db"];
        
        NSArray *arrDetailLargeImages = [userDefaults valueForKey:@"ArrDetailImages"];
        
        dispatch_async(dispatch_get_main_queue(), ^(void) {
            //stop your HUD here
            //This is run on the main thread
            //Run UI Updates

            [self stopIndicator];
            
            if ([utility isInternetConnected]) {
                
                if ([arrTemp count] > 0) {
                   
                    int columnNum = 0;
                    NSString *path;
                    
                    if ([_strLanguageCode isEqualToString:@"txt_eng"]) {
                        columnNum = 38;
                        
                        NSInteger indexOfPlaceAudio = [self.dbHelper.arrColumnNames indexOfObject:DB_TABLEFIELD_AUDIO];
                        path = [NSString stringWithFormat:@"%@", [[arrTemp objectAtIndex:0] objectAtIndex:indexOfPlaceAudio]];
                        
                    }else if([_strLanguageCode isEqualToString:@"txt_islenska"]) {
                        columnNum = 39;
                        NSInteger indexOfPlaceAudioIs = [self.dbHelper.arrColumnNames indexOfObject:DB_TABLEFIELD_AUDIO_ISLENSKA];
                        path = [NSString stringWithFormat:@"%@", [[arrTemp objectAtIndex:0] objectAtIndex:indexOfPlaceAudioIs]];
                        
                    }else if([_strLanguageCode isEqualToString:@"txt_German"]) {
                        columnNum = 40;
                        NSInteger indexOfPlaceAudioGr = [self.dbHelper.arrColumnNames indexOfObject:DB_TABLEFIELD_AUDIO_GERMAN];
                        path = [NSString stringWithFormat:@"%@", [[arrTemp objectAtIndex:0] objectAtIndex:indexOfPlaceAudioGr]];
                        
                    }else if([_strLanguageCode isEqualToString:@"txt_French"]) {
                        columnNum = 41;
                        NSInteger indexOfPlaceAudioFr = [self.dbHelper.arrColumnNames indexOfObject:DB_TABLEFIELD_AUDIO_FRENCH];
                        path = [NSString stringWithFormat:@"%@", [[arrTemp objectAtIndex:0] objectAtIndex:indexOfPlaceAudioFr]];
                        
                    }else if([_strLanguageCode isEqualToString:@"txt_Spanish"]) {
                        columnNum = 42;
                        NSInteger indexOfPlaceAudioEs = [self.dbHelper.arrColumnNames indexOfObject:DB_TABLEFIELD_AUDIO_SPANISH];
                        path = [NSString stringWithFormat:@"%@", [[arrTemp objectAtIndex:0] objectAtIndex:indexOfPlaceAudioEs]];
                        
                    }else if([_strLanguageCode isEqualToString:@"txt_Chinese"]) {
                        columnNum = 43;
                        NSInteger indexOfPlaceAudioCh = [self.dbHelper.arrColumnNames indexOfObject:DB_TABLEFIELD_AUDIO_CHINESE];
                        path = [NSString stringWithFormat:@"%@", [[arrTemp objectAtIndex:0] objectAtIndex:indexOfPlaceAudioCh]];
                        
                    }else if([_strLanguageCode isEqualToString:@"txt_Danish"]) {
                        columnNum = 44;
                        NSInteger indexOfPlaceAudioDa = [self.dbHelper.arrColumnNames indexOfObject:DB_TABLEFIELD_AUDIO_DANISH];
                        path = [NSString stringWithFormat:@"%@", [[arrTemp objectAtIndex:0] objectAtIndex:indexOfPlaceAudioDa]];
                    }
                    
                    
                    NSString *strDownFlag = [NSString stringWithFormat:@"%@", [[arrTemp objectAtIndex:0] objectAtIndex:columnNum]];
                    
                    if ([strDownFlag isEqualToString:@"1"]) {
                        
                        PlaceDetailViewController *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"PlaceDetailViewController"];
                        VC.strSelectedLanguage = [[NSUserDefaults standardUserDefaults] valueForKey:PREFERENCE_LANGUAGE];
//                        VC.arrPlaceDetail = arrTemp;
                        VC.strAudioPath = path;
                        
                        NSLog(@"selected index %ld",(long)indexPath.row);
                        VC.arrLargeDetailImages = [arrDetailLargeImages objectAtIndex:indexPath.row];
                        
                        NSArray *arrDetailImage = [self.dbHelper checkIfDataAlreadyExist:DB_TABLENAME_IMAGEMASTER place_id:strPlaceId colomnNM:@"place_id"];
                        NSLog(@"Detail Image%@",arrDetailImage);
                        
                        NSMutableArray *arrImagePath = [[NSMutableArray alloc]init];
                        for (int j=0; j<arrDetailImage.count; j++) {
                            NSString *strPath = [NSString stringWithFormat:@"%@",[[arrDetailImage objectAtIndex:j]objectAtIndex:2]];
                            [arrImagePath addObject: strPath];
                        }
                       
                        NSLog(@"arrImagePath Image%@",arrImagePath);
                        
                        VC.arrDetailImages = arrImagePath;
                        VC.selectedIndex = indexPath.row;
                        [self.navigationController pushViewController:VC animated:YES];

                    }else {
                        
                        PlaceDetailViewController *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"PlaceDetailViewController"];
                        VC.strSelectedLanguage = [[NSUserDefaults standardUserDefaults] valueForKey:PREFERENCE_LANGUAGE];
                        VC.dicPlaceDetail = [arrPlacesData objectAtIndex:indexPath.row];
                        VC.arrPlaceDetail = arrTemp;
                        VC.selectedIndex = indexPath.row;

                        [self.navigationController pushViewController:VC animated:YES];

                    }
                    
                }else {
                }
                [self stopIndicator];
                
            }else {
                [self stopIndicator];
                
                if ([arrTemp count] > 0) {
                    
                    int columnNum = 0;
                    NSString *path;
                    
                    if ([_strLanguageCode isEqualToString:@"txt_eng"]) {
                        columnNum = 38;
                     
                        NSInteger indexOfPlaceAudio = [self.dbHelper.arrColumnNames indexOfObject:DB_TABLEFIELD_AUDIO];
                        path = [NSString stringWithFormat:@"%@", [[arrTemp objectAtIndex:0] objectAtIndex:indexOfPlaceAudio]];
                        
                    }else if([_strLanguageCode isEqualToString:@"txt_islenska"]) {
                        columnNum = 39;
                        NSInteger indexOfPlaceAudioIs = [self.dbHelper.arrColumnNames indexOfObject:DB_TABLEFIELD_AUDIO_ISLENSKA];
                        path = [NSString stringWithFormat:@"%@", [[arrTemp objectAtIndex:0] objectAtIndex:indexOfPlaceAudioIs]];
                        
                    }else if([_strLanguageCode isEqualToString:@"txt_German"]) {
                        columnNum = 40;
                        NSInteger indexOfPlaceAudioGr = [self.dbHelper.arrColumnNames indexOfObject:DB_TABLEFIELD_AUDIO_GERMAN];
                        path = [NSString stringWithFormat:@"%@", [[arrTemp objectAtIndex:0] objectAtIndex:indexOfPlaceAudioGr]];
                        
                    }else if([_strLanguageCode isEqualToString:@"txt_French"]) {
                        columnNum = 41;
                        NSInteger indexOfPlaceAudioFr = [self.dbHelper.arrColumnNames indexOfObject:DB_TABLEFIELD_AUDIO_FRENCH];
                        path = [NSString stringWithFormat:@"%@", [[arrTemp objectAtIndex:0] objectAtIndex:indexOfPlaceAudioFr]];
                        
                    }else if([_strLanguageCode isEqualToString:@"txt_Spanish"]) {
                        columnNum = 42;
                        NSInteger indexOfPlaceAudioEs = [self.dbHelper.arrColumnNames indexOfObject:DB_TABLEFIELD_AUDIO_SPANISH];
                        path = [NSString stringWithFormat:@"%@", [[arrTemp objectAtIndex:0] objectAtIndex:indexOfPlaceAudioEs]];
                        
                    }else if([_strLanguageCode isEqualToString:@"txt_Chinese"]) {
                        columnNum = 43;
                        NSInteger indexOfPlaceAudioCh = [self.dbHelper.arrColumnNames indexOfObject:DB_TABLEFIELD_AUDIO_CHINESE];
                        path = [NSString stringWithFormat:@"%@", [[arrTemp objectAtIndex:0] objectAtIndex:indexOfPlaceAudioCh]];
                        
                    }else if([_strLanguageCode isEqualToString:@"txt_Danish"]) {
                        columnNum = 44;
                        NSInteger indexOfPlaceAudioDa = [self.dbHelper.arrColumnNames indexOfObject:DB_TABLEFIELD_AUDIO_DANISH];
                        path = [NSString stringWithFormat:@"%@", [[arrTemp objectAtIndex:0] objectAtIndex:indexOfPlaceAudioDa]];
                    }
                    
                    NSString *strDownFlag = [NSString stringWithFormat:@"%@", [[arrTemp objectAtIndex:0] objectAtIndex:columnNum]];
//                    NSString *strDownFlag = [NSString stringWithFormat:@"%@", [[arrTemp objectAtIndex:0] objectAtIndex:14]];

                    if ([strDownFlag isEqualToString:@"1"]) {
                        
                        PlaceDetailViewController *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"PlaceDetailViewController"];
//                        VC.dicPlaceDetail = [arrPlacesData objectAtIndex:indexPath.row];
                        VC.arrPlaceDetail = arrTemp;
                        VC.strAudioPath = path;
                        VC.strSelectedLanguage = [[NSUserDefaults standardUserDefaults] valueForKey:PREFERENCE_LANGUAGE];
                        
                        NSArray *arrDetailImage = [self.dbHelper checkIfDataAlreadyExist:DB_TABLENAME_IMAGEMASTER place_id:strPlaceId colomnNM:@"place_id"];
                        NSLog(@"Detail Image%@",arrDetailImage);
                        
                        NSMutableArray *arrImagePath = [[NSMutableArray alloc]init];
                        for (int j=0; j<arrDetailImage.count; j++) {
                            NSString * strPath = [NSString stringWithFormat:@"%@",[[arrDetailImage objectAtIndex:j]objectAtIndex:2]];
                            [arrImagePath addObject: strPath];
                        }
                        
                        NSLog(@"arrImagePath Image%@",arrImagePath);
                        
                        VC.arrDetailImages = arrImagePath;
                        
                        [self.navigationController pushViewController:VC animated:YES];
                        
                    }else {
                        
                        UIAlertController *alert = [UIAlertController alertControllerWithTitle:[Diclabels valueForKey:@"txt_thingvellir"] message:[Diclabels valueForKey:@"MSG_INTERNET_UNAVAILABLE"] preferredStyle:UIAlertControllerStyleAlert];
                        
                        UIAlertAction *ok = [UIAlertAction actionWithTitle:[Diclabels valueForKey:@"OK"] style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) { }];
                        
                        [alert addAction:ok];
                        [self presentViewController:alert animated:YES completion:nil];
                    }
                }
            }
        });
    });
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSMutableDictionary *dictParam = [arrPlacesData objectAtIndex:indexPath.row];
    NSMutableDictionary *dicMut = [dictParam mutableCopy];

//    NSString *strForStatus = [[arrPlacesData valueForKey:@"isVisited"] objectAtIndex:indexPath.row];
    strPlaceId = [[arrPlacesData valueForKey:@"place_id"] objectAtIndex:indexPath.row];

//    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
//        //Background Thread
//        dispatch_async(dispatch_get_main_queue(), ^(void){
//            //Run UI Updates
//        });
//    });

    NSArray *arrResult = [self.dbHelper checkIfDataAlreadyExist:DB_TABLENAME_PLACEMASTER place_id:strPlaceId colomnNM:@"place_id"];

    if (arrResult.count > 0) {
        NSString *strForStatus = [NSString stringWithFormat:@"%@", [[arrResult objectAtIndex:0] objectAtIndex:10]];

        if ([strForStatus isEqualToString:@"1"]) {
            UITableViewRowAction *button = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:[Diclabels valueForKey:@"txt_visited"] handler:^(UITableViewRowAction *action, NSIndexPath *indexPath)
                                            {
                                                [dicMut setValue:@"0" forKey:@"isVisited"];
                                                [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys:@"0", DB_TABLEFIELD_VISITED, nil]];

                                                NSLog(@"Button tapped. Row item value %ld", (long)indexPath.row);

                                                [self loadMapView];
                                                
                                                [self.tblPlaceList beginUpdates];
                                                [self.tblPlaceList reloadRowsAtIndexPaths:[NSArray arrayWithObjects:indexPath, nil] withRowAnimation:UITableViewRowAnimationNone];
                                                [self.tblPlaceList endUpdates];
//                                                [_tblPlaceList reloadData];

                                            }];

            button.backgroundColor = colorFromRGB(40, 124, 57);
            return @[button];

        }else {

            UITableViewRowAction *button1 = [UITableViewRowAction rowActionWithStyle:indexPath.row title:[Diclabels valueForKey:@"txt_visit"] handler:^(UITableViewRowAction *action, NSIndexPath *indexPath)
                                             {
                                                 [dicMut setValue:@"1" forKey:@"isVisited"];
                                                 [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys:@"1", DB_TABLEFIELD_VISITED, nil]];
                                                 NSLog(@"Button tapped. Row item value %ld", (long)indexPath.row);
                                                 [self loadMapView];
                                                 
                                                 [self.tblPlaceList beginUpdates];
                                                 [self.tblPlaceList reloadRowsAtIndexPaths:[NSArray arrayWithObjects:indexPath, nil] withRowAnimation:UITableViewRowAnimationNone];
                                                 [self.tblPlaceList endUpdates];
//                                                 [_tblPlaceList reloadData];

                                             }];

            button1.backgroundColor = colorFromRGB(40, 124, 57);
            return @[button1];
        }
    }
    return 0;
    
}

//- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView
//           editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    return UITableViewCellEditingStyleNone;
//}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    // you need to implement this method too or nothing will work:
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

#pragma mark ---------------------------------
#pragma mark -- Button Actions
#pragma mark ---------------------------------


- (IBAction)btnPinTapped:(UIButton *)sender {

    if (_tblPlaceList.isHidden) {
        _tblPlaceList.hidden = NO;
//        _mainVwBottomConstraint.constant = 70;
//        _mapVwBottomConstraint.constant = 70;
//        _segmentBottomContraint.constant = 70;

        btnFilter.enabled = YES;
        _btnDownloadAll.enabled = YES;
        [btnPin setImage:[UIImage imageNamed:@"visited_place_white"] forState:UIControlStateNormal];

    }else{
        _tblPlaceList.hidden = YES;
//        _mainVwBottomConstraint.constant = 0;
//        _mapVwBottomConstraint.constant = 0;
//        _segmentBottomContraint.constant = 19;

        btnFilter.enabled = NO;
        _btnDownloadAll.enabled = NO;
        [self loadMapView];
        [btnPin setImage:[UIImage imageNamed:@"side_menu"] forState:UIControlStateNormal];
        
    }
}

- (IBAction)btnMenuTapped:(UIButton *)sender {
    NSLog(@"Left Menu");
    
    if ([utility isInternetConnected]) {
        btnMenu.enabled = YES;
        btnLanguage.enabled = YES;
        [btnLanguage setTitleColor:colorFromRGB(255, 255, 255) forState:UIControlStateNormal];
        
        _tblPlaceList.hidden = NO;
//        _mainVwBottomConstraint.constant = 70;
//        _mapVwBottomConstraint.constant = 70;
//        _segmentBottomContraint.constant = 70;

        btnFilter.enabled = YES;
        _btnDownloadAll.enabled = YES;
        
        //    [btnMenu setImage:[UIImage imageNamed:@"side_menu_close"] forState:UIControlStateNormal];
        // toggle the left side menu
        [self.menuContainerViewController toggleLeftSideMenuCompletion:^{
            NSLog(@"Left Menu");
            //    [btnMenu setImage:[UIImage imageNamed:@"side_menu"] forState:UIControlStateNormal];
        }];
        
    }else {
        btnMenu.enabled = NO;
        btnLanguage.enabled = NO;
        [btnLanguage setTitleColor:colorFromRGBA(255, 255, 255, 0.5) forState:UIControlStateNormal];
    }
    
   
}

- (IBAction)btnSelectLanguageTapped:(UIButton *)sender {
    
    if ([utility isInternetConnected]) {
        btnMenu.enabled = YES;
        btnLanguage.enabled = YES;
        [btnLanguage setTitleColor:colorFromRGB(255, 255, 255) forState:UIControlStateNormal];
        
        LanguageViewController *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"LanguageViewController"];
        VC.delegate = self;
        VC.strSelectedLanguage = _strLanguageCode;
        [self.navigationController pushViewController:VC animated:YES];
        
    }else {
        btnMenu.enabled = NO;
        btnLanguage.enabled = NO;
        [btnLanguage setTitleColor:colorFromRGBA(255, 255, 255, 0.5) forState:UIControlStateNormal];
    }
    
   
}

- (IBAction)btnDownloadTapped:(UIButton *)sender {
    
    [self showLoadingIndicator];
    [NSTimer scheduledTimerWithTimeInterval:1.0f target:self selector:@selector(DownAllBtnClicked:) userInfo:nil repeats:NO];
    
}

- (IBAction)DownAllBtnClicked:(id)sender{
    
    if ([utility isInternetConnected]) {
        
        //        strDown = [userDefaults valueForKey:PREFERENCE_STRDOWN];
        
        if ([_strLanguageCode isEqualToString:@"txt_eng"]) {
            strDown = [userDefaults valueForKey:PREFERENCE_DOWN_FLAG_EN];
            
        }else if([_strLanguageCode isEqualToString:@"txt_islenska"]) {
            strDown = [userDefaults valueForKey:PREFERENCE_DOWN_FLAG_IS];
            
        }else if([_strLanguageCode isEqualToString:@"txt_German"]) {
            strDown = [userDefaults valueForKey:PREFERENCE_DOWN_FLAG_GR];
            
        }else if([_strLanguageCode isEqualToString:@"txt_French"]) {
            strDown = [userDefaults valueForKey:PREFERENCE_DOWN_FLAG_FR];
            
        }else if([_strLanguageCode isEqualToString:@"txt_Spanish"]) {
            strDown = [userDefaults valueForKey:PREFERENCE_DOWN_FLAG_ES];
            
        }else if([_strLanguageCode isEqualToString:@"txt_Chinese"]) {
            strDown = [userDefaults valueForKey:PREFERENCE_DOWN_FLAG_CH];
            
        }else if([_strLanguageCode isEqualToString:@"txt_Danish"]) {
            strDown = [userDefaults valueForKey:PREFERENCE_DOWN_FLAG_DA];
        }
        NSLog(@"String down %@",strDown);
        NSInteger anIndex = 0;
        
        
        if ([strDown isEqualToString:@"0"])
        {
            [self showLoadingIndicator];
            
            // Downloading all data
            NSLog(@"----Download----");
            
            //            [userDefaults setObject:@"1" forKey:PREFERENCE_STRDOWN];
            
            if ([_strLanguageCode isEqualToString:@"txt_eng"]) {
                [userDefaults setObject:@"1" forKey:PREFERENCE_DOWN_FLAG_EN];
                
            }else if([_strLanguageCode isEqualToString:@"txt_islenska"]) {
                [userDefaults setObject:@"1" forKey:PREFERENCE_DOWN_FLAG_IS];
                
            }else if([_strLanguageCode isEqualToString:@"txt_German"]) {
                [userDefaults setObject:@"1" forKey:PREFERENCE_DOWN_FLAG_GR];
                
            }else if([_strLanguageCode isEqualToString:@"txt_French"]) {
                [userDefaults setObject:@"1" forKey:PREFERENCE_DOWN_FLAG_FR];
                
            }else if([_strLanguageCode isEqualToString:@"txt_Spanish"]) {
                [userDefaults setObject:@"1" forKey:PREFERENCE_DOWN_FLAG_ES];
                
            }else if([_strLanguageCode isEqualToString:@"txt_Chinese"]) {
                [userDefaults setObject:@"1" forKey:PREFERENCE_DOWN_FLAG_CH];
                
            }else if([_strLanguageCode isEqualToString:@"txt_Danish"]) {
                [userDefaults setObject:@"1" forKey:PREFERENCE_DOWN_FLAG_DA];
            }
            
            for(int i=0; i<arrPlacesData.count; i++) {
                
                strPlaceId = [[arrPlacesData valueForKey:@"place_id"] objectAtIndex:i];
                
                NSArray *arrResult = [self.dbHelper checkIfDataAlreadyExist:DB_TABLENAME_PLACEMASTER place_id:strPlaceId colomnNM:@"place_id"];
                
                if (arrResult.count > 0) {
                    
                    int columnNum = 0;
                    if ([_strLanguageCode isEqualToString:@"txt_eng"]) {
                        columnNum = 38;
                        
                    }else if([_strLanguageCode isEqualToString:@"txt_islenska"]) {
                        columnNum = 39;
                        
                    }else if([_strLanguageCode isEqualToString:@"txt_German"]) {
                        columnNum = 40;
                        
                    }else if([_strLanguageCode isEqualToString:@"txt_French"]) {
                        columnNum = 41;
                        
                    }else if([_strLanguageCode isEqualToString:@"txt_Spanish"]) {
                        columnNum = 42;
                        
                    }else if([_strLanguageCode isEqualToString:@"txt_Chinese"]) {
                        columnNum = 43;
                        
                    }else if([_strLanguageCode isEqualToString:@"txt_Danish"]) {
                        columnNum = 44;
                    }
                    
                    NSString *strDownFlag = [NSString stringWithFormat:@"%@", [[arrResult objectAtIndex:0] objectAtIndex:columnNum]];
                    
                    //                    NSString *strDownFlag = [NSString stringWithFormat:@"%@", [[arrResult objectAtIndex:0] objectAtIndex:14]];
                    
                    if ([strDownFlag isEqualToString:@"0"]) {
                        
                        if ([_strLanguageCode isEqualToString:@"txt_eng"]) {
                            
                            NSString *url = [NSString stringWithFormat:@"%@",[[arrPlacesData valueForKey:@"place_audio"] objectAtIndex:i]];
                            
                            //                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                            
                            NSURL *audioURL = [NSURL URLWithString:url];
                            NSString *strAudioUrl = [NSString stringWithFormat:@"%@",audioURL];
                            
                            if ([utility isStringEmpty:strAudioUrl]) {
                                NSLog(@"No Audio");
                            }else {
                                pngData = [NSData dataWithContentsOfURL:audioURL];
                                
                                NSString *anImageName = [NSString stringWithFormat:@"%ldENAudio.mp3",(long)anIndex++];
                                NSString *anAudioPath = [NSString stringWithFormat:@"%@/%@", documentsPath, anImageName];
                                //            NSLog(@"anAudioPath File path is %@ ::",anAudioPath);
                                
                                [pngData writeToFile:anAudioPath atomically:YES]; //Write the file
                                
                                
                                [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys:anAudioPath, DB_TABLEFIELD_AUDIO, nil]];
                                
                            }
                            
                            
                            [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys: @"1", DB_TABLEFIELD_DOWN_FLAG_EN, nil]];
                            
                            //                            [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys:
                            //                                                                                                         @"1", DB_TABLEFIELD_DOWN_FLAG_EN,
                            //                                                                                                         nil]];
                            //                            [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys:
                            //                                                                                                         [arrLocalAudio objectAtIndex:sender.tag],
                            //                                                                                                         DB_TABLEFIELD_AUDIO,
                            //                                                                                                         nil]];
                            
                        }else if([_strLanguageCode isEqualToString:@"txt_islenska"]) {
                            
                            
                            //                    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                            
                            //                            [dictmutTest setValue:@"1" forKey:@"down_txt_islenska"];
                            
                            NSString *url = [NSString stringWithFormat:@"%@",[[arrPlacesData valueForKey:@"place_audio_islenska"] objectAtIndex:i]];
                            
                            //                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                            
                            NSURL *audioURL = [NSURL URLWithString:url];
                            
                            //                        dispatch_async(dispatch_get_main_queue(), ^(void) {
                            //                            [MBProgressHUD hideHUDForView:self.view animated:YES];
                            
                            pngData = [NSData dataWithContentsOfURL:audioURL];
                            
                            NSString *anImageName = [NSString stringWithFormat:@"%ldISAudio.mp3",(long)anIndex++];
                            NSString *anAudioPath = [NSString stringWithFormat:@"%@/%@", documentsPath, anImageName];
                            //            NSLog(@"anAudioPath File path is %@ ::",anAudioPath);
                            
                            [pngData writeToFile:anAudioPath atomically:YES]; //Write the file
                            
                            
                            [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys:anAudioPath, DB_TABLEFIELD_AUDIO_ISLENSKA, nil]];
                            
                            //                        });
                            //                    });
                            
                            [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                                                         @"1", DB_TABLEFIELD_DOWN_FLAG_IS,
                                                                                                         nil]];
                            
                            //                            [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys:
                            //                                                                                                         [arrAudioIslenska objectAtIndex:sender.tag], DB_TABLEFIELD_AUDIO_ISLENSKA,
                            //                                                                                                         nil]];
                        }else if([_strLanguageCode isEqualToString:@"txt_German"]) {
                            
                            NSArray *arrGetGRAudio = [userDefaults objectForKey:@"GRLocalAudio"];
                            [arrAudioGerman removeAllObjects];
                            [arrAudioGerman addObjectsFromArray: arrGetGRAudio];
                            
                            [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                                                         @"1", DB_TABLEFIELD_DOWN_FLAG_GE,
                                                                                                         nil]];
                            
                            //                            [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys:
                            //                                                                                                         [arrAudioGerman objectAtIndex:sender.tag], DB_TABLEFIELD_AUDIO_GERMAN,
                            //                                                                                                         nil]];
                            
                        }else if([_strLanguageCode isEqualToString:@"txt_French"]) {
                            
                            NSArray *arrGetFrAudio = [userDefaults objectForKey:@"FRLocalAudio"];
                            [arrAudioFrench removeAllObjects];
                            [arrAudioFrench addObjectsFromArray: arrGetFrAudio];
                            
                            [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                                                         @"1", DB_TABLEFIELD_DOWN_FLAG_FR,
                                                                                                         nil]];
                            
                            //                            [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys:
                            //                                                                                                         [arrAudioFrench objectAtIndex:sender.tag], DB_TABLEFIELD_AUDIO_FRENCH,
                            //                                                                                                         nil]];
                        }else if([_strLanguageCode isEqualToString:@"txt_Spanish"]) {
                            
                            NSArray *arrGetEsAudio = [userDefaults objectForKey:@"ESLocalAudio"];
                            [arrAudioSpanish removeAllObjects];
                            [arrAudioSpanish addObjectsFromArray: arrGetEsAudio];
                            
                            [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                                                         @"1", DB_TABLEFIELD_DOWN_FLAG_ES,
                                                                                                         nil]];
                            
                            //                            [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys:
                            //                                                                                                         [arrAudioSpanish objectAtIndex:sender.tag], DB_TABLEFIELD_AUDIO_SPANISH,
                            //                                                                                                         nil]];
                            
                        }else if([_strLanguageCode isEqualToString:@"txt_Chinese"]) {
                            
                            NSArray *arrGetChAudio = [userDefaults objectForKey:@"CHLocalAudio"];
                            [arrAudioChinese removeAllObjects];
                            [arrAudioChinese addObjectsFromArray: arrGetChAudio];
                            
                            [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                                                         @"1", DB_TABLEFIELD_DOWN_FLAG_CH,
                                                                                                         nil]];
                            
                            //                            [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys:
                            //                                                                                                         [arrAudioChinese objectAtIndex:sender.tag], DB_TABLEFIELD_AUDIO_CHINESE,
                            //                                                                                                         nil]];
                            
                        }else if([_strLanguageCode isEqualToString:@"txt_Danish"]) {
                            
                            NSArray *arrGetDaAudio = [userDefaults objectForKey:@"DALocalAudio"];
                            [arrAudioDanish removeAllObjects];
                            [arrAudioDanish addObjectsFromArray: arrGetDaAudio];
                            
                            [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                                                         @"1", DB_TABLEFIELD_DOWN_FLAG_DA,
                                                                                                         nil]];
                            //                            [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys:
                            //                                                                                                         [arrAudioDanish objectAtIndex:sender.tag], DB_TABLEFIELD_AUDIO_DANISH,
                            //                                                                                                         nil]];
                        }
                        
                        // ============ All Image store in db ==========
                        //                        for (int k=0; k < arrLocalImg.count; k++) {
                        //                            [self.dbHelper inserDataIntoTable:DB_TABLENAME_IMAGEMASTER tableData:[NSDictionary dictionaryWithObjectsAndKeys:[[arrPlacesData valueForKey:@"place_id"] objectAtIndex:i], DB_TABLEFIELD_PLACEID, [arrLocalImg objectAtIndex:k], DB_TABLEFIELD_PLACEIMAGE_NAME, nil]];
                        //                        }
                        
                        [arrPlacesDetailImgs removeAllObjects];
                        NSArray *arrImages1 = [[arrPlacesData valueForKey:@"place_images"] objectAtIndex:i];
                        for (NSDictionary *dicPlaces in arrImages1) {
                            [arrPlacesDetailImgs addObject:[dicPlaces valueForKey:@"place_img_name"]];
                        }
                        NSLog(@"arrPlacesDetailImgs %@:",arrPlacesDetailImgs);
                        
                        for (int n=0; n<arrPlacesDetailImgs.count; n++) {
                            
                            //            for (NSArray *arr in arrPlacesDetailImgs) {
                            
                            NSURL *imageURL = [NSURL URLWithString:[arrPlacesDetailImgs objectAtIndex:n]];
                            
                            
                            pngData = [NSData dataWithContentsOfURL:imageURL];
                            
                            //            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                            //            NSString *documentsPath = [paths objectAtIndex:0]; //Get the docs directory
                            
//                            NSString *anImageName = [NSString stringWithFormat:@"%ld.%ldplaceDetail.png", (long)sender.tag,(long)anIndex++];
                            NSString *anImageName = [NSString stringWithFormat:@"all.%ldplaceDetail.png",(long)anIndex++];
                            NSString *anPlacesDetailImagePath = [NSString stringWithFormat:@"%@/%@", documentsPath, anImageName];
                            
                            [pngData writeToFile:anPlacesDetailImagePath atomically:YES]; //Write the file
                            [arrPlacesDetailImgsPath addObject:anPlacesDetailImagePath];
                        }
                        //        }
                        NSLog(@"arrPlacesDetailImgsPath %@:",arrPlacesDetailImgsPath);
                        
                        for (int k=0; k < arrPlacesDetailImgsPath.count; k++) {
                            [self.dbHelper inserDataIntoTable:DB_TABLENAME_IMAGEMASTER tableData:[NSDictionary dictionaryWithObjectsAndKeys:[[arrPlacesData valueForKey:@"place_id"] objectAtIndex:i], DB_TABLEFIELD_PLACEID, [arrPlacesDetailImgsPath objectAtIndex:k], DB_TABLEFIELD_PLACEIMAGE_NAME, nil]];
                        }
                        [arrPlacesDetailImgsPath removeAllObjects];
                        
                    }
                }
            }
            
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                
                [self stopIndicator];
                [_btnDownloadAll setImage:[UIImage imageNamed:@"delete"] forState:UIControlStateNormal];
                
                
            }];
        }
        else
        {
            // Deleting all data
            NSLog(@"----Delete----");
            
            //            [userDefaults setObject:@"0" forKey:PREFERENCE_STRDOWN];
            if ([_strLanguageCode isEqualToString:@"txt_eng"]) {
                [userDefaults setObject:@"0" forKey:PREFERENCE_DOWN_FLAG_EN];
            }else if([_strLanguageCode isEqualToString:@"txt_islenska"]) {
                [userDefaults setObject:@"0" forKey:PREFERENCE_DOWN_FLAG_IS];
            }else if([_strLanguageCode isEqualToString:@"txt_German"]) {
                [userDefaults setObject:@"0" forKey:PREFERENCE_DOWN_FLAG_GR];
            }else if([_strLanguageCode isEqualToString:@"txt_French"]) {
                [userDefaults setObject:@"0" forKey:PREFERENCE_DOWN_FLAG_FR];
            }else if([_strLanguageCode isEqualToString:@"txt_Spanish"]) {
                [userDefaults setObject:@"0" forKey:PREFERENCE_DOWN_FLAG_ES];
            }else if([_strLanguageCode isEqualToString:@"txt_Chinese"]) {
                [userDefaults setObject:@"0" forKey:PREFERENCE_DOWN_FLAG_CH];
            }else if([_strLanguageCode isEqualToString:@"txt_Danish"]) {
                [userDefaults setObject:@"0" forKey:PREFERENCE_DOWN_FLAG_DA];
            }
            
            for(int i=0; i<arrPlacesData.count; i++) {
                
                strPlaceId = [[arrPlacesData valueForKey:@"place_id"] objectAtIndex:i];
                
                NSArray *arrResult = [self.dbHelper checkIfDataAlreadyExist:DB_TABLENAME_PLACEMASTER place_id:strPlaceId colomnNM:@"place_id"];
                
                if (arrResult.count > 0) {
                    int columnNum = 0;
                    if ([_strLanguageCode isEqualToString:@"txt_eng"]) {
                        columnNum = 38;
                    }else if([_strLanguageCode isEqualToString:@"txt_islenska"]) {
                        columnNum = 39;
                    }else if([_strLanguageCode isEqualToString:@"txt_German"]) {
                        columnNum = 40;
                    }else if([_strLanguageCode isEqualToString:@"txt_French"]) {
                        columnNum = 41;
                    }else if([_strLanguageCode isEqualToString:@"txt_Spanish"]) {
                        columnNum = 42;
                    }else if([_strLanguageCode isEqualToString:@"txt_Chinese"]) {
                        columnNum = 43;
                    }else if([_strLanguageCode isEqualToString:@"txt_Danish"]) {
                        columnNum = 44;
                    }
                    
                    NSString *strDownFlag = [NSString stringWithFormat:@"%@", [[arrResult objectAtIndex:0] objectAtIndex:columnNum]];
                    
                    //                    NSString *strDownFlag = [NSString stringWithFormat:@"%@", [[arrResult objectAtIndex:0] objectAtIndex:14]];
                    
                    if ([strDownFlag isEqualToString:@"1"]) {
                        
                        if ([_strLanguageCode isEqualToString:@"txt_eng"]) {
                            
                            [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                                                         @"0",DB_TABLEFIELD_DOWN_FLAG_EN,
                                                                                                         nil]];
                            [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                                                         @"", DB_TABLEFIELD_AUDIO,
                                                                                                         nil]];
                        }else if([_strLanguageCode isEqualToString:@"txt_islenska"]) {
                            
                            [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                                                         @"0",DB_TABLEFIELD_DOWN_FLAG_IS,
                                                                                                         nil]];
                            [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                                                         @"", DB_TABLEFIELD_AUDIO_ISLENSKA,
                                                                                                         nil]];
                        }else if([_strLanguageCode isEqualToString:@"txt_German"]) {
                            
                            [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                                                         @"0",DB_TABLEFIELD_DOWN_FLAG_GE,
                                                                                                         nil]];
                            [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                                                         @"", DB_TABLEFIELD_AUDIO_GERMAN,
                                                                                                         nil]];
                        }else if([_strLanguageCode isEqualToString:@"txt_French"]) {
                            
                            [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                                                         @"0",DB_TABLEFIELD_DOWN_FLAG_FR,
                                                                                                         nil]];
                            [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                                                         @"", DB_TABLEFIELD_AUDIO_FRENCH,
                                                                                                         nil]];
                        }else if([_strLanguageCode isEqualToString:@"txt_Spanish"]) {
                            
                            [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                                                         @"0",DB_TABLEFIELD_DOWN_FLAG_ES,
                                                                                                         nil]];
                            [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                                                         @"", DB_TABLEFIELD_AUDIO_SPANISH,
                                                                                                         nil]];
                        }else if([_strLanguageCode isEqualToString:@"txt_Chinese"]) {
                            
                            [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                                                         @"0",DB_TABLEFIELD_DOWN_FLAG_CH,
                                                                                                         nil]];
                            [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                                                         @"", DB_TABLEFIELD_AUDIO_CHINESE,
                                                                                                         nil]];
                        }else if([_strLanguageCode isEqualToString:@"txt_Danish"]) {
                            
                            [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                                                         @"0",DB_TABLEFIELD_DOWN_FLAG_DA,
                                                                                                         nil]];
                            [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                                                         @"", DB_TABLEFIELD_AUDIO_DANISH,
                                                                                                         nil]];
                        }
                        
                        [self.dbHelper delete:DB_TABLENAME_IMAGEMASTER placeId:strPlaceId colomnNM:DB_TABLEFIELD_PLACEID];
                    }
                }
            }
            
            [_btnDownloadAll setImage:[UIImage imageNamed:@"download_white"] forState:UIControlStateNormal];
        }
        
        [_tblPlaceList reloadData];
        
    }else {
        
        //        strDown = [userDefaults valueForKey:PREFERENCE_STRDOWN];
        
        if ([_strLanguageCode isEqualToString:@"txt_eng"]) {
            strDown = [userDefaults valueForKey:PREFERENCE_DOWN_FLAG_EN];
            
        }else if([_strLanguageCode isEqualToString:@"txt_islenska"]) {
            strDown = [userDefaults valueForKey:PREFERENCE_DOWN_FLAG_IS];
            
        }else if([_strLanguageCode isEqualToString:@"txt_German"]) {
            strDown = [userDefaults valueForKey:PREFERENCE_DOWN_FLAG_GR];
            
        }else if([_strLanguageCode isEqualToString:@"txt_French"]) {
            strDown = [userDefaults valueForKey:PREFERENCE_DOWN_FLAG_FR];
            
        }else if([_strLanguageCode isEqualToString:@"txt_Spanish"]) {
            strDown = [userDefaults valueForKey:PREFERENCE_DOWN_FLAG_ES];
            
        }else if([_strLanguageCode isEqualToString:@"txt_Chinese"]) {
            strDown = [userDefaults valueForKey:PREFERENCE_DOWN_FLAG_CH];
            
        }else if([_strLanguageCode isEqualToString:@"txt_Danish"]) {
            strDown = [userDefaults valueForKey:PREFERENCE_DOWN_FLAG_DA];
        }
        NSLog(@"String down %@",strDown);
        
        
        if ([strDown isEqualToString:@"0"]) {
            
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:[Diclabels valueForKey:@"txt_thingvellir"] message:[Diclabels valueForKey:@"MSG_INTERNET_UNAVAILABLE"] preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *ok = [UIAlertAction actionWithTitle:[Diclabels valueForKey:@"OK"] style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) { }];
            
            [alert addAction:ok];
            [self presentViewController:alert animated:YES completion:nil];
            
        }else {
            
            // Deleting all data
            NSLog(@"----Delete----");
            if ([_strLanguageCode isEqualToString:@"txt_eng"]) {
                [userDefaults setObject:@"0" forKey:PREFERENCE_DOWN_FLAG_EN];
            }else if([_strLanguageCode isEqualToString:@"txt_islenska"]) {
                [userDefaults setObject:@"0" forKey:PREFERENCE_DOWN_FLAG_IS];
            }else if([_strLanguageCode isEqualToString:@"txt_German"]) {
                [userDefaults setObject:@"0" forKey:PREFERENCE_DOWN_FLAG_GR];
            }else if([_strLanguageCode isEqualToString:@"txt_French"]) {
                [userDefaults setObject:@"0" forKey:PREFERENCE_DOWN_FLAG_FR];
            }else if([_strLanguageCode isEqualToString:@"txt_Spanish"]) {
                [userDefaults setObject:@"0" forKey:PREFERENCE_DOWN_FLAG_ES];
            }else if([_strLanguageCode isEqualToString:@"txt_Chinese"]) {
                [userDefaults setObject:@"0" forKey:PREFERENCE_DOWN_FLAG_CH];
            }else if([_strLanguageCode isEqualToString:@"txt_Danish"]) {
                [userDefaults setObject:@"0" forKey:PREFERENCE_DOWN_FLAG_DA];
            }
            //            [userDefaults setObject:@"0" forKey:PREFERENCE_STRDOWN];
            
            for(int i=0; i<arrPlacesData.count; i++) {
                
                strPlaceId = [[arrPlacesData valueForKey:@"place_id"] objectAtIndex:i];
                
                NSArray *arrResult = [self.dbHelper checkIfDataAlreadyExist:DB_TABLENAME_PLACEMASTER place_id:strPlaceId colomnNM:@"place_id"];
                
                if (arrResult.count > 0) {
                    
                    int columnNum = 0;
                    if ([_strLanguageCode isEqualToString:@"txt_eng"]) {
                        columnNum = 38;
                    }else if([_strLanguageCode isEqualToString:@"txt_islenska"]) {
                        columnNum = 39;
                        
                    }else if([_strLanguageCode isEqualToString:@"txt_German"]) {
                        columnNum = 40;
                        
                    }else if([_strLanguageCode isEqualToString:@"txt_French"]) {
                        columnNum = 41;
                        
                    }else if([_strLanguageCode isEqualToString:@"txt_Spanish"]) {
                        columnNum = 42;
                        
                    }else if([_strLanguageCode isEqualToString:@"txt_Chinese"]) {
                        columnNum = 43;
                        
                    }else if([_strLanguageCode isEqualToString:@"txt_Danish"]) {
                        columnNum = 44;
                    }
                    
                    NSString *strDownFlag = [NSString stringWithFormat:@"%@", [[arrResult objectAtIndex:0] objectAtIndex:columnNum]];
                    
                    //                    NSString *strDownFlag = [NSString stringWithFormat:@"%@", [[arrResult objectAtIndex:0] objectAtIndex:14]];
                    
                    if ([strDownFlag isEqualToString:@"1"]) {
                        
                        if ([_strLanguageCode isEqualToString:@"txt_eng"]) {
                            
                            [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                                                         @"0",DB_TABLEFIELD_DOWN_FLAG_EN,
                                                                                                         nil]];
                            [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                                                         @"", DB_TABLEFIELD_AUDIO,
                                                                                                         nil]];
                        }else if([_strLanguageCode isEqualToString:@"txt_islenska"]) {
                            
                            [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                                                         @"0",DB_TABLEFIELD_DOWN_FLAG_IS,
                                                                                                         nil]];
                            [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                                                         @"", DB_TABLEFIELD_AUDIO_ISLENSKA,
                                                                                                         nil]];
                        }else if([_strLanguageCode isEqualToString:@"txt_German"]) {
                            
                            [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                                                         @"0",DB_TABLEFIELD_DOWN_FLAG_GE,
                                                                                                         nil]];
                            [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                                                         @"", DB_TABLEFIELD_AUDIO_GERMAN,
                                                                                                         nil]];
                        }else if([_strLanguageCode isEqualToString:@"txt_French"]) {
                            
                            [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                                                         @"0",DB_TABLEFIELD_DOWN_FLAG_FR,
                                                                                                         nil]];
                            [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                                                         @"", DB_TABLEFIELD_AUDIO_FRENCH,
                                                                                                         nil]];
                        }else if([_strLanguageCode isEqualToString:@"txt_Spanish"]) {
                            
                            [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                                                         @"0",DB_TABLEFIELD_DOWN_FLAG_ES,
                                                                                                         nil]];
                            [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                                                         @"", DB_TABLEFIELD_AUDIO_SPANISH,
                                                                                                         nil]];
                        }else if([_strLanguageCode isEqualToString:@"txt_Chinese"]) {
                            
                            [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                                                         @"0",DB_TABLEFIELD_DOWN_FLAG_CH,
                                                                                                         nil]];
                            [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                                                         @"", DB_TABLEFIELD_AUDIO_CHINESE,
                                                                                                         nil]];
                        }else if([_strLanguageCode isEqualToString:@"txt_Danish"]) {
                            
                            [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                                                         @"0",DB_TABLEFIELD_DOWN_FLAG_DA,
                                                                                                         nil]];
                            [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                                                         @"", DB_TABLEFIELD_AUDIO_DANISH,
                                                                                                         nil]];
                        }
                        
                        [self.dbHelper delete:DB_TABLENAME_IMAGEMASTER placeId:strPlaceId colomnNM:DB_TABLEFIELD_PLACEID];
                    }
                }
            }
            
            [_btnDownloadAll setImage:[UIImage imageNamed:@"download_white"] forState:UIControlStateNormal];
            
        }
        
        [_tblPlaceList reloadData];
        
    }
    
    [self stopIndicator];

    UIAlertController *alert = [UIAlertController alertControllerWithTitle:[Diclabels valueForKey:@"txt_thingvellir"] message:[Diclabels valueForKey:@"MSG_SUCCESS"] preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *ok = [UIAlertAction actionWithTitle:[Diclabels valueForKey:@"OK"] style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {}];
    
    [alert addAction:ok];
    [self presentViewController:alert animated:YES completion:nil];
}

- (IBAction)btnFilterTapped:(UIButton *)sender {
    NSArray *arrGetMain = [userDefaults objectForKey:@"MainArrayData"];
    [arrPlacesData removeAllObjects];
    [arrPlacesData addObjectsFromArray: arrGetMain];
   
    if (_filterVw.isHidden) {
        _filterVw.hidden = NO;
        [self openWithAnimation:_filterVw];
    }else{
        _filterVw.hidden = YES;
        [self closeWithAnimation:_filterVw];
    }
}

- (IBAction)hideView:(UIControl *)sender {
    _filterVw.hidden = YES;
    [self closeWithAnimation:_filterVw];
}

- (IBAction)btnLikeTapped:(UIButton *)sender {

    PlaceCell *cell = (PlaceCell *)[_tblPlaceList dequeueReusableCellWithIdentifier:@"PlaceCell"];
 
    NSMutableDictionary *mutDictParam = [[NSMutableDictionary alloc] init];
    
    mutDictParam = [arrPlacesData objectAtIndex:sender.tag];
    NSMutableDictionary *dictmutTest = [mutDictParam mutableCopy];
//    NSString *strForStatus = [[arrPlacesData valueForKey:@"isLike"] objectAtIndex:sender.tag];

    strPlaceId = [[arrPlacesData valueForKey:@"place_id"] objectAtIndex:sender.tag];
    NSLog(@"%@", strPlaceId);
 
    NSArray *arrResult = [self.dbHelper checkIfDataAlreadyExist:DB_TABLENAME_PLACEMASTER place_id:strPlaceId colomnNM:@"place_id"];
    
    if (arrResult.count > 0) {
        
        NSString *strForStatus = [NSString stringWithFormat:@"%@", [[arrResult objectAtIndex:0] objectAtIndex:9]];
        
        if ([strForStatus isEqualToString:@"0"]) {
        
            [dictmutTest setValue:@"1" forKey:@"isLike"];
            cell.btnLike.selected = YES;
            cell.likeImgVw.image = [UIImage imageNamed:@"favourite_red"];

            [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                                         @"1",DB_TABLEFIELD_LIKE,
                                                                                         nil]];
        }else {
        
            [dictmutTest setValue:@"0" forKey:@"isLike"];
            cell.btnLike.selected = NO;
            cell.likeImgVw.image = [UIImage imageNamed:@"favourite_gray"];
            [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                                         @"0", DB_TABLEFIELD_LIKE,
                                                                                         nil]];
        }
    }
    
    [_tblPlaceList reloadData];
    
}

- (IBAction)btnFavTapped:(UIButton *)sender {
    selected = !selected;
    
    if (_btnFav.isSelected && _btnVisited.isSelected) {
        _btnFav.selected = NO;
        [_btnFav setImage:[UIImage imageNamed:@"radio_unchecked"] forState:UIControlStateNormal];
        
        _btnVisited.selected = YES;
        [_btnVisited setImage:[UIImage imageNamed:@"radio_checked"] forState:UIControlStateNormal];
    
    }else {
        _btnFav.selected = YES;
        [_btnFav setImage:[UIImage imageNamed:@"radio_checked"] forState:UIControlStateNormal];
        
        _btnVisited.selected = NO;
        [_btnVisited setImage:[UIImage imageNamed:@"radio_unchecked"] forState:UIControlStateNormal];
    }
    
}

- (IBAction)btnVisitedTapped:(UIButton *)sender {
    selected = !selected;
    
    if (_btnVisited.isSelected && _btnFav.isSelected) {
      
        _btnVisited.selected = NO;
        [_btnVisited setImage:[UIImage imageNamed:@"radio_unchecked"] forState:UIControlStateNormal];
        
        _btnFav.selected = YES;
        [_btnFav setImage:[UIImage imageNamed:@"radio_checked"] forState:UIControlStateNormal];
    
    }else {
        _btnVisited.selected = YES;
        [_btnVisited setImage:[UIImage imageNamed:@"radio_checked"] forState:UIControlStateNormal];
   
        _btnFav.selected = NO;
        [_btnFav setImage:[UIImage imageNamed:@"radio_unchecked"] forState:UIControlStateNormal];
    }
}

- (IBAction)btnApplyTapped:(UIButton *)sender {
    
    if (_btnFav.isSelected == NO && _btnVisited.isSelected == NO) {
        NSLog(@"plz select any option");
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:[Diclabels valueForKey:@"txt_thingvellir"] message:[Diclabels valueForKey:@"MSG_PLEASE_SELECT_FILTER_OPTION"] preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *ok = [UIAlertAction actionWithTitle:[Diclabels valueForKey:@"OK"] style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            _lblNoData.hidden = YES;

        }];
        
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
        
    }
    
    [_filterVw setHidden:YES];
    [arrLikeVisitedTemp removeAllObjects];

    if (_btnFav.isSelected) {
        for (int i = 0; i < arrPlacesData.count; i++) {
            NSString *placeId = [[arrPlacesData objectAtIndex:i]valueForKey:@"place_id"];
            NSArray *arrCheck = [self.dbHelper loadFavTableData:DB_TABLENAME_PLACEMASTER placeId:placeId];
            if (arrCheck.count > 0) {
                [arrLikeVisitedTemp addObject:[arrPlacesData objectAtIndex:i]];
            }else {
                NSLog(@"not added");
            }
        }
        [arrPlacesData removeAllObjects];
        [arrPlacesData addObjectsFromArray:arrLikeVisitedTemp];
        
    }else if (_btnVisited.isSelected){
       
        for (int i = 0; i < arrPlacesData.count; i++) {
            NSString *placeId = [[arrPlacesData objectAtIndex:i]valueForKey:@"place_id"];
            NSArray *arrCheck = [self.dbHelper loadVisitedTableData:DB_TABLENAME_PLACEMASTER placeId:placeId];
            if (arrCheck.count > 0) {
                [arrLikeVisitedTemp addObject:[arrPlacesData objectAtIndex:i]];
            }else{
                NSLog(@"not added");
            }
        }
        [arrPlacesData removeAllObjects];
        [arrPlacesData addObjectsFromArray:arrLikeVisitedTemp];

    }
//    NSLog(@"arr :: %@", arrLikeVisitedTemp);
    
    if (arrLikeVisitedTemp.count == 0) {
        _lblNoData.hidden = NO;
        _lblNoData.text = [Diclabels valueForKey:@"txt_no_data"];//@"No Data";
    }else {
        _lblNoData.hidden = YES;
    }
    
    [_tblPlaceList reloadData];
    [self closeWithAnimation:_filterVw];
}

- (IBAction)btnResetTapped:(UIButton *)sender {
    
    [_filterVw setHidden:YES];
    _lblNoData.hidden = YES;

    NSArray * arrGetMain = [userDefaults objectForKey:@"MainArrayData"];
    [arrPlacesData removeAllObjects];
    [arrPlacesData addObjectsFromArray: arrGetMain];
    [_tblPlaceList reloadData];
    
    _btnFav.selected = NO;
    [_btnFav setImage:[UIImage imageNamed:@"radio_unchecked"] forState:UIControlStateNormal];
    
    _btnVisited.selected = NO;
    [_btnVisited setImage:[UIImage imageNamed:@"radio_unchecked"] forState:UIControlStateNormal];

}


//- (void)SaveAndDeleteTapped:(UIButton *)sender {
//    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//    [NSTimer scheduledTimerWithTimeInterval:1.0f target:self selector:@selector(btnSaveAndDeleteTapped:) userInfo:nil repeats:NO];
//}

- (void)btnSaveAndDeleteTapped:(UIButton *)sender {

    NSIndexPath *indexPathValue = [NSIndexPath indexPathForRow:sender.tag inSection:0];
    PlaceCell *cell = (PlaceCell *)[_tblPlaceList cellForRowAtIndexPath:indexPathValue];
//    //[_tblPlaceList dequeueReusableCellWithIdentifier:@"PlaceCell" forIndexPath:indexPathValue];

//    UIActivityIndicatorView * activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
//
//    cell.accessoryView = activityIndicator;
//
//    [_tblPlaceList reloadData];
    
    NSMutableDictionary *mutDictParam = [[NSMutableDictionary alloc] init];
    
    mutDictParam = [arrPlacesData objectAtIndex:sender.tag];
    NSMutableDictionary *dictmutTest = [mutDictParam mutableCopy];
    //    NSString *strForStatus = [[arrPlacesData valueForKey:@"isLike"] objectAtIndex:sender.tag];
    
    strPlaceId = [[arrPlacesData valueForKey:@"place_id"] objectAtIndex:sender.tag];
    NSLog(@"%@", strPlaceId);
    
    NSArray *arrResult = [self.dbHelper checkIfDataAlreadyExist:DB_TABLENAME_PLACEMASTER place_id:strPlaceId colomnNM:@"place_id"];
//    NSLog(@"arrResult is %@",arrResult);
 
    if ([utility isInternetConnected]) {
     
        if (arrResult.count > 0) {
            
//            NSString *strTemp = [NSString stringWithFormat:@"down_%@+", _strLanguageCode];
            
            int columnNum = 0;
            if ([_strLanguageCode isEqualToString:@"txt_eng"]) {
                columnNum = 38;
            }else if([_strLanguageCode isEqualToString:@"txt_islenska"]) {
                columnNum = 39;
            }else if([_strLanguageCode isEqualToString:@"txt_German"]) {
                columnNum = 40;
            }else if([_strLanguageCode isEqualToString:@"txt_French"]) {
                columnNum = 41;
            }else if([_strLanguageCode isEqualToString:@"txt_Spanish"]) {
                columnNum = 42;
            }else if([_strLanguageCode isEqualToString:@"txt_Chinese"]) {
                columnNum = 43;
            }else if([_strLanguageCode isEqualToString:@"txt_Danish"]) {
                columnNum = 44;
            }
            
            NSString *strForStatus = [NSString stringWithFormat:@"%@", [[arrResult objectAtIndex:0] objectAtIndex:columnNum]];
           
            if ([strForStatus isEqualToString:@"0"]) {
                
//                [cell.activityIndicator startAnimating];

//                [dictmutTest setValue:@"1" forKey:@"down_flag"];
                cell.imgSaveDel.image = [UIImage imageNamed:@"delete_place"];
                
                if ([_strLanguageCode isEqualToString:@"txt_eng"]) {
                    
//                    [MBProgressHUD showHUDAddedTo:self.view animated:YES];

                    [dictmutTest setValue:@"1" forKey:@"down_txt_eng"];
  
                    NSString *url = [NSString stringWithFormat:@"%@",[[arrPlacesData valueForKey:@"place_audio"] objectAtIndex:sender.tag]];
                    
//                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    
                    NSURL *audioURL = [NSURL URLWithString:url];
                    NSString *strAudioUrl = [NSString stringWithFormat:@"%@",audioURL];
                    
                    if ([utility isStringEmpty:strAudioUrl]) {
                        NSLog(@"No Audio");
                    }else {
                        pngData = [NSData dataWithContentsOfURL:audioURL];
                        
                        NSString *anImageName = [NSString stringWithFormat:@"%@ENAudio.mp3",[formatter stringFromDate:[NSDate date]]];
                        NSString *anAudioPath = [NSString stringWithFormat:@"%@/%@", documentsPath, anImageName];
                        //            NSLog(@"anAudioPath File path is %@ ::",anAudioPath);
                        
                        [pngData writeToFile:anAudioPath atomically:YES]; //Write the file
                     
                        
                        [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys:anAudioPath, DB_TABLEFIELD_AUDIO, nil]];
                        
                    }
                    
                    
                    [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys: @"1", DB_TABLEFIELD_DOWN_FLAG_EN, nil]];
//                        dispatch_async(dispatch_get_main_queue(), ^(void) {
//                            [MBProgressHUD hideHUDForView:self.view animated:YES];
                    
//                        });
//                    });

//                    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
//                        [activityIndicator stopAnimating];
//                        activityIndicator.hidden = YES;
//
//                    }];

               
                }else if([_strLanguageCode isEqualToString:@"txt_islenska"]) {
                    
//                    [MBProgressHUD showHUDAddedTo:self.view animated:YES];

                    [dictmutTest setValue:@"1" forKey:@"down_txt_islenska"];

                    NSString *url = [NSString stringWithFormat:@"%@",[[arrPlacesData valueForKey:@"place_audio_islenska"] objectAtIndex:sender.tag]];
                    
//                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    
                        NSURL *audioURL = [NSURL URLWithString:url];
                        
//                        dispatch_async(dispatch_get_main_queue(), ^(void) {
//                            [MBProgressHUD hideHUDForView:self.view animated:YES];
                    
                            pngData = [NSData dataWithContentsOfURL:audioURL];
                            
                            NSString *anImageName = [NSString stringWithFormat:@"%@ISAudio.mp3",[formatter stringFromDate:[NSDate date]]];
                            NSString *anAudioPath = [NSString stringWithFormat:@"%@/%@", documentsPath, anImageName];
                            //            NSLog(@"anAudioPath File path is %@ ::",anAudioPath);
                            
                            [pngData writeToFile:anAudioPath atomically:YES]; //Write the file
                            
                            [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                                                         @"1", DB_TABLEFIELD_DOWN_FLAG_IS,
                                                                                                         nil]];
                 
                            [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys:anAudioPath, DB_TABLEFIELD_AUDIO_ISLENSKA, nil]];
                            
//                        });
//                    });
                   
                    
                }else if([_strLanguageCode isEqualToString:@"txt_German"]) {
                    
//                    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                    
                    [dictmutTest setValue:@"1" forKey:@"down_txt_german"];

                    NSString *url = [NSString stringWithFormat:@"%@",[[arrPlacesData valueForKey:@"place_audio_german"] objectAtIndex:sender.tag]];
                    
//                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    
                        NSURL *audioURL = [NSURL URLWithString:url];
                        
//                        dispatch_async(dispatch_get_main_queue(), ^(void) {
//                            [MBProgressHUD hideHUDForView:self.view animated:YES];
                    
                            pngData = [NSData dataWithContentsOfURL:audioURL];
                            
                            NSString *anImageName = [NSString stringWithFormat:@"%@GRAudio.mp3",[formatter stringFromDate:[NSDate date]]];
                            NSString *anAudioPath = [NSString stringWithFormat:@"%@/%@", documentsPath, anImageName];
                            //            NSLog(@"anAudioPath File path is %@ ::",anAudioPath);
                            
                            [pngData writeToFile:anAudioPath atomically:YES]; //Write the file
                            
                            [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                                                         @"1", DB_TABLEFIELD_DOWN_FLAG_GE,
                                                                                                         nil]];
                            
                            [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys:anAudioPath, DB_TABLEFIELD_AUDIO_GERMAN, nil]];
                            
//                        });
//                    });
                
                    
                }else if([_strLanguageCode isEqualToString:@"txt_French"]) {
//                    [MBProgressHUD showHUDAddedTo:self.view animated:YES];

                    [dictmutTest setValue:@"1" forKey:@"down_txt_french"];

                    NSString *url = [NSString stringWithFormat:@"%@",[[arrPlacesData valueForKey:@"place_audio_french"] objectAtIndex:sender.tag]];
                    
//                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    
                        NSURL *audioURL = [NSURL URLWithString:url];
                        
//                        dispatch_async(dispatch_get_main_queue(), ^(void) {
//
//                            [MBProgressHUD hideHUDForView:self.view animated:YES];
                    
                            pngData = [NSData dataWithContentsOfURL:audioURL];
                            
                            NSString *anImageName = [NSString stringWithFormat:@"%@FRAudio.mp3",[formatter stringFromDate:[NSDate date]]];
                            NSString *anAudioPath = [NSString stringWithFormat:@"%@/%@", documentsPath, anImageName];
                            //            NSLog(@"anAudioPath File path is %@ ::",anAudioPath);
                            
                            [pngData writeToFile:anAudioPath atomically:YES]; //Write the file
                            
                            [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                                                         @"1", DB_TABLEFIELD_DOWN_FLAG_FR,
                                                                                                         nil]];
                            
                            [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys:anAudioPath, DB_TABLEFIELD_AUDIO_FRENCH, nil]];
//                        });
//                    });
                    
                    
                }else if([_strLanguageCode isEqualToString:@"txt_Spanish"]) {

//                    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                    
                    [dictmutTest setValue:@"1" forKey:@"down_txt_spanish"];

                    NSString *url = [NSString stringWithFormat:@"%@",[[arrPlacesData valueForKey:@"place_audio_spanish"] objectAtIndex:sender.tag]];
                    
//                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    
                        NSURL *audioURL = [NSURL URLWithString:url];
                        
//                        dispatch_async(dispatch_get_main_queue(), ^(void) {
//
//                            [MBProgressHUD hideHUDForView:self.view animated:YES];
                    
                            pngData = [NSData dataWithContentsOfURL:audioURL];
                            
                            NSString *anImageName = [NSString stringWithFormat:@"%@ESAudio.mp3",[formatter stringFromDate:[NSDate date]]];
                            NSString *anAudioPath = [NSString stringWithFormat:@"%@/%@", documentsPath, anImageName];
                            //            NSLog(@"anAudioPath File path is %@ ::",anAudioPath);
                            
                            [pngData writeToFile:anAudioPath atomically:YES]; //Write the file
                            
                            [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                                                         @"1", DB_TABLEFIELD_DOWN_FLAG_ES,
                                                                                                         nil]];
                            
                            [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys:anAudioPath, DB_TABLEFIELD_AUDIO_SPANISH, nil]];
                    
//                        });
//                    });
                    
                    
                }else if([_strLanguageCode isEqualToString:@"txt_Chinese"]) {

//                    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                    
                    [dictmutTest setValue:@"1" forKey:@"down_txt_chinese"];

                    NSString *url = [NSString stringWithFormat:@"%@",[[arrPlacesData valueForKey:@"place_audio_chinese"] objectAtIndex:sender.tag]];
                    
//                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    
                        NSURL *audioURL = [NSURL URLWithString:url];
                        
//                        dispatch_async(dispatch_get_main_queue(), ^(void) {
//
//                            [MBProgressHUD hideHUDForView:self.view animated:YES];
                    
                            pngData = [NSData dataWithContentsOfURL:audioURL];
                            
                            NSString *anImageName = [NSString stringWithFormat:@"%@CHAudio.mp3",[formatter stringFromDate:[NSDate date]]];
                            NSString *anAudioPath = [NSString stringWithFormat:@"%@/%@", documentsPath, anImageName];
                            //            NSLog(@"anAudioPath File path is %@ ::",anAudioPath);
                            
                            [pngData writeToFile:anAudioPath atomically:YES]; //Write the file
                            
                            [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                                                         @"1", DB_TABLEFIELD_DOWN_FLAG_CH,
                                                                                                         nil]];
                            
                            [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys:anAudioPath, DB_TABLEFIELD_AUDIO_CHINESE, nil]];
                            
//                        });
//                    });
          
                    
                }else if([_strLanguageCode isEqualToString:@"txt_Danish"]) {

//                    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                    
                    [dictmutTest setValue:@"1" forKey:@"down_txt_danish"];

                    NSString *url = [NSString stringWithFormat:@"%@",[[arrPlacesData valueForKey:@"place_audio_danish"] objectAtIndex:sender.tag]];
                    
//                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    
                        NSURL *audioURL = [NSURL URLWithString:url];
                        
//                        dispatch_async(dispatch_get_main_queue(), ^(void) {
                    
//                            [MBProgressHUD hideHUDForView:self.view animated:YES];
                    
                            pngData = [NSData dataWithContentsOfURL:audioURL];
                            
                            NSString *anImageName = [NSString stringWithFormat:@"%@DAAudio.mp3",[formatter stringFromDate:[NSDate date]]];
                            NSString *anAudioPath = [NSString stringWithFormat:@"%@/%@", documentsPath, anImageName];
                            //            NSLog(@"anAudioPath File path is %@ ::",anAudioPath);
                            
                            [pngData writeToFile:anAudioPath atomically:YES]; //Write the file
                            
                            [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                                                         @"1", DB_TABLEFIELD_DOWN_FLAG_DA,
                                                                                                         nil]];
                            [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys:anAudioPath, DB_TABLEFIELD_AUDIO_DANISH, nil]];
                            
//                        });
//                    });
                    
                }
                
//                [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys:
//                                                                                             @"1", DB_TABLEFIELD_DOWNLOAD_FLAG,
//                                                                                             nil]];
                
                NSInteger anIndex = 0;

                [arrPlacesDetailImgs removeAllObjects];
                NSArray *arrImages1 = [[arrPlacesData valueForKey:@"place_images"] objectAtIndex:sender.tag];
                for (NSDictionary *dicPlaces in arrImages1) {
                    [arrPlacesDetailImgs addObject:[dicPlaces valueForKey:@"place_img_name"]];
                }
                NSLog(@"arrPlacesDetailImgs %@:",arrPlacesDetailImgs);
                
                for (int i=0; i<arrPlacesDetailImgs.count; i++) {
                    
                    //            for (NSArray *arr in arrPlacesDetailImgs) {
                    
                    NSURL *imageURL = [NSURL URLWithString:[arrPlacesDetailImgs objectAtIndex:i]];
                    
                    
                    pngData = [NSData dataWithContentsOfURL:imageURL];
                    
                    //            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                    //            NSString *documentsPath = [paths objectAtIndex:0]; //Get the docs directory
                    
                    NSString *anImageName = [NSString stringWithFormat:@"%ld.%ldplaceDetail.png", (long)sender.tag,(long)anIndex++];
                    NSString *anPlacesDetailImagePath = [NSString stringWithFormat:@"%@/%@", documentsPath, anImageName];
                    
                    [pngData writeToFile:anPlacesDetailImagePath atomically:YES]; //Write the file
                    [arrPlacesDetailImgsPath addObject:anPlacesDetailImagePath];
                }
                //        }
                NSLog(@"arrPlacesDetailImgsPath %@:",arrPlacesDetailImgsPath);
                
                for (int k=0; k < arrPlacesDetailImgsPath.count; k++) {
                    [self.dbHelper inserDataIntoTable:DB_TABLENAME_IMAGEMASTER tableData:[NSDictionary dictionaryWithObjectsAndKeys:[[arrPlacesData valueForKey:@"place_id"] objectAtIndex:sender.tag], DB_TABLEFIELD_PLACEID, [arrPlacesDetailImgsPath objectAtIndex:k], DB_TABLEFIELD_PLACEIMAGE_NAME, nil]];
                }
                [arrPlacesDetailImgsPath removeAllObjects];
              
//                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
//                    [activityIndicator stopAnimating];
//                    activityIndicator.hidden = YES;
//
//                }];
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:[Diclabels valueForKey:@"txt_thingvellir"] message:[Diclabels valueForKey:@"MSG_SUCCESS"] preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *ok = [UIAlertAction actionWithTitle:[Diclabels valueForKey:@"OK"] style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {}];
                
                [alert addAction:ok];
                [self presentViewController:alert animated:YES completion:nil];
                
            }else {
                
//                [dictmutTest setValue:@"0" forKey:@"down_flag"];
                cell.imgSaveDel.image = [UIImage imageNamed:@"download_gray"];
                
                if ([_strLanguageCode isEqualToString:@"txt_eng"]) {
                    [dictmutTest setValue:@"0" forKey:@"down_txt_eng"];
                    
                    [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                                                 @"0",DB_TABLEFIELD_DOWN_FLAG_EN,
                                                                                                 nil]];
                    [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                                                 @"", DB_TABLEFIELD_AUDIO,
                                                                                                 nil]];
                    
                }else if([_strLanguageCode isEqualToString:@"txt_islenska"]) {
                    [dictmutTest setValue:@"0" forKey:@"down_txt_islenska"];
                    [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                                                 @"0",DB_TABLEFIELD_DOWN_FLAG_IS,
                                                                                                 nil]];
                    [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                                                 @"", DB_TABLEFIELD_AUDIO_ISLENSKA,
                                                                                                 nil]];
                }else if([_strLanguageCode isEqualToString:@"txt_German"]) {
                    [dictmutTest setValue:@"0" forKey:@"down_txt_german"];
                    [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                                                 @"0",DB_TABLEFIELD_DOWN_FLAG_GE,
                                                                                                 nil]];
                    [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                                                 @"", DB_TABLEFIELD_AUDIO_GERMAN,
                                                                                                 nil]];
                }else if([_strLanguageCode isEqualToString:@"txt_French"]) {
                    [dictmutTest setValue:@"0" forKey:@"down_txt_french"];
                    [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                                                 @"0",DB_TABLEFIELD_DOWN_FLAG_FR,
                                                                                                 nil]];
                    [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                                                 @"", DB_TABLEFIELD_AUDIO_FRENCH,
                                                                                                 nil]];
                }else if([_strLanguageCode isEqualToString:@"txt_Spanish"]) {
                    [dictmutTest setValue:@"0" forKey:@"down_txt_spanish"];
                    [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                                                 @"0",DB_TABLEFIELD_DOWN_FLAG_ES,
                                                                                                 nil]];
                    [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                                                 @"", DB_TABLEFIELD_AUDIO_SPANISH,
                                                                                                 nil]];
                }else if([_strLanguageCode isEqualToString:@"txt_Chinese"]) {
                    [dictmutTest setValue:@"0" forKey:@"down_txt_chinese"];
                    [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                                                 @"0",DB_TABLEFIELD_DOWN_FLAG_CH,
                                                                                                 nil]];
                    [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                                                 @"", DB_TABLEFIELD_AUDIO_CHINESE,
                                                                                                 nil]];
                }else if([_strLanguageCode isEqualToString:@"txt_Danish"]) {
                    [dictmutTest setValue:@"0" forKey:@"down_txt_danish"];
                    [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                                                 @"0",DB_TABLEFIELD_DOWN_FLAG_DA,
                                                                                                 nil]];
                    [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                                                 @"", DB_TABLEFIELD_AUDIO_DANISH,
                                                                                                 nil]];
                }
                
//                [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys:
//                                                                                             @"0",DB_TABLEFIELD_DOWNLOAD_FLAG,
//                                                                                             nil]];
              
                [self.dbHelper delete:DB_TABLENAME_IMAGEMASTER placeId:strPlaceId colomnNM:DB_TABLEFIELD_PLACEID];

                UIAlertController *alert = [UIAlertController alertControllerWithTitle:[Diclabels valueForKey:@"txt_thingvellir"] message:[Diclabels valueForKey:@"MSG_SUCCESS"] preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *ok = [UIAlertAction actionWithTitle:[Diclabels valueForKey:@"OK"] style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {}];
                
                [alert addAction:ok];
                [self presentViewController:alert animated:YES completion:nil];
            }
//            _btnDownloadAll.selected = YES;
            
//            [userDefaults setObject:@"0" forKey:PREFERENCE_STRDOWN];
            if ([_strLanguageCode isEqualToString:@"txt_eng"]) {
                [userDefaults setObject:@"0" forKey:PREFERENCE_DOWN_FLAG_EN];
            }else if([_strLanguageCode isEqualToString:@"txt_islenska"]) {
                [userDefaults setObject:@"0" forKey:PREFERENCE_DOWN_FLAG_IS];
            }else if([_strLanguageCode isEqualToString:@"txt_German"]) {
                [userDefaults setObject:@"0" forKey:PREFERENCE_DOWN_FLAG_GR];
            }else if([_strLanguageCode isEqualToString:@"txt_French"]) {
                [userDefaults setObject:@"0" forKey:PREFERENCE_DOWN_FLAG_FR];
            }else if([_strLanguageCode isEqualToString:@"txt_Spanish"]) {
                [userDefaults setObject:@"0" forKey:PREFERENCE_DOWN_FLAG_ES];
            }else if([_strLanguageCode isEqualToString:@"txt_Chinese"]) {
                [userDefaults setObject:@"0" forKey:PREFERENCE_DOWN_FLAG_CH];
            }else if([_strLanguageCode isEqualToString:@"txt_Danish"]) {
                [userDefaults setObject:@"0" forKey:PREFERENCE_DOWN_FLAG_DA];
            }
            
            [_btnDownloadAll setImage:[UIImage imageNamed:@"download_white"] forState:UIControlStateNormal];

        }
        [self getAllDownFlag];

        
//        NSArray *arrResult = [self.dbHelper checkIfDataAlreadyExist:DB_TABLENAME_PLACEMASTER place_id:strPlaceId colomnNM:@"place_id"];

//        if (arrResult.count > 0) {
//
//            int columnNum = 0;
//            if ([_strLanguageCode isEqualToString:@"txt_eng"]) {
//                columnNum = 38;
//            }else if([_strLanguageCode isEqualToString:@"txt_islenska"]) {
//                columnNum = 39;
//            }else if([_strLanguageCode isEqualToString:@"txt_German"]) {
//                columnNum = 40;
//            }else if([_strLanguageCode isEqualToString:@"txt_French"]) {
//                columnNum = 41;
//            }else if([_strLanguageCode isEqualToString:@"txt_Spanish"]) {
//                columnNum = 42;
//            }else if([_strLanguageCode isEqualToString:@"txt_Chinese"]) {
//                columnNum = 43;
//            }else if([_strLanguageCode isEqualToString:@"txt_Danish"]) {
//                columnNum = 44;
//            }
//
//            NSString *strForStatus = [NSString stringWithFormat:@"%@", [[arrResult objectAtIndex:0] objectAtIndex:columnNum]];
//
//            //        int allDownloadedOrnot = 0;
//            for (int j = 0; j<arrPlacesData.count; j++) {
//                NSIndexPath * indexPathValue1 = [NSIndexPath indexPathForRow:j inSection:0];
//                PlaceCell *cell = (PlaceCell *)[_tblPlaceList dequeueReusableCellWithIdentifier:@"PlaceCell" forIndexPath:indexPathValue1];
//
//                if (cell.imgSaveDel.image != [UIImage imageNamed:@"delete"]) {
//                    strForStatus = @"1";
//                }
//            }
//            if ([strForStatus isEqualToString:@"1"]) {
//                [_btnDownloadAll setImage:[UIImage imageNamed:@"delete"] forState:UIControlStateNormal];
//            }
//        }

        [_tblPlaceList reloadData];
        
    }else {
        
        if (arrResult.count > 0) {
            
            int columnNum = 0;
            if ([_strLanguageCode isEqualToString:@"txt_eng"]) {
                columnNum = 38;
            }else if([_strLanguageCode isEqualToString:@"txt_islenska"]) {
                columnNum = 39;
            }else if([_strLanguageCode isEqualToString:@"txt_German"]) {
                columnNum = 40;
            }else if([_strLanguageCode isEqualToString:@"txt_French"]) {
                columnNum = 41;
            }else if([_strLanguageCode isEqualToString:@"txt_Spanish"]) {
                columnNum = 42;
            }else if([_strLanguageCode isEqualToString:@"txt_Chinese"]) {
                columnNum = 43;
            }else if([_strLanguageCode isEqualToString:@"txt_Danish"]) {
                columnNum = 44;
            }
            
            NSString *strForStatus = [NSString stringWithFormat:@"%@", [[arrResult objectAtIndex:0] objectAtIndex:columnNum]];
//            NSString *strForStatus = [NSString stringWithFormat:@"%@", [[arrResult objectAtIndex:0] objectAtIndex:14]];
            
            if ([strForStatus isEqualToString:@"0"]) {
         
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:[Diclabels valueForKey:@"txt_thingvellir"] message:[Diclabels valueForKey:@"MSG_INTERNET_UNAVAILABLE"] preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *ok = [UIAlertAction actionWithTitle:[Diclabels valueForKey:@"OK"] style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) { }];
                
                [alert addAction:ok];
                [self presentViewController:alert animated:YES completion:nil];

            }else {
//                [dictmutTest setValue:@"0" forKey:@"down_flag"];
                
                cell.imgSaveDel.image = [UIImage imageNamed:@"download_gray"];
            
                if ([_strLanguageCode isEqualToString:@"txt_eng"]) {
                    [dictmutTest setValue:@"0" forKey:@"down_txt_eng"];
                    
                    [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                                                 @"0",DB_TABLEFIELD_DOWN_FLAG_EN,
                                                                                                 nil]];
                    [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                                                 @"", DB_TABLEFIELD_AUDIO,
                                                                                                 nil]];
                }else if([_strLanguageCode isEqualToString:@"txt_islenska"]) {
                    [dictmutTest setValue:@"0" forKey:@"down_txt_islenska"];
                    [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                                                 @"0",DB_TABLEFIELD_DOWN_FLAG_IS,
                                                                                                 nil]];
                    [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                                                 @"", DB_TABLEFIELD_AUDIO_ISLENSKA,
                                                                                                 nil]];
                }else if([_strLanguageCode isEqualToString:@"txt_German"]) {
                    [dictmutTest setValue:@"0" forKey:@"down_txt_german"];
                    [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                                                 @"0",DB_TABLEFIELD_DOWN_FLAG_GE,
                                                                                                 nil]];
                    [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                                                 @"", DB_TABLEFIELD_AUDIO_GERMAN,
                                                                                                 nil]];
                }else if([_strLanguageCode isEqualToString:@"txt_French"]) {
                    [dictmutTest setValue:@"0" forKey:@"down_txt_french"];
                    [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                                                 @"0",DB_TABLEFIELD_DOWN_FLAG_FR,
                                                                                                 nil]];
                    [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                                                 @"", DB_TABLEFIELD_AUDIO_FRENCH,
                                                                                                 nil]];
                }else if([_strLanguageCode isEqualToString:@"txt_Spanish"]) {
                    [dictmutTest setValue:@"0" forKey:@"down_txt_spanish"];
                    [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                                                 @"0",DB_TABLEFIELD_DOWN_FLAG_ES,
                                                                                                 nil]];
                    [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                                                 @"", DB_TABLEFIELD_AUDIO_SPANISH,
                                                                                                 nil]];
                }else if([_strLanguageCode isEqualToString:@"txt_Chinese"]) {
                    [dictmutTest setValue:@"0" forKey:@"down_txt_chinese"];
                    [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                                                 @"0",DB_TABLEFIELD_DOWN_FLAG_CH,
                                                                                                 nil]];
                    [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                                                 @"", DB_TABLEFIELD_AUDIO_CHINESE,
                                                                                                 nil]];
                }else if([_strLanguageCode isEqualToString:@"txt_Danish"]) {
                    [dictmutTest setValue:@"0" forKey:@"down_txt_danish"];
                    [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                                                 @"0",DB_TABLEFIELD_DOWN_FLAG_DA,
                                                                                                 nil]];
                    [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                                                 @"", DB_TABLEFIELD_AUDIO_DANISH,
                                                                                                 nil]];
                }
                
                //                [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys:
                //@"0",DB_TABLEFIELD_DOWNLOAD_FLAG,
                //                                                                                             nil]];
                
                [self.dbHelper delete:DB_TABLENAME_IMAGEMASTER placeId:strPlaceId colomnNM:DB_TABLEFIELD_PLACEID];
                
            }
            
//            [userDefaults setObject:@"0" forKey:PREFERENCE_STRDOWN];
            
            if ([_strLanguageCode isEqualToString:@"txt_eng"]) {
                [userDefaults setObject:@"0" forKey:PREFERENCE_DOWN_FLAG_EN];
            }else if([_strLanguageCode isEqualToString:@"txt_islenska"]) {
                [userDefaults setObject:@"0" forKey:PREFERENCE_DOWN_FLAG_IS];
            }else if([_strLanguageCode isEqualToString:@"txt_German"]) {
                [userDefaults setObject:@"0" forKey:PREFERENCE_DOWN_FLAG_GR];
            }else if([_strLanguageCode isEqualToString:@"txt_French"]) {
                [userDefaults setObject:@"0" forKey:PREFERENCE_DOWN_FLAG_FR];
            }else if([_strLanguageCode isEqualToString:@"txt_Spanish"]) {
                [userDefaults setObject:@"0" forKey:PREFERENCE_DOWN_FLAG_ES];
            }else if([_strLanguageCode isEqualToString:@"txt_Chinese"]) {
                [userDefaults setObject:@"0" forKey:PREFERENCE_DOWN_FLAG_CH];
            }else if([_strLanguageCode isEqualToString:@"txt_Danish"]) {
                [userDefaults setObject:@"0" forKey:PREFERENCE_DOWN_FLAG_DA];
            }
            
            [_btnDownloadAll setImage:[UIImage imageNamed:@"download_white"] forState:UIControlStateNormal];
        }
        
        [_tblPlaceList reloadData];
    }
    
   
}

/* Open Dialog With FadeOut Animation */
- (void)openWithAnimation:(UIView *) view {
//    view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    
    view.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.001, 0.001);
    
    [[[UIApplication sharedApplication] keyWindow] addSubview:view];
    
    view.transform = CGAffineTransformMakeScale(1.3, 1.3);
    view.alpha = 0;
    [UIView animateWithDuration:.35 animations:^{
        view.alpha = 1;
        view.transform = CGAffineTransformMakeScale(1, 1);
    }];
}

/* Close Dialog With FadeOut Animation */
- (void) closeWithAnimation:(UIView *) view
{
    [UIView animateWithDuration:.35 animations:^{
        view.transform = CGAffineTransformMakeScale(1.3, 1.3);
        view.alpha = 0.0;
    } completion:^(BOOL finished) {
        if (finished) {
            [view removeFromSuperview];
        }
    }];
}

#pragma mark -------------------------
#pragma mark -- Map function
#pragma mark -------------------------

- (void)loadMapView {

    [_mapVw clear];
    anIndex = 0;
    GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] init];

    for (NSDictionary *dictionary in arrPlacesData)
    {
        strPlaceId = [dictionary valueForKey:@"place_id"];
    
        NSArray *arrResult = [self.dbHelper checkIfDataAlreadyExist:DB_TABLENAME_PLACEMASTER place_id:strPlaceId colomnNM:@"place_id"];
//        [userDefaults setObject:arrTemp forKey:@"Db"];

        NSString *strVisitLocalStatus = [NSString stringWithFormat:@"%@", [[arrResult objectAtIndex:0] objectAtIndex:10]];
        NSString *strLikeStatus = [NSString stringWithFormat:@"%@", [[arrResult objectAtIndex:0] objectAtIndex:9]];
        
        int columnNum = 0;
        if ([_strLanguageCode isEqualToString:@"txt_eng"]) {
            columnNum = 38;
        }else if([_strLanguageCode isEqualToString:@"txt_islenska"]) {
            columnNum = 39;
        }else if([_strLanguageCode isEqualToString:@"txt_German"]) {
            columnNum = 40;
        }else if([_strLanguageCode isEqualToString:@"txt_French"]) {
            columnNum = 41;
        }else if([_strLanguageCode isEqualToString:@"txt_Spanish"]) {
            columnNum = 42;
        }else if([_strLanguageCode isEqualToString:@"txt_Chinese"]) {
            columnNum = 43;
        }else if([_strLanguageCode isEqualToString:@"txt_Danish"]) {
            columnNum = 44;
        }
        
        NSString *strDownloadStatus = [NSString stringWithFormat:@"%@", [[arrResult objectAtIndex:0] objectAtIndex:columnNum]];
//        coordinates.latitude = [dictionary[@"latitude"] floatValue];
//        coordinates.longitude = [dictionary[@"longitude"] floatValue];
      
        CLLocation *originLocation = [[CLLocation alloc] initWithLatitude:locationManager.location.coordinate.latitude longitude:locationManager.location.coordinate.longitude];
        NSString *strLatitudeDes  = [NSString stringWithFormat:@"%f", [dictionary[@"latitude"] doubleValue]];
        NSString *strLongtudeDes = [NSString stringWithFormat:@"%f", [dictionary[@"longitude"] doubleValue]];
        
        coordinates.latitude = [strLatitudeDes doubleValue];
        coordinates.longitude = [strLongtudeDes doubleValue];
        CLLocation *destinationLocation = [[CLLocation alloc] initWithLatitude:coordinates.latitude longitude:coordinates.longitude];
        
        CLLocationDistance distance = [originLocation distanceFromLocation:destinationLocation];
        NSLog(@"distance is %f",distance/1000.0);
        
        NSString *dist = [NSString stringWithFormat:@"%.2f km", (distance/1000.0)];

        
        // Creates a marker in the center of the map.
        GMSMarker *marker = [[GMSMarker alloc] init];
        
        if ([strVisitLocalStatus isEqualToString:@"1"]) {
            marker.icon = [UIImage imageNamed:(@"map_pin_place_visited")];
        }else {
            marker.icon = [UIImage imageNamed:(@"map_pin_selected.png")];
        }
//        marker.icon = [UIImage imageNamed:(@"map_pin_selected.png")];
        marker.position = CLLocationCoordinate2DMake(coordinates.latitude, coordinates.longitude);
        bounds = [bounds includingCoordinate:marker.position];
        marker.infoWindowAnchor = CGPointMake(0.5, 0.20);//CGPointMake(1.1, 0.70);
        
        //Create Json dict
        NSMutableArray *arrTemp = [[NSMutableArray alloc] init];
        NSMutableDictionary *dictTemp = [[NSMutableDictionary alloc]init];
        
        [dictTemp setValue:[dictionary valueForKey:@"distance"] forKey:@"distance"];
//        [dictTemp setValue:dist forKey:@"distance"];
        
//        [dictTemp setValue:[dictionary valueForKey:@"distance"] forKey:@"distance"];
//        [dictTemp setValue:[dictionary valueForKeyPath:@"image.place_img_name"] forKey:@"image"];

        [dictTemp setValue:strLikeStatus forKey:@"isLike"];
        [dictTemp setValue:strVisitLocalStatus forKey:@"isVisited"];
        
        if ([_strLanguageCode isEqualToString:@"txt_eng"]) {
            marker.title = dictionary[@"place_title"];
            [dictTemp setValue:strDownloadStatus forKey:@"down_txt_eng"];
            
        }else if([_strLanguageCode isEqualToString:@"txt_islenska"]) {
            marker.title = dictionary[@"place_title_islenska"];
            [dictTemp setValue:strDownloadStatus forKey:@"down_txt_islenska"];
            
        }else if([_strLanguageCode isEqualToString:@"txt_German"]) {
            marker.title = dictionary[@"place_title_german"];
            [dictTemp setValue:strDownloadStatus forKey:@"down_txt_german"];
            
        }else if([_strLanguageCode isEqualToString:@"txt_French"]) {
            marker.title = dictionary[@"place_title_french"];
            [dictTemp setValue:strDownloadStatus forKey:@"down_txt_french"];
            
        }else if([_strLanguageCode isEqualToString:@"txt_Spanish"]) {
            marker.title = dictionary[@"place_title_spanish"];
            [dictTemp setValue:strDownloadStatus forKey:@"down_txt_spanish"];
            
        }else if([_strLanguageCode isEqualToString:@"txt_Chinese"]) {
            marker.title = dictionary[@"place_title_chinese"];
            [dictTemp setValue:strDownloadStatus forKey:@"down_txt_chinese"];
            
        }else if([_strLanguageCode isEqualToString:@"txt_Danish"]) {
            marker.title = dictionary[@"place_title_danish"];
            [dictTemp setValue:strDownloadStatus forKey:@"down_txt_danish"];
        }
    

        [dictTemp setValue:[dictionary valueForKeyPath:@"thumbImage"] forKey:@"thumbImage"];
        [dictTemp setObject:dictionary forKey:@"selectedDict"];
        [dictTemp setObject:arrResult forKey:@"SelecteLocalDataArray"];
        
        NSString *strNum = [NSString stringWithFormat:@"%d",anIndex];
        [dictTemp setObject:strNum forKey:@"markerIndex"];
        anIndex++;
        
        NSArray *arrDetailLargeImages = [userDefaults valueForKey:@"ArrDetailImages"];
        
//        NSArray *arrDetailImage = [self.dbHelper checkIfDataAlreadyExist:DB_TABLENAME_IMAGEMASTER place_id:strPlaceId colomnNM:@"place_id"];
//        NSLog(@"Detail Image%@",arrDetailImage);
//
//        NSMutableArray *arrImagePath = [[NSMutableArray alloc]init];
//        for (int j=0; j<arrDetailImage.count; j++) {
//            NSString * strPath = [NSString stringWithFormat:@"%@",[[arrDetailImage objectAtIndex:j]objectAtIndex:2]];
//            [arrImagePath addObject: strPath];
//        }
        
//        NSLog(@"arrImagePath Image%@",arrImagePath);
        
        [dictTemp setObject:arrDetailLargeImages forKey:@"ArrayDetailLargeImage"];

        [arrTemp addObject:dictTemp];
        NSString *strJson = [self getString:arrTemp];
        marker.snippet = strJson;
        
        GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:coordinates.latitude
                                                                longitude:coordinates.longitude
                                                                     zoom:10];
        _mapVw.camera = camera;
        
        marker.map = _mapVw;
    }
    
    [_mapVw animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds withPadding:30.0f]];

}

- (NSString *)getString: (NSMutableArray *)arr {
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:arr options:NSJSONWritingPrettyPrinted error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    return jsonString;
}

- (NSArray *)getArrayFromJson: (NSString *)str {
    NSData* data = [str dataUsingEncoding:NSUTF8StringEncoding];
    NSError *e;
    NSArray *array = [NSJSONSerialization JSONObjectWithData:data options:nil error:&e];
    return array;
}

#pragma mark -------------------------
#pragma mark -- GMSMapViewDelegate
#pragma mark -------------------------

- (UIView *)mapView:(GMSMapView *)mapView markerInfoWindow:(GMSMarker *)marker {

    CustomInfoWindow *infoWindow = [[[NSBundle mainBundle] loadNibNamed:@"InfoWindow" owner:self options:nil] objectAtIndex:0];

    NSString *strJson = marker.snippet;
    NSArray *arr = [self getArrayFromJson:strJson];
    
    UIImage *myImage = [UIImage imageWithContentsOfFile:[[arr valueForKey:@"thumbImage"] objectAtIndex:0]];
    infoWindow.placeImage.image = myImage;
    
//    [infoWindow.placeImage sd_setImageWithURL:[[arr valueForKey:@"image"]objectAtIndex:0]];
    infoWindow.placeTitle.text = marker.title;
    
    infoWindow.lblKm.text = [NSString stringWithFormat:@"%@ km", [[arr valueForKey:@"distance"]objectAtIndex:0]];
    
    NSString *strFavLocalStatus = [NSString stringWithFormat:@"%@", [[arr valueForKey:@"isLike"] objectAtIndex:0]];
    
    if ([strFavLocalStatus isEqualToString:@"0"]) {
        infoWindow.likeImgVw.image = [UIImage imageNamed:@"favourite_gray"];
    }else {
        infoWindow.likeImgVw.image = [UIImage imageNamed:@"favourite_red"];
    }
    
    
    NSString *strVisitLocalStatus = [NSString stringWithFormat:@"%@", [[arr valueForKey:@"isVisited"] objectAtIndex:0]];
    
    if ([strVisitLocalStatus isEqualToString:@"0"]) {
        infoWindow.pinImgVw.image = [UIImage imageNamed:@"map_pin"];
    }else {
        infoWindow.pinImgVw.image = [UIImage imageNamed:@"map_pin_place_visited"];
    }
    
    
    NSString *strDownLiveFlag;
    
    if ([_strLanguageCode isEqualToString:@"txt_eng"]) {
        strDownLiveFlag = [NSString stringWithFormat:@"%@", [[arr valueForKey:@"down_txt_eng"] objectAtIndex:0]];
        
    }else if([_strLanguageCode isEqualToString:@"txt_islenska"]) {
        strDownLiveFlag = [NSString stringWithFormat:@"%@", [[arr valueForKey:@"down_txt_islenska"] objectAtIndex:0]];
        
    }else if([_strLanguageCode isEqualToString:@"txt_German"]) {
        strDownLiveFlag = [NSString stringWithFormat:@"%@", [[arr valueForKey:@"down_txt_german"] objectAtIndex:0]];
        
    }else if([_strLanguageCode isEqualToString:@"txt_French"]) {
        strDownLiveFlag = [NSString stringWithFormat:@"%@", [[arr valueForKey:@"down_txt_french"] objectAtIndex:0]];
        
    }else if([_strLanguageCode isEqualToString:@"txt_Spanish"]) {
        strDownLiveFlag = [NSString stringWithFormat:@"%@", [[arr valueForKey:@"down_txt_spanish"] objectAtIndex:0]];
        
    }else if([_strLanguageCode isEqualToString:@"txt_Chinese"]) {
        strDownLiveFlag = [NSString stringWithFormat:@"%@", [[arr valueForKey:@"down_txt_chinese"] objectAtIndex:0]];
        
    }else if([_strLanguageCode isEqualToString:@"txt_Danish"]) {
        strDownLiveFlag = [NSString stringWithFormat:@"%@", [[arr valueForKey:@"down_txt_danish"] objectAtIndex:0]];
    }
    
    
    if ([strDownLiveFlag isEqualToString:@"0"]) {
        infoWindow.imgSaveDel.image = [UIImage imageNamed:@"download_gray"];
    }else {
        infoWindow.imgSaveDel.image = [UIImage imageNamed:@"delete_place"];
    }
  
    return infoWindow;
}

- (void)mapView:(GMSMapView *)mapView didTapInfoWindowOfMarker:(GMSMarker *)marker {
    NSLog(@"Click on infowindow");
    
    NSString *strSnipppetData = marker.snippet;
    NSArray *arrObj = [self getArrayFromJson:strSnipppetData];
    NSDictionary *dictpass = [[arrObj valueForKey:@"selectedDict"] objectAtIndex:0];
    NSArray *arrPass1 = [[arrObj valueForKey:@"SelecteLocalDataArray"] objectAtIndex:0];
    
//    NSArray *arrResult = [self.dbHelper checkIfDataAlreadyExist:DB_TABLENAME_PLACEMASTER place_id:strPlaceId colomnNM:@"place_id"];
    [userDefaults setObject:arrPass1 forKey:@"Db"];
    
    NSArray *arrDetailLargeImages = [userDefaults valueForKey:@"ArrDetailImages"];

    strPlaceId = [dictpass valueForKey:@"place_id"];//[[arrPlacesData valueForKey:@"place_id"] objectAtIndex:indexPath.row];

    if ([utility isInternetConnected]) {
      
        int columnNum = 0;
        NSString *path;
        
        if ([_strLanguageCode isEqualToString:@"txt_eng"]) {
            columnNum = 38;
            
            NSInteger indexOfPlaceAudio = [self.dbHelper.arrColumnNames indexOfObject:DB_TABLEFIELD_AUDIO];
            path = [NSString stringWithFormat:@"%@", [[arrPass1 objectAtIndex:0] objectAtIndex:indexOfPlaceAudio]];
            
        }else if([_strLanguageCode isEqualToString:@"txt_islenska"]) {
            columnNum = 39;
            NSInteger indexOfPlaceAudioIs = [self.dbHelper.arrColumnNames indexOfObject:DB_TABLEFIELD_AUDIO_ISLENSKA];
            path = [NSString stringWithFormat:@"%@", [[arrPass1 objectAtIndex:0] objectAtIndex:indexOfPlaceAudioIs]];
            
        }else if([_strLanguageCode isEqualToString:@"txt_German"]) {
            columnNum = 40;
            NSInteger indexOfPlaceAudioGr = [self.dbHelper.arrColumnNames indexOfObject:DB_TABLEFIELD_AUDIO_GERMAN];
            path = [NSString stringWithFormat:@"%@", [[arrPass1 objectAtIndex:0] objectAtIndex:indexOfPlaceAudioGr]];
            
        }else if([_strLanguageCode isEqualToString:@"txt_French"]) {
            columnNum = 41;
            NSInteger indexOfPlaceAudioFr = [self.dbHelper.arrColumnNames indexOfObject:DB_TABLEFIELD_AUDIO_FRENCH];
            path = [NSString stringWithFormat:@"%@", [[arrPass1 objectAtIndex:0] objectAtIndex:indexOfPlaceAudioFr]];
            
        }else if([_strLanguageCode isEqualToString:@"txt_Spanish"]) {
            columnNum = 42;
            NSInteger indexOfPlaceAudioEs = [self.dbHelper.arrColumnNames indexOfObject:DB_TABLEFIELD_AUDIO_SPANISH];
            path = [NSString stringWithFormat:@"%@", [[arrPass1 objectAtIndex:0] objectAtIndex:indexOfPlaceAudioEs]];
            
        }else if([_strLanguageCode isEqualToString:@"txt_Chinese"]) {
            columnNum = 43;
            NSInteger indexOfPlaceAudioCh = [self.dbHelper.arrColumnNames indexOfObject:DB_TABLEFIELD_AUDIO_CHINESE];
            path = [NSString stringWithFormat:@"%@", [[arrPass1 objectAtIndex:0] objectAtIndex:indexOfPlaceAudioCh]];
            
        }else if([_strLanguageCode isEqualToString:@"txt_Danish"]) {
            columnNum = 44;
            NSInteger indexOfPlaceAudioDa = [self.dbHelper.arrColumnNames indexOfObject:DB_TABLEFIELD_AUDIO_DANISH];
            path = [NSString stringWithFormat:@"%@", [[arrPass1 objectAtIndex:0] objectAtIndex:indexOfPlaceAudioDa]];
        }
        
        
        NSString *strDownFlag = [NSString stringWithFormat:@"%@", [[arrPass1 objectAtIndex:0] objectAtIndex:columnNum]];
        
        if ([strDownFlag isEqualToString:@"1"]) {
            NSLog(@"Downloaded");
            PlaceDetailViewController *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"PlaceDetailViewController"];
            VC.strSelectedLanguage = [[NSUserDefaults standardUserDefaults] valueForKey:PREFERENCE_LANGUAGE];

            VC.arrPlaceDetail = arrPass1;
            VC.strAudioPath = path;
            
            int ind = [[[arrObj objectAtIndex:0]valueForKey:@"markerIndex"] integerValue];
            VC.arrLargeDetailImages = [arrDetailLargeImages objectAtIndex:ind];//[arrDetailLargeImages objectAtIndex:[[arrObj valueForKey:@"markerIndex"]objectAtIndex:0]];//[[arrObj valueForKey:@"ArrayDetailLargeImage"] objectAtIndex:0];
            
//
            NSArray *arrDetailImage = [self.dbHelper checkIfDataAlreadyExist:DB_TABLENAME_IMAGEMASTER place_id:strPlaceId colomnNM:@"place_id"];
            NSLog(@"Detail Image%@",arrDetailImage);

            NSMutableArray *arrImagePath = [[NSMutableArray alloc]init];
            for (int j=0; j<arrDetailImage.count; j++) {
                NSString * strPath = [NSString stringWithFormat:@"%@",[[arrDetailImage objectAtIndex:j]objectAtIndex:2]];
                [arrImagePath addObject: strPath];
            }

            NSLog(@"arrImagePath Image%@",arrImagePath);
//
            VC.arrDetailImages = arrImagePath;
            VC.selectedIndex = [[arrObj valueForKey:@"markerIndex"] objectAtIndex:0];//indexPath.row;
            
            [self.navigationController pushViewController:VC animated:YES];
            
        }else {
            
            PlaceDetailViewController *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"PlaceDetailViewController"];
//            NSString *strSnipppetData = marker.snippet;
//            NSArray *arrObj = [self getArrayFromJson:strSnipppetData];
//            NSDictionary *dictpass = [[arrObj valueForKey:@"selectedDict"] objectAtIndex:0];
            VC.dicPlaceDetail = dictpass;
//            NSArray *arrPass1 = [[arrObj valueForKey:@"SelecteLocalDataArray"] objectAtIndex:0];
//            VC.arrPlaceDetail = arrPass1;
            [self.navigationController pushViewController:VC animated:YES];

        }
 
    }else {
        
        int columnNum = 0;
        NSString *path;
        
        if ([_strLanguageCode isEqualToString:@"txt_eng"]) {
            columnNum = 38;
            
            NSInteger indexOfPlaceAudio = [self.dbHelper.arrColumnNames indexOfObject:DB_TABLEFIELD_AUDIO];
            path = [NSString stringWithFormat:@"%@", [[arrPass1 objectAtIndex:0] objectAtIndex:indexOfPlaceAudio]];
            
        }else if([_strLanguageCode isEqualToString:@"txt_islenska"]) {
            columnNum = 39;
            NSInteger indexOfPlaceAudioIs = [self.dbHelper.arrColumnNames indexOfObject:DB_TABLEFIELD_AUDIO_ISLENSKA];
            path = [NSString stringWithFormat:@"%@", [[arrPass1 objectAtIndex:0] objectAtIndex:indexOfPlaceAudioIs]];
            
        }else if([_strLanguageCode isEqualToString:@"txt_German"]) {
            columnNum = 40;
            NSInteger indexOfPlaceAudioGr = [self.dbHelper.arrColumnNames indexOfObject:DB_TABLEFIELD_AUDIO_GERMAN];
            path = [NSString stringWithFormat:@"%@", [[arrPass1 objectAtIndex:0] objectAtIndex:indexOfPlaceAudioGr]];
            
        }else if([_strLanguageCode isEqualToString:@"txt_French"]) {
            columnNum = 41;
            NSInteger indexOfPlaceAudioFr = [self.dbHelper.arrColumnNames indexOfObject:DB_TABLEFIELD_AUDIO_FRENCH];
            path = [NSString stringWithFormat:@"%@", [[arrPass1 objectAtIndex:0] objectAtIndex:indexOfPlaceAudioFr]];
            
        }else if([_strLanguageCode isEqualToString:@"txt_Spanish"]) {
            columnNum = 42;
            NSInteger indexOfPlaceAudioEs = [self.dbHelper.arrColumnNames indexOfObject:DB_TABLEFIELD_AUDIO_SPANISH];
            path = [NSString stringWithFormat:@"%@", [[arrPass1 objectAtIndex:0] objectAtIndex:indexOfPlaceAudioEs]];
            
        }else if([_strLanguageCode isEqualToString:@"txt_Chinese"]) {
            columnNum = 43;
            NSInteger indexOfPlaceAudioCh = [self.dbHelper.arrColumnNames indexOfObject:DB_TABLEFIELD_AUDIO_CHINESE];
            path = [NSString stringWithFormat:@"%@", [[arrPass1 objectAtIndex:0] objectAtIndex:indexOfPlaceAudioCh]];
            
        }else if([_strLanguageCode isEqualToString:@"txt_Danish"]) {
            columnNum = 44;
            NSInteger indexOfPlaceAudioDa = [self.dbHelper.arrColumnNames indexOfObject:DB_TABLEFIELD_AUDIO_DANISH];
            path = [NSString stringWithFormat:@"%@", [[arrPass1 objectAtIndex:0] objectAtIndex:indexOfPlaceAudioDa]];
        }
        
        
        NSString *strDownFlag = [NSString stringWithFormat:@"%@", [[arrPass1 objectAtIndex:0] objectAtIndex:columnNum]];
        
        if ([strDownFlag isEqualToString:@"1"]) {
            NSLog(@"Downloaded");
            PlaceDetailViewController *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"PlaceDetailViewController"];
            VC.strSelectedLanguage = [[NSUserDefaults standardUserDefaults] valueForKey:PREFERENCE_LANGUAGE];
            
            VC.arrPlaceDetail = arrPass1;
            VC.strAudioPath = path;
            
            int ind = [[[arrObj objectAtIndex:0]valueForKey:@"markerIndex"] integerValue];
            VC.arrLargeDetailImages = [arrDetailLargeImages objectAtIndex:ind];//[arrDetailLargeImages objectAtIndex:[[arrObj valueForKey:@"markerIndex"]objectAtIndex:0]];//[[arrObj valueForKey:@"ArrayDetailLargeImage"] objectAtIndex:0];
            
            //
            NSArray *arrDetailImage = [self.dbHelper checkIfDataAlreadyExist:DB_TABLENAME_IMAGEMASTER place_id:strPlaceId colomnNM:@"place_id"];
            NSLog(@"Detail Image%@",arrDetailImage);
            
            NSMutableArray *arrImagePath = [[NSMutableArray alloc]init];
            for (int j=0; j<arrDetailImage.count; j++) {
                NSString * strPath = [NSString stringWithFormat:@"%@",[[arrDetailImage objectAtIndex:j]objectAtIndex:2]];
                [arrImagePath addObject: strPath];
            }
            
            NSLog(@"arrImagePath Image%@",arrImagePath);
            //
            VC.arrDetailImages = arrImagePath;
            VC.selectedIndex = [[arrObj valueForKey:@"markerIndex"] objectAtIndex:0];//indexPath.row;
            
            [self.navigationController pushViewController:VC animated:YES];
            
        }else {
            
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:[Diclabels valueForKey:@"txt_thingvellir"] message:[Diclabels valueForKey:@"MSG_INTERNET_UNAVAILABLE"] preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *ok = [UIAlertAction actionWithTitle:[Diclabels valueForKey:@"OK"] style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) { }];
            
            [alert addAction:ok];
            [self presentViewController:alert animated:YES completion:nil];
        }
    }
}

- (void)setLabels {
    
    NSDictionary *labels = [[NSUserDefaults standardUserDefaults] objectForKey: PREFERENCE_LANGUAGE_LABEL];
    
    Diclabels = [[NSDictionary alloc] initWithDictionary:labels];
    
    _navTitle.text = [Diclabels objectForKey:@"txt_thingvellir"];
    
    _lblFilter.text = [Diclabels objectForKey:@"txt_filter"];
    _lblFav.text = [Diclabels objectForKey:@"txt_favourite"];
    _lblPlace.text = [Diclabels objectForKey:@"txt_place"];
  
    [_btnFav setTitle:[Diclabels objectForKey:@"txt_favourite"] forState:UIControlStateNormal];
    [_btnVisited setTitle:[Diclabels objectForKey:@"txt_visited"] forState:UIControlStateNormal];
    [_btnReset setTitle:[Diclabels objectForKey:@"txt_reset"] forState:UIControlStateNormal];
    [_btnApply setTitle:[Diclabels objectForKey:@"txt_apply"] forState:UIControlStateNormal];
    
    [_changeMapTypeSegment setTitle:[Diclabels valueForKey:@"txt_normal"] forSegmentAtIndex:0];
    [_changeMapTypeSegment setTitle:[Diclabels valueForKey:@"txt_satellite"] forSegmentAtIndex:1];

}

#pragma mark -------------------------
#pragma mark API response
#pragma mark -------------------------

- (void)getRandomString {
    [self showLoadingIndicator];

    [APIHelper GET:kApiGetRandomString parameters:[NSArray new] progress:^(NSProgress * _Nonnull downloadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSLog(@"responseObject %@ :", responseObject);
        [[NSNotificationCenter defaultCenter] postNotificationName:@"Menu" object:self];

        rendomStr = [responseObject valueForKey:@"random_code"];
        NSString *strIfKeyExists  = @"";
        
        if ([userDefaults objectForKey:PREFERENCE_RENDOM_CODE] != nil) {
            strIfKeyExists = [userDefaults valueForKey:PREFERENCE_RENDOM_CODE];
        }
        else
        {
            [userDefaults setObject:rendomStr forKey:PREFERENCE_RENDOM_CODE];
            [userDefaults synchronize];
        }
        
        if ([strIfKeyExists isEqualToString:rendomStr]) {
          
            NSLog(@"both key are same no need to dl");
            NSArray *arrGetMain = [userDefaults objectForKey:@"MainArrayData"];
            [arrPlacesData removeAllObjects];
            [arrPlacesData addObjectsFromArray: arrGetMain];
            [_tblPlaceList reloadData];
            
            NSArray *arrGetImageMain = [userDefaults valueForKey:@"MainImageData"];
            [arrLocalPlacesImg removeAllObjects];
            [arrLocalPlacesImg addObjectsFromArray:arrGetImageMain];
            [_tblPlaceList reloadData];
            
        }else {
//            _btnDownloadAll.enabled = YES;
            [userDefaults setObject:rendomStr forKey:PREFERENCE_RENDOM_CODE];
            [self getPlacesList];

        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            
            [self stopIndicator];
            
            NSArray *arrGetMain = [userDefaults objectForKey:@"MainArrayData"];
            [arrPlacesData removeAllObjects];
            [arrPlacesData addObjectsFromArray: arrGetMain];
            
            NSArray *arrGetImageMain = [userDefaults valueForKey:@"MainImageData"];
            [arrLocalPlacesImg removeAllObjects];
            [arrLocalPlacesImg addObjectsFromArray:arrGetImageMain];
            [_tblPlaceList reloadData];
            
//            [self setDownloadAllButtonImage];

        }];
    }];
}

- (void)getLabels {
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionaryWithObjectsAndKeys:_strLanguageCode, @"language", nil];
    
    NSLog(@"Parameters is: %@",parameters);
    
    [self showLoadingIndicator];
    
    [APIHelper POST:kApiGetLabel parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [self stopIndicator];

//        NSLog(@"responseObject %@ :", responseObject);
        
        arrLabels = (NSMutableArray *)[responseObject valueForKey:@"result"];
        
        [[NSUserDefaults standardUserDefaults] setObject:arrLabels forKey:PREFERENCE_LANGUAGE_LABEL];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [self setLabels];
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        NSDictionary *labels = [[NSUserDefaults standardUserDefaults] objectForKey:PREFERENCE_LANGUAGE_LABEL];
        
        Diclabels = [[NSDictionary alloc] initWithDictionary:labels];
        
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            
            [self stopIndicator];
            
            UIAlertController *alertController = [UIAlertController
                                                  alertControllerWithTitle:[Diclabels objectForKey:@"txt_thingvellir"]
                                                  message:nil
                                                  preferredStyle:UIAlertControllerStyleAlert
                                                  ];
            
            if (error.code == NSURLErrorNotConnectedToInternet) {
                alertController.message = [Diclabels objectForKey:@"MSG_INTERNET_UNAVAILABLE"];
            }else{
                alertController.message = [Diclabels objectForKey:@"MSG_REQUEST_FAIL"];
            }
            
            [alertController addAction:[UIAlertAction
                                        actionWithTitle:[Diclabels objectForKey:@"OK"]
                                        style:UIAlertActionStyleCancel
                                        handler:^(UIAlertAction * _Nonnull action) {
                                        }]
             ];
            
            [alertController addAction:[UIAlertAction
                                        actionWithTitle:[Diclabels objectForKey:@"txt_retry"]
                                        style:UIAlertActionStyleDefault
                                        handler:^(UIAlertAction * _Nonnull action) {
                                            
                                            [self getLabels];
                                        }]
             ];
            
            [self presentViewController:alertController animated:YES completion:nil];
            
        }];
    
    }];
}

- (void)getPlacesList {
    
    _strLanguageCode = [userDefaults valueForKey:PREFERENCE_LANGUAGE];

    NSMutableDictionary *parameters = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                       [NSNumber numberWithFloat:locationManager.location.coordinate.latitude], @"latitude",
                                       [NSNumber numberWithFloat:locationManager.location.coordinate.longitude], @"longitude",
                                       @"", @"code",
                                       nil];
    
    NSLog(@"Parameters is: %@",parameters);
    
    [self showLoadingIndicator];
    
    [APIHelper POST:kApiGetPlaces parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {

    } progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [userDefaults setBool:YES forKey:PREFERENCE_CHECK_TIME];

//        NSLog(@"responseObject %@ :", responseObject);
//        [self stopIndicator];
//            NSArray *arr = [responseObject valueForKey:@"result"];
//            for (int i= 0; i < arr.count; i++) {
//                NSLog(@"Response obj distance is %@", [[arr valueForKey:@"distance"]objectAtIndex:i ]);
//            }
        [arrPlacesData addObjectsFromArray:[responseObject valueForKey:@"result"]];
        
        
        NSSortDescriptor *distanceDescriptor = [[NSSortDescriptor alloc] initWithKey:@"distance" ascending:YES];
        NSArray *sortDescriptors = @[distanceDescriptor];

        NSArray *ordered = [arrPlacesData sortedArrayUsingDescriptors:sortDescriptors];
        
        for (int i = 0; i< arrPlacesData.count; i++) {
            NSLog(@"Distance is %@",[ordered valueForKey:@"distance"]);
        }

        [arrPlacesData removeAllObjects];
        [arrPlacesData addObjectsFromArray:ordered];
        
        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0ul);
        dispatch_async(queue, ^{

        NSArray *arrImages = [arrPlacesData valueForKey:@"image"];
//        NSLog(@"arrImages %@:",arrImages);

        for (NSDictionary *dicPlaces in arrImages) {
            [arrPlaceImages addObject:[dicPlaces valueForKey:@"place_img_name"]];
        }
//        NSLog(@"arrPlaceImages %@:",arrPlaceImages);
        
        
        NSInteger anIndex = 0;
        
        for (int i=0; i<arrPlaceImages.count; i++) {
            
            NSURL *imageURL = [NSURL URLWithString:[arrPlaceImages objectAtIndex:i]];
            pngData = [NSData dataWithContentsOfURL:imageURL];
            
            //            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            //            NSString *documentsPath = [paths objectAtIndex:0]; //Get the docs directory
            
            NSString *anImageName = [NSString stringWithFormat:@"%ldplace.png", (long)anIndex++];
            NSString *anPlacesImagePath = [NSString stringWithFormat:@"%@/%@", documentsPath, anImageName];
            
            [pngData writeToFile:anPlacesImagePath atomically:YES]; //Write the file
            [arrLocalPlacesImg addObject:anPlacesImagePath];
            
        }
//        NSLog(@"arrLocalPlacesImg is %@",arrLocalPlacesImg);
        
        [userDefaults setObject:arrLocalPlacesImg forKey:@"MainImageData"];
        [userDefaults synchronize];
        
        
        
//        NSLog(@"arrPlacesData %@",arrPlacesData);
        
        for (int i = 0;  i < arrPlacesData.count; i++) {
            NSMutableDictionary *dicData = [[NSMutableDictionary alloc] init];
            [dicData setDictionary:arrPlacesData[i]];
            [dicData setValue:@"0" forKey:@"isLike"];
            [dicData setValue:@"0" forKey:@"isVisited"];
            [dicData setValue:@"0" forKey:@"down_flag"];
            [dicData setValue:@"0" forKey:@"down_txt_eng"];
            [dicData setValue:@"0" forKey:@"down_txt_islenska"];
            [dicData setValue:@"0" forKey:@"down_txt_german"];
            [dicData setValue:@"0" forKey:@"down_txt_french"];
            [dicData setValue:@"0" forKey:@"down_txt_spanish"];
            [dicData setValue:@"0" forKey:@"down_txt_chinese"];
            [dicData setValue:@"0" forKey:@"down_txt_danish"];
            [dicData setValue:[arrLocalPlacesImg objectAtIndex:i] forKey:@"thumbImage"];
            [arrPlacesData replaceObjectAtIndex:i withObject:dicData];
        }
//        NSLog(@"arrPlacesData %@:",arrPlacesData);

        [userDefaults setObject:arrPlacesData forKey:@"MainArrayData"];
        [userDefaults synchronize];
        
        NSArray *arrImg = [arrPlacesData valueForKeyPath:@"place_images.place_img_name"];
//        NSLog(@"arrImage %@",arrImg);
        
        [userDefaults setObject:arrImg forKey:@"ArrDetailImages"];
        [userDefaults synchronize];
    
        
            NSString *strDeleteIds = [responseObject valueForKey:@"delete_ids"];
//            NSLog(@"strDeleteIds is :: %@", strDeleteIds);
            
            if ([strDeleteIds isEqualToString:@""] || strDeleteIds == NULL) {
            }else {
                [self.dbHelper deleteIn:DB_TABLENAME_PLACEMASTER colomnNM:DB_TABLEFIELD_PLACEID placeId:strDeleteIds];
            }
            
            //Background Thread
            for (int i = 0; i < arrPlacesData.count; i++) {
                
                strPlaceId = [[arrPlacesData valueForKey:@"place_id"] objectAtIndex:i];
                NSString *strUpdateAtDt = [[arrPlacesData valueForKey:@"update_at"] objectAtIndex:i];
                
                NSArray *arrTemp = [self.dbHelper checkIfDataAlreadyExist:DB_TABLENAME_PLACEMASTER place_id:strPlaceId colomnNM:@"place_id"];
//                NSLog(@"arrTemp :: %@", arrTemp);
                
                if ([arrTemp count] > 0) {
                    
                    NSString *strLocalDbUpdateAt = [[arrTemp objectAtIndex:0] objectAtIndex:15];
                    
                    [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                                                 [[arrPlacesData valueForKey:@"distance"] objectAtIndex:i], DB_TABLEFIELD_KM, nil]];
                    
                    
                    if (![strUpdateAtDt isEqualToString:strLocalDbUpdateAt]) {
                        
                        [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                                                     [[arrPlacesData valueForKey:@"place_title"] objectAtIndex:i], DB_TABLEFIELD_NAME,
                                                                                                     [[arrPlacesData valueForKey:@"place_title_islenska"] objectAtIndex:i], DB_TABLEFIELD_NAME_ISLENSKA,
                                                                                                     [[arrPlacesData valueForKey:@"place_desc"] objectAtIndex:i], DB_TABLEFIELD_DESCRIPTION,
                                                                                                     [[arrPlacesData valueForKey:@"place_desc_islenska"] objectAtIndex:i], DB_TABLEFIELD_DESCRIPTION_ISLENSKA,
                                                                                                     [[arrPlacesData valueForKey:@"distance"] objectAtIndex:i], DB_TABLEFIELD_KM,
                                                                                                     [[arrPlacesData valueForKey:@"latitude"] objectAtIndex:i], DB_TABLEFIELD_LATITUDE,
                                                                                                     [[arrPlacesData valueForKey:@"longitude"] objectAtIndex:i], DB_TABLEFIELD_LOGITUDE,
                                                                                                     [arrLocalPlacesImg objectAtIndex:i], DB_TABLEFIELD_IMAGE,
                                                                                                     @"", DB_TABLEFIELD_AUDIO,
                                                                                                     @"", DB_TABLEFIELD_AUDIO_ISLENSKA,
                                                                                                     [[arrPlacesData valueForKey:@"update_at"] objectAtIndex:i], DB_TABLEFIELD_UPDATE_AT,
                                                                                                     [[arrPlacesData valueForKey:@"place_desc_none_html"] objectAtIndex:i], DB_TABLEFIELD_DES_ENG_NONE_HTML,
                                                                                                     [[arrPlacesData valueForKey:@"place_desc_islenska_none_html"] objectAtIndex:i], DB_TABLEFIELD_DES_ISLENSKA_NONE_HTML,
                                                                                                     [[arrPlacesData valueForKey:@"place_title_german"] objectAtIndex:i], DB_TABLEFIELD_NAME_GERMAN,
                                                                                                     [[arrPlacesData valueForKey:@"place_desc_german_none_html"] objectAtIndex:i], DB_TABLEFIELD_DES_GERMAN_NONE_HTML,
                                                                                                     [[arrPlacesData valueForKey:@"place_desc_german"] objectAtIndex:i], DB_TABLEFIELD_DES_GERMAN,
                                                                                                     @"", DB_TABLEFIELD_AUDIO_GERMAN,
                                                                                                     [[arrPlacesData valueForKey:@"place_title_french"] objectAtIndex:i], DB_TABLEFIELD_NAME_FRENCH,
                                                                                                     [[arrPlacesData valueForKey:@"place_desc_french_none_html"] objectAtIndex:i], DB_TABLEFIELD_DES_FRENCH_NONE_HTML,
                                                                                                     [[arrPlacesData valueForKey:@"place_desc_french"] objectAtIndex:i], DB_TABLEFIELD_DES_FRENCH,
                                                                                                     @"", DB_TABLEFIELD_AUDIO_FRENCH,
                                                                                                     [[arrPlacesData valueForKey:@"place_title_spanish"] objectAtIndex:i], DB_TABLEFIELD_NAME_SPANISH,
                                                                                                     [[arrPlacesData valueForKey:@"place_desc_spanish_none_html"] objectAtIndex:i], DB_TABLEFIELD_DES_SPANISH_NONE_HTML,
                                                                                                     [[arrPlacesData valueForKey:@"place_desc_spanish"] objectAtIndex:i], DB_TABLEFIELD_DES_SPANISH,
                                                                                                     @"", DB_TABLEFIELD_AUDIO_SPANISH,
                                                                                                     [[arrPlacesData valueForKey:@"place_title_chinese"] objectAtIndex:i], DB_TABLEFIELD_NAME_CHINESE,
                                                                                                     [[arrPlacesData valueForKey:@"place_desc_chinese_none_html"] objectAtIndex:i], DB_TABLEFIELD_DES_CHINESE_NONE_HTML,
                                                                                                     [[arrPlacesData valueForKey:@"place_desc_chinese"] objectAtIndex:i], DB_TABLEFIELD_DES_CHINESE,
                                                                                                     @"", DB_TABLEFIELD_AUDIO_CHINESE,
                                                                                                     [[arrPlacesData valueForKey:@"place_title_danish"] objectAtIndex:i], DB_TABLEFIELD_NAME_DANISH,
                                                                                                     [[arrPlacesData valueForKey:@"place_desc_danish_none_html"] objectAtIndex:i], DB_TABLEFIELD_DES_DANISH_NONE_HTML,
                                                                                                     [[arrPlacesData valueForKey:@"place_desc_danish"] objectAtIndex:i], DB_TABLEFIELD_DES_DANISH,
                                                                                                     @"", DB_TABLEFIELD_AUDIO_DANISH,
                                                                                                     nil]];
                    }
                    
                }else {
                    
                    [self.dbHelper inserDataIntoTable:[NSString stringWithFormat:DB_TABLENAME_PLACEMASTER]
                                            tableData:[NSDictionary dictionaryWithObjectsAndKeys:
                                                       [[arrPlacesData valueForKey:@"place_id"] objectAtIndex:i], DB_TABLEFIELD_PLACEID,
                                                       [[arrPlacesData valueForKey:@"place_title"] objectAtIndex:i], DB_TABLEFIELD_NAME,
                                                       [[arrPlacesData valueForKey:@"place_title_islenska"] objectAtIndex:i], DB_TABLEFIELD_NAME_ISLENSKA,
                                                       [[arrPlacesData valueForKey:@"place_desc"] objectAtIndex:i], DB_TABLEFIELD_DESCRIPTION,
                                                       [[arrPlacesData valueForKey:@"place_desc_islenska"] objectAtIndex:i], DB_TABLEFIELD_DESCRIPTION_ISLENSKA,
                                                       [[arrPlacesData valueForKey:@"distance"] objectAtIndex:i], DB_TABLEFIELD_KM,
                                                       [[arrPlacesData valueForKey:@"latitude"] objectAtIndex:i], DB_TABLEFIELD_LATITUDE,
                                                       [[arrPlacesData valueForKey:@"longitude"] objectAtIndex:i], DB_TABLEFIELD_LOGITUDE,
                                                       [[arrPlacesData valueForKey:@"isLike"] objectAtIndex:i], DB_TABLEFIELD_LIKE,
                                                       [[arrPlacesData valueForKey:@"isVisited"] objectAtIndex:i], DB_TABLEFIELD_VISITED,
                                                       @"", DB_TABLEFIELD_AUDIO,
                                                       @"", DB_TABLEFIELD_AUDIO_ISLENSKA,
                                                       [arrLocalPlacesImg objectAtIndex:i], DB_TABLEFIELD_IMAGE,
                                                       [[arrPlacesData valueForKey:@"down_flag"] objectAtIndex:i], DB_TABLEFIELD_DOWNLOAD_FLAG,
                                                       [[arrPlacesData valueForKey:@"update_at"] objectAtIndex:i], DB_TABLEFIELD_UPDATE_AT,
                                                       
                                                       [[arrPlacesData valueForKey:@"place_desc_none_html"] objectAtIndex:i], DB_TABLEFIELD_DES_ENG_NONE_HTML,
                                                       [[arrPlacesData valueForKey:@"place_desc_islenska_none_html"] objectAtIndex:i], DB_TABLEFIELD_DES_ISLENSKA_NONE_HTML,
                                                       [[arrPlacesData valueForKey:@"place_title_german"] objectAtIndex:i], DB_TABLEFIELD_NAME_GERMAN,
                                                       [[arrPlacesData valueForKey:@"place_desc_german_none_html"] objectAtIndex:i], DB_TABLEFIELD_DES_GERMAN_NONE_HTML,
                                                       [[arrPlacesData valueForKey:@"place_desc_german"] objectAtIndex:i], DB_TABLEFIELD_DES_GERMAN,
                                                       @"", DB_TABLEFIELD_AUDIO_GERMAN,
                                                       [[arrPlacesData valueForKey:@"place_title_french"] objectAtIndex:i], DB_TABLEFIELD_NAME_FRENCH,
                                                       [[arrPlacesData valueForKey:@"place_desc_french_none_html"] objectAtIndex:i], DB_TABLEFIELD_DES_FRENCH_NONE_HTML,
                                                       [[arrPlacesData valueForKey:@"place_desc_french"] objectAtIndex:i], DB_TABLEFIELD_DES_FRENCH,
                                                       @"", DB_TABLEFIELD_AUDIO_FRENCH,
                                                       [[arrPlacesData valueForKey:@"place_title_spanish"] objectAtIndex:i], DB_TABLEFIELD_NAME_SPANISH,
                                                       [[arrPlacesData valueForKey:@"place_desc_spanish_none_html"] objectAtIndex:i], DB_TABLEFIELD_DES_SPANISH_NONE_HTML,
                                                       [[arrPlacesData valueForKey:@"place_desc_spanish"] objectAtIndex:i], DB_TABLEFIELD_DES_SPANISH,
                                                       @"", DB_TABLEFIELD_AUDIO_SPANISH,
                                                       [[arrPlacesData valueForKey:@"place_title_chinese"] objectAtIndex:i], DB_TABLEFIELD_NAME_CHINESE,
                                                       [[arrPlacesData valueForKey:@"place_desc_chinese_none_html"] objectAtIndex:i], DB_TABLEFIELD_DES_CHINESE_NONE_HTML,
                                                       [[arrPlacesData valueForKey:@"place_desc_chinese"] objectAtIndex:i], DB_TABLEFIELD_DES_CHINESE,
                                                       @"", DB_TABLEFIELD_AUDIO_CHINESE,
                                                       [[arrPlacesData valueForKey:@"place_title_danish"] objectAtIndex:i], DB_TABLEFIELD_NAME_DANISH,
                                                       [[arrPlacesData valueForKey:@"place_desc_danish_none_html"] objectAtIndex:i], DB_TABLEFIELD_DES_DANISH_NONE_HTML,
                                                       [[arrPlacesData valueForKey:@"place_desc_danish"] objectAtIndex:i], DB_TABLEFIELD_DES_DANISH,
                                                       @"", DB_TABLEFIELD_AUDIO_DANISH,
                                                       
                                                       [[arrPlacesData valueForKey:@"down_txt_eng"] objectAtIndex:i], DB_TABLEFIELD_DOWN_FLAG_EN,
                                                       [[arrPlacesData valueForKey:@"down_txt_islenska"] objectAtIndex:i], DB_TABLEFIELD_DOWN_FLAG_IS,
                                                       [[arrPlacesData valueForKey:@"down_txt_german"] objectAtIndex:i], DB_TABLEFIELD_DOWN_FLAG_GE,
                                                       [[arrPlacesData valueForKey:@"down_txt_french"] objectAtIndex:i], DB_TABLEFIELD_DOWN_FLAG_FR,
                                                       [[arrPlacesData valueForKey:@"down_txt_spanish"] objectAtIndex:i], DB_TABLEFIELD_DOWN_FLAG_ES,
                                                       [[arrPlacesData valueForKey:@"down_txt_chinese"] objectAtIndex:i], DB_TABLEFIELD_DOWN_FLAG_CH,
                                                       [[arrPlacesData valueForKey:@"down_txt_danish"] objectAtIndex:i], DB_TABLEFIELD_DOWN_FLAG_DA,
                                                       nil]];
                }
            }
            
            dispatch_sync(dispatch_get_main_queue(), ^{
                //Run UI Updates
//                [MBProgressHUD hideHUDForView:self.view animated:YES];
                [self stopIndicator];
                [_tblPlaceList reloadData];
            });
        });
        
        
//        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
//        }];

    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
       
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            
            [self stopIndicator];
            
            NSArray *arrGetMain = [userDefaults objectForKey:@"MainArrayData"];
            [arrPlacesData removeAllObjects];
            [arrPlacesData addObjectsFromArray: arrGetMain];
//            [_tblPlaceList reloadData];
            
            NSArray *arrGetImageMain = [userDefaults valueForKey:@"MainImageData"];
            [arrLocalPlacesImg removeAllObjects];
            [arrLocalPlacesImg addObjectsFromArray:arrGetImageMain];
            [_tblPlaceList reloadData];
            
        }];
    }];

}

- (void)getNewDistancePlacesList {
    
    _strLanguageCode = [userDefaults valueForKey:PREFERENCE_LANGUAGE];
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                       [NSNumber numberWithFloat:locationManager.location.coordinate.latitude], @"latitude",
                                       [NSNumber numberWithFloat:locationManager.location.coordinate.longitude], @"longitude",
                                       @"", @"code",
                                       nil];
    
    NSLog(@"Parameters is: %@",parameters);
    
    [APIHelper POST:kApiGetPlaces parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSLog(@"responseObject %@ :", responseObject);
        
        [arrNewPlaces addObjectsFromArray:[responseObject valueForKey:@"result"]];
        
        NSMutableDictionary *mutDict = [[NSMutableDictionary alloc] init];
        
        for (int j=0; j<arrNewPlaces.count; j++) {
            
            for (int k=0; k<arrPlacesData.count; k++) {
                
                NSString *strOldPlaceId = [[arrPlacesData valueForKey:@"place_id"] objectAtIndex:k];
                NSString *strNewPlaceId = [[arrNewPlaces valueForKey:@"place_id"] objectAtIndex:j];
                
                if (strOldPlaceId == strNewPlaceId) {
                    
                    mutDict = [arrPlacesData objectAtIndex:k];
                    NSMutableDictionary *dictMutCopy = [mutDict mutableCopy];
                    
                    [dictMutCopy setValue:[[arrNewPlaces valueForKey:@"distance"] objectAtIndex:j] forKey:@"distance"];
                    [arrPlacesData replaceObjectAtIndex:k withObject:dictMutCopy];
                    NSLog(@"Replaced");
                    
                }else {
                    NSLog(@"Not replaced");
                }
                
            }
            
        }
        
        NSLog(@"New array place data :: %@", arrPlacesData);
        
        NSSortDescriptor *distanceDescriptor = [[NSSortDescriptor alloc] initWithKey:@"distance" ascending:YES];
        NSArray *sortDescriptors = @[distanceDescriptor];
        
        NSArray *ordered = [arrPlacesData sortedArrayUsingDescriptors:sortDescriptors];
        
        [arrPlacesData removeAllObjects];
        [arrPlacesData addObjectsFromArray:ordered];
        
        [userDefaults setObject:arrPlacesData forKey:@"MainArrayData"];
        
        [self.tblPlaceList reloadData];
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            
            [self stopIndicator];
            
            NSArray *arrGetMain = [userDefaults objectForKey:@"MainArrayData"];
            [arrPlacesData removeAllObjects];
            [arrPlacesData addObjectsFromArray: arrGetMain];
            //            [_tblPlaceList reloadData];
            
            NSArray *arrGetImageMain = [userDefaults valueForKey:@"MainImageData"];
            [arrLocalPlacesImg removeAllObjects];
            [arrLocalPlacesImg addObjectsFromArray:arrGetImageMain];
            [_tblPlaceList reloadData];
            
        }];
    }];
    
}



@end

//
//  LanguageViewController.m
//  Thingvellir
//
//  Created by saturncube on 26/05/18.
//  Copyright © 2018 saturncube. All rights reserved.
//

#import "LanguageViewController.h"
#import "MenuCell.h"
#import "PlaceListViewController.h"

@interface LanguageViewController ()<UITableViewDelegate, UITableViewDataSource> {
    
    NSMutableArray *arrLanguages;
    NSArray *arrLanguageCode, *arrLanguageTitle;
   
    NSIndexPath *checkedIndexPath;
    NSString *selectedLanguageCode, *selectedLanguageTitle;
    
    NSDictionary *Diclabels;

    Utility *utility;
}

@property (strong, nonatomic) IBOutlet UILabel *navTitle;
@property (strong, nonatomic) IBOutlet UIButton *btnDone;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *navigationVwHeightContraint;
@property (strong, nonatomic) IBOutlet UITableView *tblLanguageList;
@property (nonatomic, retain) NSIndexPath *checkedIndexPath;

@end

@implementation LanguageViewController

@synthesize checkedIndexPath;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //============ Set Font ==================
    [_navTitle setFont:[UIFont fontWithName:FONT_ROBOTO_REGULAR size:15]];
    [_btnDone.titleLabel setFont:[UIFont fontWithName:FONT_ROBOTO_REGULAR size:15]];
    
    //============ Tableview ==================
    _tblLanguageList.tableFooterView = [UIView new];
    
    //============ Array initialised ==================
    arrLanguages = [[NSMutableArray alloc]init];
    
    utility = [[Utility alloc]init];
    
    //============ Navigationbar custom height ==================
    if (DEVICE_IS_IPHONE_X) {
        _navigationVwHeightContraint.constant = 84;
    }
    
    //============ API method call ==================
    [self getLanguageList];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
   
    //============ Set Data method call ==================
    [self setData];
}

#pragma mark ========
#pragma mark Set Data
#pragma mark ========

- (void)setData {
    
    NSDictionary *value = [[NSUserDefaults standardUserDefaults] objectForKey:PREFERENCE_LANGUAGE_LABEL];
    
    Diclabels = [[NSDictionary alloc] initWithDictionary:value];
    NSLog(@"Diclabels %@",Diclabels);
    
    //=============== Set data according selected language ===================
    
    if ([_strSelectedLanguage isEqualToString:@"txt_eng"]) {
        
        [_btnDone setTitle:[Diclabels valueForKey:@"txt_done"] forState:UIControlStateNormal];
        _navTitle.text = [Diclabels valueForKey:@"txt_change_language"];

    }else if([_strSelectedLanguage isEqualToString:@"txt_islenska"]) {
        _navTitle.text = [Diclabels valueForKey:@"txt_change_language"];
        [_btnDone setTitle:[Diclabels valueForKey:@"txt_done"] forState:UIControlStateNormal];

    }else if([_strSelectedLanguage isEqualToString:@"txt_German"]) {
        _navTitle.text = [Diclabels valueForKey:@"txt_change_language"];
        [_btnDone setTitle:[Diclabels valueForKey:@"txt_done"] forState:UIControlStateNormal];

    }else if([_strSelectedLanguage isEqualToString:@"txt_French"]) {
        _navTitle.text = [Diclabels valueForKey:@"txt_change_language"];
        [_btnDone setTitle:[Diclabels valueForKey:@"txt_done"] forState:UIControlStateNormal];

    }else if([_strSelectedLanguage isEqualToString:@"txt_Chinese"]) {
        _navTitle.text = [Diclabels valueForKey:@"txt_change_language"];
        [_btnDone setTitle:[Diclabels valueForKey:@"txt_done"] forState:UIControlStateNormal];

    }else if([_strSelectedLanguage isEqualToString:@"txt_Danish"]) {
        _navTitle.text = [Diclabels valueForKey:@"txt_change_language"];
        [_btnDone setTitle:[Diclabels valueForKey:@"txt_done"] forState:UIControlStateNormal];

    }else if([_strSelectedLanguage isEqualToString:@"txt_Spanish"]) {
        _navTitle.text = [Diclabels valueForKey:@"txt_change_language"];
        [_btnDone setTitle:[Diclabels valueForKey:@"txt_done"] forState:UIControlStateNormal];
    }
}

#pragma mark Button Actions

- (IBAction)btnBackTapped:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnDoneTapped:(UIButton *)sender {
    
    if ([utility isStringEmpty:selectedLanguageCode]) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:[Diclabels valueForKey:@"txt_thingvellir"] message:[Diclabels valueForKey:@"MSG_PLEASE_SELECT_LANGUAGE"] preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *ok = [UIAlertAction actionWithTitle:[Diclabels valueForKey:@"OK"] style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {}];
        
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
        
    }else {
        [self.delegate dataFromController:selectedLanguageCode];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"LanguageChanged"
                                                            object:self];
        UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
        PlaceListViewController *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"PlaceListViewController"];
        NSArray *controllers = [NSArray arrayWithObject:VC];
        navigationController.viewControllers = controllers;
        
    }
    
}


#pragma mark - TableView Datasource Delegate Event

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return arrLanguages.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    MenuCell *cell = (MenuCell *)[tableView dequeueReusableCellWithIdentifier:@"MenuCell"];
    
    cell.lblTitle.text = [[arrLanguages valueForKey:@"language_name"] objectAtIndex:indexPath.row];
//    cell.menuImgVw.image = [[arrLanguages valueForKey:@"language_flag"] objectAtIndex:indexPath.row];
    
    [cell.menuImgVw sd_setImageWithURL:[NSURL URLWithString: [[arrLanguages valueForKey:@"language_flag"] objectAtIndex:indexPath.row]] placeholderImage:[UIImage imageNamed:@"picture"]];
    
    [cell.lblTitle setFont:[UIFont fontWithName:FONT_ROBOTO_REGULAR size:14]];
    
//    cell.btnCheck.hidden = NO;
//    cell.btnCheck.tag = indexPath.row;
//    [cell.btnCheck addTarget:self action:@selector(btnCheckBoxTapped:) forControlEvents:UIControlEventTouchUpInside];
//    [[NSUserDefaults standardUserDefaults] setObject:_strSelectedLanguage forKey:PREFERENCE_LANGUAGE];

  
    if([self.checkedIndexPath isEqual:indexPath])
    {
        [cell.checkMarkImgVw setImage:[UIImage imageNamed:@"tick_image"]];
    }
    else
    {
        [cell.checkMarkImgVw setImage:[UIImage imageNamed:@"tick_image_blank"]];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
 
    if(self.checkedIndexPath)
    {
        MenuCell* uncheckCell = [tableView cellForRowAtIndexPath:self.checkedIndexPath];
        [uncheckCell.checkMarkImgVw setImage:[UIImage imageNamed:@"tick_image_blank"]];
    }
    
    MenuCell* cell = [tableView cellForRowAtIndexPath:indexPath];
    [cell.checkMarkImgVw setImage:[UIImage imageNamed:@"tick_image"]];

    self.checkedIndexPath = indexPath;
    
    selectedLanguageCode = [arrLanguageCode objectAtIndex:indexPath.row];

}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(self.checkedIndexPath)
    {
        MenuCell* uncheckCell = [tableView cellForRowAtIndexPath:self.checkedIndexPath];
        [uncheckCell.checkMarkImgVw setImage:[UIImage imageNamed:@"tick_image_blank"]];
    }
    
    if([self.checkedIndexPath isEqual:indexPath])
    {
        self.checkedIndexPath = nil;
    }
    else
    {
        MenuCell* cell = [tableView cellForRowAtIndexPath:indexPath];
        [cell.checkMarkImgVw setImage:[UIImage imageNamed:@"tick_image"]];
        self.checkedIndexPath = indexPath;
    }

}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)btnCheckBoxTapped:(UIButton *)sender {
//    MenuCell* cell = (MenuCell*)[sender superview];
//    NSIndexPath* indexPath = [_tblLanguageList indexPathForCell:cell];
    CGPoint touchPoint = [sender convertPoint:CGPointZero toView:_tblLanguageList];
    NSIndexPath *clickedButtonIndexPath = [_tblLanguageList indexPathForRowAtPoint:touchPoint];
    
    MenuCell *cell = [_tblLanguageList cellForRowAtIndexPath:clickedButtonIndexPath];
    
    if ([sender isSelected]) {
        [sender setSelected: NO];
        [cell.btnCheck setImage:[UIImage imageNamed:@"tick_image_blank"] forState:UIControlStateNormal];
        NSLog(@"Selected row: %ld", (long)clickedButtonIndexPath.row);
    } else {
        [sender setSelected: YES];
        [cell.btnCheck setImage:[UIImage imageNamed:@"tick_image"] forState:UIControlStateNormal];
        NSLog(@"Selected row: %ld", (long)clickedButtonIndexPath.row);
    }
}

#pragma mark -------------------------
#pragma mark API response
#pragma mark -------------------------

- (void)getLanguageList {
    [self showLoadingIndicator];
    
    [APIHelper GET:kApiGetLanguage parameters:[NSArray new] progress:^(NSProgress * _Nonnull downloadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"responseObject %@ :", responseObject);
        
        arrLanguages = (NSMutableArray *)[responseObject valueForKey:@"result"];
        arrLanguageCode = [arrLanguages valueForKey:@"code"];
        arrLanguageTitle = [arrLanguages valueForKey:@"language_name"];
        NSLog(@"languageCode %@",arrLanguageCode);
        
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            
            [self stopIndicator];
            
            [_tblLanguageList reloadData];
            
        }];

    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            
            [self stopIndicator];

            UIAlertController *alertController = [UIAlertController
                                                  alertControllerWithTitle:[Diclabels valueForKey:@"txt_thingvellir"]
                                                  message:nil
                                                  preferredStyle:UIAlertControllerStyleAlert
                                                  ];
            
            if (error.code == NSURLErrorNotConnectedToInternet) {
                alertController.message = [Diclabels valueForKey:@"MSG_INTERNET_UNAVAILABLE"];
            }else{
                alertController.message = [Diclabels valueForKey:@"MSG_REQUEST_FAIL"];
            }
            
            [alertController addAction:[UIAlertAction
                                        actionWithTitle:[Diclabels valueForKey:@"OK"]
                                        style:UIAlertActionStyleCancel
                                        handler:^(UIAlertAction * _Nonnull action) {
                                            [self.navigationController popViewControllerAnimated:YES];
                                        }]
             ];
            
            [alertController addAction:[UIAlertAction
                                        actionWithTitle:[Diclabels objectForKey:@"txt_retry"]
                                        style:UIAlertActionStyleDefault
                                        handler:^(UIAlertAction * _Nonnull action) {
                                            
                                            [self getLanguageList];
                                        }]
             ];
            
            [self presentViewController:alertController animated:YES completion:nil];
            
        }];
    }];
}

@end

//
//  PWSMapViewController.m
//  Thingvellir
//
//  Created by saturncube on 12/07/18.
//  Copyright © 2018 saturncube. All rights reserved.
//

#import "PWSMapViewController.h"
#import <GoogleMaps/GoogleMaps.h>
#import <CoreLocation/CoreLocation.h>

@interface PWSMapViewController ()<CLLocationManagerDelegate, GMSMapViewDelegate> {
    
    CLLocationCoordinate2D coordinates;
    CLLocation *currentLocation;
    
    CLLocationManager *locationManager;
    NSDictionary *Diclabels;
    
}

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *navigationVwHeightContraint;

@property (strong, nonatomic) IBOutlet UILabel *navTitle;
@property (strong, nonatomic) IBOutlet GMSMapView *mapView;

@property (strong, nonatomic) IBOutlet UISegmentedControl *changeMapTypeSegment;


@end

@implementation PWSMapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    _arrMutData = [[NSMutableArray alloc] init];
    
    NSLog(@"_arrMutData %@",_arrMutData);

    
    [self setData];
    
    //============ Get Current Location ==================
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.distanceFilter  = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [locationManager startUpdatingLocation];
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        [locationManager requestWhenInUseAuthorization];
    }
    
    [locationManager startUpdatingLocation];
  
    
    
    // Map pins
    NSArray *arrLatLongTemp = [_arrMutData valueForKey:@"locations"];
    
    GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] init];
    
    for (NSDictionary *dictionary in arrLatLongTemp)
    {
        coordinates.latitude = [dictionary[@"latitude"] floatValue];
        coordinates.longitude = [dictionary[@"longitude"] floatValue];
        // Creates a marker in the center of the map.
        
        GMSMarker *marker = [[GMSMarker alloc] init];
        
        if ([_strLocationType isEqualToString:@"2"]) {
            marker.icon = [UIImage imageNamed:(@"souvenier_shops_pin")];
            
        }else if ([_strLocationType isEqualToString:@"3"]) {
            marker.icon = [UIImage imageNamed:(@"map_parking")];
            
        }else if ([_strLocationType isEqualToString:@"4"]) {
            marker.icon = [UIImage imageNamed:(@"wc_pin.png")];
        
        }else if ([_strLocationType isEqualToString:@"5"]) {
            marker.icon = [UIImage imageNamed:(@"camping_pin.png")];
        }
        
        marker.position = CLLocationCoordinate2DMake(coordinates.latitude, coordinates.longitude);
        bounds = [bounds includingCoordinate:marker.position];
        //        marker.title = dictionary[@"place_title"];
        marker.map = _mapView;
    }
    
    [_mapView animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds withPadding:30.0f]];
    
//    ==================================
    
//    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:coordinates.latitude
//                                                            longitude:coordinates.longitude
//                                                                 zoom:12];
//    
    //============ Navigationbar and Map custom height ==================
    if (DEVICE_IS_IPHONE_X) {
        _navigationVwHeightContraint.constant = 84;
        
//        _mapView = [GMSMapView mapWithFrame:CGRectMake(_mapView.frame.origin.x , _mapView.frame.origin.y+20 , _mapView.frame.size.width, _mapView.frame.size.height+22) camera:camera];
        
    }else if (DEVICE_IS_IPHONE_4) {
//        _mapView = [GMSMapView mapWithFrame:CGRectMake(_mapView.frame.origin.x , _mapView.frame.origin.y , _mapView.frame.size.width, _mapView.frame.size.height) camera:camera];
    }else if (DEVICE_IS_IPHONE_5) {
//        _mapView = [GMSMapView mapWithFrame:CGRectMake(_mapView.frame.origin.x , _mapView.frame.origin.y , _mapView.frame.size.width, _mapView.frame.size.height) camera:camera];
    }else if (DEVICE_IS_IPHONE_6) {
//        _mapView = [GMSMapView mapWithFrame:CGRectMake(_mapView.frame.origin.x , _mapView.frame.origin.y , _mapView.frame.size.width, _mapView.frame.size.height) camera:camera];
        
        //        _changeMapTypeSegment = CGRectMake(_changeMapTypeSegment.frame.origin.x, _changeMapTypeSegment.frame.origin.y + 20, _changeMapTypeSegment.frame.size.width, _changeMapTypeSegment.frame.size.height);
        
    }else if (DEVICE_IS_IPHONE_6_PLUS) {
//        _mapView = [GMSMapView mapWithFrame:CGRectMake(_mapView.frame.origin.x , _mapView.frame.origin.y , _mapView.frame.size.width, _mapView.frame.size.height) camera:camera];
    }
  
//    [self.view addSubview: _mapView];
//    [self.view addSubview:_changeMapTypeSegment];

    
    
    _mapView.delegate = self;
    
    [_mapView setMyLocationEnabled:YES];
    
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [locationManager stopUpdatingLocation];
    locationManager = nil;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)setData {
    [_navTitle setFont:[UIFont fontWithName:FONT_ROBOTO_REGULAR size:15]];

    //============ Segment ==================
    _changeMapTypeSegment.layer.cornerRadius = 4;
    _changeMapTypeSegment.clipsToBounds = YES;
    
    _strLanguage = [[NSUserDefaults standardUserDefaults] valueForKey:PREFERENCE_LANGUAGE];
    
    NSDictionary *value = [[NSUserDefaults standardUserDefaults] objectForKey:PREFERENCE_LANGUAGE_LABEL];
    Diclabels = [[NSDictionary alloc] initWithDictionary:value];
    
    
    if ([_strLanguage isEqualToString:@"txt_eng"]) {
        
        _navTitle.text = _strNavTitle;
        [_changeMapTypeSegment setTitle:[Diclabels valueForKey:@"txt_normal"] forSegmentAtIndex:0];
        [_changeMapTypeSegment setTitle:[Diclabels valueForKey:@"txt_satellite"] forSegmentAtIndex:1];
        
    }else if([_strLanguage isEqualToString:@"txt_islenska"]) {
        
        _navTitle.text = _strNavTitle;
        [_changeMapTypeSegment setTitle:[Diclabels valueForKey:@"txt_normal"] forSegmentAtIndex:0];
        [_changeMapTypeSegment setTitle:[Diclabels valueForKey:@"txt_satellite"] forSegmentAtIndex:1];
        
    }else if([_strLanguage isEqualToString:@"txt_German"]) {
        
        _navTitle.text = _strNavTitle;
        [_changeMapTypeSegment setTitle:[Diclabels valueForKey:@"txt_normal"] forSegmentAtIndex:0];
        [_changeMapTypeSegment setTitle:[Diclabels valueForKey:@"txt_satellite"] forSegmentAtIndex:1];
        
    }else if([_strLanguage isEqualToString:@"txt_French"]) {
        
        _navTitle.text = _strNavTitle;
        [_changeMapTypeSegment setTitle:[Diclabels valueForKey:@"txt_normal"] forSegmentAtIndex:0];
        [_changeMapTypeSegment setTitle:[Diclabels valueForKey:@"txt_satellite"] forSegmentAtIndex:1];
        
    }else if([_strLanguage isEqualToString:@"txt_Chinese"]) {
        
        _navTitle.text = _strNavTitle;
        [_changeMapTypeSegment setTitle:[Diclabels valueForKey:@"txt_normal"] forSegmentAtIndex:0];
        [_changeMapTypeSegment setTitle:[Diclabels valueForKey:@"txt_satellite"] forSegmentAtIndex:1];
        
    }else if([_strLanguage isEqualToString:@"txt_Danish"]) {
        
        _navTitle.text = _strNavTitle;
        [_changeMapTypeSegment setTitle:[Diclabels valueForKey:@"txt_normal"] forSegmentAtIndex:0];
        [_changeMapTypeSegment setTitle:[Diclabels valueForKey:@"txt_satellite"] forSegmentAtIndex:1];
        
    }else if([_strLanguage isEqualToString:@"txt_Spanish"]) {
        
        _navTitle.text = _strNavTitle;
        [_changeMapTypeSegment setTitle:[Diclabels valueForKey:@"txt_normal"] forSegmentAtIndex:0];
        [_changeMapTypeSegment setTitle:[Diclabels valueForKey:@"txt_satellite"] forSegmentAtIndex:1];
        
    }
}


#pragma mark -------------------------
#pragma mark -- CLLocationManagerDelegate
#pragma mark -------------------------

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    NSLog(@"OldLocation %f %f", oldLocation.coordinate.latitude, oldLocation.coordinate.longitude);
    NSLog(@"NewLocation %f %f", newLocation.coordinate.latitude, newLocation.coordinate.longitude);
    coordinates = newLocation.coordinate;
}

- (IBAction)btnBackTapped:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark -------------------
#pragma mark --- Select Map Type
#pragma mark -------------------

- (IBAction)changeMapTypeSegmentTapped:(UISegmentedControl *)sender {
    NSInteger selectedSegment = sender.selectedSegmentIndex;
    
    if (selectedSegment == 0) {
        _mapView.mapType = kGMSTypeNormal;
    }else {
        _mapView.mapType = kGMSTypeSatellite;
    }
}

@end


//
//  BaseViewController.h
//  Thingvellir
//
//  Created by saturncube on 25/05/18.
//  Copyright © 2018 saturncube. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"

@interface BaseViewController : UIViewController

//@property (nonatomic,strong)MBProgressHUD   *progressIndicator;

-(IBAction)didTapBack:(id)sender;

/**
 * Show Loading Progress indicator
 */
- (void)showLoadingIndicator;

/**
 * Stop Progress indicator
 */
- (void)stopIndicator;

@end

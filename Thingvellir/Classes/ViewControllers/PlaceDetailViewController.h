//
//  PlaceDetailViewController.h
//  Thingvellir
//
//  Created by saturncube on 25/05/18.
//  Copyright © 2018 saturncube. All rights reserved.
//


#import "BaseViewController.h"

@interface PlaceDetailViewController : BaseViewController

@property NSDictionary *dicPlaceDetail;
@property NSArray *arrPlaceDetail, *arrDetailImages, *arrLargeDetailImages;
@property NSString *strSelectedLanguage, *strAudioPath, *strImagePath;
@property NSInteger selectedIndex;

@end

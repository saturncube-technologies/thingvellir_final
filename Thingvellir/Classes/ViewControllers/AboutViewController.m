//
//  AboutViewController.m
//  Thingvellir
//
//  Created by saturncube on 01/06/18.
//  Copyright © 2018 saturncube. All rights reserved.
//

#import "AboutViewController.h"
#import "LanguageViewController.h"
#import "PlaceListViewController.h"

@interface AboutViewController ()<UIWebViewDelegate, LanguageViewControllerDelegate> {
    
    NSMutableArray *arrDetail;
    NSString *decodedString, *base64String;
    NSDictionary *Diclabels;

}

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *navigationVwHeightContraint;

@property (strong, nonatomic) IBOutlet UILabel *naviTitle;
@property (strong, nonatomic) IBOutlet UIButton *btnLanguage;

@property (strong, nonatomic) IBOutlet UIImageView *logoImageView;
@property (strong, nonatomic) IBOutlet UILabel *lblAppName;
@property (strong, nonatomic) IBOutlet UILabel *lblAppVersion;

@property (strong, nonatomic) IBOutlet UILabel *lblDetail;

@property (strong, nonatomic) IBOutlet UIWebView *detailWebVw;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *webVwHeightContraint;

@end

@implementation AboutViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //============ Array initialised ==================
    arrDetail = [[NSMutableArray alloc]init];
    
    //============ Navigationbar custom height ==================
    if (DEVICE_IS_IPHONE_X) {
        _navigationVwHeightContraint.constant = 84;
    }
  
  
    [self getAboutData];
        
    NSDictionary *value = [[NSUserDefaults standardUserDefaults] objectForKey:PREFERENCE_LANGUAGE_LABEL];
    Diclabels = [[NSDictionary alloc] initWithDictionary:value];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidLayoutSubviews {
    
    [super viewDidLayoutSubviews];
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
    
        _logoImageView.layer.cornerRadius = 5;
        _logoImageView.clipsToBounds = YES;
        
        [_naviTitle setFont:[UIFont fontWithName:FONT_ROBOTO_REGULAR size:15]];
        [_btnLanguage.titleLabel setFont:[UIFont fontWithName:FONT_ROBOTO_REGULAR size:15]];
        
        _lblAppName.textColor = colorFromRGB(33, 33, 33);
        [_lblAppName setFont:[UIFont fontWithName:FONT_ROBOTO_REGULAR size:13]];
        
        _lblAppVersion.textColor = colorFromRGB(237, 33, 41);
        [_lblAppVersion setFont:[UIFont fontWithName:FONT_ROBOTO_REGULAR size:11]];
        
        _lblDetail.textColor = colorFromRGB(102, 102, 102);
        [_lblDetail setFont:[UIFont fontWithName:FONT_ROBOTO_REGULAR size:13]];
        
    }];
    
}

- (void)dataFromController:(NSString *)string {
    NSLog(@" string is : %@",string);
    _strLanguage = [NSString stringWithFormat:@"%@", string];
    [[NSUserDefaults standardUserDefaults] setObject:_strLanguage forKey:PREFERENCE_LANGUAGE];
    NSLog(@"_strLanguageCode : %@",_strLanguage);
}


#pragma mark -------------------------
#pragma mark Button Actions
#pragma mark -------------------------

- (IBAction)btnBackTapped:(UIButton *)sender {
    UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
    PlaceListViewController *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"PlaceListViewController"];
    NSArray *controllers = [NSArray arrayWithObject:VC];
    navigationController.viewControllers = controllers;
}

- (IBAction)btnSelectLanguageTapped:(UIButton *)sender {
    LanguageViewController *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"LanguageViewController"];
    VC.delegate = self;
    VC.strSelectedLanguage = _strLanguage;
    [self.navigationController pushViewController:VC animated:YES];
}

- (IBAction)btnShareTapped:(UIButton *)sender {
    
    NSURL *myWebsite = [NSURL URLWithString:[arrDetail valueForKey:@"shareable_app_link"]];
    
    NSArray *objectsToShare = @[myWebsite];
    
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
    
    activityVC.excludedActivityTypes = @[UIActivityTypeAirDrop,
                                         UIActivityTypePrint,
                                         UIActivityTypeAssignToContact,
                                         UIActivityTypeSaveToCameraRoll,
                                         UIActivityTypeAddToReadingList,
                                         UIActivityTypePostToFlickr,
                                         UIActivityTypePostToVimeo,
                                         UIActivityTypeMail,
                                         UIActivityTypePostToFacebook,
                                         UIActivityTypePostToTwitter,
                                         UIActivityTypeMessage,
                                         ];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:activityVC animated:YES completion:nil];
    });

    if (DEVICE_IS_IPAD) {
        activityVC.popoverPresentationController.sourceRect = CGRectMake(0, self.view.frame.size.height,[[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.width);
        activityVC.popoverPresentationController.sourceView = self.view;
    }

}


#pragma mark -------------------------
#pragma mark API response
#pragma mark -------------------------

- (void)getAboutData {
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                       _strType, @"type",
                                       nil];
    
    NSLog(@"Parameters is: %@",parameters);
    
    [self showLoadingIndicator];
    
    [APIHelper POST:kApiContentData parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSLog(@"responseObject %@ :", responseObject);
        
        arrDetail = (NSMutableArray *)[responseObject valueForKey:@"result"];
        
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            [self setData];
            [self stopIndicator];
            
        }];
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            
            [self stopIndicator];
            
            UIAlertController *alertController = [UIAlertController
                                                  alertControllerWithTitle:[Diclabels valueForKey:@"txt_thingvellir"]

                                                  message:nil
                                                  preferredStyle:UIAlertControllerStyleAlert
                                                  ];
            
            if (error.code == NSURLErrorNotConnectedToInternet) {
                alertController.message = [Diclabels objectForKey:@"MSG_INTERNET_UNAVAILABLE"];
            }else{
                alertController.message = [Diclabels objectForKey:@"MSG_REQUEST_FAIL"];
            }
            
            [alertController addAction:[UIAlertAction
                                        actionWithTitle:[Diclabels valueForKey:@"OK"]
                                        style:UIAlertActionStyleCancel
                                        handler:^(UIAlertAction * _Nonnull action) {
                                            [self.navigationController popViewControllerAnimated:YES];
                                        }]
             ];
            
            [alertController addAction:[UIAlertAction
                                        actionWithTitle:[Diclabels objectForKey:@"txt_retry"]
                                        style:UIAlertActionStyleDefault
                                        handler:^(UIAlertAction * _Nonnull action) {
                                            
                                            [self getAboutData];
                                        }]
             ];
            
            [self presentViewController:alertController animated:YES completion:nil];
            
        }];
    }];
}

#pragma mark -------------------------
#pragma mark SetData
#pragma mark -------------------------

- (void)setData {
    
    _naviTitle.text = _strNavTitle;
    
    [_logoImageView sd_setImageWithURL:[NSURL URLWithString:[arrDetail valueForKeyPath:@"image.image_name"]]];

    //    [userImageView setImageWithURL:[NSURL URLWithString:strImagePath] placeholderImage:[UIImage imageNamed:@"default-img-manage-profile"] options:SDWebImageRefreshCached usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
  
//    _lblDetail.text = [arrDetail valueForKey:@"content_desc"];
    
    
    //=============== Set data according selected language ===================
    
    if ([_strLanguage isEqualToString:@"txt_eng"]) {
        
        base64String = [arrDetail valueForKey:@"content_desc"];
        [_btnLanguage setTitle:@"EN" forState:UIControlStateNormal];

    }else if([_strLanguage isEqualToString:@"txt_islenska"]) {
        
        base64String = [arrDetail valueForKey:@"content_desc_islenska"];
        [_btnLanguage setTitle:@"IS" forState:UIControlStateNormal];

    }else if([_strLanguage isEqualToString:@"txt_German"]) {
        
        base64String = [arrDetail valueForKey:@"content_desc_german"];
        [_btnLanguage setTitle:@"DE" forState:UIControlStateNormal];

    }else if([_strLanguage isEqualToString:@"txt_French"]) {
        
        base64String = [arrDetail valueForKey:@"content_desc_french"];
        [_btnLanguage setTitle:@"FR" forState:UIControlStateNormal];

    }else if([_strLanguage isEqualToString:@"txt_Chinese"]) {
        
        base64String = [arrDetail valueForKey:@"content_desc_chinese"];
        [_btnLanguage setTitle:@"ZH" forState:UIControlStateNormal];

    }else if([_strLanguage isEqualToString:@"txt_Danish"]) {
        
        base64String = [arrDetail valueForKey:@"content_desc_danish"];
        [_btnLanguage setTitle:@"DA" forState:UIControlStateNormal];

    }else if([_strLanguage isEqualToString:@"txt_Spanish"]) {
        
        base64String = [arrDetail valueForKey:@"content_desc_spanish"];
        [_btnLanguage setTitle:@"ES" forState:UIControlStateNormal];

    }
    
    [self decodeString:base64String];
    [_detailWebVw loadHTMLString:decodedString baseURL:nil];

}

- (void)decodeString: (NSString *)base64String {
    
    NSData *decodedData = [[NSData alloc] initWithBase64EncodedString:base64String options:0];
    decodedString = [[NSString alloc] initWithData:decodedData encoding:NSUTF8StringEncoding];
    decodedString = [NSString stringWithFormat:@"<span style=\"font-family: %@; font-size: %i\">%@</span>",
                     @"Roboto-Regular",
                     14,
                     decodedString];
    NSLog(@"Decode String Value: %@", decodedString);
}

#pragma mark ----------------
#pragma mark WebView Delegate
#pragma mark ----------------

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    //    [MBProgressHUD showHUDAddedTo:self.detailWebVw animated:YES];
    CGFloat height = [[webView stringByEvaluatingJavaScriptFromString:@"document.body.scrollHeight"] floatValue];
    NSLog(@"Webview height is:: %f", height);
    _webVwHeightContraint.constant = height;
    //    [MBProgressHUD hideHUDForView:self.detailWebVw animated:YES];
}

@end

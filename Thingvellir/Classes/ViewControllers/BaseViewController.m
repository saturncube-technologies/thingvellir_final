//
//  BaseViewController.m
//  Thingvellir
//
//  Created by saturncube on 25/05/18.
//  Copyright © 2018 saturncube. All rights reserved.
//

#import "BaseViewController.h"
#import "UIImage+GIF.h"

@interface BaseViewController () {
    MBProgressHUD *HUD;
}

@end

@implementation BaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self configureNavigationBar];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) configureNavigationBar{
    
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.tintColor   = [UIColor whiteColor];
    [self.navigationController.navigationBar setBarTintColor:colorFromRGB(43 , 132, 210)];
    
//    UIImage *btnBackImage = [UIImage imageNamed:@"IconBack"];
//    _btnBack = [[UIBarButtonItem alloc] initWithImage:btnBackImage style:UIBarButtonItemStylePlain target:self action:@selector(btnBackTapped:)];
//    self.navigationItem.leftBarButtonItem = _btnBack;
    
}

- (void)btnBackTapped:(id)send{
    
}

- (BOOL)prefersStatusBarHidden{
    
    return NO;
}

- (UIStatusBarStyle)preferredStatusBarStyle{
    
    return UIStatusBarStyleLightContent;
}

#pragma mark - Button Click Event
-(IBAction)didTapBack:(id)sender
{
    [self.view endEditing:YES];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)showLoadingIndicator {
    
    [HUD hideAnimated:true];
    HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    HUD.bezelView.color = [UIColor clearColor];
    UIImageView *imageViewAnimatedGif = [[UIImageView alloc]init];
    
    //The key here is to save the GIF file or URL download directly into a NSData instead of making it a UIImage. Bypassing UIImage will let the GIF file keep the animation.
    NSString *filePath = [[NSBundle mainBundle] pathForResource: @"process-bar1" ofType: @"gif"];
    NSData *gifData = [NSData dataWithContentsOfFile: filePath];
    imageViewAnimatedGif.image = [UIImage sd_animatedGIFWithData:gifData];
    
    HUD.customView = [[UIImageView alloc] initWithImage:imageViewAnimatedGif.image];

    HUD.mode = MBProgressHUDModeCustomView;
//    HUD.contentColor = [UIColor redColor];
    HUD.backgroundColor = [UIColor clearColor];
    [HUD showAnimated:YES];
    
}

- (void)stopIndicator {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

@end

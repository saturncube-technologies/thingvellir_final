//
//  LeftMenuViewController.h
//  Thingvellir
//
//  Created by saturncube on 26/05/18.
//  Copyright © 2018 saturncube. All rights reserved.
//

#import "BaseViewController.h"

@interface LeftMenuViewController : BaseViewController

@property NSString *strLanguageCode;

@end

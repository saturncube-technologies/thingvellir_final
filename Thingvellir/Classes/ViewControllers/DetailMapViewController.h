//
//  DetailMapViewController.h
//  Thingvellir
//
//  Created by saturncube on 03/07/18.
//  Copyright © 2018 saturncube. All rights reserved.
//

#import "BaseViewController.h"

@interface DetailMapViewController : BaseViewController

@property NSDictionary *dicDetails;
@property NSString *strLanguage;

@property NSArray *arrDetails;

@end

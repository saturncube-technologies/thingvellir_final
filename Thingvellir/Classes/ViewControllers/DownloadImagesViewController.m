//
//  DownloadImagesViewController.m
//  Thingvellir
//
//  Created by saturncube on 01/06/18.
//  Copyright © 2018 saturncube. All rights reserved.
//

#import "DownloadImagesViewController.h"
#import "LanguageViewController.h"
#import "PlaceListViewController.h"
#import "ImagesCell.h"

@interface DownloadImagesViewController ()<UICollectionViewDelegate, UICollectionViewDataSource, LanguageViewControllerDelegate> {
    
    NSMutableArray *arrImages, *arrSelectedImages;
    NSDictionary *Diclabels;

}

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *navigationVwHeightContraint;

@property (strong, nonatomic) IBOutlet UILabel *naviTitle;
@property (strong, nonatomic) IBOutlet UIButton *btnLanguage;

@property (strong, nonatomic) IBOutlet UICollectionView *imagesCollectionVw;

@end

@implementation DownloadImagesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //============ Navigationbar custom height ==================
    if (DEVICE_IS_IPHONE_X) {
        _navigationVwHeightContraint.constant = 84;
    }
    
    //============ Array initialised ==================
    arrImages = [[NSMutableArray alloc] init];
    arrSelectedImages = [[NSMutableArray alloc] init];
    
    self.imagesCollectionVw.allowsMultipleSelection = YES;

    [self getAllImages];
    [self setData];
    
    
    NSDictionary *value = [[NSUserDefaults standardUserDefaults] objectForKey:PREFERENCE_LANGUAGE_LABEL];
    Diclabels = [[NSDictionary alloc] initWithDictionary:value];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dataFromController:(NSString *)string {
    NSLog(@" string is : %@",string);
    _strLanguage = [NSString stringWithFormat:@"%@", string];
    [[NSUserDefaults standardUserDefaults] setObject:_strLanguage forKey:PREFERENCE_LANGUAGE];
    NSLog(@"_strLanguageCode : %@",_strLanguage);
}

- (void)setData {
    _naviTitle.text = _strNavTitle;
    
    //=============== Set data according selected language ===================
    
    if ([_strLanguage isEqualToString:@"txt_eng"]) {
        
        [_btnLanguage setTitle:@"EN" forState:UIControlStateNormal];
        
    }else if([_strLanguage isEqualToString:@"txt_islenska"]) {
        
        [_btnLanguage setTitle:@"IS" forState:UIControlStateNormal];
        
    }else if([_strLanguage isEqualToString:@"txt_German"]) {
        
        [_btnLanguage setTitle:@"DE" forState:UIControlStateNormal];
        
    }else if([_strLanguage isEqualToString:@"txt_French"]) {
        
        [_btnLanguage setTitle:@"FR" forState:UIControlStateNormal];
        
    }else if([_strLanguage isEqualToString:@"txt_Chinese"]) {
        
        [_btnLanguage setTitle:@"ZH" forState:UIControlStateNormal];
        
    }else if([_strLanguage isEqualToString:@"txt_Danish"]) {
        
        [_btnLanguage setTitle:@"DA" forState:UIControlStateNormal];
        
    }else if([_strLanguage isEqualToString:@"txt_Spanish"]) {
        
        [_btnLanguage setTitle:@"ES" forState:UIControlStateNormal];
        
    }
}

#pragma mark Button Actions

- (IBAction)btnBackTapped:(UIButton *)sender {
    UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
    PlaceListViewController *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"PlaceListViewController"];
    NSArray *controllers = [NSArray arrayWithObject:VC];
    navigationController.viewControllers = controllers;
}

- (IBAction)btnSelectLanguageTapped:(UIButton *)sender {
    LanguageViewController *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"LanguageViewController"];
    VC.delegate = self;
    VC.strSelectedLanguage = _strLanguage;
    [self.navigationController pushViewController:VC animated:YES];
}

- (IBAction)btnDowloadImagesTapped:(UIButton *)sender {
    
    if (arrSelectedImages.count == 0) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:[Diclabels valueForKey:@"txt_thingvellir"] message:[Diclabels valueForKey:@"MSG_PLEASE_SELECT_IMAGE"] preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *ok = [UIAlertAction actionWithTitle:[Diclabels valueForKey:@"OK"] style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {}];
        
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
    }else
    
    [self showLoadingIndicator];
    
    for (int i=0; i<arrSelectedImages.count; i++) {

    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0),
                   ^{
                       NSURL *imageUrl = [NSURL URLWithString:[arrSelectedImages objectAtIndex:i]];
                       
                       dispatch_sync(dispatch_get_main_queue(), ^{
                          
                           [self stopIndicator];
                           
                           UIImage *aImage = [UIImage imageWithData:[NSData dataWithContentsOfURL:imageUrl]];
                           UIImageWriteToSavedPhotosAlbum(aImage, nil, nil, nil);
                           
                           UIAlertController *alert = [UIAlertController alertControllerWithTitle:[Diclabels valueForKey:@"txt_thingvellir"] message:[Diclabels valueForKey:@"MSG_PHOTO_SAVED_SUCCESSFULLY"] preferredStyle:UIAlertControllerStyleAlert];
                           
                           UIAlertAction *ok = [UIAlertAction actionWithTitle:[Diclabels valueForKey:@"OK"] style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                                                {
                                                    [_imagesCollectionVw reloadData];
                                                    [arrSelectedImages removeAllObjects];
                                                }];
                           
                           [alert addAction:ok];
                           [self presentViewController:alert animated:YES completion:nil];
                       });
                   });
    }

//    for (int i=0; i<arrSelectedImages.count; i++) {
//
//        NSURL *imageUrl = [NSURL URLWithString:[arrSelectedImages objectAtIndex:i]];
//        UIImage *aImage = [UIImage imageWithData:[NSData dataWithContentsOfURL:imageUrl]];
//        UIImageWriteToSavedPhotosAlbum(aImage, nil, nil, nil);
//
//    }
    
//    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
  

}


#pragma mark -------------------------
#pragma mark CollectionView Datasource Delegate Event
#pragma mark -------------------------

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return arrImages.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    ImagesCell *cell = (ImagesCell *)[_imagesCollectionVw dequeueReusableCellWithReuseIdentifier:@"ImagesCell" forIndexPath:indexPath];
    
    [cell.placeImageView sd_setImageWithURL:[NSURL URLWithString:[[arrImages valueForKey:@"place_img_name"] objectAtIndex:indexPath.row]]];
    
    cell.placeImageView.layer.cornerRadius = 3;
    cell.placeImageView.clipsToBounds = YES;
    

    if (cell.selectImageView.image == [UIImage imageNamed:@"tick_image_blank"]) {
        cell.selectImageView.image = [UIImage imageNamed:@"tick_image"];
    } else {
        cell.selectImageView.image = [UIImage imageNamed:@"tick_image_blank"];
    }
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (DEVICE_IS_IPHONE_X) {
        return CGSizeMake(_imagesCollectionVw.frame.size.width/3, _imagesCollectionVw.frame.size.height/6);
    }else if(DEVICE_IS_IPAD){
        return CGSizeMake(_imagesCollectionVw.frame.size.width/3, _imagesCollectionVw.frame.size.height/5);
    }else
    
    return CGSizeMake(_imagesCollectionVw.frame.size.width/3, _imagesCollectionVw.frame.size.height/5);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    ImagesCell *cell = (ImagesCell *)[collectionView cellForItemAtIndexPath:indexPath];
    
    if (arrSelectedImages.count > 9) {
      
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:[Diclabels valueForKey:@"txt_thingvellir"] message:[Diclabels valueForKey:@"MSG_PHOTO_SELECTION"] preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *ok = [UIAlertAction actionWithTitle:[Diclabels valueForKey:@"OK"] style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {}];

        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
   
    }else {
        NSLog(@"the clicked indexPath.row is - %ld",(long)indexPath.row);
        NSString *selectedImage = [[arrImages valueForKey:@"place_img_name_large"] objectAtIndex:indexPath.row];
        [arrSelectedImages addObject:selectedImage];
        NSLog(@"Array selected images %@",arrSelectedImages);
        [cell.selectImageView setImage:[UIImage imageNamed:@"tick_image"]];
    }

}

- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath {
    ImagesCell *cell = (ImagesCell *)[collectionView cellForItemAtIndexPath:indexPath];

    NSLog(@"the clicked indexPath.row is - %ld",(long)indexPath.row);
    NSString *deSelectedImage = [[arrImages valueForKey:@"place_img_name_large"] objectAtIndex:indexPath.row];
    [arrSelectedImages removeObject:deSelectedImage];
    NSLog(@"Array selected images %@",arrSelectedImages);
    [cell.selectImageView setImage:[UIImage imageNamed:@"tick_image_blank"]];
}

#pragma mark -------------------------
#pragma mark API response
#pragma mark -------------------------

- (void)getAllImages {
    
    [self showLoadingIndicator];

    [APIHelper GET:kApiGetAllImages parameters:[NSArray new] progress:^(NSProgress * _Nonnull downloadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"responseObject %@ :", responseObject);
        
        arrImages = (NSMutableArray *)[responseObject valueForKey:@"result"];
        
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            
            [self stopIndicator];
            [_imagesCollectionVw reloadData];
            
        }];
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
       
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            
            [self stopIndicator];
            
            UIAlertController *alertController = [UIAlertController
                                                  alertControllerWithTitle:[Diclabels valueForKey:@"txt_thingvellir"]
                                                  message:nil
                                                  preferredStyle:UIAlertControllerStyleAlert
                                                  ];
            
            if (error.code == NSURLErrorNotConnectedToInternet) {
                alertController.message = [Diclabels objectForKey:@"MSG_INTERNET_UNAVAILABLE"];
            }else{
                alertController.message = [Diclabels objectForKey:@"MSG_REQUEST_FAIL"];
            }
            
            [alertController addAction:[UIAlertAction
                                        actionWithTitle:[Diclabels valueForKey:@"OK"]
                                        style:UIAlertActionStyleCancel
                                        handler:^(UIAlertAction * _Nonnull action) {
                                            [self.navigationController popViewControllerAnimated:YES];
                                        }]
             ];
            
            [alertController addAction:[UIAlertAction
                                        actionWithTitle:[Diclabels objectForKey:@"txt_retry"]
                                        style:UIAlertActionStyleDefault
                                        handler:^(UIAlertAction * _Nonnull action) {
                                            
                                            [self getAllImages];
                                        }]
             ];
            
            [self presentViewController:alertController animated:YES completion:nil];
            
        }];
    }];
}

@end

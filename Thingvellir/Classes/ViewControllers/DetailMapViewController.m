//
//  DetailMapViewController.m
//  Thingvellir
//
//  Created by saturncube on 03/07/18.
//  Copyright © 2018 saturncube. All rights reserved.
//

#import "DetailMapViewController.h"

#import <GoogleMaps/GoogleMaps.h>
#import <CoreLocation/CoreLocation.h>

#import "Utility.h"
#import "DBHelper.h"

@interface DetailMapViewController ()<CLLocationManagerDelegate, GMSMapViewDelegate> {
    
    CLLocationCoordinate2D coordinates;
    CLLocation *currentLocation;
    
    CLLocationManager *locationManager;
    NSDictionary *Diclabels;
    
    Utility *utility;
    
}

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *navigationVwHeightContraint;

@property (strong, nonatomic) IBOutlet UILabel *navTitle;
@property (strong, nonatomic) IBOutlet GMSMapView *mapView;

@property (strong, nonatomic) IBOutlet UISegmentedControl *changeMapTypeSegment;

@property (nonatomic, strong) DBHelper *dbHelper;

@end

@implementation DetailMapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.dbHelper = [[DBHelper alloc] init];

    utility = [[Utility alloc] init];
  
    NSLog(@"_dicDetails %@",_dicDetails);
    NSLog(@"_arrDetails %@",_arrDetails);
    
    
    //============ Get Current Location ==================
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.distanceFilter  = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [locationManager startUpdatingLocation];
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        [locationManager requestWhenInUseAuthorization];
    }
    
    [locationManager startUpdatingLocation];
    //    coordinates = CLLocationCoordinate2DMake(locationManager.location.coordinate.latitude, locationManager.location.coordinate.longitude);
    

    _strLanguage = [[NSUserDefaults standardUserDefaults] valueForKey:PREFERENCE_LANGUAGE];

    if ([utility isInternetConnected]) {
        
        int columnNum = 0;
        
        if ([_strLanguage isEqualToString:@"txt_eng"]) {
            columnNum = 38;
            
        }else if([_strLanguage isEqualToString:@"txt_islenska"]) {
            columnNum = 39;
            
        }else if([_strLanguage isEqualToString:@"txt_German"]) {
            columnNum = 40;
            
        }else if([_strLanguage isEqualToString:@"txt_French"]) {
            columnNum = 41;
            
        }else if([_strLanguage isEqualToString:@"txt_Spanish"]) {
            columnNum = 42;
            
        }else if([_strLanguage isEqualToString:@"txt_Chinese"]) {
            columnNum = 43;
            
        }else if([_strLanguage isEqualToString:@"txt_Danish"]) {
            columnNum = 44;
        }
        
        NSString *strDownloadStatus = [NSString stringWithFormat:@"%@", [[_arrDetails objectAtIndex:0] objectAtIndex:columnNum]];
        
        if ([strDownloadStatus isEqualToString:@"1"]) {
        
            NSString *strLatitudeDes = [NSString stringWithFormat:@"%@", [[_arrDetails objectAtIndex:0] objectAtIndex:7]];
            NSString *strLongtudeDes = [NSString stringWithFormat:@"%@", [[_arrDetails objectAtIndex:0] objectAtIndex:8]];
            
            coordinates.latitude = [strLatitudeDes doubleValue];
            coordinates.longitude = [strLongtudeDes doubleValue];
            
            
            [self setOfflineData];

        }else {
            coordinates.latitude = [[_dicDetails valueForKey:@"latitude"] floatValue];
            coordinates.longitude = [[_dicDetails valueForKey:@"longitude"] floatValue];
            
            [self setData];
            
        }

    }else {
        
        NSString *strLatitudeDes = [NSString stringWithFormat:@"%@", [[_arrDetails objectAtIndex:0] objectAtIndex:7]];
        NSString *strLongtudeDes = [NSString stringWithFormat:@"%@", [[_arrDetails objectAtIndex:0] objectAtIndex:8]];
        
        coordinates.latitude = [strLatitudeDes doubleValue];
        coordinates.longitude = [strLongtudeDes doubleValue];
        
        [self setOfflineData];
    }
 
    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:coordinates.latitude
                                                            longitude:coordinates.longitude
                                                                 zoom:12];
   
    //============ Navigationbar and Map custom height ==================
    if (DEVICE_IS_IPHONE_X) {
        _navigationVwHeightContraint.constant = 84;
        
        _mapView = [GMSMapView mapWithFrame:CGRectMake(_mapView.frame.origin.x , _mapView.frame.origin.y+20 , _mapView.frame.size.width, _mapView.frame.size.height+22) camera:camera];
        
    }else if (DEVICE_IS_IPHONE_4) {
         _mapView = [GMSMapView mapWithFrame:CGRectMake(_mapView.frame.origin.x , _mapView.frame.origin.y , _mapView.frame.size.width, _mapView.frame.size.height) camera:camera];
    }else if (DEVICE_IS_IPHONE_5) {
         _mapView = [GMSMapView mapWithFrame:CGRectMake(_mapView.frame.origin.x , _mapView.frame.origin.y , _mapView.frame.size.width, _mapView.frame.size.height) camera:camera];
    }else if (DEVICE_IS_IPHONE_6) {
         _mapView = [GMSMapView mapWithFrame:CGRectMake(_mapView.frame.origin.x , _mapView.frame.origin.y , _mapView.frame.size.width, _mapView.frame.size.height) camera:camera];
        
//        _changeMapTypeSegment = CGRectMake(_changeMapTypeSegment.frame.origin.x, _changeMapTypeSegment.frame.origin.y + 20, _changeMapTypeSegment.frame.size.width, _changeMapTypeSegment.frame.size.height);
        
    }else if (DEVICE_IS_IPHONE_6_PLUS) {
         _mapView = [GMSMapView mapWithFrame:CGRectMake(_mapView.frame.origin.x , _mapView.frame.origin.y , _mapView.frame.size.width, _mapView.frame.size.height) camera:camera];
    }
    else if (DEVICE_IS_IPAD) {
       
    }
    
    [self.view addSubview: _mapView];
    [self.view addSubview:_changeMapTypeSegment];
   
    GMSMarker *marker = [[GMSMarker alloc] init];
    
    marker.position = CLLocationCoordinate2DMake(coordinates.latitude, coordinates.longitude);
    marker.icon = [UIImage imageNamed:(@"map_pin_selected.png")];
    marker.map = _mapView;
  
    _mapView.delegate = self;
    
    [_mapView setMyLocationEnabled:YES];
    
}

- (void)setData {
    [_navTitle setFont:[UIFont fontWithName:FONT_ROBOTO_REGULAR size:15]];

    //============ Segment ==================
    _changeMapTypeSegment.layer.cornerRadius = 4;
    _changeMapTypeSegment.clipsToBounds = YES;
    
    _strLanguage = [[NSUserDefaults standardUserDefaults] valueForKey:PREFERENCE_LANGUAGE];

    NSDictionary *value = [[NSUserDefaults standardUserDefaults] objectForKey:PREFERENCE_LANGUAGE_LABEL];
    Diclabels = [[NSDictionary alloc] initWithDictionary:value];
    
    if ([_strLanguage isEqualToString:@"txt_eng"]) {
        
        _navTitle.text = [_dicDetails valueForKey:@"place_title"];
        [_changeMapTypeSegment setTitle:[Diclabels valueForKey:@"txt_normal"] forSegmentAtIndex:0];
        [_changeMapTypeSegment setTitle:[Diclabels valueForKey:@"txt_satellite"] forSegmentAtIndex:1];

    }else if([_strLanguage isEqualToString:@"txt_islenska"]) {
        
        _navTitle.text = [_dicDetails valueForKey:@"place_title_islenska"];

        [_changeMapTypeSegment setTitle:[Diclabels valueForKey:@"txt_normal"] forSegmentAtIndex:0];
        [_changeMapTypeSegment setTitle:[Diclabels valueForKey:@"txt_satellite"] forSegmentAtIndex:1];
        
    }else if([_strLanguage isEqualToString:@"txt_German"]) {
        
        _navTitle.text = [_dicDetails valueForKey:@"place_title_german"];
        [_changeMapTypeSegment setTitle:[Diclabels valueForKey:@"txt_normal"] forSegmentAtIndex:0];
        [_changeMapTypeSegment setTitle:[Diclabels valueForKey:@"txt_satellite"] forSegmentAtIndex:1];
        
    }else if([_strLanguage isEqualToString:@"txt_French"]) {
        
        _navTitle.text = [_dicDetails valueForKey:@"place_title_french"];
        [_changeMapTypeSegment setTitle:[Diclabels valueForKey:@"txt_normal"] forSegmentAtIndex:0];
        [_changeMapTypeSegment setTitle:[Diclabels valueForKey:@"txt_satellite"] forSegmentAtIndex:1];
        
    }else if([_strLanguage isEqualToString:@"txt_Chinese"]) {
        
        _navTitle.text = [_dicDetails valueForKey:@"place_title_chinese"];
        [_changeMapTypeSegment setTitle:[Diclabels valueForKey:@"txt_normal"] forSegmentAtIndex:0];
        [_changeMapTypeSegment setTitle:[Diclabels valueForKey:@"txt_satellite"] forSegmentAtIndex:1];

    }else if([_strLanguage isEqualToString:@"txt_Danish"]) {
        
        _navTitle.text = [_dicDetails valueForKey:@"place_title_danish"];
        [_changeMapTypeSegment setTitle:[Diclabels valueForKey:@"txt_normal"] forSegmentAtIndex:0];
        [_changeMapTypeSegment setTitle:[Diclabels valueForKey:@"txt_satellite"] forSegmentAtIndex:1];

    }else if([_strLanguage isEqualToString:@"txt_Spanish"]) {
        
        _navTitle.text = [_dicDetails valueForKey:@"place_title_spanish"];
        [_changeMapTypeSegment setTitle:[Diclabels valueForKey:@"txt_normal"] forSegmentAtIndex:0];
        [_changeMapTypeSegment setTitle:[Diclabels valueForKey:@"txt_satellite"] forSegmentAtIndex:1];

    }
}

- (void)setOfflineData {
    
    NSInteger indexOfTitleGr = [self.dbHelper.arrColumnNames indexOfObject:DB_TABLEFIELD_NAME_GERMAN];
    NSInteger indexOfTitleFr = [self.dbHelper.arrColumnNames indexOfObject:DB_TABLEFIELD_NAME_FRENCH];
    NSInteger indexOfTitleEs = [self.dbHelper.arrColumnNames indexOfObject:DB_TABLEFIELD_NAME_SPANISH];
    NSInteger indexOfTitleCh = [self.dbHelper.arrColumnNames indexOfObject:DB_TABLEFIELD_NAME_CHINESE];
    NSInteger indexOfTitleDa = [self.dbHelper.arrColumnNames indexOfObject:DB_TABLEFIELD_NAME_DANISH];
    
    //============ Segment ==================
    _changeMapTypeSegment.layer.cornerRadius = 4;
    _changeMapTypeSegment.clipsToBounds = YES;
    
    _strLanguage = [[NSUserDefaults standardUserDefaults] valueForKey:PREFERENCE_LANGUAGE];
    
    NSDictionary *value = [[NSUserDefaults standardUserDefaults] objectForKey:PREFERENCE_LANGUAGE_LABEL];
    Diclabels = [[NSDictionary alloc] initWithDictionary:value];
    
    if ([_strLanguage isEqualToString:@"txt_eng"]) {
        
        _navTitle.text = [[_arrDetails objectAtIndex:0] objectAtIndex:2];//[_dicDetails valueForKey:@"place_title"];
        [_changeMapTypeSegment setTitle:[Diclabels valueForKey:@"txt_normal"] forSegmentAtIndex:0];
        [_changeMapTypeSegment setTitle:[Diclabels valueForKey:@"txt_satellite"] forSegmentAtIndex:1];
        
    }else if([_strLanguage isEqualToString:@"txt_islenska"]) {
        
        _navTitle.text = [[_arrDetails objectAtIndex:0] objectAtIndex:3];
        
        [_changeMapTypeSegment setTitle:[Diclabels valueForKey:@"txt_normal"] forSegmentAtIndex:0];
        [_changeMapTypeSegment setTitle:[Diclabels valueForKey:@"txt_satellite"] forSegmentAtIndex:1];
        
    }else if([_strLanguage isEqualToString:@"txt_German"]) {
        
        _navTitle.text = [[_arrDetails objectAtIndex:0] objectAtIndex:indexOfTitleGr];
        [_changeMapTypeSegment setTitle:[Diclabels valueForKey:@"txt_normal"] forSegmentAtIndex:0];
        [_changeMapTypeSegment setTitle:[Diclabels valueForKey:@"txt_satellite"] forSegmentAtIndex:1];
        
    }else if([_strLanguage isEqualToString:@"txt_French"]) {
        
        _navTitle.text = [[_arrDetails objectAtIndex:0] objectAtIndex:indexOfTitleFr];
        [_changeMapTypeSegment setTitle:[Diclabels valueForKey:@"txt_normal"] forSegmentAtIndex:0];
        [_changeMapTypeSegment setTitle:[Diclabels valueForKey:@"txt_satellite"] forSegmentAtIndex:1];
        
    }else if([_strLanguage isEqualToString:@"txt_Chinese"]) {
        
        _navTitle.text = [[_arrDetails objectAtIndex:0] objectAtIndex:indexOfTitleCh];
        [_changeMapTypeSegment setTitle:[Diclabels valueForKey:@"txt_normal"] forSegmentAtIndex:0];
        [_changeMapTypeSegment setTitle:[Diclabels valueForKey:@"txt_satellite"] forSegmentAtIndex:1];
        
    }else if([_strLanguage isEqualToString:@"txt_Danish"]) {
        
        _navTitle.text = [[_arrDetails objectAtIndex:0] objectAtIndex:indexOfTitleDa];
        [_changeMapTypeSegment setTitle:[Diclabels valueForKey:@"txt_normal"] forSegmentAtIndex:0];
        [_changeMapTypeSegment setTitle:[Diclabels valueForKey:@"txt_satellite"] forSegmentAtIndex:1];
        
    }else if([_strLanguage isEqualToString:@"txt_Spanish"]) {
        
        _navTitle.text = [[_arrDetails objectAtIndex:0] objectAtIndex:indexOfTitleEs];
        [_changeMapTypeSegment setTitle:[Diclabels valueForKey:@"txt_normal"] forSegmentAtIndex:0];
        [_changeMapTypeSegment setTitle:[Diclabels valueForKey:@"txt_satellite"] forSegmentAtIndex:1];
        
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -------------------------
#pragma mark -- CLLocationManagerDelegate
#pragma mark -------------------------

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    NSLog(@"OldLocation %f %f", oldLocation.coordinate.latitude, oldLocation.coordinate.longitude);
    NSLog(@"NewLocation %f %f", newLocation.coordinate.latitude, newLocation.coordinate.longitude);
    coordinates = newLocation.coordinate;
}

- (IBAction)btnBackTapped:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark -------------------
#pragma mark --- Select Map Type
#pragma mark -------------------

- (IBAction)changeMapTypeSegmentTapped:(UISegmentedControl *)sender {
    NSInteger selectedSegment = sender.selectedSegmentIndex;
    
    if (selectedSegment == 0) {
        _mapView.mapType = kGMSTypeNormal;
    }else {
        _mapView.mapType = kGMSTypeSatellite;
    }
}

@end

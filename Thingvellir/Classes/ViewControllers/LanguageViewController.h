//
//  LanguageViewController.h
//  Thingvellir
//
//  Created by saturncube on 26/05/18.
//  Copyright © 2018 saturncube. All rights reserved.
//

#import "BaseViewController.h"

@protocol LanguageViewControllerDelegate <NSObject>

@required
- (void)dataFromController:(NSString *)string;
@end

@interface LanguageViewController : BaseViewController

@property (nonatomic, weak) id<LanguageViewControllerDelegate> delegate;

@property NSString *strSelectedLanguage;

@end

//
//  AdmissionInfoViewController.h
//  Thingvellir
//
//  Created by saturncube on 01/06/18.
//  Copyright © 2018 saturncube. All rights reserved.
//

#import "BaseViewController.h"

@interface AdmissionInfoViewController : BaseViewController

@property NSString *strType, *strNavTitle, *strLanguage;

@end

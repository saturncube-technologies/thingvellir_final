//
//  ParkingWCShopViewController.h
//  Thingvellir
//
//  Created by saturncube on 01/06/18.
//  Copyright © 2018 saturncube. All rights reserved.
//

#import "BaseViewController.h"

@interface ParkingWCShopViewController : BaseViewController

@property NSString *strLocationType, *strNavTitle, *strLanguage;

@end

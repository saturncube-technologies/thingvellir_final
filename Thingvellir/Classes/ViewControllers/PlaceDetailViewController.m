//
//  PlaceDetailViewController.m
//  Thingvellir
//
//  Created by saturncube on 25/05/18.
//  Copyright © 2018 saturncube. All rights reserved.
//

#import "PlaceDetailViewController.h"
#import "LanguageViewController.h"
#import "ImagesCell.h"
#import "DetailMapViewController.h"

#import <GoogleMaps/GoogleMaps.h>
#import <CoreLocation/CoreLocation.h>
#import <AudioToolbox/AudioToolbox.h>
#import <AVFoundation/AVFoundation.h>

#import "DBHelper.h"
#import "UIImage+Resize.h"

@interface PlaceDetailViewController ()<AVAudioPlayerDelegate, CLLocationManagerDelegate, GMSMapViewDelegate, UIWebViewDelegate, LanguageViewControllerDelegate> {

    NSMutableArray *arrImages, *arrLocalPlacesImg, *arrLargeImgs, *arrAudioPath, *arrDetailImgs, *arrDetailImgsPath;
    CLLocationCoordinate2D coordinates;
    CLLocation *currentLocation;
    CLLocationManager *locationManager;
    
    NSString *decodedString, *base64String, *strPlaceId, *strAudioUrl, *anImagePath, *anAudioPath;
    
    NSURL *audioURL;
    NSData *pngData, *pngData1;
//    NSArray *paths;
    Utility *utility;
    
    NSDictionary *Diclabels;
    
    NSUInteger index;

    BOOL playing;
    
    NSDateFormatter *formatter;
    
}

@property (strong, nonatomic) IBOutlet UIView *mainImgVw;

@property (nonatomic, strong) DBHelper *dbHelper;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *navigationVwHeightContraint;
@property (strong, nonatomic) IBOutlet UILabel *navTitle;
@property (strong, nonatomic) IBOutlet UIButton *btnLanguage;

@property (strong) AVAudioPlayer *audioPlayer;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *imgVwHeightContraint;

@property (weak, nonatomic) IBOutlet UIView *firstVw;
@property (weak, nonatomic) IBOutlet UISwitch *btnSwitch;
@property (weak, nonatomic) IBOutlet UILabel *lblPlaceVisited;
@property (strong, nonatomic) IBOutlet UIButton *btnSaveImages;

@property (weak, nonatomic) IBOutlet UIView *secondVw;
@property (weak, nonatomic) IBOutlet UIButton *btnPlay;
@property (weak, nonatomic) IBOutlet UILabel *lblStartTime;
@property (weak, nonatomic) IBOutlet UIProgressView *audioProgressVw;
@property (weak, nonatomic) IBOutlet UILabel *lblEndTime;

@property (weak, nonatomic) IBOutlet UIView *thirdVw;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblKm;
@property (strong, nonatomic) IBOutlet UIButton *btnLike;
@property (strong, nonatomic) IBOutlet UIImageView *likeImgVw;
@property (strong, nonatomic) IBOutlet UIButton *btnDownload;
@property (strong, nonatomic) IBOutlet UIImageView *downloadImgVw;
@property (strong, nonatomic) IBOutlet UILabel *lblDetail;
@property (strong, nonatomic) IBOutlet UIWebView *detailWebVw;

@property (strong, nonatomic) IBOutlet UIView *fourthVw;
@property (strong, nonatomic) IBOutlet GMSMapView *mapVw;
@property (nonatomic, strong) NSTimer *timer;
@property (strong, nonatomic) IBOutlet UISlider *audioSlider;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *fourthVwHeightConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *webVwHeightContraint;

@property (strong, nonatomic) IBOutlet UISegmentedControl *changeMapTypeSegment;
@property (strong, nonatomic) IBOutlet UIButton *btnFullScreenMap;
@property (strong, nonatomic) IBOutlet UIImageView *detailImageVw;
@property (strong, nonatomic) IBOutlet UIButton *btnPrev;
@property (strong, nonatomic) IBOutlet UIButton *btnNext;

@property (strong, nonatomic) NSArray *arrPlacesImages;

@end

@implementation PlaceDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.dbHelper = [[DBHelper alloc] init];
    
    index = 0;
    playing = NO;
    
    //============ Array initialised ==================
    arrImages         = [[NSMutableArray alloc] init];
    arrLocalPlacesImg = [[NSMutableArray alloc] init];
    arrAudioPath      = [[NSMutableArray alloc] init];
    arrLargeImgs      = [[NSMutableArray alloc] init];

    arrDetailImgs     = [[NSMutableArray alloc] init];
    arrDetailImgsPath = [[NSMutableArray alloc] init];
    utility = [[Utility alloc]init];
    
    _detailWebVw.delegate = self;
    
    NSLog(@"_arrLargeDetailImages %@",_arrLargeDetailImages);

    NSLog(@"_dicPlaceDetail %@",_dicPlaceDetail);
    
    _arrPlaceDetail = [[NSUserDefaults standardUserDefaults] valueForKey:@"Db"];
    
    NSLog(@"_arrPlaceDetail %@",_arrPlaceDetail);

    
    //============ Navigationbar and collectionview custom height ==================
    if (DEVICE_IS_IPHONE_X) {
        _navigationVwHeightContraint.constant = 84;
    }else if (DEVICE_IS_IPHONE_4) {
    }else if (DEVICE_IS_IPHONE_5) {
        _imgVwHeightContraint.constant = 190;
        _fourthVwHeightConstraint.constant = 220;
        _webVwHeightContraint.constant = 100;
    }else if (DEVICE_IS_IPHONE_6) {
    }else if (DEVICE_IS_IPHONE_6_PLUS) {
    }else if (DEVICE_IS_IPAD) {
        _imgVwHeightContraint.constant = 390;
        _fourthVwHeightConstraint.constant = 350;
        _webVwHeightContraint.constant = 100;
    }
    
    //============ Get Current Location ==================
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.distanceFilter  = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [locationManager startUpdatingLocation];
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        [locationManager requestWhenInUseAuthorization];
    }
    
    [locationManager startUpdatingLocation];
    
    coordinates = CLLocationCoordinate2DMake(locationManager.location.coordinate.latitude, locationManager.location.coordinate.longitude);
    
    _mapVw.delegate = self;

    [_mapVw setMyLocationEnabled:YES];

    [_btnPrev setEnabled:NO];
    
   
    //============ Audio Player ==================
    _strSelectedLanguage = [[NSUserDefaults standardUserDefaults] valueForKey:PREFERENCE_LANGUAGE];
    NSInteger anIndex = 0;
    
    formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd_MM_yyyy_HH_mm_ss"];
    
    [[AVAudioSession sharedInstance]
     setCategory: AVAudioSessionCategoryPlayback
     error: nil];
    //=========== Audio store in Directory =================
    
    NSError *error = nil;
    
    if ([utility isInternetConnected]) {
        
        int columnNum = 0;
        
        if ([_strSelectedLanguage isEqualToString:@"txt_eng"]) {
            columnNum = 38;
            
        }else if([_strSelectedLanguage isEqualToString:@"txt_islenska"]) {
            columnNum = 39;
            
        }else if([_strSelectedLanguage isEqualToString:@"txt_German"]) {
            columnNum = 40;
            
        }else if([_strSelectedLanguage isEqualToString:@"txt_French"]) {
            columnNum = 41;
            
        }else if([_strSelectedLanguage isEqualToString:@"txt_Spanish"]) {
            columnNum = 42;
            
        }else if([_strSelectedLanguage isEqualToString:@"txt_Chinese"]) {
            columnNum = 43;
            
        }else if([_strSelectedLanguage isEqualToString:@"txt_Danish"]) {
            columnNum = 44;
        }
        
        NSString *strDownloadStatus = [NSString stringWithFormat:@"%@", [[_arrPlaceDetail objectAtIndex:0] objectAtIndex:columnNum]];
        //        NSString *strDownloadStatus = [[_arrPlaceDetail objectAtIndex:0] objectAtIndex:14];
        
        if ([strDownloadStatus isEqualToString:@"1"]) {
      
            self.audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:_strAudioPath] error:nil];

        }else {
            
            if ([_strSelectedLanguage isEqualToString:@"txt_eng"]) {
                
                audioURL = [NSURL URLWithString:[_dicPlaceDetail valueForKey:@"place_audio"]];
                strAudioUrl = [NSString stringWithFormat:@"%@",audioURL];
                
                pngData1 = [NSData dataWithContentsOfURL:audioURL];
                
                //            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                //            NSString *documentsPath = [paths objectAtIndex:0]; //Get the docs directory
                //
                //            //        NSString *anAudioName = [NSString stringWithFormat:@"%ldEnDetailAudio.mp3", (long)anIndex++];
                //            NSString *anAudioName = [NSString stringWithFormat:@"%@.mp3", [formatter stringFromDate:[NSDate date]]];
                //
                //            anAudioPath = [NSString stringWithFormat:@"%@/%@", documentsPath, anAudioName];
                //            NSLog(@"File path is %@",anAudioPath);
                //
                //            [pngData1 writeToFile:anAudioPath atomically:YES]; //Write the file
                //            [arrAudioPath addObject:anAudioPath];
                //            NSLog(@"File path is %@",arrAudioPath);
                
            }else if([_strSelectedLanguage isEqualToString:@"txt_islenska"]) {
                
                audioURL = [NSURL URLWithString:[_dicPlaceDetail valueForKey:@"place_audio_islenska"]];
                strAudioUrl = [NSString stringWithFormat:@"%@",audioURL];
                
                pngData1 = [NSData dataWithContentsOfURL:audioURL];
                
                //            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                //            NSString *documentsPath = [paths objectAtIndex:0]; //Get the docs directory
                //
                //            NSString *anAudioName = [NSString stringWithFormat:@"%@.mp3", [formatter stringFromDate:[NSDate date]]];
                //            //[NSString stringWithFormat:@"%ldIsDetailAudio.mp3", (long)anIndex++];
                //            anAudioPath = [NSString stringWithFormat:@"%@/%@", documentsPath, anAudioName];
                //            NSLog(@"File path is %@",anAudioPath);
                //
                //            [pngData1 writeToFile:anAudioPath atomically:YES]; //Write the file
                //            [arrAudioPath addObject:anAudioPath];
                //            NSLog(@"File path is %@",arrAudioPath);
                
            }else if([_strSelectedLanguage isEqualToString:@"txt_German"]) {
                
                audioURL = [NSURL URLWithString:[_dicPlaceDetail valueForKey:@"place_audio_german"]];
                strAudioUrl = [NSString stringWithFormat:@"%@",audioURL];
                
                pngData1 = [NSData dataWithContentsOfURL:audioURL];
                
//                NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//                NSString *documentsPath = [paths objectAtIndex:0]; //Get the docs directory
//
//                NSString *anAudioName = [NSString stringWithFormat:@"%@.mp3", [formatter stringFromDate:[NSDate date]]];
//                //[NSString stringWithFormat:@"%ldGrDetailAudio.mp3", (long)anIndex++];
//                anAudioPath = [NSString stringWithFormat:@"%@/%@", documentsPath, anAudioName];
//                NSLog(@"File path is %@",anAudioPath);
//
//                [pngData1 writeToFile:anAudioPath atomically:YES]; //Write the file
//                [arrAudioPath addObject:anAudioPath];
//                NSLog(@"File path is %@",arrAudioPath);
                
            }else if([_strSelectedLanguage isEqualToString:@"txt_French"]) {
                
                audioURL = [NSURL URLWithString:[_dicPlaceDetail valueForKey:@"place_audio_french"]];
                strAudioUrl = [NSString stringWithFormat:@"%@",audioURL];
                
                pngData1 = [NSData dataWithContentsOfURL:audioURL];
                
//                NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//                NSString *documentsPath = [paths objectAtIndex:0]; //Get the docs directory
//
//                NSString *anAudioName = [NSString stringWithFormat:@"%@.mp3", [formatter stringFromDate:[NSDate date]]];
//                //[NSString stringWithFormat:@"%ldFrDetailAudio.mp3", (long)anIndex++];
//                anAudioPath = [NSString stringWithFormat:@"%@/%@", documentsPath, anAudioName];
//                NSLog(@"File path is %@",anAudioPath);
//
//                [pngData1 writeToFile:anAudioPath atomically:YES]; //Write the file
//                [arrAudioPath addObject:anAudioPath];
//                NSLog(@"File path is %@",arrAudioPath);
                
            }else if([_strSelectedLanguage isEqualToString:@"txt_Chinese"]) {
                
                audioURL = [NSURL URLWithString:[_dicPlaceDetail valueForKey:@"place_audio_chinese"]];
                strAudioUrl = [NSString stringWithFormat:@"%@",audioURL];
                
                pngData1 = [NSData dataWithContentsOfURL:audioURL];
                
//                NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//                NSString *documentsPath = [paths objectAtIndex:0]; //Get the docs directory
//
//                NSString *anAudioName = [NSString stringWithFormat:@"%@.mp3", [formatter stringFromDate:[NSDate date]]];
//                //[NSString stringWithFormat:@"%ldChDetailAudio.mp3", (long)anIndex++];
//                anAudioPath = [NSString stringWithFormat:@"%@/%@", documentsPath, anAudioName];
//                NSLog(@"File path is %@",anAudioPath);
//
//                [pngData1 writeToFile:anAudioPath atomically:YES]; //Write the file
//                [arrAudioPath addObject:anAudioPath];
//                NSLog(@"File path is %@",arrAudioPath);
                
            }else if([_strSelectedLanguage isEqualToString:@"txt_Danish"]) {
                
                audioURL = [NSURL URLWithString:[_dicPlaceDetail valueForKey:@"place_audio_danish"]];
                strAudioUrl = [NSString stringWithFormat:@"%@",audioURL];
                
                pngData1 = [NSData dataWithContentsOfURL:audioURL];
                
//                NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//                NSString *documentsPath = [paths objectAtIndex:0]; //Get the docs directory
//
//                NSString *anAudioName = [NSString stringWithFormat:@"%@.mp3", [formatter stringFromDate:[NSDate date]]];
//                //[NSString stringWithFormat:@"%ldDaDetailAudio.mp3", (long)anIndex++];
//                anAudioPath = [NSString stringWithFormat:@"%@/%@", documentsPath, anAudioName];
//                NSLog(@"File path is %@",anAudioPath);
//
//                [pngData1 writeToFile:anAudioPath atomically:YES]; //Write the file
//                [arrAudioPath addObject:anAudioPath];
//                NSLog(@"File path is %@",arrAudioPath);
                
            }else if([_strSelectedLanguage isEqualToString:@"txt_Spanish"]) {
                
                audioURL = [NSURL URLWithString:[_dicPlaceDetail valueForKey:@"place_audio_spanish"]];
                strAudioUrl = [NSString stringWithFormat:@"%@",audioURL];
                
                pngData1 = [NSData dataWithContentsOfURL:audioURL];
                
//                NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//                NSString *documentsPath = [paths objectAtIndex:0]; //Get the docs directory
//
//                NSString *anAudioName = [NSString stringWithFormat:@"%@.mp3", [formatter stringFromDate:[NSDate date]]];
//                //[NSString stringWithFormat:@"%ldEsDetailAudio.mp3", (long)anIndex++];
//                anAudioPath = [NSString stringWithFormat:@"%@/%@", documentsPath, anAudioName];
//                NSLog(@"File path is %@",anAudioPath);
//
//                [pngData1 writeToFile:anAudioPath atomically:YES]; //Write the file
//                [arrAudioPath addObject:anAudioPath];
//                NSLog(@"File path is %@",arrAudioPath);
            }
            
            self.audioPlayer = [[AVAudioPlayer alloc] initWithData:pngData1 error:nil];

        }
      
    }else {
        
        _btnLanguage.enabled = NO;
        [_btnLanguage setTitleColor:colorFromRGBA(255, 255, 255, 0.5) forState:UIControlStateNormal];
        
        self.audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:_strAudioPath] error:nil];
        
    }
    
    
    if(!self.audioPlayer)
    {
        NSLog(@"Error creating player: %@", error);
    }
    self.audioPlayer.delegate = self;
    [self.audioPlayer prepareToPlay];

    self.audioSlider.minimumValue = 0.0f;
    self.audioSlider.maximumValue = self.audioPlayer.duration;
    [self.audioSlider setMaximumTrackTintColor:[UIColor whiteColor]];

    [self updateDisplay];

    
    self.detailImageVw.clipsToBounds = YES;
    
}

- (void)dataFromController:(NSString *)string {
    NSLog(@" string is : %@",string);
    _strSelectedLanguage = [NSString stringWithFormat:@"%@", string];
    [[NSUserDefaults standardUserDefaults] setObject:_strSelectedLanguage forKey:PREFERENCE_LANGUAGE];
    NSLog(@"_strLanguageCode : %@",_strSelectedLanguage);
    
    if (DEVICE_IS_IPHONE_X) {
        _webVwHeightContraint.constant = 100;

    }else if (DEVICE_IS_IPHONE_4) {
        _webVwHeightContraint.constant = 100;

    }else if (DEVICE_IS_IPHONE_5) {
        _webVwHeightContraint.constant = 100;
        
    }else if (DEVICE_IS_IPHONE_6) {
        _webVwHeightContraint.constant = 100;

    }else if (DEVICE_IS_IPHONE_6_PLUS) {
        _webVwHeightContraint.constant = 100;
    }
    
    

    if ([utility isInternetConnected]) {
        
        int columnNum = 0;
        
        if ([_strSelectedLanguage isEqualToString:@"txt_eng"]) {
            columnNum = 38;
            
        }else if([_strSelectedLanguage isEqualToString:@"txt_islenska"]) {
            columnNum = 39;
            
        }else if([_strSelectedLanguage isEqualToString:@"txt_German"]) {
            columnNum = 40;
            
        }else if([_strSelectedLanguage isEqualToString:@"txt_French"]) {
            columnNum = 41;
            
        }else if([_strSelectedLanguage isEqualToString:@"txt_Spanish"]) {
            columnNum = 42;
            
        }else if([_strSelectedLanguage isEqualToString:@"txt_Chinese"]) {
            columnNum = 43;
            
        }else if([_strSelectedLanguage isEqualToString:@"txt_Danish"]) {
            columnNum = 44;
        }
        
        NSString *strDownloadStatus = [NSString stringWithFormat:@"%@", [[_arrPlaceDetail objectAtIndex:0] objectAtIndex:columnNum]];
        
        if ([strDownloadStatus isEqualToString:@"1"]) {
            [self setOfflineData];
        }else {
            [self setData];
        }
        
    }else {
        [self setOfflineData];
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
//    NSLog(@"%@",_dicPlaceDetail);
    if ([utility isInternetConnected]) {
        _btnLanguage.enabled = YES;
    }else {
        _btnLanguage.enabled = NO;
    }
    
    [self stopIndicator];

    _strSelectedLanguage = [[NSUserDefaults standardUserDefaults] valueForKey:PREFERENCE_LANGUAGE];

    //============ Set Data ==================
    if ([utility isInternetConnected]) {
        
        int columnNum = 0;
        
        if ([_strSelectedLanguage isEqualToString:@"txt_eng"]) {
            columnNum = 38;
            
        }else if([_strSelectedLanguage isEqualToString:@"txt_islenska"]) {
            columnNum = 39;
            
        }else if([_strSelectedLanguage isEqualToString:@"txt_German"]) {
            columnNum = 40;
            
        }else if([_strSelectedLanguage isEqualToString:@"txt_French"]) {
            columnNum = 41;
            
        }else if([_strSelectedLanguage isEqualToString:@"txt_Spanish"]) {
            columnNum = 42;
            
        }else if([_strSelectedLanguage isEqualToString:@"txt_Chinese"]) {
            columnNum = 43;
            
        }else if([_strSelectedLanguage isEqualToString:@"txt_Danish"]) {
            columnNum = 44;
        }
        
        NSString *strDownloadStatus = [NSString stringWithFormat:@"%@", [[_arrPlaceDetail objectAtIndex:0] objectAtIndex:columnNum]];
        
        if ([strDownloadStatus isEqualToString:@"1"]) {
            [self setOfflineData];
        }else {
            [self setData];
        }
  
    }else {
        [self setOfflineData];
    }
    
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:YES];
    [self stopTimer];

    [self.audioPlayer stop];
    [_btnPlay setImage:[UIImage imageNamed:@"play_button"] forState:UIControlStateNormal];
    playing = NO;
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"Db"];
    

//    [locationManager stopUpdatingLocation];
//    locationManager = nil;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    //============ Segment ==================
    _changeMapTypeSegment.layer.cornerRadius = 4;
    _changeMapTypeSegment.clipsToBounds = YES;
    
    _firstVw.backgroundColor = colorFromRGBA(0, 0, 0, 0.3);
    
    _navTitle.textColor = [UIColor whiteColor];
    [_navTitle setFont:[UIFont fontWithName:FONT_ROBOTO_REGULAR size:15]];
    
    _lblPlaceVisited.textColor = [UIColor whiteColor];
    [_lblPlaceVisited setFont:[UIFont fontWithName:FONT_ROBOTO_REGULAR size:13]];
    
    _btnSaveImages.titleLabel.font = [UIFont fontWithName:FONT_ROBOTO_REGULAR size:13];
    
    _lblTitle.textColor = colorFromRGB(33, 33, 33);
    [_lblTitle setFont:[UIFont fontWithName:FONT_ROBOTO_BOLD size:14]];
    
    _lblDetail.textColor = colorFromRGB(102, 102, 102);
    [_lblDetail setFont:[UIFont fontWithName:FONT_ROBOTO_REGULAR size:13]];
    
    _lblKm.textColor = colorFromRGB(33, 33, 33);
    [_lblKm setFont:[UIFont fontWithName:FONT_ROBOTO_REGULAR size:13]];
}


#pragma mark ---------------------------
#pragma mark -- CLLocationManagerDelegate
#pragma mark ---------------------------

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    NSLog(@"Location is updating.....");

    NSLog(@"OldLocation %f %f", oldLocation.coordinate.latitude, oldLocation.coordinate.longitude);
    NSLog(@"NewLocation %f %f", newLocation.coordinate.latitude, newLocation.coordinate.longitude);
    coordinates = newLocation.coordinate;
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse) {
        // user allowed
        [locationManager startUpdatingLocation];
    }
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    
    if ([error domain] == kCLErrorDomain) {
        
        // We handle CoreLocation-related errors here
        switch ([error code]) {
                // "Don't Allow" on two successive app launches is the same as saying "never allow". The user
                // can reset this for all apps by going to Settings > General > Reset > Reset Location Warnings.
            case kCLErrorDenied:
                
            case kCLErrorLocationUnknown:
                
            default:
                break;
        }
        
    } else {
        // We handle all non-CoreLocation errors here
    }
}

#pragma mark -------------------
#pragma mark --- Select Map Type
#pragma mark -------------------

- (IBAction)changeMapTypeSegmentTapped:(UISegmentedControl *)sender {
    NSInteger selectedSegment = sender.selectedSegmentIndex;
    
    if (selectedSegment == 0) {
        _mapVw.mapType = kGMSTypeNormal;
    }else {
        _mapVw.mapType = kGMSTypeSatellite;
    }
}

#pragma mark -------------------------
#pragma mark Button Actions
#pragma mark -------------------------

- (IBAction)btnPrevTapped:(UIButton *)sender {
    
    if ([utility isInternetConnected]) {
        
        int columnNum = 0;
        
        if ([_strSelectedLanguage isEqualToString:@"txt_eng"]) {
            columnNum = 38;
            
        }else if([_strSelectedLanguage isEqualToString:@"txt_islenska"]) {
            columnNum = 39;
            
        }else if([_strSelectedLanguage isEqualToString:@"txt_German"]) {
            columnNum = 40;
            
        }else if([_strSelectedLanguage isEqualToString:@"txt_French"]) {
            columnNum = 41;
            
        }else if([_strSelectedLanguage isEqualToString:@"txt_Spanish"]) {
            columnNum = 42;
            
        }else if([_strSelectedLanguage isEqualToString:@"txt_Chinese"]) {
            columnNum = 43;
            
        }else if([_strSelectedLanguage isEqualToString:@"txt_Danish"]) {
            columnNum = 44;
        }
        
        NSString *strDownloadStatus = [NSString stringWithFormat:@"%@", [[_arrPlaceDetail objectAtIndex:0] objectAtIndex:columnNum]];
        
        if ([strDownloadStatus isEqualToString:@"1"]) {
       
            --index;
            
            [_btnNext setEnabled:YES];
            
            if (index <= 0)
            {
                index = 0;
                [_btnPrev setEnabled:NO];
            }
            
            [UIView animateWithDuration:0.2
                             animations:^{
                                 // animations go here
                                 //                         [_detailImageVw sd_setImageWithURL:[NSURL URLWithString:[arrImages objectAtIndex:index]]];
                                 NSString *strPath = [NSString stringWithFormat:@"%@",[_arrDetailImages objectAtIndex:index]];
                                 NSLog(@"strPath %@",strPath);
                                 UIImage *myImage = [UIImage imageWithContentsOfFile:strPath];
                                 _detailImageVw.image = myImage;
                                 
                                 //                         UIImage *myImage = [UIImage imageWithContentsOfFile:[arrLocalPlacesImg objectAtIndex:index]];
                                 //                         _detailImageVw.image = myImage;
                             }];
            
        }else {
            
            --index;
            
            [_btnNext setEnabled:YES];
            
            if (index <= 0)
            {
                index = 0;
                [_btnPrev setEnabled:NO];
            }
            
            [UIView animateWithDuration:0.2
                             animations:^{
                                 // animations go here
                                [_detailImageVw sd_setImageWithURL:[NSURL URLWithString:[arrImages objectAtIndex:index]]];
                                
//                                 NSString *strPath = [NSString stringWithFormat:@"%@",[_arrDetailImages objectAtIndex:index]];
//                                 NSLog(@"strPath %@",strPath);
//                                 UIImage *myImage = [UIImage imageWithContentsOfFile:strPath];
//                                 _detailImageVw.image = myImage;
                                 
                                 //                         UIImage *myImage = [UIImage imageWithContentsOfFile:[arrLocalPlacesImg objectAtIndex:index]];
                                 //                         _detailImageVw.image = myImage;
                             }];
            
        }
        
    }else {
        
        --index;
        
        [_btnNext setEnabled:YES];
        
        if (index <= 0)
        {
            index = 0;
            [_btnPrev setEnabled:NO];
        }
        
        [UIView animateWithDuration:0.2
                         animations:^{
                             // animations go here
                             //                         [_detailImageVw sd_setImageWithURL:[NSURL URLWithString:[arrImages objectAtIndex:index]]];
                             NSString *strPath = [NSString stringWithFormat:@"%@",[_arrDetailImages objectAtIndex:index]];
                             NSLog(@"strPath %@",strPath);
                             UIImage *myImage = [UIImage imageWithContentsOfFile:strPath];
                             _detailImageVw.image = myImage;
                             
                             //                         UIImage *myImage = [UIImage imageWithContentsOfFile:[arrLocalPlacesImg objectAtIndex:index]];
                             //                         _detailImageVw.image = myImage;
                         }];
        
    }
    
   
}

- (IBAction)btnNextTapped:(UIButton *)sender {
    
    if ([utility isInternetConnected]) {
        
        int columnNum = 0;
        
        if ([_strSelectedLanguage isEqualToString:@"txt_eng"]) {
            columnNum = 38;
            
        }else if([_strSelectedLanguage isEqualToString:@"txt_islenska"]) {
            columnNum = 39;
            
        }else if([_strSelectedLanguage isEqualToString:@"txt_German"]) {
            columnNum = 40;
            
        }else if([_strSelectedLanguage isEqualToString:@"txt_French"]) {
            columnNum = 41;
            
        }else if([_strSelectedLanguage isEqualToString:@"txt_Spanish"]) {
            columnNum = 42;
            
        }else if([_strSelectedLanguage isEqualToString:@"txt_Chinese"]) {
            columnNum = 43;
            
        }else if([_strSelectedLanguage isEqualToString:@"txt_Danish"]) {
            columnNum = 44;
        }
        
        NSString *strDownloadStatus = [NSString stringWithFormat:@"%@", [[_arrPlaceDetail objectAtIndex:0] objectAtIndex:columnNum]];
        
        if ([strDownloadStatus isEqualToString:@"1"]) {
            ++index;
            
            [_btnPrev setEnabled:YES];
            
            if (index >= [_arrDetailImages count])
            {
                index = [_arrDetailImages count] - 1;
                [_btnNext setEnabled:NO];
            }
            [UIView animateWithDuration:0.2
                             animations:^{
                                 // animations go here
                                 //                         [_detailImageVw sd_setImageWithURL:[NSURL URLWithString:[arrImages objectAtIndex:index]]];
                                 //                         UIImage *myImage = [UIImage imageWithContentsOfFile:[arrLocalPlacesImg objectAtIndex:index]];
                                 //                         _detailImageVw.image = myImage;
                                 
                                 NSString *strPath = [NSString stringWithFormat:@"%@",[_arrDetailImages objectAtIndex:index]];
                                 NSLog(@"strPath %@",strPath);
                                 UIImage *myImage = [UIImage imageWithContentsOfFile:strPath];
                                 _detailImageVw.image = myImage;
                             }];
        }else {
            
            ++index;
            
            [_btnPrev setEnabled:YES];
            
            if (index >= [arrImages count])
            {
                index = [arrImages count] - 1;
                [_btnNext setEnabled:NO];
            }
            [UIView animateWithDuration:0.2
                             animations:^{
                                 // animations go here
                                [_detailImageVw sd_setImageWithURL:[NSURL URLWithString:[arrImages objectAtIndex:index]]];
                                 //                         UIImage *myImage = [UIImage imageWithContentsOfFile:[arrLocalPlacesImg objectAtIndex:index]];
                                 //                         _detailImageVw.image = myImage;
                                 
//                                 NSString *strPath = [NSString stringWithFormat:@"%@",[_arrDetailImages objectAtIndex:index]];
//                                 NSLog(@"strPath %@",strPath);
//                                 UIImage *myImage = [UIImage imageWithContentsOfFile:strPath];
//                                 _detailImageVw.image = myImage;
                             }];
        }
        
    }else {
        ++index;
        
        [_btnPrev setEnabled:YES];
        
        if (index >= [_arrDetailImages count])
        {
            index = [_arrDetailImages count] - 1;
            [_btnNext setEnabled:NO];
        }
        [UIView animateWithDuration:0.2
                         animations:^{
                             // animations go here
                             //                         [_detailImageVw sd_setImageWithURL:[NSURL URLWithString:[arrImages objectAtIndex:index]]];
                             //                         UIImage *myImage = [UIImage imageWithContentsOfFile:[arrLocalPlacesImg objectAtIndex:index]];
                             //                         _detailImageVw.image = myImage;
                             
                             NSString *strPath = [NSString stringWithFormat:@"%@",[_arrDetailImages objectAtIndex:index]];
                             NSLog(@"strPath %@",strPath);
                             UIImage *myImage = [UIImage imageWithContentsOfFile:strPath];
                             _detailImageVw.image = myImage;
                         }];
    }

}

- (IBAction)btnBackTapped:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
    
//    [[NSNotificationCenter defaultCenter] postNotificationName:@"LocationUpdate" object:self];
}

- (IBAction)btnSelectLanguageTapped:(UIButton *)sender {
    LanguageViewController *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"LanguageViewController"];
    VC.delegate = self;
    VC.strSelectedLanguage = _strSelectedLanguage;
    [self.navigationController pushViewController:VC animated:YES];
}

- (IBAction)btnSaveImagesTapped:(UIButton *)sender {
    
    if ([utility isInternetConnected]) {
        
        int columnNum = 0;
        
        if ([_strSelectedLanguage isEqualToString:@"txt_eng"]) {
            columnNum = 38;
            
        }else if([_strSelectedLanguage isEqualToString:@"txt_islenska"]) {
            columnNum = 39;
            
        }else if([_strSelectedLanguage isEqualToString:@"txt_German"]) {
            columnNum = 40;
            
        }else if([_strSelectedLanguage isEqualToString:@"txt_French"]) {
            columnNum = 41;
            
        }else if([_strSelectedLanguage isEqualToString:@"txt_Spanish"]) {
            columnNum = 42;
            
        }else if([_strSelectedLanguage isEqualToString:@"txt_Chinese"]) {
            columnNum = 43;
            
        }else if([_strSelectedLanguage isEqualToString:@"txt_Danish"]) {
            columnNum = 44;
        }
        
        NSString *strDownloadStatus = [NSString stringWithFormat:@"%@", [[_arrPlaceDetail objectAtIndex:0] objectAtIndex:columnNum]];
        
        if ([strDownloadStatus isEqualToString:@"1"]) {
      
            [self showLoadingIndicator];
            
//            NSArray *arrLargeImagesTemp = [_dicPlaceDetail valueForKey:@"place_images"];
//
//            for (NSDictionary *dicPlacesLargeImages in arrLargeImagesTemp) {
//                [arrLargeImgs addObject:[dicPlacesLargeImages valueForKey:@"place_img_name_large"]];//@"place_img_name"]];
//            }
//            NSLog(@"arrLargeImgs %@:",arrLargeImgs);
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                
                NSURL *imageUrl = [NSURL URLWithString:[_arrLargeDetailImages objectAtIndex:index]];
                
                dispatch_async(dispatch_get_main_queue(), ^(void) {
                    
                    [self stopIndicator];
                    UIImage *aImage = [UIImage imageWithData:[NSData dataWithContentsOfURL:imageUrl]];
                    UIImageWriteToSavedPhotosAlbum(aImage, nil, nil, nil);
                    
                    UIAlertController *alert = [UIAlertController alertControllerWithTitle:[Diclabels valueForKey:@"txt_thingvellir"] message:[Diclabels valueForKey:@"MSG_PHOTO_SAVED_SUCCESSFULLY"] preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction *ok = [UIAlertAction actionWithTitle:[Diclabels valueForKey:@"OK"] style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {}];
                    
                    [alert addAction:ok];
                    [self presentViewController:alert animated:YES completion:nil];
                });
            });
        }else {
         
            [self showLoadingIndicator];
            
            NSArray *arrLargeImagesTemp = [_dicPlaceDetail valueForKey:@"place_images"];
            
            for (NSDictionary *dicPlacesLargeImages in arrLargeImagesTemp) {
                [arrLargeImgs addObject:[dicPlacesLargeImages valueForKey:@"place_img_name_large"]];//@"place_img_name"]];
            }
            NSLog(@"arrLargeImgs %@:",arrLargeImgs);
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                
                NSURL *imageUrl = [NSURL URLWithString:[arrLargeImgs objectAtIndex:index]];
                
                dispatch_async(dispatch_get_main_queue(), ^(void) {
                    
                    [self stopIndicator];
                    UIImage *aImage = [UIImage imageWithData:[NSData dataWithContentsOfURL:imageUrl]];
                    UIImageWriteToSavedPhotosAlbum(aImage, nil, nil, nil);
                    
                    UIAlertController *alert = [UIAlertController alertControllerWithTitle:[Diclabels valueForKey:@"txt_thingvellir"] message:[Diclabels valueForKey:@"MSG_PHOTO_SAVED_SUCCESSFULLY"] preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction *ok = [UIAlertAction actionWithTitle:[Diclabels valueForKey:@"OK"] style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {}];
                    
                    [alert addAction:ok];
                    [self presentViewController:alert animated:YES completion:nil];
                });
            });
        }
   
    }else {
        [self stopIndicator];
        
        //            NSArray *arrLargeImagesTemp = [_dicPlaceDetail valueForKey:@"place_images"];
        //
        //            for (NSDictionary *dicPlacesLargeImages in arrLargeImagesTemp) {
        //                [arrLargeImgs addObject:[dicPlacesLargeImages valueForKey:@"place_img_name_large"]];//@"place_img_name"]];
        //            }
        //            NSLog(@"arrLargeImgs %@:",arrLargeImgs);
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            NSURL *imageUrl = [NSURL URLWithString:[_arrLargeDetailImages objectAtIndex:index]];
            
            dispatch_async(dispatch_get_main_queue(), ^(void) {
                
                [self stopIndicator];
                UIImage *aImage = [UIImage imageWithData:[NSData dataWithContentsOfURL:imageUrl]];
                UIImageWriteToSavedPhotosAlbum(aImage, nil, nil, nil);
                
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:[Diclabels valueForKey:@"txt_thingvellir"] message:[Diclabels valueForKey:@"MSG_PHOTO_SAVED_SUCCESSFULLY"] preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *ok = [UIAlertAction actionWithTitle:[Diclabels valueForKey:@"OK"] style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {}];
                
                [alert addAction:ok];
                [self presentViewController:alert animated:YES completion:nil];
            });
        });
    }
  
}

- (IBAction)btnDownloadTapped:(UIButton *)sender {
   
    if ([utility isInternetConnected]) {
        
        int columnNum = 0;
        
        if ([_strSelectedLanguage isEqualToString:@"txt_eng"]) {
            columnNum = 38;
            
        }else if([_strSelectedLanguage isEqualToString:@"txt_islenska"]) {
            columnNum = 39;
            
        }else if([_strSelectedLanguage isEqualToString:@"txt_German"]) {
            columnNum = 40;
            
        }else if([_strSelectedLanguage isEqualToString:@"txt_French"]) {
            columnNum = 41;
            
        }else if([_strSelectedLanguage isEqualToString:@"txt_Spanish"]) {
            columnNum = 42;
            
        }else if([_strSelectedLanguage isEqualToString:@"txt_Chinese"]) {
            columnNum = 43;
            
        }else if([_strSelectedLanguage isEqualToString:@"txt_Danish"]) {
            columnNum = 44;
        }
        
        NSString *strDownloadStatus = [NSString stringWithFormat:@"%@", [[_arrPlaceDetail objectAtIndex:0] objectAtIndex:columnNum]];
        
        if ([strDownloadStatus isEqualToString:@"1"]) {
      
            strPlaceId = [[_arrPlaceDetail objectAtIndex:0] objectAtIndex:1];
            [self setDownloadImgOffline];
       
        }else {
        
            strPlaceId = [_dicPlaceDetail valueForKey:@"place_id"];
            
            NSArray *arrResult = [self.dbHelper checkIfDataAlreadyExist:DB_TABLENAME_PLACEMASTER place_id:strPlaceId colomnNM:@"place_id"];
            NSLog(@"%@", arrResult);
            
            if (arrResult.count > 0) {
                
                int columnNum = 0;
                
                if ([_strSelectedLanguage isEqualToString:@"txt_eng"]) {
                    columnNum = 38;
                    
                }else if([_strSelectedLanguage isEqualToString:@"txt_islenska"]) {
                    columnNum = 39;
                    
                }else if([_strSelectedLanguage isEqualToString:@"txt_German"]) {
                    columnNum = 40;
                    
                }else if([_strSelectedLanguage isEqualToString:@"txt_French"]) {
                    columnNum = 41;
                    
                }else if([_strSelectedLanguage isEqualToString:@"txt_Spanish"]) {
                    columnNum = 42;
                    
                }else if([_strSelectedLanguage isEqualToString:@"txt_Chinese"]) {
                    columnNum = 43;
                    
                }else if([_strSelectedLanguage isEqualToString:@"txt_Danish"]) {
                    columnNum = 44;
                }
                
                NSString *strDownFlag = [NSString stringWithFormat:@"%@", [[arrResult objectAtIndex:0] objectAtIndex:columnNum]];
                
                //        NSString *strDownFlag = [NSString stringWithFormat:@"%@", [[arrResult objectAtIndex:0] objectAtIndex:14]];
                
                if ([strDownFlag isEqualToString:@"0"]) {
                    
                    if ([_strSelectedLanguage isEqualToString:@"txt_eng"]) {
                        
                        NSString *url = [NSString stringWithFormat:@"%@",[_dicPlaceDetail valueForKey:@"place_audio"]];
                        
                        NSURL *audioURL = [NSURL URLWithString:url];
                        NSString *strAudioUrl = [NSString stringWithFormat:@"%@",audioURL];
                        
                        if ([utility isStringEmpty:strAudioUrl]) {
                            NSLog(@"No Audio");
                        }else {
                            
                            pngData = [NSData dataWithContentsOfURL:audioURL];
                           
                            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                            NSString *documentsPath = [paths objectAtIndex:0]; //Get the docs directory
                            
                            NSString *anImageName = [NSString stringWithFormat:@"%@ENAudio.mp3",[formatter stringFromDate:[NSDate date]]];
                            NSString *anAudioPath = [NSString stringWithFormat:@"%@/%@", documentsPath, anImageName];
                            //            NSLog(@"anAudioPath File path is %@ ::",anAudioPath);
                            
                            [pngData writeToFile:anAudioPath atomically:YES]; //Write the file
                            
                            
                            [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys:anAudioPath, DB_TABLEFIELD_AUDIO, nil]];
                            
                        }
                
                        [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys: @"1", DB_TABLEFIELD_DOWN_FLAG_EN, nil]];
                        
                        
                    }else if([_strSelectedLanguage isEqualToString:@"txt_islenska"]) {
                        
                        NSString *url = [NSString stringWithFormat:@"%@",[_dicPlaceDetail valueForKey:@"place_audio_islenska"]];
                        
                        NSURL *audioURL = [NSURL URLWithString:url];
                        NSString *strAudioUrl = [NSString stringWithFormat:@"%@",audioURL];
                        
                        if ([utility isStringEmpty:strAudioUrl]) {
                            NSLog(@"No Audio");
                        }else {
                            
                            pngData = [NSData dataWithContentsOfURL:audioURL];
                            
                            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                            NSString *documentsPath = [paths objectAtIndex:0]; //Get the docs directory
                            
                            NSString *anImageName = [NSString stringWithFormat:@"%@ISAudio.mp3",[formatter stringFromDate:[NSDate date]]];
                            NSString *anAudioPath = [NSString stringWithFormat:@"%@/%@", documentsPath, anImageName];
                            //            NSLog(@"anAudioPath File path is %@ ::",anAudioPath);
                            
                            [pngData writeToFile:anAudioPath atomically:YES]; //Write the file
                            
                            
                            [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys:anAudioPath, DB_TABLEFIELD_AUDIO_ISLENSKA, nil]];
                            
                        }
                   
                        [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys: @"1", DB_TABLEFIELD_DOWN_FLAG_IS, nil]];
                        
                    }else if([_strSelectedLanguage isEqualToString:@"txt_German"]) {
                        
                        NSString *url = [NSString stringWithFormat:@"%@",[_dicPlaceDetail valueForKey:@"place_audio_german"]];
                        
                        NSURL *audioURL = [NSURL URLWithString:url];
                        NSString *strAudioUrl = [NSString stringWithFormat:@"%@",audioURL];
                        
                        if ([utility isStringEmpty:strAudioUrl]) {
                            NSLog(@"No Audio");
                        }else {
                            
                            pngData = [NSData dataWithContentsOfURL:audioURL];
                            
                            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                            NSString *documentsPath = [paths objectAtIndex:0]; //Get the docs directory
                            
                            NSString *anImageName = [NSString stringWithFormat:@"%@GrAudio.mp3",[formatter stringFromDate:[NSDate date]]];
                            NSString *anAudioPath = [NSString stringWithFormat:@"%@/%@", documentsPath, anImageName];
                            //            NSLog(@"anAudioPath File path is %@ ::",anAudioPath);
                            
                            [pngData writeToFile:anAudioPath atomically:YES]; //Write the file
                            
                            
                            [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys:anAudioPath, DB_TABLEFIELD_AUDIO_GERMAN, nil]];
                            
                        }
                        
                        [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys: @"1", DB_TABLEFIELD_DOWN_FLAG_GE, nil]];
                        
                    }else if([_strSelectedLanguage isEqualToString:@"txt_French"]) {
                        
                        NSString *url = [NSString stringWithFormat:@"%@",[_dicPlaceDetail valueForKey:@"place_audio_french"]];
                        
                        NSURL *audioURL = [NSURL URLWithString:url];
                        NSString *strAudioUrl = [NSString stringWithFormat:@"%@",audioURL];
                        
                        if ([utility isStringEmpty:strAudioUrl]) {
                            NSLog(@"No Audio");
                        }else {
                            
                            pngData = [NSData dataWithContentsOfURL:audioURL];
                            
                            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                            NSString *documentsPath = [paths objectAtIndex:0]; //Get the docs directory
                            
                            NSString *anImageName = [NSString stringWithFormat:@"%@FRAudio.mp3",[formatter stringFromDate:[NSDate date]]];
                            NSString *anAudioPath = [NSString stringWithFormat:@"%@/%@", documentsPath, anImageName];
                            //            NSLog(@"anAudioPath File path is %@ ::",anAudioPath);
                            
                            [pngData writeToFile:anAudioPath atomically:YES]; //Write the file
                            
                            
                            [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys:anAudioPath, DB_TABLEFIELD_AUDIO_FRENCH, nil]];
                            
                        }
                        
                        [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys: @"1", DB_TABLEFIELD_DOWN_FLAG_FR, nil]];
                        
                    }else if([_strSelectedLanguage isEqualToString:@"txt_Spanish"]) {
                        
                        NSString *url = [NSString stringWithFormat:@"%@",[_dicPlaceDetail valueForKey:@"place_audio_spanish"]];
                        
                        NSURL *audioURL = [NSURL URLWithString:url];
                        NSString *strAudioUrl = [NSString stringWithFormat:@"%@",audioURL];
                        
                        if ([utility isStringEmpty:strAudioUrl]) {
                            NSLog(@"No Audio");
                        }else {
                            
                            pngData = [NSData dataWithContentsOfURL:audioURL];
                            
                            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                            NSString *documentsPath = [paths objectAtIndex:0]; //Get the docs directory
                            
                            NSString *anImageName = [NSString stringWithFormat:@"%@ESAudio.mp3",[formatter stringFromDate:[NSDate date]]];
                            NSString *anAudioPath = [NSString stringWithFormat:@"%@/%@", documentsPath, anImageName];
                            //            NSLog(@"anAudioPath File path is %@ ::",anAudioPath);
                            
                            [pngData writeToFile:anAudioPath atomically:YES]; //Write the file
                            
                            
                            [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys:anAudioPath, DB_TABLEFIELD_AUDIO_SPANISH, nil]];
                            
                        }
                        [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys: @"1", DB_TABLEFIELD_DOWN_FLAG_ES, nil]];
                        
                    }else if([_strSelectedLanguage isEqualToString:@"txt_Chinese"]) {
                        
                        NSString *url = [NSString stringWithFormat:@"%@",[_dicPlaceDetail valueForKey:@"place_audio_chinese"]];
                        
                        NSURL *audioURL = [NSURL URLWithString:url];
                        NSString *strAudioUrl = [NSString stringWithFormat:@"%@",audioURL];
                        
                        if ([utility isStringEmpty:strAudioUrl]) {
                            NSLog(@"No Audio");
                        }else {
                            
                            pngData = [NSData dataWithContentsOfURL:audioURL];
                            
                            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                            NSString *documentsPath = [paths objectAtIndex:0]; //Get the docs directory
                            
                            NSString *anImageName = [NSString stringWithFormat:@"%@CHAudio.mp3",[formatter stringFromDate:[NSDate date]]];
                            NSString *anAudioPath = [NSString stringWithFormat:@"%@/%@", documentsPath, anImageName];
                            //            NSLog(@"anAudioPath File path is %@ ::",anAudioPath);
                            
                            [pngData writeToFile:anAudioPath atomically:YES]; //Write the file
                            
                            
                            [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys:anAudioPath, DB_TABLEFIELD_AUDIO_CHINESE, nil]];
                            
                        }
                        [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys: @"1", DB_TABLEFIELD_DOWN_FLAG_CH, nil]];
                        
                    }else if([_strSelectedLanguage isEqualToString:@"txt_Danish"]) {
                        
                        NSString *url = [NSString stringWithFormat:@"%@",[_dicPlaceDetail valueForKey:@"place_audio_danish"]];
                        
                        NSURL *audioURL = [NSURL URLWithString:url];
                        NSString *strAudioUrl = [NSString stringWithFormat:@"%@",audioURL];
                        
                        if ([utility isStringEmpty:strAudioUrl]) {
                            NSLog(@"No Audio");
                        }else {
                            
                            pngData = [NSData dataWithContentsOfURL:audioURL];
                            
                            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                            NSString *documentsPath = [paths objectAtIndex:0]; //Get the docs directory
                            
                            NSString *anImageName = [NSString stringWithFormat:@"%@DAAudio.mp3",[formatter stringFromDate:[NSDate date]]];
                            NSString *anAudioPath = [NSString stringWithFormat:@"%@/%@", documentsPath, anImageName];
                            //            NSLog(@"anAudioPath File path is %@ ::",anAudioPath);
                            
                            [pngData writeToFile:anAudioPath atomically:YES]; //Write the file
                            
                            
                            [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys:anAudioPath, DB_TABLEFIELD_AUDIO_DANISH, nil]];
                            
                        }
                        
                        [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys: @"1", DB_TABLEFIELD_DOWN_FLAG_DA, nil]];
                    }
                    
                    //            [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys: @"1", DB_TABLEFIELD_DOWNLOAD_FLAG, nil]];
                    
                    NSInteger anIndex = 0;
                    
//                    [arrDetailImgs removeAllObjects];
                    NSArray *arrImages1 = [_dicPlaceDetail valueForKey:@"place_images"];
                    for (NSDictionary *dicPlaces in arrImages1) {
                        [arrDetailImgs addObject:[dicPlaces valueForKey:@"place_img_name"]];
                    }
                    NSLog(@"arrPlacesDetailImgs %@:",arrDetailImgs);
                    
                    for (int i=0; i<arrDetailImgs.count; i++) {
                       
                        NSURL *imageURL = [NSURL URLWithString:[arrDetailImgs objectAtIndex:i]];
                        
                        pngData = [NSData dataWithContentsOfURL:imageURL];
                        
                        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                        NSString *documentsPath = [paths objectAtIndex:0]; //Get the docs directory

                        NSString *anImageName = [NSString stringWithFormat:@"%ld.%ldplaceDetail.png", (long)_selectedIndex,(long)anIndex++];
                        NSString *anPlacesDetailImagePath = [NSString stringWithFormat:@"%@/%@", documentsPath, anImageName];
                        NSLog(@"File path is %@",anPlacesDetailImagePath);

                        [pngData writeToFile:anPlacesDetailImagePath atomically:YES]; //Write the file
                        [arrDetailImgsPath addObject:anPlacesDetailImagePath];
                    }
                    NSLog(@"arrPlacesDetailImgsPath %@:",arrDetailImgsPath);
                    
                    for (int k=0; k < arrDetailImgsPath.count; k++) {
                        [self.dbHelper inserDataIntoTable:DB_TABLENAME_IMAGEMASTER tableData:[NSDictionary dictionaryWithObjectsAndKeys:[_dicPlaceDetail valueForKey:@"place_id"], DB_TABLEFIELD_PLACEID, [arrDetailImgsPath objectAtIndex:k], DB_TABLEFIELD_PLACEIMAGE_NAME, nil]];
                    }
//                    [arrDetailImgsPath removeAllObjects];
                    
                    _btnDownload.enabled = NO;
                    _btnDownload.hidden = YES;
                    _downloadImgVw.hidden = YES;
                    
                }else {
                    
                    if ([_strSelectedLanguage isEqualToString:@"txt_eng"]) {
                        [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys: @"0", DB_TABLEFIELD_DOWN_FLAG_EN, nil]];
                        
                    }else if([_strSelectedLanguage isEqualToString:@"txt_islenska"]) {
                        [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys: @"0", DB_TABLEFIELD_DOWN_FLAG_IS, nil]];
                        
                    }else if([_strSelectedLanguage isEqualToString:@"txt_German"]) {
                        [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys: @"0", DB_TABLEFIELD_DOWN_FLAG_GE, nil]];
                        
                    }else if([_strSelectedLanguage isEqualToString:@"txt_French"]) {
                        [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys: @"0", DB_TABLEFIELD_DOWN_FLAG_FR, nil]];
                        
                    }else if([_strSelectedLanguage isEqualToString:@"txt_Spanish"]) {
                        [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys: @"0", DB_TABLEFIELD_DOWN_FLAG_ES, nil]];
                        
                    }else if([_strSelectedLanguage isEqualToString:@"txt_Chinese"]) {
                        [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys: @"0", DB_TABLEFIELD_DOWN_FLAG_CH, nil]];
                        
                    }else if([_strSelectedLanguage isEqualToString:@"txt_Danish"]) {
                        [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys: @"0", DB_TABLEFIELD_DOWN_FLAG_DA, nil]];
                  
                    }
                    //            [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys: @"0",DB_TABLEFIELD_DOWNLOAD_FLAG, nil]];
                    _btnDownload.enabled = YES;
                    _btnDownload.hidden = NO;
                    _downloadImgVw.hidden = NO;
                    
                }
            }
        }
        
    }else {
        strPlaceId = [[_arrPlaceDetail objectAtIndex:0] objectAtIndex:1];
        [self setDownloadImgOffline];

    }
    
}

- (void)setDownloadImgOffline {
    
    NSArray *arrResult = [self.dbHelper checkIfDataAlreadyExist:DB_TABLENAME_PLACEMASTER place_id:strPlaceId colomnNM:@"place_id"];
    NSLog(@"%@", arrResult);
    
    if (arrResult.count > 0) {
        
        int columnNum = 0;
        
        if ([_strSelectedLanguage isEqualToString:@"txt_eng"]) {
            columnNum = 38;
            
        }else if([_strSelectedLanguage isEqualToString:@"txt_islenska"]) {
            columnNum = 39;
            
        }else if([_strSelectedLanguage isEqualToString:@"txt_German"]) {
            columnNum = 40;
            
        }else if([_strSelectedLanguage isEqualToString:@"txt_French"]) {
            columnNum = 41;
            
        }else if([_strSelectedLanguage isEqualToString:@"txt_Spanish"]) {
            columnNum = 42;
            
        }else if([_strSelectedLanguage isEqualToString:@"txt_Chinese"]) {
            columnNum = 43;
            
        }else if([_strSelectedLanguage isEqualToString:@"txt_Danish"]) {
            columnNum = 44;
        }
        
        NSString *strDownFlag = [NSString stringWithFormat:@"%@", [[arrResult objectAtIndex:0] objectAtIndex:columnNum]];
        
        //        NSString *strDownFlag = [NSString stringWithFormat:@"%@", [[arrResult objectAtIndex:0] objectAtIndex:14]];
        
        if ([strDownFlag isEqualToString:@"0"]) {
            if ([_strSelectedLanguage isEqualToString:@"txt_eng"]) {
                
                [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys: @"1", DB_TABLEFIELD_DOWN_FLAG_EN, nil]];
                
            }else if([_strSelectedLanguage isEqualToString:@"txt_islenska"]) {
                [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys: @"1", DB_TABLEFIELD_DOWN_FLAG_IS, nil]];
                
            }else if([_strSelectedLanguage isEqualToString:@"txt_German"]) {
                [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys: @"1", DB_TABLEFIELD_DOWN_FLAG_GE, nil]];
                
            }else if([_strSelectedLanguage isEqualToString:@"txt_French"]) {
                [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys: @"1", DB_TABLEFIELD_DOWN_FLAG_FR, nil]];
                
            }else if([_strSelectedLanguage isEqualToString:@"txt_Spanish"]) {
                [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys: @"1", DB_TABLEFIELD_DOWN_FLAG_ES, nil]];
                
            }else if([_strSelectedLanguage isEqualToString:@"txt_Chinese"]) {
                [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys: @"1", DB_TABLEFIELD_DOWN_FLAG_CH, nil]];
                
            }else if([_strSelectedLanguage isEqualToString:@"txt_Danish"]) {
                [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys: @"1", DB_TABLEFIELD_DOWN_FLAG_DA, nil]];
            }
            
            //            [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys: @"1", DB_TABLEFIELD_DOWNLOAD_FLAG, nil]];
            _btnDownload.enabled = NO;
            _btnDownload.hidden = YES;
            _downloadImgVw.hidden = YES;
            
        }else {
            
            if ([_strSelectedLanguage isEqualToString:@"txt_eng"]) {
                [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys: @"0", DB_TABLEFIELD_DOWN_FLAG_EN, nil]];
                
            }else if([_strSelectedLanguage isEqualToString:@"txt_islenska"]) {
                [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys: @"0", DB_TABLEFIELD_DOWN_FLAG_IS, nil]];
                
            }else if([_strSelectedLanguage isEqualToString:@"txt_German"]) {
                [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys: @"0", DB_TABLEFIELD_DOWN_FLAG_GE, nil]];
                
            }else if([_strSelectedLanguage isEqualToString:@"txt_French"]) {
                [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys: @"0", DB_TABLEFIELD_DOWN_FLAG_FR, nil]];
                
            }else if([_strSelectedLanguage isEqualToString:@"txt_Spanish"]) {
                [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys: @"0", DB_TABLEFIELD_DOWN_FLAG_ES, nil]];
                
            }else if([_strSelectedLanguage isEqualToString:@"txt_Chinese"]) {
                [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys: @"0", DB_TABLEFIELD_DOWN_FLAG_CH, nil]];
                
            }else if([_strSelectedLanguage isEqualToString:@"txt_Danish"]) {
                [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys: @"0", DB_TABLEFIELD_DOWN_FLAG_DA, nil]];
            }
            //            [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys: @"0",DB_TABLEFIELD_DOWNLOAD_FLAG, nil]];
            _btnDownload.enabled = YES;
            _btnDownload.hidden = NO;
            _downloadImgVw.hidden = NO;
            
        }
    }
}

- (IBAction)btnLikeTapped:(UIButton *)sender {
 
    if ([utility isInternetConnected]) {
        
        int columnNum = 0;
        
        if ([_strSelectedLanguage isEqualToString:@"txt_eng"]) {
            columnNum = 38;
            
        }else if([_strSelectedLanguage isEqualToString:@"txt_islenska"]) {
            columnNum = 39;
            
        }else if([_strSelectedLanguage isEqualToString:@"txt_German"]) {
            columnNum = 40;
            
        }else if([_strSelectedLanguage isEqualToString:@"txt_French"]) {
            columnNum = 41;
            
        }else if([_strSelectedLanguage isEqualToString:@"txt_Spanish"]) {
            columnNum = 42;
            
        }else if([_strSelectedLanguage isEqualToString:@"txt_Chinese"]) {
            columnNum = 43;
            
        }else if([_strSelectedLanguage isEqualToString:@"txt_Danish"]) {
            columnNum = 44;
        }
        
        NSString *strDownloadStatus = [NSString stringWithFormat:@"%@", [[_arrPlaceDetail objectAtIndex:0] objectAtIndex:columnNum]];
        
        if ([strDownloadStatus isEqualToString:@"1"]) {
            strPlaceId = [[_arrPlaceDetail objectAtIndex:0] objectAtIndex:1];

        }else{
            strPlaceId = [_dicPlaceDetail valueForKey:@"place_id"];

        }
        
    }else {
        strPlaceId = [[_arrPlaceDetail objectAtIndex:0] objectAtIndex:1];
    }
    
    NSArray *arrResult = [self.dbHelper checkIfDataAlreadyExist:DB_TABLENAME_PLACEMASTER place_id:strPlaceId colomnNM:@"place_id"];
    NSLog(@"%@", arrResult);
    
    if (arrResult.count > 0) {
        NSString *strForStatus = [NSString stringWithFormat:@"%@", [[arrResult objectAtIndex:0] objectAtIndex:9]];
        
        if ([strForStatus isEqualToString:@"0"]) {
            _btnLike.selected = YES;
            _likeImgVw.image = [UIImage imageNamed:@"favourite_red"];
            
            [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                                         @"1", DB_TABLEFIELD_LIKE,
                                                                                         nil]];
        }else {
            _btnLike.selected = NO;
            _likeImgVw.image = [UIImage imageNamed:@"favourite_gray"];
            
            [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                                         @"0", DB_TABLEFIELD_LIKE,
                                                                                         nil]];
        }
    }
}

- (IBAction)btnMapTapped:(UIButton *)sender {
    
    DetailMapViewController *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"DetailMapViewController"];
    
    if ([utility isInternetConnected]) {
     
        int columnNum = 0;
        
        if ([_strSelectedLanguage isEqualToString:@"txt_eng"]) {
            columnNum = 38;
            
        }else if([_strSelectedLanguage isEqualToString:@"txt_islenska"]) {
            columnNum = 39;
            
        }else if([_strSelectedLanguage isEqualToString:@"txt_German"]) {
            columnNum = 40;
            
        }else if([_strSelectedLanguage isEqualToString:@"txt_French"]) {
            columnNum = 41;
            
        }else if([_strSelectedLanguage isEqualToString:@"txt_Spanish"]) {
            columnNum = 42;
            
        }else if([_strSelectedLanguage isEqualToString:@"txt_Chinese"]) {
            columnNum = 43;
            
        }else if([_strSelectedLanguage isEqualToString:@"txt_Danish"]) {
            columnNum = 44;
        }
        
        NSString *strDownloadStatus = [NSString stringWithFormat:@"%@", [[_arrPlaceDetail objectAtIndex:0] objectAtIndex:columnNum]];
        
        if ([strDownloadStatus isEqualToString:@"1"]) {
        
            VC.arrDetails = _arrPlaceDetail;

        }else {
            VC.dicDetails = _dicPlaceDetail;
        }

    }else {
        VC.arrDetails = _arrPlaceDetail;
    }
  
    [self.navigationController pushViewController:VC animated:YES];
}

- (IBAction)btnPlayTapped:(id)sender {
 
    if (playing == NO) {
        [_btnPlay setImage:[UIImage imageNamed:@"pause_button.png"] forState:UIControlStateNormal];
        [self.audioPlayer play];
        self.timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(timerFired:) userInfo:nil repeats:YES];
        playing = YES;

    }else {
        [self.audioPlayer pause];
        [self stopTimer];
        [_btnPlay setImage:[UIImage imageNamed:@"play_button"] forState:UIControlStateNormal];

        [self updateDisplay];
        playing = NO;

    }
}

#pragma mark - Slider actions

- (IBAction)sliderChanged:(UISlider *)sender {
 
    if(self.timer)
        [self stopTimer];
    [self updateSliderLabels];
}

- (IBAction)currentTimeSliderTouchUpInside:(id)sender {
  
    [self.audioPlayer stop];
    self.audioPlayer.currentTime = self.audioSlider.value;
    [self.audioPlayer prepareToPlay];
    [self btnPlayTapped:self];
    
}

#pragma mark - Display Update
- (void)updateDisplay {
    NSTimeInterval currentTime = self.audioPlayer.currentTime;
    
    self.audioSlider.value = currentTime;
    [self.audioSlider setMinimumTrackTintColor:colorFromRGB(237, 33, 41)];

    [self updateSliderLabels];
}

- (void)updateSliderLabels {
    
    NSTimeInterval currentSlidertime = self.audioSlider.value;
    NSLog(@"%d", (int)currentSlidertime);
    
    
    int currentAudioPlayerValue = (int)currentSlidertime;
    int totalAudioTime = self.audioPlayer.duration;
    totalAudioTime = totalAudioTime - currentAudioPlayerValue;
    NSLog(@"%d", (int)totalAudioTime);
    self.lblStartTime.text = [NSString stringWithFormat:@"%02d:%02d" , (currentAudioPlayerValue / 60) ,(currentAudioPlayerValue % 60)];
    self.lblEndTime.text = [NSString stringWithFormat:@"%02d:%02d",(totalAudioTime / 60),(totalAudioTime % 60)];

}

#pragma mark - Timer
- (void)timerFired:(NSTimer*)timer {
    [self updateDisplay];
}

- (void)stopTimer {
    
    [self.timer invalidate];
    self.timer = nil;
    [self updateDisplay];
}

#pragma mark - AVAudioPlayerDelegate

- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag {
    
    if (flag) {
        [_timer invalidate];
        [_btnPlay setImage:[UIImage imageNamed:@"play_button"] forState:UIControlStateNormal];
        playing=NO;
    }
    
    NSLog(@"%s successfully=%@", __PRETTY_FUNCTION__, flag ? @"YES"  : @"NO");
    [self stopTimer];
    [self updateDisplay];
}

- (void)audioPlayerDecodeErrorDidOccur:(AVAudioPlayer *)player error:(NSError *)error {
    
    NSLog(@"%s error=%@", __PRETTY_FUNCTION__, error);
    [self stopTimer];
    [self updateDisplay];
}

#pragma Switch Action

- (void)switchToggled:(id)sender {
    
    if ([utility isInternetConnected]) {
        int columnNum = 0;
        
        if ([_strSelectedLanguage isEqualToString:@"txt_eng"]) {
            columnNum = 38;
            
        }else if([_strSelectedLanguage isEqualToString:@"txt_islenska"]) {
            columnNum = 39;
            
        }else if([_strSelectedLanguage isEqualToString:@"txt_German"]) {
            columnNum = 40;
            
        }else if([_strSelectedLanguage isEqualToString:@"txt_French"]) {
            columnNum = 41;
            
        }else if([_strSelectedLanguage isEqualToString:@"txt_Spanish"]) {
            columnNum = 42;
            
        }else if([_strSelectedLanguage isEqualToString:@"txt_Chinese"]) {
            columnNum = 43;
            
        }else if([_strSelectedLanguage isEqualToString:@"txt_Danish"]) {
            columnNum = 44;
        }
        
        NSString *strDownloadStatus = [NSString stringWithFormat:@"%@", [[_arrPlaceDetail objectAtIndex:0] objectAtIndex:columnNum]];
        
        if ([strDownloadStatus isEqualToString:@"1"]) {
            strPlaceId = [[_arrPlaceDetail objectAtIndex:0] objectAtIndex:1];
            
        }else {
            strPlaceId = [_dicPlaceDetail valueForKey:@"place_id"];
        }
        
    }else {
        strPlaceId = [[_arrPlaceDetail objectAtIndex:0] objectAtIndex:1];
    }
    
    if ([_btnSwitch isOn]) {
        NSLog(@"its on!");
        [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                                     @"1", DB_TABLEFIELD_VISITED,
                                                                                     nil]];
    } else {
        NSLog(@"its off!");
        [self.dbHelper Update:DB_TABLENAME_PLACEMASTER placeId:strPlaceId tableData:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                                     @"0", DB_TABLEFIELD_VISITED,
                                                                                     nil]];
    }
}


#pragma mark SetData

- (void)setData {
    
    [self showLoadingIndicator];
    
    //=========== Visit Switch =============
    [_btnSwitch addTarget:self action:@selector(switchToggled:) forControlEvents: UIControlEventTouchUpInside];
    
    NSString *strPlaceVisit = [[_arrPlaceDetail objectAtIndex:0] objectAtIndex:10];//[_dicPlaceDetail valueForKey:@"isVisited"];
    
    if ([strPlaceVisit isEqualToString:@"1"]) {
        [_btnSwitch setOn:YES];
    }else {
        [_btnSwitch setOn:NO];
    }
    
        
    //=========== Like button =============
    NSString *strLikeStatus = [[_arrPlaceDetail objectAtIndex:0] objectAtIndex:9];
    //[_dicPlaceDetail valueForKey:@"isLike"];
    
    if ([strLikeStatus isEqualToString:@"1"]) {
        _likeImgVw.image = [UIImage imageNamed:@"favourite_red"];
    }else {
        _likeImgVw.image = [UIImage imageNamed:@"favourite_gray"];
    }
        
    
    if ([utility isInternetConnected]) {
        int columnNum = 0;
        
        if ([_strSelectedLanguage isEqualToString:@"txt_eng"]) {
            columnNum = 38;
            
        }else if([_strSelectedLanguage isEqualToString:@"txt_islenska"]) {
            columnNum = 39;
            
        }else if([_strSelectedLanguage isEqualToString:@"txt_German"]) {
            columnNum = 40;
            
        }else if([_strSelectedLanguage isEqualToString:@"txt_French"]) {
            columnNum = 41;
            
        }else if([_strSelectedLanguage isEqualToString:@"txt_Spanish"]) {
            columnNum = 42;
      
        }else if([_strSelectedLanguage isEqualToString:@"txt_Chinese"]) {
            columnNum = 43;
            
        }else if([_strSelectedLanguage isEqualToString:@"txt_Danish"]) {
            columnNum = 44;
        }
        
        NSString *strDownloadStatus = [NSString stringWithFormat:@"%@", [[_arrPlaceDetail objectAtIndex:0] objectAtIndex:columnNum]];
        //        NSString *strDownloadStatus = [[_arrPlaceDetail objectAtIndex:0] objectAtIndex:14];
        
        if ([strDownloadStatus isEqualToString:@"1"]) {
            _btnDownload.enabled = NO;
            _btnDownload.hidden = YES;
            _downloadImgVw.hidden = YES;
            //        _downloadImgVw.image = [UIImage imageNamed:@"download_gray"];
            
            [self setOfflineData];
            
        }else {
           
            _btnDownload.enabled = YES;
            _btnDownload.hidden = NO;
            _downloadImgVw.hidden = NO;
            
            //        _downloadImgVw.image = [UIImage imageNamed:@""];
            
            //===========Location =============
            
            /*
            CLLocation *originLocation = [[CLLocation alloc] initWithLatitude:locationManager.location.coordinate.latitude longitude:locationManager.location.coordinate.longitude];
            NSString *strLatitudeDes = [NSString stringWithFormat:@"%@", [_dicPlaceDetail valueForKey:@"latitude"]];
            NSString *strLongtudeDes = [NSString stringWithFormat:@"%@", [_dicPlaceDetail valueForKey:@"longitude"]];
            
            coordinates.latitude = [strLatitudeDes doubleValue];
            coordinates.longitude = [strLongtudeDes doubleValue];
            CLLocation *destinationLocation = [[CLLocation alloc] initWithLatitude:coordinates.latitude longitude:coordinates.longitude];
            
            CLLocationDistance distance = [originLocation distanceFromLocation:destinationLocation];
            
            NSLog(@"distance is %f",distance/1000.0);
            */
            
            double kilometer = [[_dicPlaceDetail valueForKey:@"distance"]  doubleValue];
            self.lblKm.text = [NSString stringWithFormat:@"%.2f km", kilometer];
            // (distance/1000.0)];//[NSString stringWithFormat:@"%@ km",[_dicPlaceDetail valueForKey:@"distance"]];
            
          
            //=============== Set data according selected language ===================
            NSDictionary *value = [[NSUserDefaults standardUserDefaults] objectForKey:PREFERENCE_LANGUAGE_LABEL];
            
            Diclabels = [[NSDictionary alloc] initWithDictionary:value];
            NSLog(@"Diclabels %@",Diclabels);
            
            int columnNum = 0;
            
            if ([_strSelectedLanguage isEqualToString:@"txt_eng"]) {
                columnNum = 38;
                
                _navTitle.text = [_dicPlaceDetail valueForKey:@"place_title"];
                _lblTitle.text = [_dicPlaceDetail valueForKey:@"place_title"];
                _lblPlaceVisited.text = [Diclabels valueForKey:@"txt_place_visited"];
                [_btnSaveImages setTitle:[Diclabels valueForKey:@"txt_save_image"] forState:UIControlStateNormal];
                [_btnFullScreenMap setTitle:[Diclabels valueForKey:@"txt_full_screen_map"] forState:UIControlStateNormal];
                
                base64String = [_dicPlaceDetail valueForKey:@"place_desc"];
                
                [self decodeString:base64String];
                [_detailWebVw loadHTMLString:decodedString baseURL:nil];
                
                
                audioURL = [NSURL URLWithString:[_dicPlaceDetail valueForKey:@"place_audio"]];
                strAudioUrl = [NSString stringWithFormat:@"%@",audioURL];
                
                if ([utility isStringEmpty:strAudioUrl]) {
                    _secondVw.hidden = YES;
                }else {
                    _secondVw.hidden = NO;
                }
                
                [_btnLanguage setTitle:@"EN" forState:UIControlStateNormal];
                [_btnFullScreenMap setTitle:[Diclabels valueForKey:@"txt_full_screen_map"] forState:UIControlStateNormal];
                [_changeMapTypeSegment setTitle:[Diclabels valueForKey:@"txt_normal"] forSegmentAtIndex:0];
                [_changeMapTypeSegment setTitle:[Diclabels valueForKey:@"txt_satellite"] forSegmentAtIndex:1];
                
            }else if([_strSelectedLanguage isEqualToString:@"txt_islenska"]) {
                columnNum = 39;
                
                _navTitle.text = [_dicPlaceDetail valueForKey:@"place_title_islenska"];
                _lblTitle.text = [_dicPlaceDetail valueForKey:@"place_title_islenska"];
                _lblPlaceVisited.text = [Diclabels valueForKey:@"txt_place_visited"];
                [_btnSaveImages setTitle:[Diclabels valueForKey:@"txt_save_image"] forState:UIControlStateNormal];
                
                base64String = [_dicPlaceDetail valueForKey:@"place_desc_islenska"];
                [self decodeString:base64String];
                [_detailWebVw loadHTMLString:decodedString baseURL:nil];
                
                //            _lblKm.text = [NSString stringWithFormat:@"%@ km",[_dicPlaceDetail valueForKey:@"distance"]];
                audioURL = [NSURL URLWithString:[_dicPlaceDetail valueForKey:@"place_audio_islenska"]];
                strAudioUrl = [NSString stringWithFormat:@"%@",audioURL];
                
                if ([utility isStringEmpty:strAudioUrl]) {
                    _secondVw.hidden = YES;
                }else {
                    _secondVw.hidden = NO;
                }
                
                [_btnLanguage setTitle:@"IS" forState:UIControlStateNormal];
                [_btnFullScreenMap setTitle:[Diclabels valueForKey:@"txt_full_screen_map"] forState:UIControlStateNormal];
                [_changeMapTypeSegment setTitle:[Diclabels valueForKey:@"txt_normal"] forSegmentAtIndex:0];
                [_changeMapTypeSegment setTitle:[Diclabels valueForKey:@"txt_satellite"] forSegmentAtIndex:1];
                
            }else if([_strSelectedLanguage isEqualToString:@"txt_German"]) {
                columnNum = 40;
                
                _navTitle.text = [_dicPlaceDetail valueForKey:@"place_title_german"];
                _lblTitle.text = [_dicPlaceDetail valueForKey:@"place_title_german"];
                _lblPlaceVisited.text = [Diclabels valueForKey:@"txt_place_visited"];
                [_btnSaveImages setTitle:[Diclabels valueForKey:@"txt_save_image"] forState:UIControlStateNormal];
                
                base64String = [_dicPlaceDetail valueForKey:@"place_desc_german"];
                [self decodeString:base64String];
                [_detailWebVw loadHTMLString:decodedString baseURL:nil];
                
                //            _lblKm.text = [NSString stringWithFormat:@"%@ km",[_dicPlaceDetail valueForKey:@"distance"]];
                audioURL = [NSURL URLWithString:[_dicPlaceDetail valueForKey:@"place_audio_german"]];
                strAudioUrl = [NSString stringWithFormat:@"%@",audioURL];
                
                if ([utility isStringEmpty:strAudioUrl]) {
                    _secondVw.hidden = YES;
                }else {
                    _secondVw.hidden = NO;
                }
                
                [_btnLanguage setTitle:@"DE" forState:UIControlStateNormal];
                [_btnFullScreenMap setTitle:[Diclabels valueForKey:@"txt_full_screen_map"] forState:UIControlStateNormal];
                [_changeMapTypeSegment setTitle:[Diclabels valueForKey:@"txt_normal"] forSegmentAtIndex:0];
                [_changeMapTypeSegment setTitle:[Diclabels valueForKey:@"txt_satellite"] forSegmentAtIndex:1];
                
            }else if([_strSelectedLanguage isEqualToString:@"txt_French"]) {
                columnNum = 41;
                
                _navTitle.text = [_dicPlaceDetail valueForKey:@"place_title_french"];
                _lblTitle.text = [_dicPlaceDetail valueForKey:@"place_title_french"];
                _lblPlaceVisited.text = [Diclabels valueForKey:@"txt_place_visited"];
                [_btnSaveImages setTitle:[Diclabels valueForKey:@"txt_save_image"] forState:UIControlStateNormal];
                
                base64String = [_dicPlaceDetail valueForKey:@"place_desc_french"];
                [self decodeString:base64String];
                [_detailWebVw loadHTMLString:decodedString baseURL:nil];
                
                //            _lblKm.text = [NSString stringWithFormat:@"%@ km",[_dicPlaceDetail valueForKey:@"distance"]];
                audioURL = [NSURL URLWithString:[_dicPlaceDetail valueForKey:@"place_audio_french"]];
                strAudioUrl = [NSString stringWithFormat:@"%@",audioURL];
                
                if ([utility isStringEmpty:strAudioUrl]) {
                    _secondVw.hidden = YES;
                }else {
                    _secondVw.hidden = NO;
                }
                [_btnLanguage setTitle:@"FR" forState:UIControlStateNormal];
                [_btnFullScreenMap setTitle:[Diclabels valueForKey:@"txt_full_screen_map"] forState:UIControlStateNormal];
                [_changeMapTypeSegment setTitle:[Diclabels valueForKey:@"txt_normal"] forSegmentAtIndex:0];
                [_changeMapTypeSegment setTitle:[Diclabels valueForKey:@"txt_satellite"] forSegmentAtIndex:1];
                
            }else if([_strSelectedLanguage isEqualToString:@"txt_Chinese"]) {
                columnNum = 42;
                
                _navTitle.text = [_dicPlaceDetail valueForKey:@"place_title_chinese"];
                _lblTitle.text = [_dicPlaceDetail valueForKey:@"place_title_chinese"];
                _lblPlaceVisited.text = [Diclabels valueForKey:@"txt_place_visited"];
                [_btnSaveImages setTitle:[Diclabels valueForKey:@"txt_save_image"] forState:UIControlStateNormal];
                
                base64String = [_dicPlaceDetail valueForKey:@"place_desc_chinese"];
                [self decodeString:base64String];
                [_detailWebVw loadHTMLString:decodedString baseURL:nil];
                
                
                //            _lblKm.text = [NSString stringWithFormat:@"%@ km",[_dicPlaceDetail valueForKey:@"distance"]];
                audioURL = [NSURL URLWithString:[_dicPlaceDetail valueForKey:@"place_audio_chinese"]];
                strAudioUrl = [NSString stringWithFormat:@"%@",audioURL];
                
                if ([utility isStringEmpty:strAudioUrl]) {
                    _secondVw.hidden = YES;
                }else {
                    _secondVw.hidden = NO;
                }
                [_btnLanguage setTitle:@"ZH" forState:UIControlStateNormal];
                [_btnFullScreenMap setTitle:[Diclabels valueForKey:@"txt_full_screen_map"] forState:UIControlStateNormal];
                [_changeMapTypeSegment setTitle:[Diclabels valueForKey:@"txt_normal"] forSegmentAtIndex:0];
                [_changeMapTypeSegment setTitle:[Diclabels valueForKey:@"txt_satellite"] forSegmentAtIndex:1];
                
            }else if([_strSelectedLanguage isEqualToString:@"txt_Danish"]) {
                columnNum = 43;
                
                _navTitle.text = [_dicPlaceDetail valueForKey:@"place_title_danish"];
                _lblTitle.text = [_dicPlaceDetail valueForKey:@"place_title_danish"];
                _lblPlaceVisited.text = [Diclabels valueForKey:@"txt_place_visited"];
                [_btnSaveImages setTitle:[Diclabels valueForKey:@"txt_save_image"] forState:UIControlStateNormal];
                
                base64String = [_dicPlaceDetail valueForKey:@"place_desc_danish"];
                [self decodeString:base64String];
                [_detailWebVw loadHTMLString:decodedString baseURL:nil];
                
                //            _lblKm.text = [NSString stringWithFormat:@"%@ km",[_dicPlaceDetail valueForKey:@"distance"]];
                audioURL = [NSURL URLWithString:[_dicPlaceDetail valueForKey:@"place_audio_danish"]];
                strAudioUrl = [NSString stringWithFormat:@"%@",audioURL];
                
                if ([utility isStringEmpty:strAudioUrl]) {
                    _secondVw.hidden = YES;
                }else {
                    _secondVw.hidden = NO;
                }
                
                [_btnLanguage setTitle:@"DA" forState:UIControlStateNormal];
                [_btnFullScreenMap setTitle:[Diclabels valueForKey:@"txt_full_screen_map"] forState:UIControlStateNormal];
                [_changeMapTypeSegment setTitle:[Diclabels valueForKey:@"txt_normal"] forSegmentAtIndex:0];
                [_changeMapTypeSegment setTitle:[Diclabels valueForKey:@"txt_satellite"] forSegmentAtIndex:1];
                
            }else if([_strSelectedLanguage isEqualToString:@"txt_Spanish"]) {
                columnNum = 44;
                
                _navTitle.text = [_dicPlaceDetail valueForKey:@"place_title_spanish"];
                _lblTitle.text = [_dicPlaceDetail valueForKey:@"place_title_spanish"];
                _lblPlaceVisited.text = [Diclabels valueForKey:@"txt_place_visited"];
                [_btnSaveImages setTitle:[Diclabels valueForKey:@"txt_save_image"] forState:UIControlStateNormal];
                
                base64String = [_dicPlaceDetail valueForKey:@"place_desc_spanish"];
                [self decodeString:base64String];
                [_detailWebVw loadHTMLString:decodedString baseURL:nil];
                
                //            _lblKm.text = [NSString stringWithFormat:@"%@ km",[_dicPlaceDetail valueForKey:@"distance"]];
                audioURL = [NSURL URLWithString:[_dicPlaceDetail valueForKey:@"place_audio_spanish"]];
                strAudioUrl = [NSString stringWithFormat:@"%@",audioURL];
                
                if ([utility isStringEmpty:strAudioUrl]) {
                    _secondVw.hidden = YES;
                }else {
                    _secondVw.hidden = NO;
                }
                
                [_btnLanguage setTitle:@"ES" forState:UIControlStateNormal];
                [_btnFullScreenMap setTitle:[Diclabels valueForKey:@"txt_full_screen_map"] forState:UIControlStateNormal];
                [_changeMapTypeSegment setTitle:[Diclabels valueForKey:@"txt_normal"] forSegmentAtIndex:0];
                [_changeMapTypeSegment setTitle:[Diclabels valueForKey:@"txt_satellite"] forSegmentAtIndex:1];
            }
            
            
            
            
            //=========== Multiple Image store in Directory =================
            
            NSArray *arrImagesTemp = [_dicPlaceDetail valueForKey:@"place_images"];
            
            for (NSDictionary *dicPlacesImages in arrImagesTemp) {
                [arrImages addObject:[dicPlacesImages valueForKey:@"place_img_name"]];
            }
            NSLog(@"arrImages %@:",arrImages);
          
            if (arrImages.count <= 1) {
                _btnNext.hidden = YES;
                _btnPrev.hidden = YES;
            }
            
            [_detailImageVw sd_setImageWithURL:[NSURL URLWithString:[arrImages objectAtIndex:index]]];

            /*
            NSInteger anIndex = 0;
            
            for (int i=0; i<arrImages.count; i++) {
                
                NSURL *imageURL = [NSURL URLWithString:[arrImages objectAtIndex:i]];
                pngData = [NSData dataWithContentsOfURL:imageURL];//UIImagePNGRepresentation(image);
                
                NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                NSString *documentsPath = [paths objectAtIndex:0]; //Get the docs directory
                
                NSString *anImageName = [NSString stringWithFormat:@"%@detail.png", [formatter stringFromDate:[NSDate date]]];
                //[NSString stringWithFormat:@"%ldDetail.png", (long)anIndex++];
                anImagePath = [NSString stringWithFormat:@"%@/%@", documentsPath, anImageName];
                //            NSLog(@"File path is %@",anImagePath);
                
                [pngData writeToFile:anImagePath atomically:YES]; //Write the file
                [arrLocalPlacesImg addObject:anImagePath];
                
            }
            NSLog(@"Data %@",arrLocalPlacesImg);
            //    [_detailImageVw sd_setImageWithURL:[NSURL URLWithString:[arrLocalPlacesImg objectAtIndex:index]]];
            UIImage *myImage = [UIImage imageWithContentsOfFile:[arrLocalPlacesImg objectAtIndex:index]];
            _detailImageVw.image = myImage;
            */
            
            
            for (NSDictionary *dicPlacesLargeImages in arrImagesTemp) {
                [arrLargeImgs addObject:[dicPlacesLargeImages valueForKey:@"place_img_name_large"]];//@"place_img_name"]];
            }
            NSLog(@"arrLargeImgs %@:",arrLargeImgs);
            
            
            // Show pin in map
            GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] init];
            
            coordinates.latitude = [[_dicPlaceDetail valueForKey:@"latitude"] floatValue];
            coordinates.longitude = [[_dicPlaceDetail valueForKey:@"longitude"] floatValue];
            // Creates a marker in the center of the map.
            GMSMarker *marker = [[GMSMarker alloc] init];
            marker.icon = [UIImage imageNamed:(@"map_pin_selected.png")];
            marker.position = CLLocationCoordinate2DMake(coordinates.latitude, coordinates.longitude);
            
            GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:coordinates.latitude
                                                                    longitude:coordinates.longitude
                                                                         zoom:10];
            _mapVw.camera = camera;
        
            marker.map = _mapVw;
            
            [_mapVw animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds withPadding:12.0f]];
        }
        
    }else {
        
        [self setOfflineData];
    }

    
    [self stopIndicator];

}

#pragma mark Decode string function

- (void)decodeString:(NSString *)base64String {
    
    NSData *decodedData = [[NSData alloc] initWithBase64EncodedString:base64String options:0];
    decodedString = [[NSString alloc] initWithData:decodedData encoding:NSUTF8StringEncoding];
    decodedString = [NSString stringWithFormat:@"<span style=\"font-family: %@; font-size: %i\">%@</span>",
                  @"Roboto-Regular",
                  13,
                  decodedString];
    
    NSLog(@"Decode String Value: %@", decodedString);
}

#pragma mark ----------------
#pragma mark WebView Delegate
#pragma mark ----------------

- (void)webViewDidFinishLoad:(UIWebView *)webView {
//    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    CGFloat height = [[webView stringByEvaluatingJavaScriptFromString:@"document.body.scrollHeight"] floatValue];
    NSLog(@"Webview height is:: %f", height);
    _webVwHeightContraint.constant = height;
//    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

- (void)setOfflineData {
    
    [_btnSwitch addTarget:self action:@selector(switchToggled:) forControlEvents: UIControlEventTouchUpInside];
    
    NSString *strPlaceVisit = [[_arrPlaceDetail objectAtIndex:0] objectAtIndex:10];//[_dicPlaceDetail valueForKey:@"isVisited"];
    
    if ([strPlaceVisit isEqualToString:@"1"]) {
        [_btnSwitch setOn:YES];
    }else {
        [_btnSwitch setOn:NO];
    }
    
    
    //=========== Like button =============
    NSString *strLikeStatus = [[_arrPlaceDetail objectAtIndex:0] objectAtIndex:9];
    //[_dicPlaceDetail valueForKey:@"isLike"];
    
    if ([strLikeStatus isEqualToString:@"1"]) {
        _likeImgVw.image = [UIImage imageNamed:@"favourite_red"];
    }else {
        _likeImgVw.image = [UIImage imageNamed:@"favourite_gray"];
    }
    
    
    //=========== Display distance =============
    /*
    CLLocation *originLocation = [[CLLocation alloc] initWithLatitude:locationManager.location.coordinate.latitude longitude:locationManager.location.coordinate.longitude];
    NSString *strLatitudeDes = [NSString stringWithFormat:@"%@", [[_arrPlaceDetail objectAtIndex:0] objectAtIndex:7]];
    NSString *strLongtudeDes = [NSString stringWithFormat:@"%@", [[_arrPlaceDetail objectAtIndex:0] objectAtIndex:8]];
    
    coordinates.latitude = [strLatitudeDes doubleValue];
    coordinates.longitude = [strLongtudeDes doubleValue];
    CLLocation *destinationLocation = [[CLLocation alloc] initWithLatitude:coordinates.latitude longitude:coordinates.longitude];
    
    CLLocationDistance distance = [originLocation distanceFromLocation:destinationLocation];
    
    NSLog(@"distance is %f",distance/1000.0);
    
    _lblKm.text = [NSString stringWithFormat:@"%.2f km", (distance/1000.0)];//[NSString stringWithFormat:@"%@ km",[_dicPlaceDetail valueForKey:@"distance"]];
    */
    
    double kilometer = [[[_arrPlaceDetail objectAtIndex:0] objectAtIndex:6] doubleValue];
    self.lblKm.text = [NSString stringWithFormat:@"%.2f km", kilometer];
    
    
    //=============== Set data according selected language ===================
    NSDictionary *value = [[NSUserDefaults standardUserDefaults] objectForKey:PREFERENCE_LANGUAGE_LABEL];
    
    Diclabels = [[NSDictionary alloc] initWithDictionary:value];
    NSLog(@"Diclabels %@",Diclabels);
    
    int columnNum = 0;
   
    NSInteger indexOfTitleGr = [self.dbHelper.arrColumnNames indexOfObject:DB_TABLEFIELD_NAME_GERMAN];
    NSInteger indexOfTitleFr = [self.dbHelper.arrColumnNames indexOfObject:DB_TABLEFIELD_NAME_FRENCH];
    NSInteger indexOfTitleEs = [self.dbHelper.arrColumnNames indexOfObject:DB_TABLEFIELD_NAME_SPANISH];
    NSInteger indexOfTitleCh = [self.dbHelper.arrColumnNames indexOfObject:DB_TABLEFIELD_NAME_CHINESE];
    NSInteger indexOfTitleDa = [self.dbHelper.arrColumnNames indexOfObject:DB_TABLEFIELD_NAME_DANISH];

    NSInteger indexOfDesGr = [self.dbHelper.arrColumnNames indexOfObject:DB_TABLEFIELD_DES_GERMAN];
    NSInteger indexOfDesFr = [self.dbHelper.arrColumnNames indexOfObject:DB_TABLEFIELD_DES_FRENCH];
    NSInteger indexOfDesEs = [self.dbHelper.arrColumnNames indexOfObject:DB_TABLEFIELD_DES_SPANISH];
    NSInteger indexOfDesCh = [self.dbHelper.arrColumnNames indexOfObject:DB_TABLEFIELD_DES_CHINESE];
    NSInteger indexOfDesDa = [self.dbHelper.arrColumnNames indexOfObject:DB_TABLEFIELD_DES_DANISH];
    
    NSInteger indexOfAudioGr = [self.dbHelper.arrColumnNames indexOfObject:DB_TABLEFIELD_AUDIO_GERMAN];
    NSInteger indexOfAudioFr = [self.dbHelper.arrColumnNames indexOfObject:DB_TABLEFIELD_AUDIO_FRENCH];
    NSInteger indexOfAudioEs = [self.dbHelper.arrColumnNames indexOfObject:DB_TABLEFIELD_AUDIO_SPANISH];
    NSInteger indexOfAudioCh = [self.dbHelper.arrColumnNames indexOfObject:DB_TABLEFIELD_AUDIO_CHINESE];
    NSInteger indexOfAudioDa = [self.dbHelper.arrColumnNames indexOfObject:DB_TABLEFIELD_AUDIO_DANISH];
    
    if ([_strSelectedLanguage isEqualToString:@"txt_eng"]) {
        columnNum = 38;
        
        _navTitle.text = [[_arrPlaceDetail objectAtIndex:0] objectAtIndex:2];//[_dicPlaceDetail valueForKey:@"place_title"];
        _lblTitle.text = [[_arrPlaceDetail objectAtIndex:0] objectAtIndex:2];//[_dicPlaceDetail valueForKey:@"place_title"];
        _lblPlaceVisited.text = [Diclabels valueForKey:@"txt_place_visited"];
        [_btnSaveImages setTitle:[Diclabels valueForKey:@"txt_save_image"] forState:UIControlStateNormal];
        [_btnFullScreenMap setTitle:[Diclabels valueForKey:@"txt_full_screen_map"] forState:UIControlStateNormal];
        
        base64String = [[_arrPlaceDetail objectAtIndex:0] objectAtIndex:4];//[_dicPlaceDetail valueForKey:@"place_desc"];
        
        [self decodeString:base64String];
        [_detailWebVw loadHTMLString:decodedString baseURL:nil];
        
        
        audioURL = [[_arrPlaceDetail objectAtIndex:0] objectAtIndex:12];//[NSURL URLWithString:[_dicPlaceDetail valueForKey:@"place_audio"]];
        strAudioUrl = [NSString stringWithFormat:@"%@", audioURL];
        
        if ([utility isStringEmpty:strAudioUrl]) {
            _secondVw.hidden = YES;
        }else {
            _secondVw.hidden = NO;
        }
        
        [_btnLanguage setTitle:@"EN" forState:UIControlStateNormal];
        [_btnFullScreenMap setTitle:[Diclabels valueForKey:@"txt_full_screen_map"] forState:UIControlStateNormal];
        [_changeMapTypeSegment setTitle:[Diclabels valueForKey:@"txt_normal"] forSegmentAtIndex:0];
        [_changeMapTypeSegment setTitle:[Diclabels valueForKey:@"txt_satellite"] forSegmentAtIndex:1];
        
    }else if([_strSelectedLanguage isEqualToString:@"txt_islenska"]) {
        columnNum = 39;
        
        _navTitle.text = [[_arrPlaceDetail objectAtIndex:0] objectAtIndex:3];//[_dicPlaceDetail valueForKey:@"place_title_islenska"];
        _lblTitle.text = [[_arrPlaceDetail objectAtIndex:0] objectAtIndex:3];//[_dicPlaceDetail valueForKey:@"place_title_islenska"];
        _lblPlaceVisited.text = [Diclabels valueForKey:@"txt_place_visited"];
        [_btnSaveImages setTitle:[Diclabels valueForKey:@"txt_save_image"] forState:UIControlStateNormal];
        
        base64String = [[_arrPlaceDetail objectAtIndex:0] objectAtIndex:5];//[_dicPlaceDetail valueForKey:@"place_desc_islenska"];
        [self decodeString:base64String];
        [_detailWebVw loadHTMLString:decodedString baseURL:nil];
        
        //            _lblKm.text = [NSString stringWithFormat:@"%@ km",[_dicPlaceDetail valueForKey:@"distance"]];
        audioURL = [[_arrPlaceDetail objectAtIndex:0] objectAtIndex:13];//[NSURL URLWithString:[_dicPlaceDetail valueForKey:@"place_audio_islenska"]];
        strAudioUrl = [NSString stringWithFormat:@"%@",audioURL];
        
        if ([utility isStringEmpty:strAudioUrl]) {
            _secondVw.hidden = YES;
        }else {
            _secondVw.hidden = NO;
        }
        
        [_btnLanguage setTitle:@"IS" forState:UIControlStateNormal];
        [_btnFullScreenMap setTitle:[Diclabels valueForKey:@"txt_full_screen_map"] forState:UIControlStateNormal];
        [_changeMapTypeSegment setTitle:[Diclabels valueForKey:@"txt_normal"] forSegmentAtIndex:0];
        [_changeMapTypeSegment setTitle:[Diclabels valueForKey:@"txt_satellite"] forSegmentAtIndex:1];
        
    }else if([_strSelectedLanguage isEqualToString:@"txt_German"]) {
        columnNum = 40;
        
        _navTitle.text = [[_arrPlaceDetail objectAtIndex:0] objectAtIndex:indexOfTitleGr];//[_dicPlaceDetail valueForKey:@"place_title_german"];
        _lblTitle.text = [[_arrPlaceDetail objectAtIndex:0] objectAtIndex:indexOfTitleGr];//[_dicPlaceDetail valueForKey:@"place_title_german"];
        _lblPlaceVisited.text = [Diclabels valueForKey:@"txt_place_visited"];
        [_btnSaveImages setTitle:[Diclabels valueForKey:@"txt_save_image"] forState:UIControlStateNormal];
        
        base64String = [[_arrPlaceDetail objectAtIndex:0] objectAtIndex:indexOfDesGr];//[_dicPlaceDetail valueForKey:@"place_desc_german"];
        [self decodeString:base64String];
        [_detailWebVw loadHTMLString:decodedString baseURL:nil];
        
        //            _lblKm.text = [NSString stringWithFormat:@"%@ km",[_dicPlaceDetail valueForKey:@"distance"]];
        audioURL = [NSURL URLWithString:[[_arrPlaceDetail objectAtIndex:0] objectAtIndex:indexOfAudioGr]];
        strAudioUrl = [NSString stringWithFormat:@"%@",audioURL];
        
        if ([utility isStringEmpty:strAudioUrl]) {
            _secondVw.hidden = YES;
        }else {
            _secondVw.hidden = NO;
        }
        
        [_btnLanguage setTitle:@"DE" forState:UIControlStateNormal];
        [_btnFullScreenMap setTitle:[Diclabels valueForKey:@"txt_full_screen_map"] forState:UIControlStateNormal];
        [_changeMapTypeSegment setTitle:[Diclabels valueForKey:@"txt_normal"] forSegmentAtIndex:0];
        [_changeMapTypeSegment setTitle:[Diclabels valueForKey:@"txt_satellite"] forSegmentAtIndex:1];
        
    }else if([_strSelectedLanguage isEqualToString:@"txt_French"]) {
        columnNum = 41;
        
        _navTitle.text = [[_arrPlaceDetail objectAtIndex:0] objectAtIndex:indexOfTitleFr];
        _lblTitle.text = [[_arrPlaceDetail objectAtIndex:0] objectAtIndex:indexOfTitleFr];
        _lblPlaceVisited.text = [Diclabels valueForKey:@"txt_place_visited"];
        [_btnSaveImages setTitle:[Diclabels valueForKey:@"txt_save_image"] forState:UIControlStateNormal];
        
        base64String = [[_arrPlaceDetail objectAtIndex:0] objectAtIndex:indexOfDesFr];
        [self decodeString:base64String];
        [_detailWebVw loadHTMLString:decodedString baseURL:nil];
        
        //            _lblKm.text = [NSString stringWithFormat:@"%@ km",[_dicPlaceDetail valueForKey:@"distance"]];
        audioURL = [NSURL URLWithString:[[_arrPlaceDetail objectAtIndex:0] objectAtIndex:indexOfAudioFr]];
        strAudioUrl = [NSString stringWithFormat:@"%@",audioURL];
        
        if ([utility isStringEmpty:strAudioUrl]) {
            _secondVw.hidden = YES;
        }else {
            _secondVw.hidden = NO;
        }
        
        [_btnLanguage setTitle:@"FR" forState:UIControlStateNormal];
        [_btnFullScreenMap setTitle:[Diclabels valueForKey:@"txt_full_screen_map"] forState:UIControlStateNormal];
        [_changeMapTypeSegment setTitle:[Diclabels valueForKey:@"txt_normal"] forSegmentAtIndex:0];
        [_changeMapTypeSegment setTitle:[Diclabels valueForKey:@"txt_satellite"] forSegmentAtIndex:1];
        
    }else if([_strSelectedLanguage isEqualToString:@"txt_Chinese"]) {
        columnNum = 42;
        
        _navTitle.text = [[_arrPlaceDetail objectAtIndex:0] objectAtIndex:indexOfTitleCh];
        _lblTitle.text = [[_arrPlaceDetail objectAtIndex:0] objectAtIndex:indexOfTitleCh];
        _lblPlaceVisited.text = [Diclabels valueForKey:@"txt_place_visited"];
        [_btnSaveImages setTitle:[Diclabels valueForKey:@"txt_save_image"] forState:UIControlStateNormal];
        
        base64String = [[_arrPlaceDetail objectAtIndex:0] objectAtIndex:indexOfDesCh];
        [self decodeString:base64String];
        [_detailWebVw loadHTMLString:decodedString baseURL:nil];
        
        //            _lblKm.text = [NSString stringWithFormat:@"%@ km",[_dicPlaceDetail valueForKey:@"distance"]];
        audioURL = [NSURL URLWithString:[[_arrPlaceDetail objectAtIndex:0] objectAtIndex:indexOfAudioCh]];
        strAudioUrl = [NSString stringWithFormat:@"%@",audioURL];
        
        if ([utility isStringEmpty:strAudioUrl]) {
            _secondVw.hidden = YES;
        }else {
            _secondVw.hidden = NO;
        }
        [_btnLanguage setTitle:@"ZH" forState:UIControlStateNormal];
        [_btnFullScreenMap setTitle:[Diclabels valueForKey:@"txt_full_screen_map"] forState:UIControlStateNormal];
        [_changeMapTypeSegment setTitle:[Diclabels valueForKey:@"txt_normal"] forSegmentAtIndex:0];
        [_changeMapTypeSegment setTitle:[Diclabels valueForKey:@"txt_satellite"] forSegmentAtIndex:1];
        
    }else if([_strSelectedLanguage isEqualToString:@"txt_Danish"]) {
        columnNum = 43;
        
        _navTitle.text = [[_arrPlaceDetail objectAtIndex:0] objectAtIndex:indexOfTitleDa];
        _lblTitle.text = [[_arrPlaceDetail objectAtIndex:0] objectAtIndex:indexOfTitleDa];
        _lblPlaceVisited.text = [Diclabels valueForKey:@"txt_place_visited"];
        [_btnSaveImages setTitle:[Diclabels valueForKey:@"txt_save_image"] forState:UIControlStateNormal];
        
        base64String = [[_arrPlaceDetail objectAtIndex:0] objectAtIndex:indexOfDesDa];
        [self decodeString:base64String];
        [_detailWebVw loadHTMLString:decodedString baseURL:nil];
        
        //            _lblKm.text = [NSString stringWithFormat:@"%@ km",[_dicPlaceDetail valueForKey:@"distance"]];
        audioURL = [NSURL URLWithString:[[_arrPlaceDetail objectAtIndex:0] objectAtIndex:indexOfAudioDa]];
        strAudioUrl = [NSString stringWithFormat:@"%@",audioURL];
        
        if ([utility isStringEmpty:strAudioUrl]) {
            _secondVw.hidden = YES;
        }else {
            _secondVw.hidden = NO;
        }
        
        [_btnLanguage setTitle:@"DA" forState:UIControlStateNormal];
        [_btnFullScreenMap setTitle:[Diclabels valueForKey:@"txt_full_screen_map"] forState:UIControlStateNormal];
        [_changeMapTypeSegment setTitle:[Diclabels valueForKey:@"txt_normal"] forSegmentAtIndex:0];
        [_changeMapTypeSegment setTitle:[Diclabels valueForKey:@"txt_satellite"] forSegmentAtIndex:1];
        
    }else if([_strSelectedLanguage isEqualToString:@"txt_Spanish"]) {
        columnNum = 44;
        
        _navTitle.text = [[_arrPlaceDetail objectAtIndex:0] objectAtIndex:indexOfTitleEs];;
        _lblTitle.text = [[_arrPlaceDetail objectAtIndex:0] objectAtIndex:indexOfTitleEs];
        _lblPlaceVisited.text = [Diclabels valueForKey:@"txt_place_visited"];
        [_btnSaveImages setTitle:[Diclabels valueForKey:@"txt_save_image"] forState:UIControlStateNormal];
        
        base64String = [[_arrPlaceDetail objectAtIndex:0] objectAtIndex:indexOfDesEs];
        [self decodeString:base64String];
        [_detailWebVw loadHTMLString:decodedString baseURL:nil];
        
        //            _lblKm.text = [NSString stringWithFormat:@"%@ km",[_dicPlaceDetail valueForKey:@"distance"]];
        audioURL = [NSURL URLWithString:[[_arrPlaceDetail objectAtIndex:0] objectAtIndex:indexOfAudioEs]];
        strAudioUrl = [NSString stringWithFormat:@"%@",audioURL];
        
        if ([utility isStringEmpty:strAudioUrl]) {
            _secondVw.hidden = YES;
        }else {
            _secondVw.hidden = NO;
        }
        
        [_btnLanguage setTitle:@"ES" forState:UIControlStateNormal];
        [_btnFullScreenMap setTitle:[Diclabels valueForKey:@"txt_full_screen_map"] forState:UIControlStateNormal];
        [_changeMapTypeSegment setTitle:[Diclabels valueForKey:@"txt_normal"] forSegmentAtIndex:0];
        [_changeMapTypeSegment setTitle:[Diclabels valueForKey:@"txt_satellite"] forSegmentAtIndex:1];
    }
    
    NSString *strDownloadStatus = [NSString stringWithFormat:@"%@", [[_arrPlaceDetail objectAtIndex:0] objectAtIndex:columnNum]];
    //        NSString *strDownloadStatus = [[_arrPlaceDetail objectAtIndex:0] objectAtIndex:14];
    
    if ([strDownloadStatus isEqualToString:@"1"]) {
        _btnDownload.enabled = NO;
        _btnDownload.hidden = YES;
        _downloadImgVw.hidden = YES;
        
    }else {
        _btnDownload.enabled = YES;
        _btnDownload.hidden = NO;
        _downloadImgVw.hidden = NO;
        
    }
    
    // ===== Display Image offline ========
    if (_arrDetailImages.count <= 1) {
        _btnNext.hidden = YES;
        _btnPrev.hidden = YES;
    }
    
    NSString *strPath = [NSString stringWithFormat:@"%@",[_arrDetailImages objectAtIndex:index]];
    NSLog(@"strPath %@",strPath);
    UIImage *myImage = [UIImage imageWithContentsOfFile:strPath];
    _detailImageVw.image = myImage;
   
    
    //========= Map on Pin =============
    GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] init];
   
    // Creates a marker in the center of the map.
    GMSMarker *marker = [[GMSMarker alloc] init];
    marker.icon = [UIImage imageNamed:(@"map_pin_selected.png")];
    marker.position = CLLocationCoordinate2DMake(coordinates.latitude, coordinates.longitude);
    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:coordinates.latitude
                                                            longitude:coordinates.longitude
                                                                 zoom:10];
    
    _mapVw.camera = camera;

    marker.map = _mapVw;
    
    [_mapVw animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds withPadding:12.0f]];

}

@end

//
//  MoreAppViewController.h
//  Thingvellir
//
//  Created by saturncube on 01/06/18.
//  Copyright © 2018 saturncube. All rights reserved.
//

#import "BaseViewController.h"

@interface MoreAppViewController : BaseViewController

@property NSString *strLanguage, *strNavTitle;

@end

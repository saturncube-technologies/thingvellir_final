//
//  PWSMapViewController.h
//  Thingvellir
//
//  Created by saturncube on 12/07/18.
//  Copyright © 2018 saturncube. All rights reserved.
//

#import "BaseViewController.h"

@interface PWSMapViewController : BaseViewController

@property NSMutableArray *arrMutData;
@property NSString *strLanguage, *strLocationType, *strNavTitle;

@end

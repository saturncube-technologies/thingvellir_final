//
//  PlaceListViewController.h
//  Thingvellir
//
//  Created by saturncube on 25/05/18.
//  Copyright © 2018 saturncube. All rights reserved.
//

#import "BaseViewController.h"
#import <GoogleMaps/GoogleMaps.h>

@interface PlaceListViewController : BaseViewController <GMSMapViewDelegate>

@property NSString *strLanguageCode;

@end

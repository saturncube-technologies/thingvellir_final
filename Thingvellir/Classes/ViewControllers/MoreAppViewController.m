//
//  MoreAppViewController.m
//  Thingvellir
//
//  Created by saturncube on 01/06/18.
//  Copyright © 2018 saturncube. All rights reserved.
//

#import "MoreAppViewController.h"
#import "LanguageViewController.h"
#import "PlaceListViewController.h"
#import "MoreCell.h"

@interface MoreAppViewController ()<LanguageViewControllerDelegate> {
    
    NSMutableArray *arrData;
    NSDictionary *Diclabels;
}

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *navigationVwHeightContraint;

@property (strong, nonatomic) IBOutlet UILabel *naviTitle;
@property (strong, nonatomic) IBOutlet UIButton *btnLanguage;
@property (strong, nonatomic) IBOutlet UITableView *tblAppList;
@property (weak, nonatomic) IBOutlet UILabel *lblComingSoon;

@end

@implementation MoreAppViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //============ Array initialised ==================
    arrData = [[NSMutableArray alloc] init];
    
    //============ Navigationbar custom height ==================
    if (DEVICE_IS_IPHONE_X) {
        _navigationVwHeightContraint.constant = 84;
    }
    
    //============ Tableview ==================
    _tblAppList.tableFooterView = [UIView new];
    
    
    [self getMoreData];
    
    _naviTitle.text = _strNavTitle;
    
    
    NSDictionary *value = [[NSUserDefaults standardUserDefaults] objectForKey:PREFERENCE_LANGUAGE_LABEL];
    Diclabels = [[NSDictionary alloc] initWithDictionary:value];
    
    _lblComingSoon.text = [Diclabels valueForKey:@"txt_coming_soon"];

}

- (void)viewDidLayoutSubviews {
    
    [super viewDidLayoutSubviews];
  
    [_naviTitle setFont:[UIFont fontWithName:FONT_ROBOTO_REGULAR size:15]];

    [_lblComingSoon setFont:[UIFont fontWithName:FONT_ROBOTO_REGULAR size:14]];
    [_lblComingSoon setTextColor:colorFromRGB(102, 102, 102)];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dataFromController:(NSString *)string {
    NSLog(@" string is : %@",string);
    _strLanguage = [NSString stringWithFormat:@"%@", string];
    [[NSUserDefaults standardUserDefaults] setObject:_strLanguage forKey:PREFERENCE_LANGUAGE];
    NSLog(@"_strLanguageCode : %@",_strLanguage);
}

#pragma mark Button Actions

- (IBAction)btnBackTapped:(UIButton *)sender {
    UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
    PlaceListViewController *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"PlaceListViewController"];
    NSArray *controllers = [NSArray arrayWithObject:VC];
    navigationController.viewControllers = controllers;
}

- (IBAction)btnSelectLanguageTapped:(UIButton *)sender {
    LanguageViewController *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"LanguageViewController"];
    VC.delegate = self;
    VC.strSelectedLanguage = _strLanguage;
    [self.navigationController pushViewController:VC animated:YES];
}

#pragma mark ---------------------------------
#pragma mark -- TableView Datasource Delegate Event
#pragma mark ---------------------------------

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return arrData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    MoreCell *cell = (MoreCell *)[tableView dequeueReusableCellWithIdentifier:@"MoreCell"];
    
    [cell.moreImageVw sd_setImageWithURL:[NSURL URLWithString: [[arrData valueForKey:@"app_image"] objectAtIndex:indexPath.row]] placeholderImage:[UIImage imageNamed:@"picture"]];

   
    cell.lblTitle.textColor = colorFromRGB(33, 33, 33);
    [cell.lblTitle setFont:[UIFont fontWithName:FONT_ROBOTO_BOLD size:14]];
    
    cell.lblDetail.textColor = colorFromRGB(102, 102, 102);
    [cell.lblDetail setFont:[UIFont fontWithName:FONT_ROBOTO_REGULAR size:13]];
    
    cell.moreImageVw.clipsToBounds = YES;
    
    //=============== Set data according selected language ===================
    
    if ([_strLanguage isEqualToString:@"txt_eng"]) {
        
        cell.lblTitle.text = [[arrData valueForKey:@"app_title"] objectAtIndex:indexPath.row];
        cell.lblDetail.text = [[arrData valueForKey:@"app_desc"] objectAtIndex:indexPath.row];
        [_btnLanguage setTitle:@"EN" forState:UIControlStateNormal];

    }else if([_strLanguage isEqualToString:@"txt_islenska"]) {
        
        cell.lblTitle.text = [[arrData valueForKey:@"app_title_islenska"] objectAtIndex:indexPath.row];
        cell.lblDetail.text = [[arrData valueForKey:@"app_desc_islenska"] objectAtIndex:indexPath.row];
        [_btnLanguage setTitle:@"IS" forState:UIControlStateNormal];

    }else if([_strLanguage isEqualToString:@"txt_German"]) {
        
        cell.lblTitle.text = [[arrData valueForKey:@"app_title_german"] objectAtIndex:indexPath.row];
        cell.lblDetail.text = [[arrData valueForKey:@"app_desc_german"] objectAtIndex:indexPath.row];
        [_btnLanguage setTitle:@"DE" forState:UIControlStateNormal];

    }else if([_strLanguage isEqualToString:@"txt_French"]) {
        
        cell.lblTitle.text = [[arrData valueForKey:@"app_title_french"] objectAtIndex:indexPath.row];
        cell.lblDetail.text = [[arrData valueForKey:@"app_desc_french"] objectAtIndex:indexPath.row];
        [_btnLanguage setTitle:@"FR" forState:UIControlStateNormal];

    }else if([_strLanguage isEqualToString:@"txt_Chinese"]) {
    
        cell.lblTitle.text = [[arrData valueForKey:@"app_title_chinese"] objectAtIndex:indexPath.row];
        cell.lblDetail.text = [[arrData valueForKey:@"app_desc_chinese"] objectAtIndex:indexPath.row];
        [_btnLanguage setTitle:@"ZH" forState:UIControlStateNormal];

    }else if([_strLanguage isEqualToString:@"txt_Danish"]) {
   
        cell.lblTitle.text = [[arrData valueForKey:@"app_title_danish"] objectAtIndex:indexPath.row];
        cell.lblDetail.text = [[arrData valueForKey:@"app_desc_danish"] objectAtIndex:indexPath.row];
        [_btnLanguage setTitle:@"DA" forState:UIControlStateNormal];

    }else if([_strLanguage isEqualToString:@"txt_Spanish"]) {
   
        cell.lblTitle.text = [[arrData valueForKey:@"app_title_spanish"] objectAtIndex:indexPath.row];
        cell.lblDetail.text = [[arrData valueForKey:@"app_desc_spanish"] objectAtIndex:indexPath.row];
        [_btnLanguage setTitle:@"ES" forState:UIControlStateNormal];

    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    UIApplication *application = [UIApplication sharedApplication];
    NSURL *URL = [NSURL URLWithString:[[arrData valueForKey:@"app_link"] objectAtIndex:indexPath.row]];
    
    [application openURL:URL options:@{} completionHandler:^(BOOL success) {
        if (success) {
            NSLog(@"Opened url");
        }
    }];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

#pragma mark -------------------------
#pragma mark API response
#pragma mark -------------------------

- (void)getMoreData {

    NSMutableDictionary *parameters = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                       @"2", @"platform",
                                       nil];
    
    NSLog(@"Parameters is: %@",parameters);
    
    [self showLoadingIndicator];
    
    [APIHelper POST:kApiMore parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSLog(@"responseObject %@ :", responseObject);
        
        arrData = (NSMutableArray *)[responseObject valueForKey:@"result"];
        
        if (arrData.count == 0) {
            _lblComingSoon.hidden = NO;
        }else {
            _lblComingSoon.hidden = YES;
        }
        
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            
            [self stopIndicator];
            [_tblAppList reloadData];
            
        }];

    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            
            [self stopIndicator];
            
            UIAlertController *alertController = [UIAlertController
                                                  alertControllerWithTitle:[Diclabels valueForKey:@"txt_thingvellir"]
                                                  message:nil
                                                  preferredStyle:UIAlertControllerStyleAlert
                                                  ];
            
            if (error.code == NSURLErrorNotConnectedToInternet) {
                alertController.message = [Diclabels objectForKey:@"MSG_INTERNET_UNAVAILABLE"];
            }else{
                alertController.message = [Diclabels objectForKey:@"MSG_REQUEST_FAIL"];
            }
            
            [alertController addAction:[UIAlertAction
                                        actionWithTitle:[Diclabels objectForKey:@"OK"]
                                        style:UIAlertActionStyleCancel
                                        handler:^(UIAlertAction * _Nonnull action) {
                                            [self.navigationController popViewControllerAnimated:YES];
                                        }]
             ];
            
            [alertController addAction:[UIAlertAction
                                        actionWithTitle:[Diclabels objectForKey:@"txt_retry"]
                                        style:UIAlertActionStyleDefault
                                        handler:^(UIAlertAction * _Nonnull action) {
                                            
                                            [self getMoreData];
                                        }]
             ];
            
            [self presentViewController:alertController animated:YES completion:nil];
            
        }];
    }];
    
}

@end

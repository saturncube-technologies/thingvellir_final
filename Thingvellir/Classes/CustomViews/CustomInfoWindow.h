//
//  CustomInfoWindow.h
//  Thingvellir
//
//  Created by saturncube on 30/05/18.
//  Copyright © 2018 saturncube. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomInfoWindow : UIView
@property (strong, nonatomic) IBOutlet UIImageView *placeImage;
@property (strong, nonatomic) IBOutlet UILabel *placeTitle;
@property (strong, nonatomic) IBOutlet UILabel *lblKm;
@property (strong, nonatomic) IBOutlet UIImageView *likeImgVw;
@property (strong, nonatomic) IBOutlet UIImageView *imgSaveDel;
@property (strong, nonatomic) IBOutlet UIImageView *pinImgVw;

@end

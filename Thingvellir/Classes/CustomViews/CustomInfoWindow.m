//
//  CustomInfoWindow.m
//  Thingvellir
//
//  Created by saturncube on 30/05/18.
//  Copyright © 2018 saturncube. All rights reserved.
//

#import "CustomInfoWindow.h"

@implementation CustomInfoWindow
-(void) awakeFromNib
{
    [super awakeFromNib];
    self.placeImage.layer.cornerRadius = 3;
    self.placeImage.clipsToBounds = YES;
    
    _placeTitle.textColor = colorFromRGB(33, 33, 33);
    [_placeTitle setFont:[UIFont fontWithName:FONT_ROBOTO_REGULAR size:13]];
    
    _lblKm.textColor = colorFromRGB(102, 102, 102);
    [_lblKm setFont:[UIFont fontWithName:FONT_ROBOTO_REGULAR size:12]];
}
//- (id)initWithFrame:(CGRect)frame {
//    
//    self = [super initWithFrame:frame];
//
//    
//    return self;
//}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end

//
//  AppDelegate.m
//  Thingvellir
//
//  Created by saturncube on 24/05/18.
//  Copyright © 2018 saturncube. All rights reserved.
//

#import "AppDelegate.h"
#import "LeftMenuViewController.h"

@import GoogleMaps;

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    if ([defaults valueForKey:PREFERENCE_INTRO_FLAG] == nil) {
        [defaults setObject:@"1" forKey:PREFERENCE_INTRO_FLAG];
    }else {
    }
    
    
    [defaults objectForKey:PREFERENCE_STRDOWN];
    [defaults setObject:@"0" forKey:PREFERENCE_STRDOWN];
    
    
    if ([defaults objectForKey:PREFERENCE_DOWN_FLAG_EN] == nil) {
        [defaults setObject:@"0" forKey:PREFERENCE_DOWN_FLAG_EN];
    }
    if ([defaults objectForKey:PREFERENCE_DOWN_FLAG_IS] == nil) {
        [defaults setObject:@"0" forKey:PREFERENCE_DOWN_FLAG_IS];
    }
    if ([defaults objectForKey:PREFERENCE_DOWN_FLAG_GR] == nil) {
        [defaults setObject:@"0" forKey:PREFERENCE_DOWN_FLAG_GR];
    }
    if ([defaults objectForKey:PREFERENCE_DOWN_FLAG_FR] == nil) {
        [defaults setObject:@"0" forKey:PREFERENCE_DOWN_FLAG_FR];
    }
    if ([defaults objectForKey:PREFERENCE_DOWN_FLAG_ES] == nil) {
        [defaults setObject:@"0" forKey:PREFERENCE_DOWN_FLAG_ES];
    }
    if ([defaults objectForKey:PREFERENCE_DOWN_FLAG_CH] == nil) {
        [defaults setObject:@"0" forKey:PREFERENCE_DOWN_FLAG_CH];
    }
    if ([defaults objectForKey:PREFERENCE_DOWN_FLAG_DA] == nil) {
        [defaults setObject:@"0" forKey:PREFERENCE_DOWN_FLAG_DA];
    }
    [GMSServices provideAPIKey:@"AIzaSyC3XQdDlBMt73NklIzajMT5fw0abxDMpxA"];
//    [GMSPlacesClient provideAPIKey:@"YOUR_API_KEY"];

    // Override point for customization after application launch.
    //    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];

    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];

    LeftMenuViewController *leftMenuViewController = [storyboard instantiateViewControllerWithIdentifier:@"LeftMenuViewController"];

    MFSideMenuContainerViewController *container = [MFSideMenuContainerViewController
                                                    containerWithCenterViewController:self.window.rootViewController
                                                    leftMenuViewController:leftMenuViewController
                                                    rightMenuViewController:nil
                                                    ];

    container.panMode = MFSideMenuPanModeNone;
    
    [self isLanguageSeleted];
    
    self.window.rootViewController = container;

    return YES;
}

- (void)isLanguageSeleted {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    if ([defaults objectForKey:PREFERENCE_LANGUAGE] == nil) {
        [defaults setObject:@"txt_eng" forKey:PREFERENCE_LANGUAGE];
    } else {
        
    }
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
    [self saveContext];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

#pragma mark - Core Data stack

@synthesize persistentContainer = _persistentContainer;

- (NSPersistentContainer *)persistentContainer {
    // The persistent container for the application. This implementation creates and returns a container, having loaded the store for the application to it.
    @synchronized (self) {
        if (_persistentContainer == nil) {
            _persistentContainer = [[NSPersistentContainer alloc] initWithName:@"Thingvellir"];
            [_persistentContainer loadPersistentStoresWithCompletionHandler:^(NSPersistentStoreDescription *storeDescription, NSError *error) {
                if (error != nil) {
                    // Replace this implementation with code to handle the error appropriately.
                    // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                    
                    /*
                     Typical reasons for an error here include:
                     * The parent directory does not exist, cannot be created, or disallows writing.
                     * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                     * The device is out of space.
                     * The store could not be migrated to the current model version.
                     Check the error message to determine what the actual problem was.
                    */
                    NSLog(@"Unresolved error %@, %@", error, error.userInfo);
                    abort();
                }
            }];
        }
    }
    
    return _persistentContainer;
}

#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *context = self.persistentContainer.viewContext;
    NSError *error = nil;
    if ([context hasChanges] && ![context save:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, error.userInfo);
        abort();
    }
}

@end

//
//  Utility.h
//  Thingvellir
//
//  Created by saturncube on 13/06/18.
//  Copyright © 2018 saturncube. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <UIKit/UIKit.h>

@class Reachability;

@interface Utility : NSObject

/**
 * This will return Shared Utility
 */
+(Utility *)sharedInstance;


#pragma mark -------------------------
#pragma mark -- Connection
#pragma mark -------------------------


/**
 * Check whether internet is connected or not
 *
 * @return BOOL
 */
- (BOOL)isInternetConnected;

/**
 * Check whether server is connected or not
 *
 * @return BOOL
 */
- (BOOL)isServerConnected;




#pragma mark -------------------------
#pragma mark -- String Methods
#pragma mark -------------------------

/**
 * Check whether string is empty or not
 * @return Bool
 *      true  : if string is empty
 *      false : otherwise
 */
- (BOOL)isStringEmpty:(id)string;

/**
 * Compare two string
 * @return Bool
 *      true  : if string is equal
 *      false : otherwise
 */
- (BOOL)string:(NSString *)string
isEqualToString:(NSString *)secondString
WithCaseInsensitiveCompare:(BOOL)isCaseInsensitiveCompare;


/**
 * Return word with article
 *
 * @return NSString
 */
- (NSString *)wordWithArticle:(NSString *)word;


#pragma mark -------------------------
#pragma mark -- Other Utility Function
#pragma mark -------------------------

/**
 * Check if email is valid or not.
 *
 * @return BOOL:
 *         TRUE  : if email is valid,
 *         FALSE : if email is invalid,
 *
 */
- (BOOL)isValidateEmail:(NSString *)email;

/**
 * Check if phone number is valid or not.
 *
 * @return BOOL:
 *         TRUE  : if email is valid,
 *         FALSE : if email is invalid,
 *
 */
- (BOOL)isValidatePhoneNumber:(NSString *)phoneNumber;



#pragma mark Localazion

// Get localized string for key and comment...
- (NSString *)getLocalizedStringForKey:(NSString *)key andComment:(NSString *)comment;


#pragma mark -------------------------
#pragma mark -- Date Function
#pragma mark -------------------------

/**
 *  Application Data Format
 *
 *  @return NSDateFormatter dateFormatter
 */
- (NSDateFormatter *)dateFormatter;


#pragma mark -------------------------
#pragma mark -- Utility Function
#pragma mark -------------------------

/**
 *  Call to phone Number
 *
 *  @param phNo Phone Number to dial
 */
- (void)callPhone:(NSString *)phNo;


#pragma mark -------------------------
#pragma mark -- NSUserDefaults
#pragma mark -------------------------

/**
 *  Save Value for Key in User Defaults
 *
 *  @param key   Key
 *  @param value Value to be stored
 */
- (void)saveUserDefaultForKey:(NSString *)key Value:(NSString *)value;


/**
 *  Get Object For Key From  User Defaults
 *
 *  @param key
 *
 *  @return Object
 */
- (id)getUserDefaultForKey:(NSString *)key;

/**
 *  Remove Key- value From  User Defaults
 *
 *  @param key Key to be removed
 */
- (void)removeUserDefaultForKey:(NSString *)key;

//convert html format to Regular string
-(NSString *)stringByStrippingHTML:(NSString*)str;

- (UIImage *)scaleImage:(UIImage *)image maxWidth:(int) maxWidth maxHeight:(int) maxHeight;

@end


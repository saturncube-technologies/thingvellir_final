//
//  Utility.m
//  Thingvellir
//
//  Created by saturncube on 13/06/18.
//  Copyright © 2018 saturncube. All rights reserved.
//


#import "Utility.h"

#import "Reachability.h"

@implementation Utility

/**
 * Returns global Utility instance.
 *
 * @return Utility shared instance
 */
+ (Utility *)sharedInstance{
    
    static dispatch_once_t once;
    static Utility *instance;
    dispatch_once(&once, ^{
        instance = [self new];
    });
    
    return instance;
    
}

#pragma mark -------------------------
#pragma mark -- Connection
#pragma mark -------------------------


/**
 * Check whether internet is connected or not
 *
 * @return BOOL
 */
- (BOOL)isInternetConnected{
    
    Reachability *networkReachabilty    = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus         = [networkReachabilty currentReachabilityStatus];
    
    if(networkStatus != NotReachable){
        NSLog(@"There IS  internet connection");
        return true;
    }else{
        NSLog(@"There IS NO internet connection");
    }
    
    networkReachabilty = nil;
    
    return false;
}

/**
 * Check whether server is connected or not
 *
 * @return BOOL
 */
- (BOOL)isServerConnected{
    
    BOOL isConnected = false;
    
    Reachability *networkReachabilty    = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus         = [networkReachabilty currentReachabilityStatus];
    
    if(networkStatus != NotReachable){
        
        NSLog(@"There IS  internet connection");
        
        networkReachabilty = [Reachability reachabilityWithHostName:kApiServer];
        
        networkStatus      = [networkReachabilty currentReachabilityStatus];
        
        if(networkStatus != NotReachable) {
            
            NSLog(@"There IS server connection on " kApiServer);
            isConnected = true;
            
        }else{
            
            NSLog(@"There IS NO server connection on " kApiServer);
            isConnected = false;
            
        }
        
    }else{
        
        isConnected = false;
        NSLog(@"There IS NO internet connection");
        
    }
    
    networkReachabilty = nil;
    
    
    return isConnected;
}


#pragma mark -------------------------
#pragma mark -- String Methods
#pragma mark -------------------------

/**
 * Check whether string is empty or not
 * @return Bool
 *      true  : if string is empty
 *      false : otherwise
 */
- (BOOL)isStringEmpty:(id)string{
    
    
    if (string != nil ) {
        
        NSString *str = [NSString stringWithString:string];
        
        if (str.length == 0) {
            return true;
        }
        
        
    }else{
        return true;
    }
    
    return false;
}

/**
 * Compare two string
 * @return Bool
 *      true  : if string is equal
 *      false : otherwise
 */
- (BOOL)string:(NSString *)string
isEqualToString:(NSString *)secondString
WithCaseInsensitiveCompare:(BOOL)isCaseInsensitiveCompare{
    
    BOOL isEqualString = false;
    
    // Check if case insensitive compare or not
    if (isCaseInsensitiveCompare) {
        
        // Case insensitive Compare
        if( [ string caseInsensitiveCompare:secondString] == NSOrderedSame ) {
            isEqualString = true;
        }else{
            isEqualString = false;
        }
        
    }else{
        
        // Case sensitive Compare
        if ( [ string isEqualToString:secondString] ) {
            isEqualString = true;
        }else{
            isEqualString = false;
        }
        
    }
    
    return isEqualString;
}

/**
 * Return word with article
 *
 * @return NSString
 */
- (NSString *)wordWithArticle:(NSString *)word{
    
    NSArray *wovels = [NSArray arrayWithObjects:@"a",@"e",@"i",@"o",@"u", nil];
    
    // Check if letter is wovel
    if ([wovels containsObject:[word substringToIndex:1] ]) {
        word = [NSString stringWithFormat:@"an %@",word];
    }else{
        word = [NSString stringWithFormat:@"a %@",word];
    }
    
    return word;
}

#pragma mark -------------------------
#pragma mark -- Other Utility Function
#pragma mark -------------------------


#pragma mark Localazion

// Get localized string for key and comment...
- (NSString *)getLocalizedStringForKey:(NSString *)key andComment:(NSString *)comment{
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSArray *appleLanguagesArray = [userDefaults objectForKey:@"AppleLanguages"];
    
    // Get Current language of an application...
    NSString *currentLanguage = [appleLanguagesArray objectAtIndex:0];
    
    NSString *path = [[NSBundle mainBundle] pathForResource:currentLanguage ofType:@"lproj"];
    
    NSBundle *languageBundle = [NSBundle bundleWithPath:path];
    
    NSString *localizedString = NSLocalizedStringFromTableInBundle(key, nil, languageBundle, comment);
    
    //     NSString *localizedString = [NSString stringWithFormat:NSLocalizedString(key , comment)];
    return localizedString;
    
}

#pragma mark -------------------------
#pragma mark -- Date Function
#pragma mark -------------------------

/**
 *  Application Data Format
 *
 *  @return NSDateFormatter dateFormatter
 */
- (NSDateFormatter *)dateFormatter{
    
    static NSDateFormatter *dateFormat;
    
    // Convert string to date object
    dateFormat              = [[NSDateFormatter alloc] init];
    dateFormat.timeZone     = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
    
    [dateFormat setDateFormat:@"eee, d LLL yyyy HH:mm:ss zzz"];
    
    return dateFormat;
}


#pragma mark -------------------------
#pragma mark -- NSUserDefaults
#pragma mark -------------------------

/**
 *  Save Value for Key in User Defaults
 *
 *  @param key   Key
 *  @param value Value to be stored
 */
- (void)saveUserDefaultForKey:(NSString *)key Value:(NSString *)value{
    
    [[NSUserDefaults standardUserDefaults] setObject:value forKey:key];;
    [[NSUserDefaults standardUserDefaults] synchronize];
}

/**
 *  Get Object For Key From  User Defaults
 *
 *  @param key
 *
 *  @return Object
 */
- (id)getUserDefaultForKey:(NSString *)key{
    return [[NSUserDefaults standardUserDefaults] objectForKey:key];
}

/**
 *  Remove Key- value From  User Defaults
 *
 *  @param key Key to be removed
 */
- (void)removeUserDefaultForKey:(NSString *)key{
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:key];
}

//for convert html code to normal string
- (NSString *)stringByStrippingHTML:(NSString*)str
{
    NSRange r;
    while ((r = [str rangeOfString:@"<[^>]+>" options:NSRegularExpressionSearch]).location != NSNotFound)
    {
        str = [str stringByReplacingCharactersInRange:r withString:@""];
    }
    return str;
}


- (UIImage *)scaleImage:(UIImage *)image maxWidth:(int) maxWidth maxHeight:(int) maxHeight
{
    CGImageRef imgRef = image.CGImage;
    CGFloat width = CGImageGetWidth(imgRef);
    CGFloat height = CGImageGetHeight(imgRef);
    
    if (width <= maxWidth && height <= maxHeight)
    {
        return image;
    }
    
    CGAffineTransform transform = CGAffineTransformIdentity;
    CGRect bounds = CGRectMake(0, 0, width, height);
    
    if (width > maxWidth || height > maxHeight)
    {
        CGFloat ratio = width/height;
        
        if (ratio > 1)
        {
            bounds.size.width = maxWidth;
            bounds.size.height = bounds.size.width / ratio;
        }
        else
        {
            bounds.size.height = maxHeight;
            bounds.size.width = bounds.size.height * ratio;
        }
    }
    
    CGFloat scaleRatio = bounds.size.width / width;
    UIGraphicsBeginImageContext(bounds.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextScaleCTM(context, scaleRatio, -scaleRatio);
    CGContextTranslateCTM(context, 0, -height);
    CGContextConcatCTM(context, transform);
    CGContextDrawImage(UIGraphicsGetCurrentContext(), CGRectMake(0, 0, width, height), imgRef);
    
    UIImage *imageCopy = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return imageCopy;
    
}

@end

//
//  APIHelper.m
//  Thingvellir
//
//  Created by saturncube on 24/05/18.
//  Copyright © 2018 saturncube. All rights reserved.
//

#import "APIHelper.h"

@implementation APIHelper
    
+ (AFHTTPSessionManager *)sessionManager {
    
    // Get Manager
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    // Add Acceptable Content type
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html",@"application/json",nil];
    
//    [manager.requestSerializer setValue:@""
//                     forHTTPHeaderField:@"X-Auth"
//     ];
    
    return manager;
}
    
+ (NSString *)urlForAPI:(NSString *)APIName{
    
    NSString *apiUrl    = [NSString stringWithFormat:@"%@%@", kApiServer, APIName];
    
    return apiUrl;
}
    
+ (nullable NSURLSessionDataTask *)GET:(NSString *)APIName
                            parameters:(nullable id)parameters
                              progress:(nullable void (^)(NSProgress *downloadProgress)) downloadProgressBlock
                               success:(nullable void (^)(NSURLSessionDataTask *task, id _Nullable responseObject))successBlock
                               failure:(nullable void (^)(NSURLSessionDataTask * _Nullable task, NSError *error))failureBlock{
    
    // Get Manager
    AFHTTPSessionManager *manager = [self sessionManager];
    
    NSURLSessionDataTask *task = [manager GET:[self urlForAPI:APIName]
                                   parameters:parameters
                                     progress:^(NSProgress * _Nonnull downloadProgress) {
                                         downloadProgressBlock(downloadProgress);
                                     }success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                                         successBlock(task,responseObject);
                                     }failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                         failureBlock(task,error);
                                     }
                                  ];
    
    return task;
    
}
    
+ (nullable NSURLSessionDataTask *)POST:(NSString *)APIName
                             parameters:(nullable id)parameters
              constructingBodyWithBlock:(void (^)(id <AFMultipartFormData> formData))constructBodyBlock
                               progress:(nullable void (^)(NSProgress *uploadProgress)) uploadProgressBlock
                                success:(nullable void (^)(NSURLSessionDataTask *task, id _Nullable responseObject))successBlock
                                failure:(nullable void (^)(NSURLSessionDataTask * _Nullable task, NSError *error))failureBlock{
    
    // Get Manager
    AFHTTPSessionManager *manager = [self sessionManager];
    
    NSURLSessionDataTask *task = [manager POST:[self urlForAPI:APIName]
                                    parameters:parameters
                     constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
                         constructBodyBlock(formData);
                     } progress:^(NSProgress * _Nonnull uploadProgress) {
                         uploadProgressBlock(uploadProgress);
                     } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                         successBlock(task,responseObject);
                     } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                         failureBlock(task,error);
                     }
                                  ];
    
    return task;
    
}
    
@end

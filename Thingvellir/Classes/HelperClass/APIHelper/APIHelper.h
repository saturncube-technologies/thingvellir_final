//
//  APIHelper.h
//  Thingvellir
//
//  Created by saturncube on 24/05/18.
//  Copyright © 2018 saturncube. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"

NS_ASSUME_NONNULL_BEGIN

@interface APIHelper : NSObject
    
+ (nullable NSURLSessionDataTask *)GET:(NSString *)APIName
                            parameters:(nullable id)parameters
                              progress:(nullable void (^)(NSProgress *downloadProgress)) downloadProgressBlock
                               success:(nullable void (^)(NSURLSessionDataTask *task, id _Nullable responseObject))successBlock
                               failure:(nullable void (^)(NSURLSessionDataTask * _Nullable task, NSError *error))failureBlock;
    
+ (nullable NSURLSessionDataTask *)POST:(NSString *)APIName
                             parameters:(nullable id)parameters
              constructingBodyWithBlock:(void (^)(id <AFMultipartFormData> formData))constructBodyBlock
                               progress:(nullable void (^)(NSProgress *uploadProgress)) uploadProgressBlock
                                success:(nullable void (^)(NSURLSessionDataTask *task, id _Nullable responseObject))successBlock
                                failure:(nullable void (^)(NSURLSessionDataTask * _Nullable task, NSError *error))failureBlock;
    
@end

NS_ASSUME_NONNULL_END

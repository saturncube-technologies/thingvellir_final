//
//  LocationManager.h
//  Thingvellir
//
//  Created by saturncube on 04/06/18.
//  Copyright © 2018 saturncube. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface LocationManager : NSObject<CLLocationManagerDelegate>

@property (nonatomic, strong) CLLocationManager* locationManager;

+ (LocationManager*)sharedSingleton;

@end

//
//  PlacesInfo.m
//  Thingvellir
//
//  Created by saturncube on 08/06/18.
//  Copyright © 2018 saturncube. All rights reserved.
//

#import "PlacesInfo.h"
#import "DBHelper.h"

@implementation PlacesInfo

/**
 *  Initialization
 *
 */
- (instancetype)init{
    
    self = [super init];
    
    if (self) {
        
        _placeName                   = [NSMutableString new];
        _placeDetail                 = [NSMutableString new];

    }
    
    return self;
}

/**
 * Set data from dictionary
 */
- (void)setDataFromDictionary:(NSDictionary *)placeDetails{
    
    // Iterate all dictionary key value
    for (id key in placeDetails) {
        
        // Get value
        id value = [placeDetails objectForKey:key];
        
        // Set value according to key
        if([key isEqualToString:DB_TABLEFIELD_NAME]) {
            _placeName        = (NSMutableString*) value;
        }else if([key isEqualToString:DB_TABLEFIELD_DESCRIPTION]) {
            _placeDetail      = (NSMutableString*) value;
        }else {
            continue;
        }
        
    }
    
}

/**
 * Load Data from array
 *
 */
- (void)setDataFromArray:(NSArray *)arrData andColumns:(NSArray *)arrColumn {
    
    // Check if data is not empty
    if ( [arrData count] > 0) {
        
        arrData = [arrData firstObject];
        
        // Iterate all data
        for (int i=0; i < arrColumn.count; i++) {
            
            NSString *key       = [arrColumn objectAtIndex:i];
            id      value       = [arrData objectAtIndex:i];
            
            // Set value according to key
            if([key isEqualToString:DB_TABLEFIELD_NAME]) {
                _placeName      = (NSMutableString*) value;
            }else if([key isEqualToString:DB_TABLEFIELD_DESCRIPTION]) {
                _placeDetail    = (NSMutableString*) value;
            }else {
                continue;
            }
        }
    }
    
}

/**
 * Save data in database
 */
- (void)saveData{
    
    
    NSDictionary *tableData = @{
                                //DB_TABLEFIELD_ID:[NSNumber numberWithUnsignedInteger:_placeId],
                                DB_TABLEFIELD_NAME:_placeName,
                                DB_TABLEFIELD_DESCRIPTION:_placeDetail,
                                };
    
    DBHelper *dbHelper = [[DBHelper alloc] init];
    [dbHelper inserDataIntoTable:DB_TABLENAME_PLACEMASTER tableData:tableData];
    
}

/**
 *  Load Data from Database
 */
- (void)loadData{
    
    DBHelper *dbHelper = [[DBHelper alloc] init];
    NSArray *result = [dbHelper loadTableData:DB_TABLENAME_PLACEMASTER];
    [self setDataFromArray:result andColumns:dbHelper.arrColumnNames];
}

/**
 *  Delete User data
 */
- (void)deleteUser{
    
    DBHelper *dbHelper = [[DBHelper alloc] init];
    [dbHelper tuncateTable:DB_TABLENAME_PLACEMASTER];
    
}

@end

//
//  PlacesInfo.h
//  Thingvellir
//
//  Created by saturncube on 08/06/18.
//  Copyright © 2018 saturncube. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PlacesInfo : NSObject

@property (nonatomic)           NSUInteger                  placeId;
@property (nonatomic)           NSUInteger                  isLiked;
@property (nonatomic)           NSUInteger                  isVisited;

@property (nonatomic)           float                  *latitude;
@property (nonatomic)           float                  *logitude;
@property (nonatomic)           float                  *placeDistance;

@property (nonatomic,strong)    NSMutableString             *placeName;
@property (nonatomic,strong)    NSMutableString             *placeDetail;

/**
 * Set data from dictionary
 */
- (void)setDataFromDictionary:(NSDictionary *)placeDetails;

/**
 * Save data in database
 */
- (void)saveData;

/**
 *  Load Data from Database
 */
- (void)loadData;


/**
 *  Delete User data
 */
- (void)deleteUser;

@end

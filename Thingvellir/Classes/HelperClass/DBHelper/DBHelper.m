//
//  DBHelper.m
//  Thingvellir
//
//  Created by saturncube on 07/06/18.
//  Copyright © 2018 saturncube. All rights reserved.
//

#import "DBHelper.h"

#define APP_DB_FILENAME @"thingvellir_db"

@implementation DBHelper
/**
 *
 */
- (id)init {
    
    self = [super init];
    
    if(self){
        
        // Set the documents directory path to the documentsDirectory property.
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        
        self.documentsDirectory = [paths objectAtIndex:0];
        
        // Keep the database filename.
        self.databaseFilename   = APP_DB_FILENAME;
        
        // Check if the database file exists in the documents directory.
        NSString *destinationPath = [self.documentsDirectory stringByAppendingPathComponent:self.databaseFilename];
        NSLog(@"destinationPath is :: %@", destinationPath);
        // Check file exist or not
        if (![[NSFileManager defaultManager] fileExistsAtPath:destinationPath]) {
            
            NSError  *localError;
            [[NSFileManager defaultManager] createFileAtPath:destinationPath contents:nil attributes:nil];
            
            // Check if any error occurred during copying and display it.
            if (localError != nil) {
                NSLog(@"DBError : %@", [localError localizedDescription]);
            }else{
                [self createDatabaseTables];
            }
            
        }else{
            
        }
        
        NSLog(@"documentsDirectory root is %@",self.documentsDirectory);
        
    }
    
    return self;
    
}


- (void)runQuery:(const char *)query isQueryExecutable:(BOOL)queryExecutable{
    
    // Create a sqlite object.
    sqlite3 *sqlite3Database;
    
    // Set the database file path.
    NSString *databasePath =  [self.documentsDirectory stringByAppendingPathComponent:self.databaseFilename];
    
    // Initialize the results array.
    if (self.arrResults != nil) {
        [self.arrResults removeAllObjects];
        self.arrResults = nil;
    }
    
    self.arrResults = [[NSMutableArray alloc] init];
    
    // Initialize the column names array.
    if (self.arrColumnNames != nil) {
        [self.arrColumnNames removeAllObjects];
        self.arrColumnNames = nil;
    }
    self.arrColumnNames = [[NSMutableArray alloc] init];
    
    // Open the database.
    BOOL openDatabaseResult = sqlite3_open([databasePath UTF8String], &sqlite3Database);
    
    if (openDatabaseResult == SQLITE_OK) {
        
        // Declare a sqlite3_stmt object in which will be stored the query after having been compiled into a SQLite statement.
        sqlite3_stmt *compiledStatement;
        
        // Load all data from database to memory.
        BOOL prepareStatementResult = sqlite3_prepare_v2(sqlite3Database, query, -1, &compiledStatement, NULL);
        
        if (prepareStatementResult == SQLITE_OK) {
            
            if(!queryExecutable){
                // In this case data must be loaded from the database.
                
                // Declare an array to keep the data for each fetched row.
                NSMutableArray *arrDataRow;
                
                // Loop through the results and add them to the results array row by row.
                while (sqlite3_step(compiledStatement) == SQLITE_ROW) {
                    
                    // Initialize the mutable array that will contain the data of a fetched row.
                    arrDataRow = [[NSMutableArray alloc] init];
                    
                    // Get the total number of columns.
                    int totalColumns = sqlite3_column_count(compiledStatement);
                    
                    // Go through all columns and fetch each column data.
                    for (int i=0; i<totalColumns; i++) {
                        // Convert the column data to text (characters).
                        char *dbDataAsChars = (char *)sqlite3_column_text(compiledStatement, i);
                        
                        // If there are contents in the currenct column (field) then add them to the current row array.
                        if(dbDataAsChars != NULL){
                            // Convert the characters to string.
                            [arrDataRow addObject:[NSString stringWithUTF8String:dbDataAsChars]];
                        }
                        
                        // Keep the current column name.
                        if (self.arrColumnNames.count != totalColumns) {
                            dbDataAsChars =(char *)sqlite3_column_name(compiledStatement, i);
                            [self.arrColumnNames addObject:[NSString stringWithUTF8String:dbDataAsChars]];
                        }
                    }
                    
                    if(arrDataRow.count > 0){
                        [self.arrResults addObject:arrDataRow];
                    }
                    
                }
            } else{
                // This is the case of an executable query (insert, update, ...).
                
                // Execute the query.
                //                BOOL executeQueryResults= sqlite3_step(compiledStatement);
                int executeQueryResults= sqlite3_step(compiledStatement);
                
                
                if (executeQueryResults == SQLITE_DONE) {
                    
                    
                    self.lastInsertedRowID = sqlite3_last_insert_rowid(sqlite3Database);
                    
                    // Keep the affected rows.
                    self.affectedRows = sqlite3_changes(sqlite3Database);
                    
                    //                    NSLog(@"Affected Row : %d for Query %s", _affectedRows,query);
                    
                }else{
                    // If could not execute the query show the error message on the debugger.
                    NSLog(@"DB Error: %s", sqlite3_errmsg(sqlite3Database));
                }
            }
            
        }else {
            // In the database cannot be opened then show the error message on the debugger.
            NSLog(@"%s", sqlite3_errmsg(sqlite3Database));
        }
        
        // Release the compiled statement from memory.
        sqlite3_finalize(compiledStatement);
    }
    // Close the database.
    sqlite3_close(sqlite3Database);
}


- (NSArray *)loadDataFromDB:(NSString *)query {
    // Run the query and indicate that is not executable.
    // The query string is converted to a char* object.
    [self runQuery:[query UTF8String] isQueryExecutable:NO];
    
    // Returned the loaded results.
    return (NSArray *)self.arrResults;
}

- (void)executeQuery:(NSString *)query{
    
    // Run the query and indicate that is executable.
    [self runQuery:[query UTF8String] isQueryExecutable:YES];
}

- (NSArray *)loadTableData:(NSString *)tableName{
    
    NSString *query = [NSString stringWithFormat:@"select * from %@",tableName];
    NSArray  *result =[[NSArray alloc] initWithArray:[self loadDataFromDB:query]];
    return result;
}

- (BOOL)deleteNotInPrimaryKey:(NSString *)tableName primaryKey:(NSString *)primaryKey valueArray:(NSArray *)valueArray{
    
    if([valueArray count] > 0){
        NSString *query = nil;
        NSString * ids = [valueArray componentsJoinedByString:@","];
        query = [NSString stringWithFormat:@"DELETE FROM %@ WHERE %@  NOT IN(%@)",tableName,primaryKey,ids ];
        [self executeQuery:query];
        if (self.affectedRows > 0) {
            return true;
        }
    }
    return false;
}

- (void)dropDatabase{
    
    NSString *destinationPath =[self.documentsDirectory stringByAppendingPathComponent:self.databaseFilename];
    [[NSFileManager defaultManager] removeItemAtPath:destinationPath error:NULL];
    
}


#pragma mark - Additional Database Method

- (void)createDatabaseTables {
    
    NSMutableArray *tblQueries  =   [[NSMutableArray alloc] init];
    
    NSString *query = [NSString stringWithFormat:
                       @"CREATE TABLE `%@` (`%@` INTEGER PRIMARY KEY AUTOINCREMENT,`%@` TEXT,`%@` TEXT,`%@` TEXT,`%@` FLOAT,`%@` FLOAT,`%@` FLOAT,`%@` INTEGER,`%@` INTEGER,`%@` TEXT,`%@` TEXT,`%@` TEXT,`%@` TEXT,`%@` TEXT,`%@` TEXT,`%@` TEXT,`%@` TEXT,`%@` TEXT,`%@` TEXT,`%@` TEXT,`%@` TEXT,`%@` TEXT,`%@` TEXT,`%@` TEXT,`%@` TEXT,`%@` TEXT,`%@` TEXT,`%@` TEXT,`%@` TEXT,`%@` TEXT,`%@` TEXT,`%@` TEXT,`%@` TEXT,`%@` TEXT,`%@` TEXT,`%@` TEXT,`%@` TEXT,`%@` TEXT,`%@` TEXT,`%@` TEXT,`%@` TEXT,`%@` TEXT,`%@` TEXT,`%@` TEXT,`%@` TEXT)" ,
                       DB_TABLENAME_PLACEMASTER,
                       DB_TABLEFIELD_ID,
                       DB_TABLEFIELD_PLACEID,
                       DB_TABLEFIELD_NAME,
                       DB_TABLEFIELD_NAME_ISLENSKA,
                       DB_TABLEFIELD_DESCRIPTION,
                       DB_TABLEFIELD_DESCRIPTION_ISLENSKA,
                       DB_TABLEFIELD_KM,
                       DB_TABLEFIELD_LATITUDE,
                       DB_TABLEFIELD_LOGITUDE,
                       DB_TABLEFIELD_LIKE,
                       DB_TABLEFIELD_VISITED,
                       DB_TABLEFIELD_IMAGE,
                       DB_TABLEFIELD_AUDIO,
                       DB_TABLEFIELD_AUDIO_ISLENSKA,
                       DB_TABLEFIELD_DOWNLOAD_FLAG,
                       DB_TABLEFIELD_UPDATE_AT,
                       
                       DB_TABLEFIELD_DES_ENG_NONE_HTML,
                       DB_TABLEFIELD_DES_ISLENSKA_NONE_HTML,
                       DB_TABLEFIELD_NAME_GERMAN,
                       DB_TABLEFIELD_DES_GERMAN_NONE_HTML,
                       DB_TABLEFIELD_DES_GERMAN,
                       DB_TABLEFIELD_AUDIO_GERMAN,
                       DB_TABLEFIELD_NAME_FRENCH,
                       DB_TABLEFIELD_DES_FRENCH_NONE_HTML,
                       DB_TABLEFIELD_DES_FRENCH,
                       DB_TABLEFIELD_AUDIO_FRENCH,
                       DB_TABLEFIELD_NAME_SPANISH,
                       DB_TABLEFIELD_DES_SPANISH_NONE_HTML,
                       DB_TABLEFIELD_DES_SPANISH,
                       DB_TABLEFIELD_AUDIO_SPANISH,
                       DB_TABLEFIELD_NAME_CHINESE,
                       DB_TABLEFIELD_DES_CHINESE_NONE_HTML,
                       DB_TABLEFIELD_DES_CHINESE,
                       DB_TABLEFIELD_AUDIO_CHINESE,
                       DB_TABLEFIELD_NAME_DANISH,
                       DB_TABLEFIELD_DES_DANISH_NONE_HTML,
                       DB_TABLEFIELD_DES_DANISH,
                       DB_TABLEFIELD_AUDIO_DANISH,
                       
                       DB_TABLEFIELD_DOWN_FLAG_EN,
                       DB_TABLEFIELD_DOWN_FLAG_IS,
                       DB_TABLEFIELD_DOWN_FLAG_GE,
                       DB_TABLEFIELD_DOWN_FLAG_FR,
                       DB_TABLEFIELD_DOWN_FLAG_ES,
                       DB_TABLEFIELD_DOWN_FLAG_CH,
                       DB_TABLEFIELD_DOWN_FLAG_DA
                       ];
    
    NSString *query1 = [NSString stringWithFormat:
                       @"CREATE TABLE `%@` (`%@` INTEGER PRIMARY KEY AUTOINCREMENT,`%@` TEXT,`%@` TEXT)" ,
                       DB_TABLENAME_IMAGEMASTER,
                       DB_TABLEFIELD_IMAGE_ID,
                       DB_TABLEFIELD_PLACEID,
                       DB_TABLEFIELD_PLACEIMAGE_NAME
                       ];
    
    [tblQueries addObject:query];
    [tblQueries addObject:query1];

    // Iterate queries and execute
    for (NSString *query in tblQueries) {
        [self executeQuery:query];
        NSLog(@"Table create success");

    }
    
}

/**
 * Insert Data in given table
 *
 * @return last inserted row id
 * @return long long
 */
- (long long)inserDataIntoTable:(NSString *)table tableData:(NSDictionary *)data{
    
    NSMutableString *strColumns = [NSMutableString stringWithString:@""];
    NSMutableString *strValues  = [NSMutableString stringWithString:@""];
    
    // Iterate All key - values
    for (NSString *key in data) {
        
        [strColumns appendFormat:@"`%@`,",key];
        
        id value = [data objectForKey:key];
        
        if ([value isKindOfClass:[NSString class]] || [value isKindOfClass:[NSString class]]) {
            [strValues  appendFormat:@"\"%@\",",[data objectForKey:key]];
            
        }else{
            [strValues  appendFormat:@"%@,",[data objectForKey:key]];
        }
        
    }
    
    strColumns  = (NSMutableString *)[strColumns substringToIndex:[strColumns length]-1];
    strValues   = (NSMutableString *)[strValues substringToIndex:[strValues length]-1];
    
    
    // Build Query
    NSMutableString *query = [NSMutableString stringWithFormat:@"INSERT INTO `%@` (%@) VALUES (%@)",
                              table,
                              strColumns,
                              strValues
                              ];
    
    strColumns = strValues = nil;
    
    // Execute Query
    [self executeQuery:query];
    
//        NSLog(@"query %@",query);
    //    NSLog(@"self.lastInsertedRowID is %zd",self.lastInsertedRowID);
    
    return self.lastInsertedRowID;

}


/**
 *  Truncate Table
 *
 *  @param tableName table name
 *
 *  @return affected rows
 */
- (long long)tuncateTable:(NSString *)tableName{
    
    NSString *query = [NSString stringWithFormat:@"DELETE FROM %@",tableName];
    
    [self executeQuery:query];
    
    return _affectedRows;
}

/**
 * Get Row for given table
 *
 * @return NSArray Count
 */
- (NSArray *)getRecordFromTable:(NSString *)table forRow:(unsigned int)row WhereClause:(NSString *)whereClause{
    
    NSString *query;
    
    if ([whereClause length] > 0) {
        
        query = [NSString stringWithFormat:@"SELECT * FROM `%@` WHERE %@ LIMIT 1 OFFSET %zd",
                 table,
                 whereClause,
                 row
                 ];
    }else{
        
        query = [NSString stringWithFormat:@"SELECT * FROM `%@`  LIMIT 1 OFFSET %zd",
                 table,
                 row
                 ];
    }
    
    return [self loadDataFromDB:query];
}

- (BOOL)Update:(NSString *)tableName placeId:(NSString *)placeID tableData:(NSDictionary *)data {
    
    NSMutableString *strColumns = [NSMutableString stringWithString:@""];
    NSMutableString *strValues  = [NSMutableString stringWithString:@""];
    
    // Iterate All key - values
    for (NSString *key in data) {
        
        [strColumns appendFormat:@"`%@`,",key];
        
        id value = [data objectForKey:key];
        
        if ([value isKindOfClass:[NSString class]] || [value isKindOfClass:[NSString class]]) {
            [strValues  appendFormat:@"%@=\"%@\",",key,[data objectForKey:key]];

        }else{
            [strValues  appendFormat:@"%@=%@,",key,[data objectForKey:key]];
        }
        
    }
    
    strColumns  = (NSMutableString *)[strColumns substringToIndex:[strColumns length]-1];
    strValues   = (NSMutableString *)[strValues substringToIndex:[strValues length]-1];
    
//    query = [NSString stringWithFormat:@"update peopleInfo set firstname='%@', lastname='%@', age=%d where peopleInfoID=%d", self.txtFirstname.text, self.txtLastname.text, self.txtAge.text.intValue, self.recordIDToEdit];

    // Build Query
    NSMutableString *query = [NSMutableString stringWithFormat:@"UPDATE `%@` SET %@ WHERE place_id='%@'",
                              tableName,
                              strValues,
                              placeID
                              ];
    
    strColumns = strValues = nil;
    
    // Execute Query
    [self executeQuery:query];
    
    NSLog(@"query %@",query);
    //    NSLog(@"self.lastInsertedRowID is %zd",self.lastInsertedRowID);
    return false;
}




- (NSArray *)checkIfDataAlreadyExist:(NSString *)table place_id:(NSString *)placeId colomnNM:(NSString *)colNM {
    
    NSString *query;
    
    if (![placeId isEqualToString:@""]) {
        
        query = [NSString stringWithFormat:@"SELECT * FROM `%@` WHERE %@=%@ ",
                 table,
                 colNM,
                 placeId
                 ];
    }
    NSLog(@"%@", query);
    
    return [self loadDataFromDB:query];
}

- (NSArray *)loadFavTableData:(NSString *)tableName placeId:(NSString *)placeID{
    
    NSString *query = [NSString stringWithFormat:@"select * from %@ WHERE %@=1 AND place_id=%@",tableName, DB_TABLEFIELD_LIKE, placeID];
    NSLog(@"%@", query);

    NSArray  *result = [[NSArray alloc] initWithArray:[self loadDataFromDB:query]];
    return result;
}

- (NSArray *)loadVisitedTableData:(NSString *)tableName placeId:(NSString *)placeID{
    
    NSString *query = [NSString stringWithFormat:@"select * from %@ WHERE %@=0 AND place_id=%@",tableName, DB_TABLEFIELD_VISITED, placeID];
    NSLog(@"%@", query);
    NSArray  *result = [[NSArray alloc] initWithArray:[self loadDataFromDB:query]];
    return result;
}


- (BOOL)delete:(NSString *)tableName placeId:(NSString *)placeID colomnNM:(NSString *)colNM {
    
    if(![placeID isEqualToString:@""]) {
        NSString *query = nil;
//        NSString * ids = [valueArray componentsJoinedByString:@","];
        query = [NSString stringWithFormat:@"DELETE FROM %@ WHERE %@=%@", tableName, colNM, placeID];
        [self executeQuery:query];
        NSLog(@"%@", query);

        if (self.affectedRows > 0) {
            return true;
        }
    }
    return false;
}

- (BOOL)deleteIn:(NSString *)tableName colomnNM:(NSString *)colNM placeId:(NSString *)placeID {
    
    if(![placeID isEqualToString:@""]) {
        NSString *query = nil;
        //        NSString * ids = [valueArray componentsJoinedByString:@","];
        query = [NSString stringWithFormat:@"DELETE FROM %@ WHERE %@ IN (%@)",tableName, colNM, placeID];
        [self executeQuery:query];
        NSLog(@"%@", query);
        
        if (self.affectedRows > 0) {
            return true;
        }
    }
    return false;
}


/**
 * Get Column Count in given table
 *
 * @return Column Count
 * @return unsigned int
 */
- (unsigned int)getColumnCountForTable:(NSString *)table forRow:(NSString *)columnName{
    
    NSString *str = [NSString stringWithFormat:@"SELECT COUNT(%@) FROM %@",columnName,table];
    
    NSArray *result = [[self loadDataFromDB:str] firstObject];
    
    unsigned int totalCount = 0;
    
    //
    if (result != nil) {
        totalCount = [[result firstObject] unsignedIntValue];
    }
    
    return totalCount;
}

/**
 * Get Column Count by query
 *
 * @return Column Count
 * @return unsigned int
 */
- (unsigned int)getColumnCountByQuery:(NSString *)query{
    
    NSArray *result = [[self loadDataFromDB:query] firstObject];
    
    unsigned int totalCount = 0;
    
    //
    if (result != nil) {
        totalCount = [[result firstObject] unsignedIntValue];
    }
    
    return totalCount;
}









/*
 - (instancetype) initWithDbFile{
 
 self = [super init];
 
 if(self){
 
 // Set the documents directory path to the documentsDirectory property.
 NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
 
 // Keep the database directory.
 self.documentsDirectory =[paths objectAtIndex:0];
 
 // Keep the database filename.
 self.databaseFilename = APP_DB_FILENAME;
 
 NSLog(@"documentsDirectory root is %@",self.documentsDirectory);
 
 // Copy the database file into the documents directory if necessary.
 [self copyDatabaseIntoDocumentsDirectory];
 
 }
 
 return self;
 }
 
 
 - (void)copyDatabaseIntoDocumentsDirectory{
 
 // Check if the database file exists in the documents directory.
 NSString *destinationPath =[self.documentsDirectory stringByAppendingPathComponent:self.databaseFilename];
 
 // Check file exist or not
 if (![[NSFileManager defaultManager] fileExistsAtPath:destinationPath]) {
 
 // The database file does not exist in the documents directory, so copy it from the main bundle now.
 NSString *sourcePath =[[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:self.databaseFilename];
 NSError  *localError;
 
 NSLog(@"Source path is %@",sourcePath);
 
 //         [[NSFileManager defaultManager] createF:destinationPath withIntermediateDirectories:NO attributes:nil error:&localError];
 
 [[NSFileManager defaultManager] copyItemAtPath:sourcePath toPath:destinationPath error:&localError];
 
 // Check if any error occurred during copying and display it.
 if (localError != nil) {
 NSLog(@"DBError : %@", [localError localizedDescription]);
 }else{
 [self createDatabaseTables];
 }
 
 }
 
 }
 
 */
@end




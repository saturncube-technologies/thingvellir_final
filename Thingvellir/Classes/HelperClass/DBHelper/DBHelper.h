//
//  DBHelper.h
//  Thingvellir
//
//  Created by saturncube on 07/06/18.
//  Copyright © 2018 saturncube. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>

@interface DBHelper : NSObject

@property (nonatomic, strong)       NSString        *documentsDirectory;
@property (nonatomic, strong)       NSString        *databaseFilename;

@property (nonatomic, strong)       NSMutableArray  *arrColumnNames;
@property (nonatomic, strong)       NSMutableArray  *arrResults;
@property (nonatomic) int           affectedRows;
@property (nonatomic) long long     lastInsertedRowID;


//- (instancetype)initWithDbFile;

//- (void)copyDatabaseIntoDocumentsDirectory;

- (void)runQuery:(const char *)query isQueryExecutable:(BOOL)queryExecutable;

- (NSArray *)loadDataFromDB:(NSString *)query;

- (void)executeQuery:(NSString *)query;

- (NSArray *)loadTableData:(NSString *)tableName;
- (BOOL)deleteNotInPrimaryKey:(NSString *)tableName primaryKey:(NSString *)primaryKey valueArray:(NSArray *)valueArray;
- (BOOL)deleteIn:(NSString *)tableName colomnNM:(NSString *)colNM placeId:(NSString *)placeID;
- (void)dropDatabase;

- (NSArray *)checkIfDataAlreadyExist:(NSString *)table place_id:(NSString *)placeId colomnNM:(NSString *)colNM;
- (BOOL)delete:(NSString *)tableName placeId:(NSString *)placeID colomnNM:(NSString *)colNM;

- (BOOL)Update:(NSString *)tableName placeId:(NSString *)placeID tableData:(NSDictionary *)data;

- (NSArray *)loadFavTableData:(NSString *)tableName placeId:(NSString *)placeID;
- (NSArray *)loadVisitedTableData:(NSString *)tableName placeId:(NSString *)placeID;

/**
 * Insert Data in given table
 *
 * @return last inserted row id
 * @return long long
 */
- (long long)inserDataIntoTable:(NSString *)table tableData:(NSDictionary *)data;


/**
 *  Truncate Table
 *
 *  @param tableName table name
 *
 *  @return affected rows
 */
- (long long)tuncateTable:(NSString *)tableName;

/**
 * Get Row for given table
 *
 * @return NSArray Count
 */
- (NSArray *)getRecordFromTable:(NSString *)table forRow:(unsigned int)row WhereClause:(NSString *)whereClause;


/**
 * Get Column Count in given table
 *
 * @return Column Count
 * @return unsigned int
 */
- (unsigned int)getColumnCountForTable:(NSString *)table forRow:(NSString *)columnName;

/**
 * Get Column Count by query
 *
 * @return Column Count
 * @return unsigned int
 */
- (unsigned int)getColumnCountByQuery:(NSString *)query;

@end

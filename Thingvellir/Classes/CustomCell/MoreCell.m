//
//  MoreCell.m
//  Thingvellir
//
//  Created by saturncube on 04/06/18.
//  Copyright © 2018 saturncube. All rights reserved.
//

#import "MoreCell.h"

@implementation MoreCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

//
//  PlaceCell.m
//  Thingvellir
//
//  Created by saturncube on 25/05/18.
//  Copyright © 2018 saturncube. All rights reserved.
//

#import "PlaceCell.h"

@implementation PlaceCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.placeImgVw.layer.cornerRadius = 3;
    self.placeImgVw.clipsToBounds = YES;
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

//
//  PlaceCell.h
//  Thingvellir
//
//  Created by saturncube on 25/05/18.
//  Copyright © 2018 saturncube. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PlaceCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *placeImgVw;
@property (weak, nonatomic) IBOutlet UIImageView *likeImgVw;
@property (weak, nonatomic) IBOutlet UIButton *btnLike;
@property (weak, nonatomic) IBOutlet UILabel *lblPlaceName;
@property (weak, nonatomic) IBOutlet UILabel *lblPlaceDetail;
@property (weak, nonatomic) IBOutlet UIButton *btnSaveAndDelete;
@property (strong, nonatomic) IBOutlet UIImageView *imgSaveDel;
@property (weak, nonatomic) IBOutlet UILabel *lblKm;
@property (strong, nonatomic) IBOutlet UIView *btnView;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@end

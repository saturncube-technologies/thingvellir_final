//
//  MenuCell.h
//  Thingvellir
//
//  Created by saturncube on 26/05/18.
//  Copyright © 2018 saturncube. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *menuImgVw;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIButton *btnCheck;
@property (strong, nonatomic) IBOutlet UIImageView *checkMarkImgVw;

@end

//
//  MoreCell.h
//  Thingvellir
//
//  Created by saturncube on 04/06/18.
//  Copyright © 2018 saturncube. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MoreCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *moreImageVw;
@property (strong, nonatomic) IBOutlet UILabel *lblTitle;
@property (strong, nonatomic) IBOutlet UILabel *lblDetail;

@end

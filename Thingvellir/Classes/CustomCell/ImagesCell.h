//
//  ImagesCell.h
//  Thingvellir
//
//  Created by saturncube on 28/05/18.
//  Copyright © 2018 saturncube. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImagesCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *placeImageView;
@property (strong, nonatomic) IBOutlet UIButton *btnSelect;
@property (strong, nonatomic) IBOutlet UIImageView *selectImageView;

@end

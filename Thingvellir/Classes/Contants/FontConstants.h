//
//  FontConstants.h
//  Thingvellir
//
//  Created by saturncube on 31/05/18.
//  Copyright © 2018 saturncube. All rights reserved.
//


#ifndef FontContants_h
#define FontContants_h

#import "AppConstants.h"

// FONT
#define FONT_ROBOTO_REGULAR          @"Roboto-Regular"

#define FONT_ROBOTO_LIGHT            @"Roboto-Light"

#define FONT_ROBOTO_BLACK            @"Roboto-Black"

#define FONT_ROBOTO_BOLD             @"Roboto-Bold"

#define FONT_ROBOTO_MEDIUM           @"Roboto-Medium"

#endif

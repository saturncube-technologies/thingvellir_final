//
//  AppConstants.h
//  Thingvellir
//
//  Created by saturncube on 25/05/18.
//  Copyright © 2018 saturncube. All rights reserved.
//

#ifndef AppContants_h
#define AppContants_h


// Internet Connection...
#define INTERNET_CONNECTION_ERROR @"Internet Connection is not available. Please try again later."

#define SYSTEM_ERROR_MSG @"we are unable to process your request, please again try later."


#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)


/** UIColor: Color from RGB **/
#define colorFromRGB( r , g , b ) ( [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:1 ] )

/** UIColor: Color from RGBA **/
#define colorFromRGBA(r , g , b , a ) ( [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:a ] )



///---------------------------
/// @name Device Checks
///---------------------------

#define DEVICE_SCREEN_HAS_LENGTH(_frame, _length) ( fabs( MAX(CGRectGetHeight(_frame), CGRectGetWidth(_frame)) - _length) < DBL_EPSILON )

/**
 Runtime check for the current device.
 checks if the current device is an iPhone 4 or an Device with 480 Screen height
 */
#define DEVICE_IS_IPHONE_4 DEVICE_SCREEN_HAS_LENGTH([UIScreen mainScreen].bounds, 480.)


/**
 Runtime check for the current device.
 checks if the current device is an iPhone SE, iPhone 5 or iPod Touch 5 Gen, or an Device with 1136 Screen height
 */
#define DEVICE_IS_IPHONE_5 DEVICE_SCREEN_HAS_LENGTH([UIScreen mainScreen].bounds, 568.)

/**
 Runtime check for the current device.
 checks if the current device is an iPhone 6
 */
#define DEVICE_IS_IPHONE_6 DEVICE_SCREEN_HAS_LENGTH([UIScreen mainScreen].bounds, 667.)

/**
 Runtime check for the current device.
 checks if the current device is an iPhone 6 Plus
 */
#define DEVICE_IS_IPHONE_6_PLUS DEVICE_SCREEN_HAS_LENGTH([UIScreen mainScreen].bounds, 736.)

/**
 Runtime check for the current device.
 checks if the current device is an iPhone X
 */
#define DEVICE_IS_IPHONE_X DEVICE_SCREEN_HAS_LENGTH([UIScreen mainScreen].bounds, 812)

/**
 Runtime check for the current device.
 checks if the current device is an iPhone or iPod Touch
 */
#define DEVICE_IS_IPHONE ( UIUserInterfaceIdiomPhone == UI_USER_INTERFACE_IDIOM() )

/**
 Runtime check for the current device.
 checks if the current device is an iPad
 */
#define DEVICE_IS_IPAD ( UIUserInterfaceIdiomPad == UI_USER_INTERFACE_IDIOM())

/**
 *  Runtime get current device screen scale
 */
#define DEVICE_SCREEN_SCALE [[UIScreen mainScreen] scale]


#define DEVICE_HAS_ORIENTATION(orientation) ([[UIApplication sharedApplication] statusBarOrientation] == orientation)

#define DEVICE_ORIENTATION_POTRAIT    (  DEVICE_HAS_ORIENTATION(UIInterfaceOrientationPortrait) ||  DEVICE_HAS_ORIENTATION(UIInterfaceOrientationPortraitUpsideDown))

#define DEVICE_ORIENTATION_LANDSCAPE    (  DEVICE_HAS_ORIENTATION(UIInterfaceOrientationLandscapeLeft) ||  DEVICE_HAS_ORIENTATION(UIInterfaceOrientationLandscapeRight))


/**
 Runtime check for the current device.
 checks if the current device is an iPad
 */
#define DEVICE_IS_IPAD ( UIUserInterfaceIdiomPad == UI_USER_INTERFACE_IDIOM())

// STORY BOARD
#define STORYBAORD_IPHONE                                   @"Main-iPhone"
#define STORYBAORD_IPAD                                     @"Main-iPad"
#define STORYBAORD_NAME                         (DEVICE_IS_IPHONE)?STORYBAORD_IPHONE:STORYBAORD_IPAD

// Preference Objects
#define PREFERENCE_LANGUAGE          @"language"
#define PREFERENCE_LANGUAGE_LABEL    @"language_label"
#define PREFERENCE_RENDOM_CODE       @"rendom_code"

#define PREFERENCE_STRDOWN          @"Down_Flag"
#define PREFERENCE_DOWN_FLAG_EN     @"Down_Flag_En"
#define PREFERENCE_DOWN_FLAG_IS     @"Down_Flag_Is"
#define PREFERENCE_DOWN_FLAG_GR     @"Down_Flag_Gr"
#define PREFERENCE_DOWN_FLAG_FR     @"Down_Flag_Fr"
#define PREFERENCE_DOWN_FLAG_ES     @"Down_Flag_Es"
#define PREFERENCE_DOWN_FLAG_CH     @"Down_Flag_Ch"
#define PREFERENCE_DOWN_FLAG_DA     @"Down_Flag_Da"
#define PREFERENCE_ALLDOWNLOADED     @"ALLDOWNLOADED"


#define PREFERENCE_LOCATION_UPDATER @"Updater"
#define PREFERENCE_CHECK_TIME       @"Check_time"

#define PREFERENCE_INTRO_FLAG       @"Intro_Flag"

#pragma mark -------------------------
#pragma mark -- Error Messages
#pragma mark -------------------------

//  Error Messages
#define APP_LOCALIZED_STRING(KEY)        NSLocalizedString(KEY, nil)

#define MSG_INTERNET_UNAVAILABLE        APP_LOCALIZED_STRING(@"MSG_INTERNET_UNAVAILABLE")
#define MSG_SYSTEM_ERROR                APP_LOCALIZED_STRING(@"MSG_SYSTEM_ERROR")


#pragma mark -------------------------
#pragma mark -- Database Constant
#pragma mark -------------------------

// Database Constant

//table 1

#define DB_TABLENAME_PLACEMASTER             @"place_mst"
#define DB_TABLEFIELD_ID                     @"id"
#define DB_TABLEFIELD_PLACEID                @"place_id"
#define DB_TABLEFIELD_NAME                   @"name"
#define DB_TABLEFIELD_NAME_ISLENSKA          @"place_title_islenska"
#define DB_TABLEFIELD_DESCRIPTION            @"description"
#define DB_TABLEFIELD_DESCRIPTION_ISLENSKA   @"place_desc_islenska"
#define DB_TABLEFIELD_KM                     @"km"
#define DB_TABLEFIELD_LATITUDE               @"latitude"
#define DB_TABLEFIELD_LOGITUDE               @"longitude"
#define DB_TABLEFIELD_LIKE                   @"like"
#define DB_TABLEFIELD_VISITED                @"visited"
#define DB_TABLEFIELD_IMAGE                  @"place_img"
#define DB_TABLEFIELD_AUDIO                  @"place_audio"
#define DB_TABLEFIELD_AUDIO_ISLENSKA         @"place_audio_islenska"
#define DB_TABLEFIELD_DOWNLOAD_FLAG          @"download_flag"
#define DB_TABLEFIELD_UPDATE_AT              @"update_at"

#define DB_TABLEFIELD_DES_ENG_NONE_HTML      @"place_desc_none_html"
#define DB_TABLEFIELD_DES_ISLENSKA_NONE_HTML @"place_desc_islenska_none_html"

#define DB_TABLEFIELD_NAME_GERMAN            @"place_title_german"
#define DB_TABLEFIELD_DES_GERMAN_NONE_HTML   @"place_desc_german_none_html"
#define DB_TABLEFIELD_DES_GERMAN             @"place_desc_german"
#define DB_TABLEFIELD_AUDIO_GERMAN           @"place_audio_german"

#define DB_TABLEFIELD_NAME_FRENCH            @"place_title_french"
#define DB_TABLEFIELD_DES_FRENCH_NONE_HTML   @"place_desc_french_none_html"
#define DB_TABLEFIELD_DES_FRENCH             @"place_desc_french"
#define DB_TABLEFIELD_AUDIO_FRENCH           @"place_audio_french"

#define DB_TABLEFIELD_NAME_SPANISH           @"place_title_spanish"
#define DB_TABLEFIELD_DES_SPANISH_NONE_HTML  @"place_desc_spanish_none_html"
#define DB_TABLEFIELD_DES_SPANISH            @"place_desc_spanish"
#define DB_TABLEFIELD_AUDIO_SPANISH          @"place_audio_spanish"

#define DB_TABLEFIELD_NAME_CHINESE           @"place_title_chinese"
#define DB_TABLEFIELD_DES_CHINESE_NONE_HTML  @"place_desc_chinese_none_html"
#define DB_TABLEFIELD_DES_CHINESE            @"place_desc_chinese"
#define DB_TABLEFIELD_AUDIO_CHINESE          @"place_audio_chinese"

#define DB_TABLEFIELD_NAME_DANISH            @"place_title_danish"
#define DB_TABLEFIELD_DES_DANISH_NONE_HTML   @"place_desc_danish_none_html"
#define DB_TABLEFIELD_DES_DANISH             @"place_desc_danish"
#define DB_TABLEFIELD_AUDIO_DANISH           @"place_audio_danish"

#define DB_TABLEFIELD_DOWN_FLAG_EN           @"down_txt_eng"
#define DB_TABLEFIELD_DOWN_FLAG_IS           @"down_txt_islenska"
#define DB_TABLEFIELD_DOWN_FLAG_GE           @"down_txt_german"
#define DB_TABLEFIELD_DOWN_FLAG_FR           @"down_txt_french"
#define DB_TABLEFIELD_DOWN_FLAG_ES           @"down_txt_spanish"
#define DB_TABLEFIELD_DOWN_FLAG_CH           @"down_txt_chinese"
#define DB_TABLEFIELD_DOWN_FLAG_DA           @"down_txt_danish"


// table 2
#define DB_TABLENAME_IMAGEMASTER             @"place_img_mpg"
#define DB_TABLEFIELD_IMAGE_ID               @"id"
#define DB_TABLEFIELD_PLACEIMAGE_NAME        @"place_img_name"

#endif


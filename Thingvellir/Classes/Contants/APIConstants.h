//
//  APIConstants.h
//  Thingvellir
//
//  Created by saturncube on 25/05/18.
//  Copyright © 2018 saturncube. All rights reserved.
//

#ifndef APIContants_h
#define APIContants_h

#import "AppConstants.h"


//#define APP_NAME @"Thingvellir"

// API Constants
#define kApiServer @"http://appserver.smarticeland.is/thingvellir/api.php?action="
//@"http://13.126.172.229/thingvellir/api.php?action="


// API Host -- with http protocol in url
#define kApiHost @"http://appserver.smarticeland.is/"

#define kApiGetPlaces        @"getPlaces"
#define kApiGetLanguage      @"getLanguages"
#define kApiContentData      @"getContentData"
#define kApiGetPlacesByType  @"getPlacesByType"
#define kApiGetAllImages     @"getAllImages"
#define kApiMore             @"getAppsData"
#define kApiGetLabel         @"getLabel"
#define kApiGetRandomString  @"getRandomString"

#pragma mark -------------------------------
#pragma mark -- Localization And Messages
#pragma mark -------------------------------

// Localization
#define APP_LOCALIZED_STRING(KEY)       NSLocalizedString(KEY, nil)

#define APP_NAME                        APP_LOCALIZED_STRING(@"APP_NAME")

#define MSG_INTERNET_UNAVAILABLE        APP_LOCALIZED_STRING(@"MSG_INTERNET_UNAVAILABLE")
#define MSG_SYSTEM_ERROR                APP_LOCALIZED_STRING(@"MSG_SYSTEM_ERROR")

#define MSG_REQUEST_FAIL APP_LOCALIZED_STRING (@"MSG_REQUEST_FAIL")

#define API_RESPONSE_RESULT                 @"success"
#define API_RESPONSE_MESSAGE                @"message"
#define API_RESPONSE_DATA                   @"data"

#endif /* APIContants_h */


